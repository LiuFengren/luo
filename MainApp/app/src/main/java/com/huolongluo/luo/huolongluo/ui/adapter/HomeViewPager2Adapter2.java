package com.huolongluo.luo.huolongluo.ui.adapter;

import android.content.Context;
import android.text.TextUtils;

import com.huolongluo.luo.R;
import com.huolongluo.luo.huolongluo.bean.UserBasicInfoBean;
import com.huolongluo.luo.superAdapter.recycler.BaseViewHolder;
import com.huolongluo.luo.superAdapter.recycler.IMultiItemViewType;
import com.huolongluo.luo.superAdapter.recycler.SuperAdapter;
import com.tencent.liteav.trtccalling.model.util.ImageLoader;

import java.util.List;

public class HomeViewPager2Adapter2 extends SuperAdapter<UserBasicInfoBean> {

    /**
     * 正常布局
     */
    public static final int TYPE_NORMAL_PIC = 0;
    /**
     * 进入单身广场布局
     */
    public static final int TYPE_SQUARE_PIC = 1;
    /**
     * 推荐上墙布局
     */
    public static final int TYPE_UP_WALL = 2;

    public HomeViewPager2Adapter2(Context context, List<UserBasicInfoBean> list, IMultiItemViewType<UserBasicInfoBean> multiItemViewType) {
        super(context, list, multiItemViewType);
    }

    @Override
    public void onBind(int viewType, BaseViewHolder holder, int position, UserBasicInfoBean item) {
        if (viewType == TYPE_NORMAL_PIC) {
//            AboutMeBean aboutMeBean = item.getAboutMe();
//
//            if (aboutMeBean != null && !aboutMeBean.getUserTopPic().isEmpty()) {
//                ImageLoader.loadImage(mContext, holder.getView(R.id.iv_user_pic), aboutMeBean.getUserTopPic().get(0));
//            }

            if (item.getUserTopPic() != null && !item.getUserTopPic().isEmpty()) {
                ImageLoader.loadImage(mContext, holder.getView(R.id.iv_user_pic), item.getUserTopPic().get(0));
            }
            holder.setText(R.id.tv_home_nick_name, TextUtils.isEmpty(item.getNickName()) ? item.getUsername() : item.getNickName());
            String address[] = (item.getAddress()).split("-");
            if (address.length >= 2) {
                holder.setText(R.id.tv_address, address[0] + address[1]);
            }

            String zujiAddress[] = (item.getZujiAddress()).split("-");
            if (zujiAddress.length >= 2) {
                holder.setText(R.id.tv_zuji_address, zujiAddress[0] + zujiAddress[1] + "人");
            }
            if (!TextUtils.isEmpty(item.getBirthday())) {
                int yearIndex = item.getBirthday().indexOf("-");
                String year = "";
                if (yearIndex != -1) {
                    year = item.getBirthday().substring(0, yearIndex);
                }
                if (year.length() > 2) {
                    holder.setText(R.id.tv_old, (year.substring(2, year.length())) + "年");
                }
            }
            holder.setText(R.id.tv_height, item.getHeight() + "cm");
            holder.setText(R.id.tv_xueli, item.getXueli());
            holder.setText(R.id.tv_work, item.getWork());
        }
    }
}
