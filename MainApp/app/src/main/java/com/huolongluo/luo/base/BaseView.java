package com.huolongluo.luo.base;

import android.view.View;

import androidx.annotation.Nullable;

import com.huolongluo.luo.huolongluo.manager.DialogClickListener;

/**
 * <p>
 * Created by 火龙裸 on 2022/3/1.
 * <p/>
 * Class Note:
 * MVP中所有 View的接口
 */

public interface BaseView
{
    void showToastMessage(String msg, double seconds);

    void close();

    void showProgressDialog(String msg);

//    void showProgressDialog(String msg, int progress);

    void hideProgressDialog();

//    void showErrorMessage(String msg, String content);

    void showConfirmDialog(String title, String content, DialogClickListener dialogClickListener);

    void showConfirmNoCancelDialog(String title, String content, DialogClickListener dialogClickListener);

    /**************/
    void showDefaultListLoading();

    void showDefaultContent();

    void showDefaultEmpty();

    void showDefaultEmpty(@Nullable String text);

    void showDefaultEmpty(@Nullable String text, int resId);

    void showDefaultEmptyClick(View.OnClickListener listener);

    void showDefaultError(View.OnClickListener listener);
}
