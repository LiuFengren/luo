package com.huolongluo.luo.manager;

import android.text.TextUtils;
import android.util.Log;

import com.huolongluo.luo.MainActivity;
import com.huolongluo.luo.huolongluo.bean.UserWalletBean;
import com.huolongluo.luo.huolongluo.share.Share;
import com.huolongluo.luo.util.Arith;
import com.tencent.qcloud.tuicore.TUIConstants;
import com.tencent.qcloud.tuicore.TUICore;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class IMAudioVideoCallManager {
    private static final String TAG = "IMAudioVideoCallManager";

    /**
     * 发起通话呼叫
     *
     * @param callType 通话类型
     * @param userId   被叫ID
     */
    public static void clickStartCall(String callType, String userId) {
        Log.e(TAG, "clickStartCall: 点击发起了呼叫  呼叫类型为：" + callType + "  被呼叫的用户ID：" + userId);
        Map<String, Object> map = new HashMap<>();
        map.put(TUIConstants.TUICalling.PARAM_NAME_USERIDS, new String[]{userId});
        map.put(TUIConstants.TUICalling.PARAM_NAME_TYPE, callType);
        TUICore.callService(TUIConstants.TUICalling.SERVICE_NAME, TUIConstants.TUICalling.METHOD_NAME_CALL, map);
    }

    /**
     * 通话已建立 开始计时计费
     *
     * @param isCalling 是否主叫
     */
    public static void onCalling(boolean isCalling) {
        Log.e(TAG, "onCalling: 通话已建立，当前是否主叫：" + isCalling + "  当前通话类型：" + MainActivity.callType);
        if (isCalling) {
            double goldBalance = Double.parseDouble(Share.get().getGoldBalance());//金币余额
            if (TextUtils.equals("AUDIO", MainActivity.callType)) {
                double audioPrice = Double.parseDouble(Share.get().getAudioPrice());//一分钟语音消耗多少金币
                CallTimerManager.startOverTimer((int) (Arith.mul(Arith.div(goldBalance, audioPrice), 60)));
            } else {
                double videoPrice = Double.parseDouble(Share.get().getVideoPrice());//一分钟视频消耗多少金币
                CallTimerManager.startOverTimer((int) (Arith.mul(Arith.div(goldBalance, videoPrice), 60)));
            }
        }
    }

    /**
     * 通话已停止 开始结算费用
     *
     * @param isCalling       是否主叫
     * @param beCalledUserIDs (被叫的用户ID列表)为空说明是被叫，不为空说明当前设备是主叫
     */
    public static void onStopCalling(boolean isCalling, String[] beCalledUserIDs) {
        Log.e(TAG, "onStopCalling: 通话结束  当前是否主叫：" + isCalling + "  当前通话类型：" + MainActivity.callType + "" +
                "   剩余多少时间换算成金币：" + CallTimerManager.mOverMaxTime + "  被叫用户列表：" + Arrays.toString(beCalledUserIDs));
        if (isCalling) {
            CallTimerManager.stopOverTimer();
            if (TextUtils.equals("AUDIO", MainActivity.callType)) {
                //语音通话，相当于每秒钟0.6金币
//                int goldBalance = (int) (0.6 * CallTimerManager.mOverMaxTime);
                double audioPrice = Double.parseDouble(Share.get().getAudioPrice());
                double goldBalance = Arith.mul((CallTimerManager.mOverMaxTime / 60), audioPrice);//分钟数*每分钟消耗金币的数量

                Log.e(TAG, "onStopCalling: 语音通话后，时间结余：" + (CallTimerManager.mOverMaxTime) + "  强转为int后：" + goldBalance);
                UserWalletBean.updateUserGoldBalance(goldBalance, beCalledUserIDs);
            } else {
                //视频通话，相当于每秒钟1金币
                double videoPrice = Double.parseDouble(Share.get().getVideoPrice());
                double goldBalance = Arith.mul((CallTimerManager.mOverMaxTime / 60), videoPrice);//分钟数*每分钟消耗金币的数量

                Log.e(TAG, "onStopCalling: 视频通话后，时间结余：" + (CallTimerManager.mOverMaxTime) + "  强转为int后：" + goldBalance);
                UserWalletBean.updateUserGoldBalance(goldBalance, beCalledUserIDs);
            }
        }
    }
}
