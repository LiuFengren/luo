//package com.huolongluo.luo.huolongluo.ui.activity.recharge;
//
//import android.graphics.drawable.BitmapDrawable;
//import android.os.Bundle;
//import android.util.Log;
//import android.view.Gravity;
//import android.view.View;
//import android.view.WindowManager;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.PopupWindow;
//import android.widget.TextView;
//
//import androidx.annotation.Nullable;
//import androidx.recyclerview.widget.LinearLayoutManager;
//import androidx.recyclerview.widget.RecyclerView;
//
//import com.gyf.immersionbar.ImmersionBar;
//import com.huolongluo.luo.R;
//import com.huolongluo.luo.huolongluo.base.BaseActivity;
//import com.huolongluo.luo.huolongluo.bean.RechargeGoldListBean;
//import com.huolongluo.luo.huolongluo.ui.activity.vip.VipActivity;
//import com.huolongluo.luo.huolongluo.ui.adapter.RechargeGoldAdapter;
//import com.huolongluo.luo.superAdapter.recycler.SuperAdapter;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import butterknife.BindView;
//import cn.bmob.v3.BmobQuery;
//import cn.bmob.v3.exception.BmobException;
//import cn.bmob.v3.listener.FindListener;
//
///**
// * <p>
// * Created by 火龙裸 on 2017/9/6.
// */
//
//public class RechargeGoldActivity extends BaseActivity implements View.OnClickListener {
//    private static final String TAG = "RechargeGoldActivity";
//    @BindView(R.id.toolbar_center_title)
//    TextView toolbar_center_title;
//    @BindView(R.id.iv_left)
//    ImageView iv_left;
//    @BindView(R.id.tv_right)
//    TextView tv_right;
//    /********************* 以上为ToolBar ************************/
//    @BindView(R.id.tv_my_gold_num)
//    TextView tv_my_gold_num;
//    @BindView(R.id.rv_chongzhi_golds)
//    RecyclerView rv_chongzhi_golds;
//
//    private RechargeGoldAdapter rechargeGoldAdapter;
//    private List<RechargeGoldListBean> goodsBeanList = new ArrayList<>();//商品列表
//
//    private PopupWindow popupWindow;
//    private TextView tv_close_dialog;
//    private TextView tv_recharge_jine;
//    private TextView tv_ali_pay;
//    private TextView tv_wx_pay;
//
//    private int payType = 1;//1表示微信支付，2表示支付宝支付
//
//    @Override
//    protected int getContentViewId() {
//        return R.layout.activity_recharge_gold;
//    }
//
//    @Override
//    protected void injectDagger() {
//        activityComponent().inject(this);
//    }
//
//    private void initToolBar() {
//        ImmersionBar.with(this).statusBarDarkFont(true).init();
//        toolbar_center_title.setText("金币充值");
//        tv_right.setVisibility(View.VISIBLE);
//        tv_right.setText("充值记录");
//    }
//
//    @Override
//    protected void initViewsAndEvents(@Nullable Bundle savedInstanceState) {
//        initToolBar();
//        initPayWayPop();
//        eventClick(iv_left).subscribe(o -> // 点击返回键
//        {
//            close();
//        });
//        eventClick(tv_right).subscribe(o -> // 点击充值记录
//        {
//        });
//
//        // 微信支付
////        ll_wx_pay.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View view) {
////                payType = 1;
////                iv_wx_pay.setImageResource(R.mipmap.ic_cancel_ra_checked);
////                iv_ali_pay.setImageResource(R.mipmap.ic_cancel_ra_default);
////            }
////        });
////        // 支付宝支付
////        ll_ali_pay.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View view) {
////                payType = 2;
////                iv_wx_pay.setImageResource(R.mipmap.ic_cancel_ra_default);
////                iv_ali_pay.setImageResource(R.mipmap.ic_cancel_ra_checked);
////            }
////        });
//
////        eventClick(ll_wx_pay).subscribe(o -> // 微信支付
////        {
////
////        });
////        eventClick(ll_ali_pay).subscribe(o -> // 支付宝支付
////        {
////
////        });
//
//        queryGoods();
//    }
//
//    private void initPayWayPop() {
//        //popwindows初始化
//        popupWindow = new PopupWindow(this);
//        View view = getLayoutInflater().inflate(R.layout.dialog_select_pay, null);
//        popupWindow.setBackgroundDrawable(new BitmapDrawable());
//        popupWindow.setWidth(LinearLayout.LayoutParams.MATCH_PARENT);
//        popupWindow.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
//
//        popupWindow.setFocusable(true);
//        popupWindow.setOutsideTouchable(true);
//        popupWindow.setContentView(view);
//        popupWindow.setAnimationStyle(com.tencent.qcloud.tuikit.tuichat.R.style.pop_anim_style);
//        // 当PopUpWindow关闭的时候，整个窗体颜色
//        popupWindow.setOnDismissListener(() ->
//        {
//            backgroundAlpha(1.0f);
//        });
//
//        tv_close_dialog = view.findViewById(R.id.tv_close_dialog);
//        tv_recharge_jine = view.findViewById(R.id.tv_recharge_jine);
//        tv_ali_pay = view.findViewById(R.id.tv_ali_pay);
//        tv_wx_pay = view.findViewById(R.id.tv_wx_pay);
//        tv_close_dialog.setOnClickListener(this);
//        tv_ali_pay.setOnClickListener(this);
//        tv_wx_pay.setOnClickListener(this);
//    }
//
//    @Override
//    public void onClick(View view) {
//        switch (view.getId()) {
//            case R.id.tv_close_dialog:
//                popupWindow.dismiss();
//                break;
//            case R.id.tv_ali_pay:
//                break;
//            case R.id.tv_wx_pay:
//                break;
//        }
//    }
//
//    public void backgroundAlpha(float bgAlpha) {
//        WindowManager.LayoutParams lp = getWindow().getAttributes();
//        lp.alpha = bgAlpha; //0.0-1.0
//        getWindow().setAttributes(lp);
//    }
//
//    /**
//     * 查询多条数据
//     * 商品列表
//     */
//    private void queryGoods() {
////        showProgressDialog("");
//        BmobQuery<RechargeGoldListBean> bmobQuery = new BmobQuery<>();
//        bmobQuery.findObjects(new FindListener<RechargeGoldListBean>() {
//            @Override
//            public void done(List<RechargeGoldListBean> result, BmobException e) {
//                if (e == null) {
//                    goodsBeanList.clear();
//                    goodsBeanList.addAll(result);
//                    if (!goodsBeanList.isEmpty()) {
//                        Log.e(TAG, "done: 商品查询成功" + result.size() + " 金额：" + result.get(0).getWillPay());
//                        rv_chongzhi_golds.setNestedScrollingEnabled(false);
//                        rv_chongzhi_golds.setFocusable(false);
//                        rv_chongzhi_golds.setLayoutManager(new LinearLayoutManager(RechargeGoldActivity.this, LinearLayoutManager.VERTICAL, false) {
//                            @Override
//                            public boolean canScrollVertically() {
//                                return false;
//                            }
//                        });
////                    rv_chongzhi_golds.setLayoutManager(new LinearLayoutManager(MyAccountActivity.this, LinearLayoutManager.VERTICAL, false) {
////                        @Override
////                        public boolean canScrollVertically() {
////                            return false;
////                        }
////                    });
////                        rv_chongzhi_golds.addItemDecoration(new GridSpacingItemDecoration(3,
////                                DensityUtil.dip2px(RechargeGoldActivity.this, 10), false));
//                        rechargeGoldAdapter = new RechargeGoldAdapter(RechargeGoldActivity.this, goodsBeanList, R.layout.item_recharge_goods);
//                        rv_chongzhi_golds.setAdapter(rechargeGoldAdapter);
//                        rechargeGoldAdapter.setOnItemClickListener(new SuperAdapter.OnItemClickListener() {
//                            @Override
//                            public void onItemClick(View itemView, int viewType, int position) {
//                                if (rechargeGoldAdapter.getAllData().get(position).isForVip()) {
//                                    // TODO: 2022/4/14 会员充值
//                                    startActivity(VipActivity.class);
//                                } else {
//                                    // TODO: 2022/4/14 弹窗
//                                    // 设置PopUpWindow暗色窗体背景
//                                    backgroundAlpha(0.7f);
//                                    popupWindow.showAtLocation(rv_chongzhi_golds, Gravity.BOTTOM, 0, 0);
//                                }
//                            }
//                        });
//                    }
//                } else {
//                    Log.e(TAG, e.toString());
//                }
////                hideProgressDialog();
//            }
//        });
//    }
//}
