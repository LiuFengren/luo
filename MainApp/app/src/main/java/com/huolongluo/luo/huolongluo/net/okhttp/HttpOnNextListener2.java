package com.huolongluo.luo.huolongluo.net.okhttp;

import com.huolongluo.luo.huolongluo.net.BaseResponse;

/**
 * Created by 火龙裸 on 2020/03/12.
 */

public abstract class HttpOnNextListener2<T> {
    public abstract void onNext(T response);

    public void onFail(BaseResponse errResponse) {
    }

    public void onError(Throwable error) {
    }
}
