package com.huolongluo.luo.huolongluo.ui.fragment.home;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.flyco.tablayout.SlidingTabLayout;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.huolongluo.luo.R;
import com.huolongluo.luo.huolongluo.base.BaseActivity;
import com.huolongluo.luo.huolongluo.base.BaseFragment;
import com.huolongluo.luo.huolongluo.ui.fragment.tuijian.TuiJianFragment;
import com.huolongluo.luo.huolongluo.ui.fragment.xinren.XinRenFragment;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;

public class HomeFragment2 extends BaseFragment implements HomeContract.View, OnTabSelectListener {
    private static final String TAG = "HomeFragment";
    @Inject
    HomePresent present;
    @BindView(R.id.vp_home_fragment)
    ViewPager vp_home_fragment;
    @BindView(R.id.tl_tuijian_xinren)
    SlidingTabLayout tl_tuijian_xinren;

    private ArrayList<Fragment> mFragments = new ArrayList<>();
    private final String[] mTitles = {"推荐", "新人"};
    private MyPagerAdapter mAdapter;

    public static HomeFragment2 getInstance() {
        Bundle args = new Bundle();
        HomeFragment2 fragment = new HomeFragment2();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void initDagger() {
        ((BaseActivity) getActivity()).activityComponent().inject(this);
        present.attachView(this);
    }

    @Override
    protected int getContentViewId() {
        return R.layout.fragment_home;
    }

    @Override
    protected void initViewsAndEvents(View rootView) {
        mFragments.add(TuiJianFragment.getInstance());
        mFragments.add(XinRenFragment.getInstance());


//        View decorView = getWindow().getDecorView();
//        ViewPager vp = ViewFindUtils.find(decorView, R.id.vp);
        mAdapter = new MyPagerAdapter(getActivity().getSupportFragmentManager());
        vp_home_fragment.setAdapter(mAdapter);

        tl_tuijian_xinren.setOnTabSelectListener(this);

        tl_tuijian_xinren.setViewPager(vp_home_fragment);

        vp_home_fragment.setCurrentItem(0);
//        tl_tuijian_xinren.showDot(0);
    }

    @Override
    public void onTabSelect(int position) {
        Log.e(TAG, "onTabSelect: =================> " + position);
    }

    @Override
    public void onTabReselect(int position) {
        Log.e(TAG, "onTabReselect: ==============> " + position);
    }

    private class MyPagerAdapter extends FragmentPagerAdapter {
        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mTitles[position];
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        present.detachView();
    }
}
