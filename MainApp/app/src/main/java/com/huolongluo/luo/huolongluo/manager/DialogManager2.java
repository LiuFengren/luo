package com.huolongluo.luo.huolongluo.manager;

import android.content.Context;
import android.text.TextUtils;

import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.AttachPopupView;
import com.lxj.xpopup.core.BasePopupView;
import com.lxj.xpopup.impl.LoadingPopupView;
import com.lxj.xpopup.interfaces.OnConfirmListener;

/**
 * <p>
 * Created by 火龙裸 on 2022/3/4.
 */
public enum DialogManager2 {
    INSTANCE;

    //    private CustomDrawerPopupView drawerPopupView;
    private AttachPopupView attachPopupView;
    private BasePopupView popupView;
    private LoadingPopupView loadingPopup;
//    private CustomAttachPopup2 customAttach2;

//    interface DialogClickListener{
//        void onConfirm();
//        void onCancel();
//    }

    /**
     * 带确认和取消按钮
     * 假如不需要标题，则title传空字符串
     */
    public void showConfirmDialog(Context context, String title, String content, DialogClickListener dialogClickListener) {
        /*if(popupView==null)*/
        popupView = new XPopup.Builder(context)
//                        .hasNavigationBar(false)
//                        .hasStatusBar(false)
                .isDestroyOnDismiss(true)
                .isTouchThrough(false)//点击事件是否可穿透弹窗
                .dismissOnTouchOutside(false)
                .dismissOnBackPressed(false)
//                        .isViewMode(true)
//                        .hasBlurBg(true)
//                         .autoDismiss(false)
//                        .popupAnimation(PopupAnimation.NoAnimation)
                .asConfirm(title, content,
                        "取消", "确定",
                        new OnConfirmListener() {
                            @Override
                            public void onConfirm() {
                                dialogClickListener.onConfirm();
                            }
                        }, null, false);
        popupView.show();
    }

    /**
     * 只带确认按钮
     * 假如不需要标题，则title传空字符串
     */
    public void showConfirmNoCancelDialog(Context context, String title, String content, DialogClickListener dialogClickListener) {
        /*if(popupView==null)*/
        popupView = new XPopup.Builder(context)
//                        .hasNavigationBar(false)
//                        .hasStatusBar(false)
                .isDestroyOnDismiss(true)
                .isTouchThrough(false)//点击事件是否可穿透弹窗
                .dismissOnTouchOutside(false)
                .dismissOnBackPressed(false)
//                        .isViewMode(true)
//                        .hasBlurBg(true)
//                         .autoDismiss(false)
//                        .popupAnimation(PopupAnimation.NoAnimation)
                .asConfirm(title, content,
                        "", "确定",
                        new OnConfirmListener() {
                            @Override
                            public void onConfirm() {
                                dialogClickListener.onConfirm();
                            }
                        }, null, true);
        popupView.show();
    }

    //
    public void showProgressDialog(Context context, String message) {
        if (loadingPopup == null) {
            loadingPopup = (LoadingPopupView) new XPopup.Builder(context)
                    .dismissOnBackPressed(true)
                    .dismissOnTouchOutside(false)
                    .isLightNavigationBar(true)
                    .isViewMode(true)
//                            .asLoading(null, R.layout.custom_loading_popup)
                    .asLoading(TextUtils.isEmpty(message) ? "加载中" : message)
                    .show();
        } else {
            loadingPopup.show();
        }
    }

    private void releaseDialog() {
        if (loadingPopup != null) {
            loadingPopup.dismiss();
            loadingPopup = null;
        }
    }

    public void dismiss() {
//        Log.e("关闭对话框", "dismiss: " + (loadingPopup != null) + "   |||   " + loadingPopup.isShow());
//        Log.e("关闭对话框", "dismiss: ================>" + Log.getStackTraceString(new Throwable()));
//        XLog.e("关闭对话框 ===================： " + loadingPopup != null + "   |||   " + loadingPopup.isShow());
//        if (loadingPopup != null && loadingPopup.isShow()) {
////            loadingPopup.smartDismiss();
//            loadingPopup.dismiss();
////            loadingPopup.delayDismissWith(6000, new Runnable() {
////                @Override
////                public void run() {
////                    toast("Loading消失了！！！");
////                }
////            });
//        }

        if (loadingPopup != null) {
            loadingPopup.dismiss();
            loadingPopup = null;
        }
    }

}