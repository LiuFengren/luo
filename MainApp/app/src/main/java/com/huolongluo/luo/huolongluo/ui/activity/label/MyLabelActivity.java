package com.huolongluo.luo.huolongluo.ui.activity.label;

import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import com.gyf.immersionbar.ImmersionBar;
import com.huolongluo.luo.R;
import com.huolongluo.luo.huolongluo.base.BaseActivity;
import com.huolongluo.luo.huolongluo.bean.LabelsBean;
import com.huolongluo.luo.huolongluo.ui.adapter.MyLabelViewPager2Adapter;
import com.huolongluo.luo.huolongluo.ui.fragment.selectlabel.SelectLabelFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;

/**
 * <p>
 * Created by 火龙裸 on 2017/9/6.
 */

public class MyLabelActivity extends BaseActivity {
    private static final String TAG = "MyLabelActivity";
    @BindView(R.id.toolbar_center_title)
    TextView toolbar_center_title;
    @BindView(R.id.iv_left)
    ImageView iv_left;
    @BindView(R.id.tv_right)
    TextView tv_right;
    /********************* 以上为ToolBar ************************/
    @BindView(R.id.rv_my_select_label)
    RecyclerView rv_my_select_label;
    @BindView(R.id.tv_select_label_num)
    TextView tv_select_label_num;
    @BindView(R.id.tb_label_title)
    TabLayout tb_label_title;
    @BindView(R.id.vp_label_content)
    ViewPager2 vp_label_content;
    @BindView(R.id.tv_to_save)
    TextView tv_to_save;

//    private SelectLabelFragment myLabelViewPager2Fragment;

    private List<LabelsBean> labelsBeans = new ArrayList<>();//标签原始数据

    //    private MyLabelViewPager2Adapter mAdapter;
    private List<String> list_title = new ArrayList<>(); // tab名称列表
    private List<Fragment> list_fragment = new ArrayList<>(); // 定义要装fragment的列表
    private TabLayoutMediator tabLayoutMediator;

    @Override
    protected int getContentViewId() {
        return R.layout.activity_my_label;
    }

    @Override
    protected void injectDagger() {
        activityComponent().inject(this);
    }

    private void initToolBar() {
        ImmersionBar.with(this).statusBarDarkFont(true).init();
        toolbar_center_title.setText("我的标签");
//        tv_right.setVisibility(View.VISIBLE);
//        tv_right.setText("充值记录");
    }

    @Override
    protected void initViewsAndEvents(@Nullable Bundle savedInstanceState) {
        initToolBar();
        eventClick(iv_left).subscribe(o -> // 点击返回键
        {
            close();
        });
        eventClick(tv_to_save).subscribe(o -> // 点击保存
        {
        });

        queryLabels();
    }

    /**
     * 查询标签
     * 商品列表
     */
    private void queryLabels() {
//        showProgressDialog("");
        BmobQuery<LabelsBean> bmobQuery = new BmobQuery<>();
        bmobQuery.findObjects(new FindListener<LabelsBean>() {
            @Override
            public void done(List<LabelsBean> result, BmobException e) {
                if (e == null) {
                    labelsBeans.clear();
                    labelsBeans.addAll(result);
                    if (!labelsBeans.isEmpty()) {
                        list_title.clear();
                        list_fragment.clear();
                        for (int i = 0; i < result.size(); i++) {
                            list_title.add(result.get(i).getLabelType());
                            list_fragment.add(SelectLabelFragment.getInstance(result.get(i).getLabelContent()));
                        }

                        vp_label_content.setAdapter(new MyLabelViewPager2Adapter(getSupportFragmentManager(), getLifecycle(), list_fragment));

                        //关联TabLayout 添加attach()
                        new TabLayoutMediator(tb_label_title, vp_label_content, new TabLayoutMediator.TabConfigurationStrategy() {
                            @Override
                            public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {
                                tab.setText(list_title.get(position));
                            }
                        }).attach();

                        vp_label_content.registerOnPageChangeCallback(callback);
                        /********************************/
//                        vipPackageBeanList.get(vipPackageBeanList.size() - 1).setChecked(true);
//                        vipPackageAdapter.addAll(vipPackageBeanList);
                        Log.e(TAG, "done: 标签查询成功" + result.size() + " 金额：" + result.get(0).getLabelContent());
//                        rv_chongzhi_zhuanshi.setNestedScrollingEnabled(false);
//                        rv_chongzhi_zhuanshi.setFocusable(false);
//                        rv_chongzhi_zhuanshi.setLayoutManager(new GridLayoutManager(VipActivity.this, 3) {
//                            @Override
//                            public boolean canScrollVertically() {
//                                return false;
//                            }
//                        });
////                    rv_chongzhi_zhuanshi.setLayoutManager(new LinearLayoutManager(MyAccountActivity.this, LinearLayoutManager.VERTICAL, false) {
////                        @Override
////                        public boolean canScrollVertically() {
////                            return false;
////                        }
////                    });
//                        rv_chongzhi_zhuanshi.addItemDecoration(new GridSpacingItemDecoration(3,
//                                DensityUtil.dip2px(VipActivity.this, 10), false));
//                        rechargeGoodsAdapter = new RechargeGoodsAdapter(VipActivity.this, vipPackageBeanList, R.layout.item_chognzhi_zhuanshi);
//                        rv_chongzhi_zhuanshi.setAdapter(rechargeGoodsAdapter);
                    }
                } else {
                    Log.e(TAG, e.toString());
                }
//                hideProgressDialog();
            }
        });
    }

    private ViewPager2.OnPageChangeCallback callback = new ViewPager2.OnPageChangeCallback() {
        @Override
        public void onPageSelected(int position) {
            super.onPageSelected(position);
            Log.e(TAG, "onPageSelected: 改变选中了：" + position);
        }
    };
}
