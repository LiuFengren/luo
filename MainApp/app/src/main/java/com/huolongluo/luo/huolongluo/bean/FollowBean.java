package com.huolongluo.luo.huolongluo.bean;

import cn.bmob.v3.BmobObject;

public class FollowBean extends BmobObject {
    private String username;//用户名
    private String followToUserName;//关注了谁的用户名
    private String headPic;//用户头像
    private String followToUserPic;//被关注的人的用户头像
    private String userBirthDay;//用户生日
    private String folloToUserBirthDay;//被关注的人的生日
    private double userHeight;//用户身高
    private double followToUserHeight;//被关注的人的用户身高
    private String address;//用户住址
    private String followToUserAddress;//被关注的人的用户住址
    private String userSex;//用户性别
    private String followToUserSex;//被关注的人的用户性别
    private String userNickName;//用户昵称
    private String followUserNickName;//被关注的人的用户昵称

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFollowToUserName() {
        return followToUserName;
    }

    public void setFollowToUserName(String followToUserName) {
        this.followToUserName = followToUserName;
    }

    public String getHeadPic() {
        return headPic;
    }

    public void setHeadPic(String headPic) {
        this.headPic = headPic;
    }

    public String getFollowToUserPic() {
        return followToUserPic;
    }

    public void setFollowToUserPic(String followToUserPic) {
        this.followToUserPic = followToUserPic;
    }

    public String getUserBirthDay() {
        return userBirthDay;
    }

    public void setUserBirthDay(String userBirthDay) {
        this.userBirthDay = userBirthDay;
    }

    public String getFolloToUserBirthDay() {
        return folloToUserBirthDay;
    }

    public void setFolloToUserBirthDay(String folloToUserBirthDay) {
        this.folloToUserBirthDay = folloToUserBirthDay;
    }

    public double getUserHeight() {
        return userHeight;
    }

    public void setUserHeight(double userHeight) {
        this.userHeight = userHeight;
    }

    public double getFollowToUserHeight() {
        return followToUserHeight;
    }

    public void setFollowToUserHeight(double followToUserHeight) {
        this.followToUserHeight = followToUserHeight;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getFollowToUserAddress() {
        return followToUserAddress;
    }

    public void setFollowToUserAddress(String followToUserAddress) {
        this.followToUserAddress = followToUserAddress;
    }

    public String getUserSex() {
        return userSex;
    }

    public void setUserSex(String userSex) {
        this.userSex = userSex;
    }

    public String getFollowToUserSex() {
        return followToUserSex;
    }

    public void setFollowToUserSex(String followToUserSex) {
        this.followToUserSex = followToUserSex;
    }

    public String getUserNickName() {
        return userNickName;
    }

    public void setUserNickName(String userNickName) {
        this.userNickName = userNickName;
    }

    public String getFollowUserNickName() {
        return followUserNickName;
    }

    public void setFollowUserNickName(String followUserNickName) {
        this.followUserNickName = followUserNickName;
    }
}
