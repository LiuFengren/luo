package com.huolongluo.luo.huolongluo.ui.activity.accountset;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.gyf.immersionbar.ImmersionBar;
import com.huolongluo.luo.R;
import com.huolongluo.luo.huolongluo.base.BaseActivity;
import com.huolongluo.luo.huolongluo.bean.UserBean;
import com.huolongluo.luo.huolongluo.manager.DialogClickListener;
import com.huolongluo.luo.huolongluo.share.Share;
import com.huolongluo.luo.huolongluo.ui.activity.login.LoginActivity;
import com.huolongluo.luo.manager.AppManager;
import com.huolongluo.luo.util.CleanCacheUtils;
import com.huolongluo.luo.util.ToastSimple;

import butterknife.BindView;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.UpdateListener;

/**
 * <p>
 * Created by 火龙裸 on 2017/9/6.
 */

public class AccountSetActivity extends BaseActivity {
    private static final String TAG = "AccountSetActivity";
    @BindView(R.id.toolbar_center_title)
    TextView toolbar_center_title;
    @BindView(R.id.iv_left)
    ImageView iv_left;
    @BindView(R.id.tv_right)
    TextView tv_right;
    /********************* 以上为ToolBar ************************/
    @BindView(R.id.ll_clear_cache)
    LinearLayout ll_clear_cache;
    @BindView(R.id.tv_clear_cache)
    TextView tv_clear_cache;
    @BindView(R.id.ll_cancel_account)
    LinearLayout ll_cancel_account;

    @Override
    protected int getContentViewId() {
        return R.layout.activity_account_set;
    }

    @Override
    protected void injectDagger() {
        activityComponent().inject(this);
    }

    private void initToolBar() {
        ImmersionBar.with(this).statusBarDarkFont(true).init();
        toolbar_center_title.setText("账号设置");
    }

    @Override
    protected void initViewsAndEvents(@Nullable Bundle savedInstanceState) {
        initToolBar();
        tv_clear_cache.setText(CleanCacheUtils.getTotalCacheSize(this));
        eventClick(iv_left).subscribe(o -> // 点击返回键
        {
            close();
        });
        eventClick(ll_clear_cache).subscribe(o -> // 点击清理缓存
        {
            CleanCacheUtils.clearAllCache(this);
            tv_clear_cache.setText(CleanCacheUtils.getTotalCacheSize(this));
            ToastSimple.show("清理成功", 2);
        });
        eventClick(ll_cancel_account).subscribe(o -> // 点击注销账号
        {
            showConfirmDialog("注销账号", "账号注销意味着账号删除，账户所有积分和金币、历史交易等数据都将会被清理，已确保所有关系已完结且无纠纷了吗？", new DialogClickListener() {
                @Override
                public void onConfirm() {
                    UserBean userBean = new UserBean();
                    userBean.setObjectId(Share.get().getBmobCurrentUserObject());
                    userBean.delete(new UpdateListener() {
                        @Override
                        public void done(BmobException e) {
                            if(e==null){
                                Log.e(TAG, "账号注销成功:" + userBean.getUpdatedAt());
                                View view = LayoutInflater.from(AccountSetActivity.this).inflate(R.layout.toast_text, null);
                                TextView tv_toast_message = view.findViewById(R.id.tv_toast_message);
                                tv_toast_message.setText("账号注销成功");
                                ToastSimple.makeText(Gravity.CENTER, 1, view).show();

                                UserBean.clearLocalSpUserBasicInfo();
                                AppManager.get().finishAllToActivity(AccountSetActivity.this, LoginActivity.class);
                            }else{
                                Log.e(TAG, "账号注销失败：" + e.getMessage());
                                View view = LayoutInflater.from(AccountSetActivity.this).inflate(R.layout.toast_text, null);
                                TextView tv_toast_message = view.findViewById(R.id.tv_toast_message);
                                tv_toast_message.setText("账号注销失败:" + e.getMessage());
                                ToastSimple.makeText(Gravity.CENTER, 1, view).show();
                            }
                        }

                    });
                }

                @Override
                public void onCancel() {
                }
            });
        });
    }
}
