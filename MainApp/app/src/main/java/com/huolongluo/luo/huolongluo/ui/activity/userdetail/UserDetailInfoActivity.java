package com.huolongluo.luo.huolongluo.ui.activity.userdetail;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ConvertUtils;
import com.gyf.immersionbar.ImmersionBar;
import com.huolongluo.luo.R;
import com.huolongluo.luo.huolongluo.base.BaseActivity;
import com.huolongluo.luo.huolongluo.bean.AboutMeBean;
import com.huolongluo.luo.huolongluo.bean.FollowBean;
import com.huolongluo.luo.huolongluo.bean.UserBasicInfoBean;
import com.huolongluo.luo.huolongluo.bean.UserWalletBean;
import com.huolongluo.luo.huolongluo.bean.VisitBean;
import com.huolongluo.luo.huolongluo.constants.Constants;
import com.huolongluo.luo.huolongluo.manager.DialogClickListener;
import com.huolongluo.luo.huolongluo.share.Event;
import com.huolongluo.luo.huolongluo.share.Share;
import com.huolongluo.luo.huolongluo.ui.activity.account.MyAccountActivity;
import com.huolongluo.luo.huolongluo.ui.activity.editbaseinfo.EditBaseInfoActivity;
import com.huolongluo.luo.huolongluo.ui.activity.editdatacard.EditDataCardActivity;
import com.huolongluo.luo.huolongluo.ui.activity.edittoppic.EditTopPicActivity;
import com.huolongluo.luo.huolongluo.ui.adapter.BannerImageAdapter;
import com.huolongluo.luo.huolongluo.ui.adapter.UserInfoDetailCardPicAdapter;
import com.huolongluo.luo.util.Arith;
import com.huolongluo.luo.util.StringUtil;
import com.huolongluo.luo.util.ToastSimple;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.ImageViewerPopupView;
import com.lxj.xpopup.interfaces.OnSrcViewUpdateListener;
import com.lxj.xpopup.util.SmartGlideImageLoader;
import com.tencent.imsdk.v2.V2TIMConversation;
import com.tencent.qcloud.tuicore.TUIConstants;
import com.tencent.qcloud.tuicore.TUICore;
import com.youth.banner.Banner;
import com.youth.banner.indicator.CircleIndicator;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.SaveListener;
import cn.bmob.v3.listener.UpdateListener;

/**
 * <p>
 * Created by 火龙裸 on 2017/9/6.
 */
public class UserDetailInfoActivity extends BaseActivity {
    private static final String TAG = "UserInfoDetailActivity";
    @BindView(R.id.toolbar_center_title)
    TextView toolbar_center_title;
    @BindView(R.id.iv_left)
    ImageView iv_left;
//    @BindView(R.id.tv_right)
//    TextView tv_right;
    /********************* 以上为ToolBar ************************/
    @BindView(R.id.nested_scroll_view)
    NestedScrollView nested_scroll_view;
    @BindView(R.id.ll_handle_user)
    LinearLayout ll_handle_user;
    @BindView(R.id.tv_edit_user_pic)
    TextView tv_edit_user_pic;
    @BindView(R.id.iv_banner)
    ImageView iv_banner;
    @BindView(R.id.banner_user_pic)
    Banner banner_user_pic;
    @BindView(R.id.tv_nick_name)
    TextView tv_nick_name;
    @BindView(R.id.tv_follow_or_no)
    TextView tv_follow_or_no;
    @BindView(R.id.tv_edit_base_info)
    TextView tv_edit_base_info;
    @BindView(R.id.tv_age)
    TextView tv_age;
    @BindView(R.id.tv_xingzuo)
    TextView tv_xingzuo;
    @BindView(R.id.tv_height)
    TextView tv_height;
    @BindView(R.id.tv_weight)
    TextView tv_weight;
    @BindView(R.id.tv_address)
    TextView tv_address;
    @BindView(R.id.tv_zuji)
    TextView tv_zuji;
    @BindView(R.id.tv_xueli)
    TextView tv_xueli;
    @BindView(R.id.tv_work)
    TextView tv_work;
    @BindView(R.id.tv_wechat_state)
    TextView tv_wechat_state;

    @BindView(R.id.ll_about_me)
    CardView ll_about_me;//关于我

    @BindView(R.id.ll_self_desc)
    LinearLayout ll_self_desc;
    @BindView(R.id.tv_edit_self_desc)
    TextView tv_edit_self_desc;
    @BindView(R.id.tv_self_desc)
    TextView tv_self_desc;
    @BindView(R.id.rv_self_desc)
    RecyclerView rv_self_desc;

    @BindView(R.id.ll_family_bg)
    LinearLayout ll_family_bg;
    @BindView(R.id.tv_edit_family_bg)
    TextView tv_edit_family_bg;
    @BindView(R.id.tv_family_bg)
    TextView tv_family_bg;
    @BindView(R.id.rv_family_bg)
    RecyclerView rv_family_bg;

    @BindView(R.id.ll_hobby)
    LinearLayout ll_hobby;
    @BindView(R.id.tv_edit_hobby)
    TextView tv_edit_hobby;
    @BindView(R.id.tv_hobby)
    TextView tv_hobby;
    @BindView(R.id.rv_hobby)
    RecyclerView rv_hobby;

    @BindView(R.id.ll_love_point)
    LinearLayout ll_love_point;
    @BindView(R.id.tv_edit_lobe_point)
    TextView tv_edit_lobe_point;
    @BindView(R.id.tv_love_point)
    TextView tv_love_point;
    @BindView(R.id.rv_love_point)
    RecyclerView rv_love_point;

    @BindView(R.id.ll_ideal_ta)
    LinearLayout ll_ideal_ta;
    @BindView(R.id.tv_edit_ideal_ta)
    TextView tv_edit_ideal_ta;
    @BindView(R.id.tv_ideal_ta)
    TextView tv_ideal_ta;
    @BindView(R.id.rv_ideal_ta)
    RecyclerView rv_ideal_ta;

    @BindView(R.id.ll_why_single)
    LinearLayout ll_why_single;
    @BindView(R.id.tv_edit_why_single)
    TextView tv_edit_why_single;
    @BindView(R.id.tv_why_single)
    TextView tv_why_single;
    @BindView(R.id.rv_why_single)
    RecyclerView rv_why_single;

    @BindView(R.id.ll_expect_life)
    LinearLayout ll_expect_life;
    @BindView(R.id.tv_edit_expect_life)
    TextView tv_edit_expect_life;
    @BindView(R.id.tv_expect_life)
    TextView tv_expect_life;
    @BindView(R.id.rv_expect_life)
    RecyclerView rv_expect_life;

//    @BindView(R.id.ll_my_label)
//    CardView ll_my_label;//我的标签
//    @BindView(R.id.tv_edit_my_label)
//    TextView tv_edit_my_label;

    @BindView(R.id.btn_unlock_wx)
    Button btn_unlock_wx;//解锁微信
    @BindView(R.id.btn_to_know_ta)
    Button btn_to_know_ta;//想认识TA

    //    private UserDetailInfoBean userBasicInfo = new UserDetailInfoBean();//用户详情
    private UserBasicInfoBean userBasicInfoBean;//用户资料
    private AboutMeBean aboutMeBean;//关于我
//    private UserBasicInfoBean.UserDetailInfoBean userDetailInfoBean;//用户详细资料
//    private UserBasicInfoBean.UserDetailInfoBean.UserPicBean userPicBean;//用户精美图片

    //    private boolean isEditUserInfo = false;
    private String username = "";

    private String followObjectId = "";

    @Override
    protected int getContentViewId() {
        return R.layout.activity_userinfo_detail;
    }

    @Override
    protected void injectDagger() {
        activityComponent().inject(this);
    }

    private void initToolBar() {
        ImmersionBar.with(this).statusBarDarkFont(true).init();
        toolbar_center_title.setText("个人主页");
    }

    @Override
    protected void initViewsAndEvents(@Nullable Bundle savedInstanceState) {
        EventBus.getDefault().register(this);
        initToolBar();
        if (getBundle() != null) {
            username = getBundle().getString(Constants.USER_NAME);
//            userBasicInfo = (UserBasicInfoBean) getBundle().getSerializable(Constants.USER_INFO);
//            isEditUserInfo = getBundle().getBoolean(Constants.IS_EDIT_USER_INFO);
            Log.e(TAG, "initViewsAndEvents: 传进来的用户名：" + username + "  本地保存的用户名：" + Share.get().getUserName());
        }

        eventClick(iv_left).subscribe(o -> // 点击返回键
        {
            close();
        });
        eventClick(tv_edit_user_pic).subscribe(o -> // 编辑用户精美图片
        {
            Log.e(TAG, "initViewsAndEvents: 编辑用户精美图片");
            if (userBasicInfoBean != null) {
                Bundle bundle = new Bundle();
                bundle.putSerializable(Constants.USER_BASE_INFO, userBasicInfoBean);
                startActivity(EditTopPicActivity.class, bundle);
            }
        });
        eventClick(tv_edit_base_info).subscribe(o -> // 编辑基础资料
        {
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.USER_BASE_INFO, userBasicInfoBean);
            startActivity(EditBaseInfoActivity.class, bundle);
        });
        eventClick(tv_edit_self_desc).subscribe(o -> // 自我描述
        {
            if (aboutMeBean != null) {
                Bundle bundle = new Bundle();
                bundle.putSerializable(Constants.USER_BASE_INFO, userBasicInfoBean);
                bundle.putSerializable(Constants.USER_ABOUT_ME, aboutMeBean);
                bundle.putInt(Constants.EDIT_DATA_CARD_TYPE, 1);
//            if (aboutMeBean != null) {
//                bundle.putString(Constants.EDIT_DATA_CARD_TEXT, aboutMeBean.getSelfDescription());
//            }
//            bundle.putStringArrayList(Constants.EDIT_DATA_CARD_PIC, (ArrayList<String>) userBasicInfo.getUserPic().getSelfDescriptionPic());
                startActivity(EditDataCardActivity.class, bundle);
            }
        });
        eventClick(tv_edit_family_bg).subscribe(o -> // 家庭背景
        {
            if (aboutMeBean != null) {
                Bundle bundle = new Bundle();
                bundle.putSerializable(Constants.USER_BASE_INFO, userBasicInfoBean);
                bundle.putSerializable(Constants.USER_ABOUT_ME, aboutMeBean);
                bundle.putInt(Constants.EDIT_DATA_CARD_TYPE, 2);
//            if (aboutMeBean != null) {
//                bundle.putString(Constants.EDIT_DATA_CARD_TEXT, aboutMeBean.getFamilyBackground());
//            }
//            bundle.putStringArrayList(Constants.EDIT_DATA_CARD_PIC, (ArrayList<String>) userBasicInfo.getFamilyBackgroundPic());
                startActivity(EditDataCardActivity.class, bundle);
            }
        });
        eventClick(tv_edit_hobby).subscribe(o -> // 兴趣爱好
        {
            if (aboutMeBean != null) {
                Bundle bundle = new Bundle();
                bundle.putSerializable(Constants.USER_BASE_INFO, userBasicInfoBean);
                bundle.putSerializable(Constants.USER_ABOUT_ME, aboutMeBean);
                bundle.putInt(Constants.EDIT_DATA_CARD_TYPE, 3);
//            if (aboutMeBean != null) {
//                bundle.putString(Constants.EDIT_DATA_CARD_TEXT, aboutMeBean.getHobby());
//            }
//            bundle.putStringArrayList(Constants.EDIT_DATA_CARD_PIC, (ArrayList<String>) userBasicInfo.getHobbyPic());
                startActivity(EditDataCardActivity.class, bundle);
            }
        });
        eventClick(tv_edit_lobe_point).subscribe(o -> // 爱情观
        {
            if (aboutMeBean != null) {
                Bundle bundle = new Bundle();
                bundle.putSerializable(Constants.USER_BASE_INFO, userBasicInfoBean);
                bundle.putSerializable(Constants.USER_ABOUT_ME, aboutMeBean);
                bundle.putInt(Constants.EDIT_DATA_CARD_TYPE, 4);
//            if (aboutMeBean != null) {
//                bundle.putString(Constants.EDIT_DATA_CARD_TEXT, aboutMeBean.getLoveIdea());
//            }
//            bundle.putStringArrayList(Constants.EDIT_DATA_CARD_PIC, (ArrayList<String>) userBasicInfo.getLoveIdeaPic());
                startActivity(EditDataCardActivity.class, bundle);
            }
        });
        eventClick(tv_edit_ideal_ta).subscribe(o -> // 理想的另一半
        {
            if (aboutMeBean != null) {
                Bundle bundle = new Bundle();
                bundle.putSerializable(Constants.USER_BASE_INFO, userBasicInfoBean);
                bundle.putSerializable(Constants.USER_ABOUT_ME, aboutMeBean);
                bundle.putInt(Constants.EDIT_DATA_CARD_TYPE, 5);
//            if (aboutMeBean != null) {
//                bundle.putString(Constants.EDIT_DATA_CARD_TEXT, aboutMeBean.getIdealPartner());
//            }
//            bundle.putStringArrayList(Constants.EDIT_DATA_CARD_PIC, (ArrayList<String>) userBasicInfo.getIdealPartnerPic());
                startActivity(EditDataCardActivity.class, bundle);
            }
        });
        eventClick(tv_edit_why_single).subscribe(o -> // 我为什么单身
        {
            if (aboutMeBean != null) {
                Bundle bundle = new Bundle();
                bundle.putSerializable(Constants.USER_BASE_INFO, userBasicInfoBean);
                bundle.putSerializable(Constants.USER_ABOUT_ME, aboutMeBean);
                bundle.putInt(Constants.EDIT_DATA_CARD_TYPE, 6);
//            if (aboutMeBean != null) {
//                bundle.putString(Constants.EDIT_DATA_CARD_TEXT, aboutMeBean.getWhySingle());
//            }
//            bundle.putStringArrayList(Constants.EDIT_DATA_CARD_PIC, (ArrayList<String>) userBasicInfo.getWhySinglePic());
                startActivity(EditDataCardActivity.class, bundle);
            }
        });
        eventClick(tv_edit_expect_life).subscribe(o -> // 如果遇到对的人，我期待什么样的生活？
        {
            if (aboutMeBean != null) {
                Bundle bundle = new Bundle();
                bundle.putSerializable(Constants.USER_BASE_INFO, userBasicInfoBean);
                bundle.putSerializable(Constants.USER_ABOUT_ME, aboutMeBean);
                bundle.putInt(Constants.EDIT_DATA_CARD_TYPE, 7);
//            if (aboutMeBean != null) {
//                bundle.putString(Constants.EDIT_DATA_CARD_TEXT, aboutMeBean.getQiDaiShengHuo());
//            }
//            bundle.putStringArrayList(Constants.EDIT_DATA_CARD_PIC, (ArrayList<String>) userBasicInfo.getQiDaiShengHuoPic());
                startActivity(EditDataCardActivity.class, bundle);
            }
        });

//        eventClick(tv_edit_my_label).subscribe(o -> // 编辑标签
//        {
//            startActivity(MyLabelActivity.class);
//        });
        eventClick(tv_follow_or_no).subscribe(o -> // 关注，或取消
        {
            if (TextUtils.equals("关注", tv_follow_or_no.getText().toString())) {
                addFollow();
            } else {
                cancelFollow();
            }
        });
        eventClick(btn_unlock_wx).subscribe(o -> // 解锁微信
        {
            if (userBasicInfoBean.isHeYanWeChat()) {
                showConfirmDialog("解锁微信", "解锁获取对方微信，将消耗" + (Share.get().getUnLockWechatPrice()) + "金币，同时红娘也将助力向对方推荐你，确认解锁吗？", new DialogClickListener() {
                    @Override
                    public void onConfirm() {
                        double goldBalance = Double.parseDouble(Share.get().getGoldBalance());//金币余额
                        double unlockWechatPrice = Double.parseDouble(Share.get().getUnLockWechatPrice());//解锁微信价格
                        if (goldBalance >= unlockWechatPrice) {
                            //金币余额足够
                            double toUpdateGold = goldBalance - unlockWechatPrice;
                            updateUserWallet(toUpdateGold);
                        } else {
                            View view = LayoutInflater.from(UserDetailInfoActivity.this).inflate(R.layout.toast_text, null);
                            TextView tv_toast_message = view.findViewById(R.id.tv_toast_message);
                            tv_toast_message.setText("金币余额不足");
                            ToastSimple.makeText(Gravity.CENTER, 2, view).show();
                            startActivity(MyAccountActivity.class);//跳转到充值界面
                        }
                    }

                    @Override
                    public void onCancel() {
                    }
                });
            }
        });
        eventClick(btn_to_know_ta).subscribe(o -> // 想认识TA
        {
            startChatActivity(username, V2TIMConversation.V2TIM_C2C, userBasicInfoBean.getNickName(), "");
        });

        if (TextUtils.equals(username, Share.get().getUserName())) {
            tv_follow_or_no.setVisibility(View.GONE);
            btn_unlock_wx.setVisibility(View.GONE);
            btn_to_know_ta.setVisibility(View.GONE);
            tv_edit_user_pic.setVisibility(View.VISIBLE);
            tv_edit_base_info.setVisibility(View.VISIBLE);
            tv_edit_self_desc.setVisibility(View.VISIBLE);
            tv_edit_family_bg.setVisibility(View.VISIBLE);
            tv_edit_hobby.setVisibility(View.VISIBLE);
            tv_edit_lobe_point.setVisibility(View.VISIBLE);
            tv_edit_ideal_ta.setVisibility(View.VISIBLE);
            tv_edit_why_single.setVisibility(View.VISIBLE);
            tv_edit_expect_life.setVisibility(View.VISIBLE);
        } else {
            tv_edit_user_pic.setVisibility(View.GONE);
            tv_edit_base_info.setVisibility(View.GONE);
            tv_edit_self_desc.setVisibility(View.GONE);
            tv_edit_family_bg.setVisibility(View.GONE);
            tv_edit_hobby.setVisibility(View.GONE);
            tv_edit_lobe_point.setVisibility(View.GONE);
            tv_edit_ideal_ta.setVisibility(View.GONE);
            tv_edit_why_single.setVisibility(View.GONE);
            tv_edit_expect_life.setVisibility(View.GONE);
        }

        showDefaultListLoading();

        queryUserBasicInfo();
        queryAboutMe();
        queryUserWallet();
    }

    /**
     * 启动聊天界面去发送消息
     *
     * @param chatId    用户ID
     * @param chatType  V2TIMConversation.V2TIM_C2C单聊；
     * @param chatName  聊天窗口顶部显示的名称文字
     * @param groupType 单聊的话，直接传空字符串
     */
    public static void startChatActivity(String chatId, int chatType, String chatName, String groupType) {
        if (TextUtils.isEmpty(chatId)) {
            return;
        }
        Bundle bundle = new Bundle();
        bundle.putString(TUIConstants.TUIChat.CHAT_ID, chatId);
        bundle.putString(TUIConstants.TUIChat.CHAT_NAME, chatName);
        bundle.putInt(TUIConstants.TUIChat.CHAT_TYPE, chatType);
        if (chatType == V2TIMConversation.V2TIM_C2C) {
            //启动单聊
            TUICore.startActivity(TUIConstants.TUIChat.C2C_CHAT_ACTIVITY_NAME, bundle);
        } else if (chatType == V2TIMConversation.V2TIM_GROUP) {
            //启动群聊
            bundle.putString(TUIConstants.TUIChat.GROUP_TYPE, groupType);
            TUICore.startActivity(TUIConstants.TUIChat.GROUP_CHAT_ACTIVITY_NAME, bundle);
        }
    }

    /**
     * 添加用户到访数据
     */
    private void addVisitBeanData() {
        if (userBasicInfoBean == null) {
            return;
        }
        VisitBean visitBean = new VisitBean();
        visitBean.setUsername(Share.get().getUserName());
        visitBean.setVisitToUserName(username);
        visitBean.setNickName(Share.get().getNickName());
        visitBean.setHeadPic(Share.get().getHeadPic());
        visitBean.setSex(Share.get().getSex());
        visitBean.setUnLock(false);
        visitBean.setBirtyday(Share.get().getBirthDay());
        visitBean.setAddress(Share.get().getAddress());
        visitBean.save(new SaveListener<String>() {
            @Override
            public void done(String objectId, BmobException e) {
                if (e == null) {
                    Log.e(TAG, "添加 用户到访数据 成功，返回objectId为：" + objectId);
                } else {
                    Log.e(TAG, "创建 用户到访数据 数据失败：" + e.getMessage());
                }
            }
        });
    }

    /**
     * 查询指定userName的数据
     */
    private void queryUserBasicInfo() {
        Log.e(TAG, "queryUserDetailInfo: 查询谁的用户详情信息：" + username);
        //开始查找UserBasicInfoBean表里面username为自己的数据
        BmobQuery<UserBasicInfoBean> categoryBmobQuery = new BmobQuery<>();
        categoryBmobQuery.addWhereEqualTo("username", username);
        categoryBmobQuery.findObjects(new FindListener<UserBasicInfoBean>() {
            @Override
            public void done(List<UserBasicInfoBean> result, BmobException e) {
                if (e == null) {
                    if (!result.isEmpty()) {
                        Log.e(TAG, "查询成功 当前用户详情信息的objectId>>>>>：" + result.get(0).getObjectId());
                        userBasicInfoBean = result.get(0);
                        if (TextUtils.equals(Share.get().getUserName(), username)) {
                            UserBasicInfoBean.updateLocalSpUserBasicInfo(userBasicInfoBean);
                        } else {
                            queryIsFollow();
                            addVisitBeanData();//添加用户到访数据
                        }
                        setUI();
                    } else {
                        Log.e(TAG, "done: 查找UserBasicInfoBean表里面username为自己的数据为空");
                        //没有查询到用户详情数据库表
                        if (TextUtils.equals(Share.get().getUserName(), username)) {
                            createUserDetailInfoBean();
                        } else {
                            showDefaultEmpty();
                        }
                    }
                } else {
                    Log.e(TAG, "done: 查询用户详情信息失败：" + e.toString());
                    ToastSimple.show(e.getMessage());
                    showDefaultError(view -> {
                        queryUserBasicInfo();
                    });
                }
            }
        });


        /************************************/
        /*BmobQuery<User> categoryBmobQuery = new BmobQuery<>();
        categoryBmobQuery.addWhereEqualTo("username", username);
        categoryBmobQuery.findObjects(new FindListener<UserDetailInfoBean>() {
            @Override
            public void done(List<UserDetailInfoBean> result, BmobException e) {
                if (e == null) {
                    if (!result.isEmpty()) {
                        userBasicInfo = result.get(0);
                        queryIsFollow();
                        setUI();
                    } else {
                        //没有查询到用户详情数据库表
                        if (TextUtils.equals(Share.get().getUserName(), username)) {
                            createUserDetailInfoBean();
                        } else {
                            showDefaultEmpty();
                        }
                    }
                    Log.e(TAG, "done: 查询查询指定username的详情数据成功:" + result.size());
                } else {
                    Log.e("BMOB", e.toString());
                    ToastSimple.show(e.getMessage());
                    showDefaultError(view -> {
                        queryUserDetailInfo();
                    });
                }
            }
        });*/
    }

    /**
     * 查询“关于我”
     */
    private void queryAboutMe() {
        BmobQuery<AboutMeBean> categoryBmobQuery = new BmobQuery<>();
        categoryBmobQuery.addWhereEqualTo("username", username);
        categoryBmobQuery.findObjects(new FindListener<AboutMeBean>() {
            @Override
            public void done(List<AboutMeBean> result, BmobException e) {
                if (e == null) {
                    if (!result.isEmpty()) {
                        Log.e(TAG, "查询成功 “关于我” 当前用户的objectId>>>>>：" + result.get(0).getObjectId());
                        aboutMeBean = result.get(0);
                        queryIsFollow();
                        setUI();
                    } else {
                        Log.e(TAG, "done: 查找“关于我” AboutMeBean表里面username为自己的数据为空");
                        //没有查询到用户详情数据库表
                        if (TextUtils.equals(Share.get().getUserName(), username)) {
                            createAboutMeBean();
                        }
                    }
                } else {
                    Log.e("BMOB", e.toString());
                    ToastSimple.show(e.getMessage());
                    showDefaultError(view -> {
                        queryAboutMe();
                    });
                }
            }
        });
    }

    /**
     * 添加一行用户详情数据到数据库
     */
    private void createUserDetailInfoBean() {
        UserBasicInfoBean userBasic = new UserBasicInfoBean();
//        UserDetailInfoBean userDetailInfo = new UserDetailInfoBean();
        userBasic.setUsername(username);
        userBasic.setNickName("");
        userBasic.setWechat("");
        userBasic.setHeadPic(Share.get().getHeadPic());
        userBasic.setBirthday("");
        userBasic.setSex(Share.get().getSex());
        userBasic.setAddress("");
        userBasic.setZujiAddress("");
        userBasic.setXueli("");
        userBasic.setWork("");
        userBasic.setHeight(0);
        userBasic.setWeight(0);
        userBasic.setBiYeSchool("");
        userBasic.setYearInCome("");
        userBasic.setHeYanWeChat(false);

        userBasic.save(new SaveListener<String>() {
            @Override
            public void done(String objectId, BmobException e) {
                if (e == null) {
                    Log.e(TAG, "添加数据成功，返回objectId为：" + objectId);
                    followObjectId = objectId;
                    userBasicInfoBean = userBasic;
                    queryIsFollow();
                    setUI();
                } else {
                    Log.e(TAG, "done: 创建用户详情数据失败：" + e.getMessage());
                    ToastSimple.show("创建用户详情数据失败：" + e.getMessage());
                    showDefaultError(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            createUserDetailInfoBean();
                        }
                    });
                }
            }
        });
    }

    /**
     * 添加一行“关于我”数据到数据库
     */
    private void createAboutMeBean() {
        AboutMeBean aboutMeBean = new AboutMeBean();
//        UserDetailInfoBean userDetailInfo = new UserDetailInfoBean();
        aboutMeBean.setUsername(username);
        aboutMeBean.setSelfDescription("");
        aboutMeBean.setSelfDescriptionPic(new ArrayList<>());
        aboutMeBean.setFamilyBackground("");
        aboutMeBean.setFamilyBackgroundPic(new ArrayList<>());
        aboutMeBean.setHobby("");
        aboutMeBean.setHobbyPic(new ArrayList<>());
        aboutMeBean.setLoveIdea("");
        aboutMeBean.setLoveIdeaPic(new ArrayList<>());
        aboutMeBean.setIdealPartner("");
        aboutMeBean.setIdealPartnerPic(new ArrayList<>());
        aboutMeBean.setWhySingle("");
        aboutMeBean.setWhySinglePic(new ArrayList<>());
        aboutMeBean.setQiDaiShengHuo("");
        aboutMeBean.setQiDaiShengHuoPic(new ArrayList<>());

        aboutMeBean.save(new SaveListener<String>() {
            @Override
            public void done(String objectId, BmobException e) {
                if (e == null) {
                    Log.e(TAG, "添加数据成功，返回objectId为：" + objectId);
                    UserDetailInfoActivity.this.aboutMeBean = aboutMeBean;
//                    queryIsFollow();
//                    setUI();
                } else {
                    Log.e(TAG, "done: 创建用户详情数据失败：" + e.getMessage());
                    ToastSimple.show("创建用户详情数据失败：" + e.getMessage());
                    showDefaultError(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            createAboutMeBean();
                        }
                    });
                }
            }
        });
    }

    /**
     * 查询是否关注了对方
     */
    private void queryIsFollow() {
        if (!TextUtils.equals(username, Share.get().getUserName())) {
            BmobQuery<FollowBean> categoryBmobQuery = new BmobQuery<>();
            categoryBmobQuery.addWhereEqualTo("username", Share.get().getUserName());
            categoryBmobQuery.addWhereEqualTo("followToUserName", username);
            categoryBmobQuery.findObjects(new FindListener<FollowBean>() {
                @Override
                public void done(List<FollowBean> result, BmobException e) {
                    if (e == null) {
                        if (!result.isEmpty()) {
                            followObjectId = result.get(0).getObjectId();
                            tv_follow_or_no.setText("已关注");
                        } else {
                            tv_follow_or_no.setText("关注");
                        }
                        tv_follow_or_no.setVisibility(View.VISIBLE);
                        Log.e(TAG, "done: 查询查询指定userName的是否已关注成功:" + result.size());
                    } else {
                        Log.e(TAG, "查询查询指定userName的是否已关注失败 " + e.toString());
                        ToastSimple.show(e.getMessage());
                    }
                }
            });
        }
    }

    /**
     * 关注某人
     */
    private void addFollow() {
        FollowBean followBean = new FollowBean();
        followBean.setUsername(Share.get().getUserName());
        followBean.setFollowToUserName(userBasicInfoBean.getUsername());
        followBean.setHeadPic(Share.get().getHeadPic());
        followBean.setFollowToUserPic(userBasicInfoBean.getHeadPic());
        followBean.setUserBirthDay(Share.get().getBirthDay());
        followBean.setFolloToUserBirthDay(userBasicInfoBean.getBirthday());
        followBean.setUserHeight(Share.get().getHeight());
        followBean.setFollowToUserHeight(userBasicInfoBean.getHeight());
        followBean.setAddress(Share.get().getAddress());
        followBean.setFollowToUserAddress(userBasicInfoBean.getAddress());
        followBean.setUserSex(Share.get().getSex());
        followBean.setFollowToUserSex(userBasicInfoBean.getSex());
        followBean.setUserNickName(Share.get().getNickName());
        followBean.setFollowUserNickName(userBasicInfoBean.getNickName());
        followBean.save(new SaveListener<String>() {
            @Override
            public void done(String objectId, BmobException e) {
                if (e == null) {
                    followObjectId = objectId;
                    tv_follow_or_no.setText("已关注");
                    Log.e(TAG, "done: 关注成功： username为：" + userBasicInfoBean.getUsername());
                } else {
                    ToastSimple.show(e.getMessage());
                    Log.e(TAG, "done: 关注失败：" + e.getMessage());
                }
            }
        });
    }

    /**
     * 取消关注某人
     */
    private void cancelFollow() {
        if (!TextUtils.isEmpty(followObjectId)) {
            FollowBean followBean = new FollowBean();
            followBean.setObjectId(followObjectId);
            followBean.delete(new UpdateListener() {
                @Override
                public void done(BmobException e) {
                    if (e == null) {
                        tv_follow_or_no.setText("关注");
                        Log.e(TAG, "done: 取消关注成功");
                    } else {
                        ToastSimple.show(e.getMessage());
                        Log.e(TAG, "done: 取消关注失败：" + e.getMessage());
                    }
                }
            });
        } else {
            Log.e(TAG, "cancelFollow: 关注ID为空");
        }
    }

    private void setUI() {
        if (userBasicInfoBean == null) {
            return;
        }
        showDefaultContent();
//        UserBasicInfoBean.UserDetailInfoBean userDetailInfoBean = userBasicInfoBean.getDetail();
//            UserBasicInfoBean.UserDetailInfoBean.UserPicBean userPicBean = userDetailInfoBean.getUserPic();
        if (userBasicInfoBean.getUserTopPic() != null && (!userBasicInfoBean.getUserTopPic().isEmpty())) {
            BannerImageAdapter bannerImageAdapter = new BannerImageAdapter(this, userBasicInfoBean.getUserTopPic());

            ArrayList<Object> list = new ArrayList<>();
            for (int i = 0; i < userBasicInfoBean.getUserTopPic().size(); i++) {
                list.add(userBasicInfoBean.getUserTopPic().get(i));
            }

            banner_user_pic.setAdapter(bannerImageAdapter)
//              .setCurrentItem(0,false)
                    .addBannerLifecycleObserver(this)//添加生命周期观察者
                    .setIndicator(new CircleIndicator(this))//设置指示器
                    .setOnBannerListener((data, position) -> {
                        Log.e(TAG, "setUI: 点击了图片：" + data);

                        new XPopup.Builder(this)
//                            .animationDuration(1000)
                                .isTouchThrough(true)
                                .asImageViewer(iv_banner, position, list,
                                        false, true, -1, -1, ConvertUtils.dp2px(10), true,
                                        Color.rgb(32, 36, 46),
                                        new OnSrcViewUpdateListener() {
                                            @Override
                                            public void onSrcViewUpdate(ImageViewerPopupView popupView, int position) {
//                                                RecyclerView rv = (RecyclerView) holder.itemView.getParent();
//                                                popupView.updateSrcView((ImageView) rv.getChildAt(position));
                                            }
                                        }, new SmartGlideImageLoader(true, R.mipmap.ic_launcher), null)
                                .show();
                    });
        }


//        if (userBasicInfo.getUserPic() != null && userBasicInfo.getUserPic().getUserTopPic() != null && !userBasicInfo.getUserPic().getUserTopPic().isEmpty()) {
//            //自定义的图片适配器，也可以使用默认的BannerImageAdapter
//            BannerImageAdapter bannerImageAdapter = new BannerImageAdapter(this, userBasicInfo.getUserPic().getUserTopPic());
//            banner_user_pic.setAdapter(bannerImageAdapter)
////              .setCurrentItem(0,false)
//                    .addBannerLifecycleObserver(this)//添加生命周期观察者
//                    .setIndicator(new CircleIndicator(this))//设置指示器
//                    .setOnBannerListener((data, position) -> {
//                        Log.e(TAG, "setUI: 点击了图片：" + data);
//                    });
//        }
        tv_nick_name.setText(TextUtils.isEmpty(userBasicInfoBean.getNickName()) ? userBasicInfoBean.getUsername() : userBasicInfoBean.getNickName());
        if (!TextUtils.isEmpty(userBasicInfoBean.getBirthday())) {
            int yearIndex = userBasicInfoBean.getBirthday().indexOf("-");
            String year = "";
            if (yearIndex != -1) {
                year = userBasicInfoBean.getBirthday().substring(0, yearIndex);
            }
            tv_age.setText(year + "年");

            String[] birthDay = userBasicInfoBean.getBirthday().split("-");
            Log.e(TAG, "setUI: 截取出来的生日为：" + birthDay.toString());
            if (birthDay.length == 3) {
                tv_xingzuo.setText(StringUtil.calculateXingzuo(birthDay[1], birthDay[2]));
            }
        }
        tv_height.setText(userBasicInfoBean.getHeight() + "cm");
        tv_weight.setText(userBasicInfoBean.getWeight() + "kg");
        tv_address.setText("现居：" + userBasicInfoBean.getAddress());
        tv_zuji.setText("祖籍：" + userBasicInfoBean.getZujiAddress());
        tv_xueli.setText(userBasicInfoBean.getXueli());
        tv_work.setText(userBasicInfoBean.getWork());
        if (userBasicInfoBean.isHeYanWeChat()) {
            //微信号核验通过了
            btn_unlock_wx.setVisibility(View.VISIBLE);
            tv_wechat_state.setText("已核验");
        } else {
            btn_unlock_wx.setVisibility(View.GONE);
            tv_wechat_state.setText("待核验");
        }
        /**************/
//        tv_self_desc.setText(userBasicInfo.getSelfDescription());
//        tv_family_bg.setText(userBasicInfo.getFamilyBackground());
//        tv_hobby.setText(userBasicInfo.getHobby());
//        tv_love_point.setText(userBasicInfo.getLoveIdea());
//        tv_ideal_ta.setText(userBasicInfo.getIdealPartner());
//        tv_why_single.setText(userBasicInfo.getWhySingle());
//        tv_expect_life.setText(userBasicInfo.getQiDaiShengHuo());
        /************/
        setDetailInfoUi(1);
        setDetailInfoUi(2);
        setDetailInfoUi(3);
        setDetailInfoUi(4);
        setDetailInfoUi(5);
        setDetailInfoUi(6);
        setDetailInfoUi(7);
    }

    /**
     * 刷新用户任务列表
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void updateUserBaseInfo(Event.updateUserBaseInfo event) {
        if (event.result != null) {
            userBasicInfoBean = event.result;
            setUI();
        }
    }

    /**
     * 刷新用户“顶部精美图片”
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void updataUserTopPic(Event.updataUserTopPic event) {
//        if (userBasicInfo != null) {
        if (userBasicInfoBean != null) {
            userBasicInfoBean.setUserTopPic(event.picUrl);
            setUI();
        } else {
            Log.e(TAG, "updataUserTopPic: 用户基本信息为空，无法保存精美图片");
        }
    }

    /**
     * 刷新用户“关于我”
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void updataUserInfoDetail(Event.updataUserInfoDetail event) {
//        if (userBasicInfo != null) {
        if (aboutMeBean == null) {
            aboutMeBean = new AboutMeBean();
        }
        switch (event.updataCardType) {
            case 1:
                aboutMeBean.setSelfDescription(event.updataCardText);
                aboutMeBean.setSelfDescriptionPic(event.updataCardPic);
                break;
            case 2:
                aboutMeBean.setFamilyBackground(event.updataCardText);
                aboutMeBean.setFamilyBackgroundPic(event.updataCardPic);
                break;
            case 3:
                aboutMeBean.setHobby(event.updataCardText);
                aboutMeBean.setHobbyPic(event.updataCardPic);
                break;
            case 4:
                aboutMeBean.setLoveIdea(event.updataCardText);
                aboutMeBean.setLoveIdeaPic(event.updataCardPic);
                break;
            case 5:
                aboutMeBean.setIdealPartner(event.updataCardText);
                aboutMeBean.setIdealPartnerPic(event.updataCardPic);
                break;
            case 6:
                aboutMeBean.setWhySingle(event.updataCardText);
                aboutMeBean.setWhySinglePic(event.updataCardPic);
                break;
            case 7:
                aboutMeBean.setQiDaiShengHuo(event.updataCardText);
                aboutMeBean.setQiDaiShengHuoPic(event.updataCardPic);
                break;
        }
//        }
        setDetailInfoUi(event.updataCardType);
    }

    private void setDetailInfoUi(int updataCardType) {
        /*UserBasicInfoBean.UserDetailInfoBean userDetailInfoBean = userBasicInfoBean.getDetail();
        UserBasicInfoBean.UserDetailInfoBean.UserPicBean userPicBean;
        if (userDetailInfoBean != null) {
            userPicBean = userDetailInfoBean.getUserPic();
        }*/
        switch (updataCardType) {
            case 1:
                if (aboutMeBean != null) {
                    tv_self_desc.setText(aboutMeBean.getSelfDescription());
                    if (aboutMeBean.getSelfDescriptionPic() != null && !aboutMeBean.getSelfDescriptionPic().isEmpty()) {
                        rv_self_desc.setNestedScrollingEnabled(false);
                        rv_self_desc.setFocusable(false);
                        rv_self_desc.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false) {
                            @Override
                            public boolean canScrollVertically() {
                                return false;
                            }
                        });
                        UserInfoDetailCardPicAdapter userInfoDetailCardPicAdapter = new UserInfoDetailCardPicAdapter(this, aboutMeBean.getSelfDescriptionPic(), R.layout.item_userinfo_card_pic);
                        rv_self_desc.setAdapter(userInfoDetailCardPicAdapter);
                    }
                }
                break;
            case 2:
                if (aboutMeBean != null) {
                    tv_family_bg.setText(aboutMeBean.getFamilyBackground());
                    if (aboutMeBean.getFamilyBackgroundPic() != null && !aboutMeBean.getFamilyBackgroundPic().isEmpty()) {
                        rv_family_bg.setNestedScrollingEnabled(false);
                        rv_family_bg.setFocusable(false);
                        rv_family_bg.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false) {
                            @Override
                            public boolean canScrollVertically() {
                                return false;
                            }
                        });
                        UserInfoDetailCardPicAdapter userInfoDetailCardPicAdapter = new UserInfoDetailCardPicAdapter(this, aboutMeBean.getFamilyBackgroundPic(), R.layout.item_userinfo_card_pic);
                        rv_family_bg.setAdapter(userInfoDetailCardPicAdapter);
                    }
                }
                break;
            case 3:
                if (aboutMeBean != null) {
                    tv_hobby.setText(aboutMeBean.getHobby());
                    if (aboutMeBean.getHobbyPic() != null && !aboutMeBean.getHobbyPic().isEmpty()) {
                        rv_hobby.setNestedScrollingEnabled(false);
                        rv_hobby.setFocusable(false);
                        rv_hobby.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false) {
                            @Override
                            public boolean canScrollVertically() {
                                return false;
                            }
                        });
                        UserInfoDetailCardPicAdapter userInfoDetailCardPicAdapter = new UserInfoDetailCardPicAdapter(this, aboutMeBean.getHobbyPic(), R.layout.item_userinfo_card_pic);
                        rv_hobby.setAdapter(userInfoDetailCardPicAdapter);
                    }
                }
                break;
            case 4:
                if (aboutMeBean != null) {
                    tv_love_point.setText(aboutMeBean.getLoveIdea());
                    if (aboutMeBean.getLoveIdeaPic() != null && !aboutMeBean.getLoveIdeaPic().isEmpty()) {
                        rv_love_point.setNestedScrollingEnabled(false);
                        rv_love_point.setFocusable(false);
                        rv_love_point.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false) {
                            @Override
                            public boolean canScrollVertically() {
                                return false;
                            }
                        });
                        UserInfoDetailCardPicAdapter userInfoDetailCardPicAdapter = new UserInfoDetailCardPicAdapter(this, aboutMeBean.getLoveIdeaPic(), R.layout.item_userinfo_card_pic);
                        rv_love_point.setAdapter(userInfoDetailCardPicAdapter);
                    }
                }
                break;
            case 5:
                if (aboutMeBean != null) {
                    tv_ideal_ta.setText(aboutMeBean.getIdealPartner());
                    if (aboutMeBean.getIdealPartnerPic() != null && !aboutMeBean.getIdealPartnerPic().isEmpty()) {
                        rv_ideal_ta.setNestedScrollingEnabled(false);
                        rv_ideal_ta.setFocusable(false);
                        rv_ideal_ta.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false) {
                            @Override
                            public boolean canScrollVertically() {
                                return false;
                            }
                        });
                        UserInfoDetailCardPicAdapter userInfoDetailCardPicAdapter = new UserInfoDetailCardPicAdapter(this, aboutMeBean.getIdealPartnerPic(), R.layout.item_userinfo_card_pic);
                        rv_ideal_ta.setAdapter(userInfoDetailCardPicAdapter);
                    }
                }
                break;
            case 6:
                if (aboutMeBean != null) {
                    tv_why_single.setText(aboutMeBean.getWhySingle());
                    if (aboutMeBean.getWhySinglePic() != null && !aboutMeBean.getWhySinglePic().isEmpty()) {
                        rv_why_single.setNestedScrollingEnabled(false);
                        rv_why_single.setFocusable(false);
                        rv_why_single.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false) {
                            @Override
                            public boolean canScrollVertically() {
                                return false;
                            }
                        });
                        UserInfoDetailCardPicAdapter userInfoDetailCardPicAdapter = new UserInfoDetailCardPicAdapter(this, aboutMeBean.getWhySinglePic(), R.layout.item_userinfo_card_pic);
                        rv_why_single.setAdapter(userInfoDetailCardPicAdapter);
                    }
                }
                break;
            case 7:
                if (aboutMeBean != null) {
                    tv_expect_life.setText(aboutMeBean.getQiDaiShengHuo());
                    if (aboutMeBean.getQiDaiShengHuoPic() != null && !aboutMeBean.getQiDaiShengHuoPic().isEmpty()) {
                        rv_expect_life.setNestedScrollingEnabled(false);
                        rv_expect_life.setFocusable(false);
                        rv_expect_life.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false) {
                            @Override
                            public boolean canScrollVertically() {
                                return false;
                            }
                        });
                        UserInfoDetailCardPicAdapter userInfoDetailCardPicAdapter = new UserInfoDetailCardPicAdapter(this, aboutMeBean.getQiDaiShengHuoPic(), R.layout.item_userinfo_card_pic);
                        rv_expect_life.setAdapter(userInfoDetailCardPicAdapter);
                    }
                }
                break;
        }
    }

    private void queryUserWallet() {
        BmobQuery<UserWalletBean> categoryBmobQuery = new BmobQuery<>();
        categoryBmobQuery.addWhereEqualTo("username", Share.get().getUserName());
        categoryBmobQuery.findObjects(new FindListener<UserWalletBean>() {
            @Override
            public void done(List<UserWalletBean> result, BmobException e) {
                if (e == null) {
                    if (!result.isEmpty()) {
                        UserWalletBean userWalletBean = result.get(0);
                        Share.get().setUser_wallet_bean_object_id(userWalletBean.getObjectId());
                        Share.get().setIsVip(userWalletBean.isVip());
                        Share.get().setGoldBalance(userWalletBean.getGoldBalance());
                        Share.get().setIntegralBalance(userWalletBean.getIntegralBalance());
                    } else {
//                        createUserWallet();//给用户创建一个钱包
                        UserWalletBean.createUserWallet();
                    }
                } else {
                    Log.e(TAG, e.toString());
                }
            }
        });
    }

    /**
     * 更新用户钱包的金币数量
     *
     * @param toUpdateGold 将金币更新为多少
     */
    private void updateUserWallet(double toUpdateGold) {
        BmobQuery<UserWalletBean> categoryBmobQuery = new BmobQuery<>();
        categoryBmobQuery.addWhereEqualTo("username", Share.get().getUserName());
        categoryBmobQuery.findObjects(new FindListener<UserWalletBean>() {
            @Override
            public void done(List<UserWalletBean> result, BmobException e) {
                if (e == null) {
                    if (!result.isEmpty()) {
                        //更新UserWalletBean表里面id为6b6c11c537的数据，address内容更新为“北京朝阳”
                        UserWalletBean p2 = result.get(0);
                        Share.get().setUser_wallet_bean_object_id(p2.getObjectId());
                        p2.setGoldBalance(toUpdateGold + "");
                        p2.update(p2.getObjectId(), new UpdateListener() {
                            @Override
                            public void done(BmobException e) {
                                if (e == null) {
                                    Share.get().setGoldBalance(toUpdateGold + "");
                                    Log.e(TAG, "金币余额 更新成功:" + p2.getUpdatedAt());
                                    if (userBasicInfoBean != null) {
                                        UserWalletBean.updateUnlockWechatTargetIdIntegralBalance(userBasicInfoBean);
                                    }

                                    ClipboardManager cm = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);//创建普通字符型ClipData
                                    ClipData mClipData = ClipData.newPlainText("simple text", userBasicInfoBean.getWechat());//将ClipData内容放到系统剪贴板里。
                                    cm.setPrimaryClip(mClipData);
                                    showConfirmNoCancelDialog("提示", "微信号已复制，快粘贴加为好友吧", new DialogClickListener() {
                                        @Override
                                        public void onConfirm() {
                                        }

                                        @Override
                                        public void onCancel() {
                                        }
                                    });
                                } else {
                                    Log.e(TAG, "金币余额 更新失败：" + e.getMessage());
                                    View view = LayoutInflater.from(UserDetailInfoActivity.this).inflate(R.layout.toast_text, null);
                                    TextView tv_toast_message = view.findViewById(R.id.tv_toast_message);
                                    tv_toast_message.setText(e.getMessage());
                                    ToastSimple.makeText(Gravity.CENTER, 2, view).show();
                                }
                            }
                        });
                    } else {
                        Log.e(TAG, "更新金币余额 用户钱包不存在");
//                        createUserWallet();//给用户创建一个钱包
                        UserWalletBean.createUserWallet();
                    }
                } else {
                    Log.e(TAG, e.toString());
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
