package com.huolongluo.luo.huolongluo.ui.activity.editjob;

import android.os.Bundle;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.gyf.immersionbar.ImmersionBar;
import com.huolongluo.luo.R;
import com.huolongluo.luo.huolongluo.base.BaseActivity;
import com.huolongluo.luo.huolongluo.share.Event;
import com.huolongluo.luo.util.ToastSimple;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;

/**
 * <p>
 * Created by 火龙裸 on 2017/9/6.
 */

public class EditJobActivity extends BaseActivity {
    private static final String TAG = "EditJobActivity";
    @BindView(R.id.toolbar_center_title)
    TextView toolbar_center_title;
    @BindView(R.id.iv_left)
    ImageView iv_left;
    @BindView(R.id.tv_right)
    TextView tv_right;
    /********************* 以上为ToolBar ************************/
    @BindView(R.id.et_job_name)
    EditText et_job_name;
    @BindView(R.id.tv_sure)
    TextView tv_sure;

    @Override
    protected int getContentViewId() {
        return R.layout.activity_edit_job;
    }

    @Override
    protected void injectDagger() {
        activityComponent().inject(this);
    }

    private void initToolBar() {
        ImmersionBar.with(this).statusBarDarkFont(true).init();
        toolbar_center_title.setText("职业");
//        tv_right.setVisibility(View.VISIBLE);
//        tv_right.setText("充值记录");
    }

    @Override
    protected void initViewsAndEvents(@Nullable Bundle savedInstanceState) {
        initToolBar();
        eventClick(iv_left).subscribe(o -> // 点击返回键
        {
            close();
        });
        eventClick(tv_sure).subscribe(o -> // 点击确定
        {
            String etJobNameNameContent = et_job_name.getText().toString().trim();
            if (TextUtils.isEmpty(etJobNameNameContent)) {
                ToastSimple.show("职业不能为空");
                return;
            }
            EventBus.getDefault().post(new Event.editJob(etJobNameNameContent));
            close();
        });

    }
}
