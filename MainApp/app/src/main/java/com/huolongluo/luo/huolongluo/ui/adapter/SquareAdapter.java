package com.huolongluo.luo.huolongluo.ui.adapter;

import android.content.Context;
import android.text.TextUtils;

import com.huolongluo.luo.R;
import com.huolongluo.luo.huolongluo.bean.UserBasicInfoBean;
import com.huolongluo.luo.superAdapter.recycler.BaseViewHolder;
import com.huolongluo.luo.superAdapter.recycler.SuperAdapter;
import com.tencent.liteav.trtccalling.model.util.ImageLoader;

import java.util.List;

/**
 * Created by 火龙裸 on 2018/6/10 0010.
 */

public class SquareAdapter extends SuperAdapter<UserBasicInfoBean> {
    public SquareAdapter(Context context, List<UserBasicInfoBean> list, int layoutResId) {
        super(context, list, layoutResId);
    }

    @Override
    public void onBind(int viewType, BaseViewHolder holder, int position, UserBasicInfoBean item) {
//        AboutMeBean aboutMeBean = item.getAboutMe();
//
//        if (aboutMeBean != null && !aboutMeBean.getUserTopPic().isEmpty()) {
//            ImageLoader.loadImage(mContext, holder.getView(R.id.iv_user_pic), aboutMeBean.getUserTopPic().get(0));
////            Glide.with(mContext).load(item.getUserPic()).error(loadTransform(context, errorResId, radius)).into(imageView);
//        }

        if (item.getUserTopPic() != null && !item.getUserTopPic().isEmpty()) {
            ImageLoader.loadImage(mContext, holder.getView(R.id.iv_user_pic), item.getUserTopPic().get(0));
        }

        holder.setText(R.id.tv_nick_name, item.getNickName());
        holder.setText(R.id.tv_address, item.getAddress());
        holder.setText(R.id.tv_zuji, item.getZujiAddress());
        if (!TextUtils.isEmpty(item.getBirthday())) {
            int yearIndex = item.getBirthday().indexOf("-");
            String year = "";
            if (yearIndex != -1) {
                year = item.getBirthday().substring(0, yearIndex);
            }
            holder.setText(R.id.tv_old, year + "年");
        }
        holder.setText(R.id.tv_height, item.getHeight() + "cm");
        holder.setText(R.id.tv_xueli, item.getXueli());
        holder.setText(R.id.tv_work, item.getWork());
//        if (item.getGiftGold() <= 0) {
//            holder.setVisibility(R.id.tv_vip_gift_num, View.INVISIBLE);
//        } else {
//            holder.setText(R.id.tv_vip_gift_num,"送" + item.getGiftGold() + "金币");
//            holder.setVisibility(R.id.tv_vip_gift_num, View.VISIBLE);
//        }
//
//        if (item.isChecked()) {
//            holder.getView(R.id.ll_chongzhi_type).setBackgroundResource(R.mipmap.icon_charge_item_selected);
//        } else {
//            holder.getView(R.id.ll_chongzhi_type).setBackgroundResource(0);
//        }
//
//        // 点击item
//        RxView.clicks(holder.getItemView()).throttleFirst(500, TimeUnit.MILLISECONDS).observeOn(AndroidSchedulers.mainThread()).subscribe(v ->
//        {
//            Log.e("TAG", "onBind: 点击了：" + position + "  共：" + getItemCount());
//
//            for (int i = 0; i < getItemCount(); i++) {
//                getAllData().get(i).setChecked(false);
//            }
//            item.setChecked(true);
//            for (int i = 0; i < getItemCount(); i++) {
//                Log.e("TAG", "onBind: 最终的选中情况：" + item.isChecked());
//            }
//            notifyDataSetChanged();
//        });

        // 点击任务弹出窗体
//        RxView.clicks(holder.getView(R.id.tv_task_name)).throttleFirst(500, TimeUnit.MILLISECONDS).observeOn(AndroidSchedulers.mainThread()).subscribe(v ->
//        {
//            EventBus.getDefault().post(new Event.Task(item.getContent()+""));
//        });
//        // 点击 提交 任务
//        RxView.clicks(holder.getView(R.id.tv_submit01)).throttleFirst(500, TimeUnit.MILLISECONDS).observeOn(AndroidSchedulers.mainThread()).subscribe(v ->
//        {
//            Intent intent = new Intent(mContext, SubMitTaskActivity.class);
//            intent.putExtra("tId", item.getId()+"");
//            intent.putExtra("position", position);
//            intent.putExtra("title", item.getContent());
//            mContext.startActivity(intent);
//        });

    }
}
