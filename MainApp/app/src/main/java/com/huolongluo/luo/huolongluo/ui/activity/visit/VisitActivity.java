package com.huolongluo.luo.huolongluo.ui.activity.visit;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gyf.immersionbar.ImmersionBar;
import com.huolongluo.luo.R;
import com.huolongluo.luo.huolongluo.base.BaseActivity;
import com.huolongluo.luo.huolongluo.bean.UserWalletBean;
import com.huolongluo.luo.huolongluo.bean.VisitBean;
import com.huolongluo.luo.huolongluo.constants.Constants;
import com.huolongluo.luo.huolongluo.manager.DialogClickListener;
import com.huolongluo.luo.huolongluo.share.Share;
import com.huolongluo.luo.huolongluo.ui.activity.account.MyAccountActivity;
import com.huolongluo.luo.huolongluo.ui.activity.userdetail.UserDetailInfoActivity;
import com.huolongluo.luo.huolongluo.ui.adapter.VisitAdapter;
import com.huolongluo.luo.superAdapter.recycler.SuperAdapter;
import com.huolongluo.luo.util.ToastSimple;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnLoadMoreListener;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.SaveListener;
import cn.bmob.v3.listener.UpdateListener;

/**
 * <p>
 * Created by 火龙裸 on 2017/9/6.
 */

public class VisitActivity extends BaseActivity {
    private static final String TAG = "VisitActivity";
    @BindView(R.id.toolbar_center_title)
    TextView toolbar_center_title;
    @BindView(R.id.iv_left)
    ImageView iv_left;
    /********************* 以上为ToolBar ************************/
    @BindView(R.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    @BindView(R.id.rv_visit)
    RecyclerView rv_visit;

    private List<VisitBean> visitBeanList = new ArrayList<>();
    private VisitAdapter visitAdapter;

    private String currentUserWalletBeanObjectId = "";
    private String unLockUserObjectId = "";

    @Override
    protected int getContentViewId() {
        return R.layout.activity_visit;
    }

    @Override
    protected void injectDagger() {
        activityComponent().inject(this);
    }

    private void initToolBar() {
        ImmersionBar.with(this).statusBarDarkFont(true).init();
        toolbar_center_title.setText("最近来访");
    }

    @Override
    protected void initViewsAndEvents(@Nullable Bundle savedInstanceState) {
        initToolBar();
        eventClick(iv_left).subscribe(o -> // 点击返回键
        {
            close();
        });

        rv_visit.setNestedScrollingEnabled(false);
        rv_visit.setFocusable(false);
        rv_visit.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
//        rv_visit.addItemDecoration(new GridSpacingItemDecoration(3,
//                DensityUtil.dip2px(this, 10), false));
        visitAdapter = new VisitAdapter(this, visitBeanList, R.layout.item_visit);
        rv_visit.setAdapter(visitAdapter);

        visitAdapter.setOnItemClickListener(new SuperAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int viewType, int position) {
                if (visitAdapter.getAllData().get(position).isUnLock() || Share.get().getIsVip()) {
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.USER_NAME, visitAdapter.getAllData().get(position).getUsername());
                    startActivity(UserDetailInfoActivity.class, bundle);
                } else {
                    Log.e(TAG, "onItemClick: 弹窗20金币解锁");
                    showConfirmDialog("解锁身份", "解锁TA的身份需消耗20金币或开通会员免金币", new DialogClickListener() {
                        @Override
                        public void onConfirm() {
                            double goldBalance = Double.parseDouble(Share.get().getGoldBalance());//金币余额
                            if (goldBalance - 20 >= 0) {
                                //更新用户钱包里的金币余额
                                unLockUserObjectId = visitAdapter.getAllData().get(position).getObjectId();
                                updateUserWalletBean(position, goldBalance - 20);
                            } else {
                                View view = LayoutInflater.from(VisitActivity.this).inflate(R.layout.toast_text, null);
                                TextView tv_toast_message = view.findViewById(R.id.tv_toast_message);
                                tv_toast_message.setText("金币余额不足，请充值");
                                ToastSimple.makeText(Gravity.CENTER, 2, view).show();
                                startActivity(MyAccountActivity.class);
                            }
                        }

                        @Override
                        public void onCancel() {
                        }
                    });
                }
            }
        });

        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull final RefreshLayout refreshLayout) {
                queryVisitUser(Constants.MODE_REFRESH);
                //查询个人钱包余额，是否会员
                queryUserWallet();
            }
        });
        refreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout rl) {
//                mAdapter.addData(movies);
                queryVisitUser(Constants.MODE_LOAD_MORE);
//                rl.finishLoadMoreWithNoMoreData();
            }
        });

        queryVisitUser(Constants.MODE_REFRESH);
        //查询个人钱包余额，是否会员
        queryUserWallet();
    }

    /**
     * 更新用户钱包里的金币余额
     * @param goldBalance 要更新为多少金币
     */
    private void updateUserWalletBean(int position, double goldBalance) {
        if (!TextUtils.isEmpty(currentUserWalletBeanObjectId)) {
            //更新Person表里面id为6b6c11c537的数据，address内容更新为“北京朝阳”
            UserWalletBean p2 = new UserWalletBean();
            p2.setGoldBalance(goldBalance + "");
            p2.update(currentUserWalletBeanObjectId, new UpdateListener() {
                @Override
                public void done(BmobException e) {
                    if (e == null) {
                        Log.e(TAG, "更新用户钱包里的金币余额 成功:" + p2.getUpdatedAt());
                        Share.get().setGoldBalance(goldBalance + "");
                        //解锁对应的用户
                        unLockUser(position);
                    } else {
                        Log.e(TAG, "更新用户钱包里的金币余额 失败：" + e.getMessage());
                    }
                }
            });
        } else {
            Log.e(TAG, "updateUserWalletBean: 要更新的金币余额 数据objectId为空");
        }
    }

    private void queryUserWallet() {
        BmobQuery<UserWalletBean> categoryBmobQuery = new BmobQuery<>();
        categoryBmobQuery.addWhereEqualTo("username", Share.get().getUserName());
        categoryBmobQuery.findObjects(new FindListener<UserWalletBean>() {
            @Override
            public void done(List<UserWalletBean> result, BmobException e) {
                if (e == null) {
                    if (!result.isEmpty()) {
                        UserWalletBean userWalletBean = result.get(0);
                        currentUserWalletBeanObjectId = userWalletBean.getObjectId();
                        Share.get().setUser_wallet_bean_object_id(currentUserWalletBeanObjectId);
                        Share.get().setIsVip(userWalletBean.isVip());
                        Share.get().setGoldBalance(userWalletBean.getGoldBalance());
                        Share.get().setIntegralBalance(userWalletBean.getIntegralBalance());
                    } else {
//                        createUserWallet();//给用户创建一个钱包
                        UserWalletBean.createUserWallet();
                    }
                } else {
                    Log.e(TAG, e.toString());
                }
            }
        });
    }

//    /**
//     * 给用户创建一个钱包
//     */
//    private void createUserWallet() {
//        UserWalletBean p2 = new UserWalletBean();
//        p2.setUsername(Share.get().getUserName());
//        p2.setVip(false);
//        p2.setGoldBalance(100);
//        p2.setIntegralBalance(0);
//        p2.save(new SaveListener<String>() {
//            @Override
//            public void done(String objectId, BmobException e) {
//                if (e == null) {
//                    Log.e(TAG, "给用户创建一个钱包 数据成功，返回objectId为：" + objectId);
//                    Share.get().setGoldBalance(100);
//                } else {
//                    Log.e(TAG, "给用户创建一个钱包 创建数据失败：" + e.getMessage());
//                }
//            }
//        });
//    }

    /**
     * 查询谁访问了我
     *
     * @param refreshType 刷新还是上拉加载
     */
    private void queryVisitUser(int refreshType) {
        BmobQuery<VisitBean> categoryBmobQuery = new BmobQuery<>();
        categoryBmobQuery.addWhereEqualTo("visitToUserName", Share.get().getUserName());
        categoryBmobQuery.setLimit(10);//每次最多查询10条
        if (refreshType == Constants.MODE_LOAD_MORE) {
            categoryBmobQuery.setSkip(visitAdapter.getItemCount());// 忽略前多少条数据，查询后面的
        }
        categoryBmobQuery.findObjects(new FindListener<VisitBean>() {
            @Override
            public void done(List<VisitBean> result, BmobException e) {
                if (e == null) {
                    if (!result.isEmpty()) {
                        Collections.reverse(result);
                        if (refreshType == Constants.MODE_REFRESH) {
                            visitBeanList.clear();
                            visitBeanList.addAll(result);
                            if (!visitBeanList.isEmpty()) {
                                visitAdapter.replaceAll(visitBeanList);
                            }
                        } else {
                            visitBeanList.addAll(result);
                            if (!visitBeanList.isEmpty()) {
                                visitAdapter.addAll(result);
                            }
                        }
                    } else {
                        refreshLayout.finishLoadMoreWithNoMoreData();
                    }
                    Log.e(TAG, "done: 到访用户 查询成功" + result.size());
                } else {
                    Log.e(TAG, e.toString());
                }
                refreshLayout.finishRefresh();
                refreshLayout.finishLoadMore();
            }
        });
    }

    private void unLockUser(int position){
        if (!TextUtils.isEmpty(unLockUserObjectId)) {
            //更新VisitBean表里面id为6b6c11c537的数据，设置为解锁状态
            VisitBean p2 = new VisitBean();
            p2.setUnLock(true);
            p2.update(unLockUserObjectId, new UpdateListener() {
                @Override
                public void done(BmobException e) {
                    if(e==null){
                        Log.e(TAG, "解锁用户 更新成功:"+p2.getUpdatedAt());
                        visitBeanList.get(position).setUnLock(true);
                        visitAdapter.getAllData().get(position).setUnLock(true);
                        visitAdapter.notifyItemChanged(position);
                    }else{
                        Log.e(TAG, "解锁用户  更新失败：" + e.getMessage());
                    }
                }

            });
        }
    }
}
