package com.huolongluo.luo.huolongluo.ui.adapter;

import android.content.Context;

import com.huolongluo.luo.huolongluo.bean.ItemBean;
import com.huolongluo.luo.superAdapter.recycler.BaseViewHolder;
import com.huolongluo.luo.superAdapter.recycler.SuperAdapter;

import java.util.List;

public class BottomSheetDialogAdapter extends SuperAdapter<ItemBean> {
    public BottomSheetDialogAdapter(Context context, List<ItemBean> list, int layoutResId) {
        super(context, list, layoutResId);
    }

    @Override
    public void onBind(int viewType, BaseViewHolder holder, int position, ItemBean item) {

    }
}
