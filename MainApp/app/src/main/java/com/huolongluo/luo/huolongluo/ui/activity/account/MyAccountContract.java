package com.huolongluo.luo.huolongluo.ui.activity.account;

import com.huolongluo.luo.base.BasePresenter;
import com.huolongluo.luo.base.BaseView;
import com.huolongluo.luo.huolongluo.bean.WXCreateOrderBean;
import com.huolongluo.luo.huolongluo.bean.WXCreateOrderEntity;
import com.huolongluo.luo.huolongluo.bean.WXCreateOrderEntity2;

import rx.Subscription;

/**
 * <p>
 * Created by 火龙裸 on 2017/9/6.
 */

public interface MyAccountContract
{
    interface View extends BaseView
    {
        void wxCreateOrderSuccess(WXCreateOrderBean wxCreateOrderBean);
    }

    interface Presenter extends BasePresenter<View>
    {
        Subscription wxCreateOrder(String AuthorizationHeader, WXCreateOrderEntity wxCreateOrderEntity);
        Subscription wxCreateOrder2(WXCreateOrderEntity2 wxCreateOrderEntity);
    }
}
