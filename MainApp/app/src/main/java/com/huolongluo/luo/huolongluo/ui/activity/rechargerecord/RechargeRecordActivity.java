package com.huolongluo.luo.huolongluo.ui.activity.rechargerecord;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gyf.immersionbar.ImmersionBar;
import com.huolongluo.luo.R;
import com.huolongluo.luo.huolongluo.base.BaseActivity;
import com.huolongluo.luo.huolongluo.bean.RechargeRecordBean;
import com.huolongluo.luo.huolongluo.constants.Constants;
import com.huolongluo.luo.huolongluo.share.Share;
import com.huolongluo.luo.huolongluo.ui.adapter.RechargeRecordAdapter;
import com.huolongluo.luo.superAdapter.recycler.SuperAdapter;
import com.huolongluo.luo.util.ToastSimple;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnLoadMoreListener;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;

/**
 * <p>
 * Created by 火龙裸 on 2017/9/6.
 */
public class RechargeRecordActivity extends BaseActivity {
    private static final String TAG = "RechargeRecordActivity";
    @BindView(R.id.toolbar_center_title)
    TextView toolbar_center_title;
    @BindView(R.id.iv_left)
    ImageView iv_left;
    /********************* 以上为ToolBar ************************/
    @BindView(R.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    @BindView(R.id.rv_recharge_record)
    RecyclerView rv_recharge_record;

    private List<RechargeRecordBean> rechargeRecordBeanList = new ArrayList<>();
    private RechargeRecordAdapter rechargeRecordAdapter;

    private String currentUserWalletBeanObjectId = "";
    private String unLockUserObjectId = "";

    @Override
    protected int getContentViewId() {
        return R.layout.activity_recharge_record;
    }

    @Override
    protected void injectDagger() {
        activityComponent().inject(this);
    }

    private void initToolBar() {
        ImmersionBar.with(this).statusBarDarkFont(true).init();
        toolbar_center_title.setText("充值记录");
    }

    @Override
    protected void initViewsAndEvents(@Nullable Bundle savedInstanceState) {
        initToolBar();
        eventClick(iv_left).subscribe(o -> // 点击返回键
        {
            close();
        });

        rv_recharge_record.setNestedScrollingEnabled(false);
        rv_recharge_record.setFocusable(false);
        rv_recharge_record.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
//        rv_recharge_record.addItemDecoration(new GridSpacingItemDecoration(3,
//                DensityUtil.dip2px(this, 10), false));
        rechargeRecordAdapter = new RechargeRecordAdapter(this, rechargeRecordBeanList, R.layout.item_recharge_record);
        rv_recharge_record.setAdapter(rechargeRecordAdapter);

        rechargeRecordAdapter.setOnItemClickListener(new SuperAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int viewType, int position) {
            }
        });

        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull final RefreshLayout refreshLayout) {
                queryRechargeRecordBean(Constants.MODE_REFRESH);
            }
        });
        refreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout rl) {
                queryRechargeRecordBean(Constants.MODE_LOAD_MORE);
            }
        });

        queryRechargeRecordBean(Constants.MODE_REFRESH);
    }

    /**
     * 查询谁访问了我
     *
     * @param refreshType 刷新还是上拉加载
     */
    private void queryRechargeRecordBean(int refreshType) {
        BmobQuery<RechargeRecordBean> categoryBmobQuery = new BmobQuery<>();
        categoryBmobQuery.addWhereEqualTo("username", Share.get().getUserName());
        categoryBmobQuery.setLimit(10);//每次最多查询10条
        if (refreshType == Constants.MODE_LOAD_MORE) {
            categoryBmobQuery.setSkip(rechargeRecordAdapter.getItemCount());// 忽略前多少条数据，查询后面的
        }
        categoryBmobQuery.findObjects(new FindListener<RechargeRecordBean>() {
            @Override
            public void done(List<RechargeRecordBean> result, BmobException e) {
                if (e == null) {
                    if (!result.isEmpty()) {
                        Collections.reverse(result);
                        if (refreshType == Constants.MODE_REFRESH) {
                            rechargeRecordBeanList.clear();
                            rechargeRecordBeanList.addAll(result);
                            if (!rechargeRecordBeanList.isEmpty()) {
                                rechargeRecordAdapter.replaceAll(rechargeRecordBeanList);
                            }
                        } else {
                            rechargeRecordBeanList.addAll(result);
                            if (!rechargeRecordBeanList.isEmpty()) {
                                rechargeRecordAdapter.addAll(result);
                            }
                        }
                    } else {
                        refreshLayout.finishLoadMoreWithNoMoreData();
                    }
                    Log.e(TAG, "done: 充值记录 查询成功" + result.size());
                } else {
                    Log.e(TAG, e.toString());
                    ToastSimple.show(e.toString());
                }

                refreshLayout.finishRefresh();
                refreshLayout.finishLoadMore();
            }
        });
    }
}
