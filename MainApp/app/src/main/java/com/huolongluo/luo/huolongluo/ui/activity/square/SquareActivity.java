package com.huolongluo.luo.huolongluo.ui.activity.square;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gyf.immersionbar.ImmersionBar;
import com.huolongluo.luo.R;
import com.huolongluo.luo.huolongluo.base.BaseActivity;
import com.huolongluo.luo.huolongluo.bean.UserBasicInfoBean;
import com.huolongluo.luo.huolongluo.constants.Constants;
import com.huolongluo.luo.huolongluo.share.Share;
import com.huolongluo.luo.huolongluo.ui.activity.userdetail.UserDetailInfoActivity;
import com.huolongluo.luo.huolongluo.ui.adapter.SquareAdapter;
import com.huolongluo.luo.superAdapter.recycler.SuperAdapter;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnLoadMoreListener;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;

/**
 * <p>
 * Created by 火龙裸 on 2017/9/6.
 */

public class SquareActivity extends BaseActivity {
    private static final String TAG = "SquareActivity";
    @BindView(R.id.toolbar_center_title)
    TextView toolbar_center_title;
    @BindView(R.id.iv_left)
    ImageView iv_left;
    @BindView(R.id.tv_right)
    TextView tv_right;
    /********************* 以上为ToolBar ************************/
    @BindView(R.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    @BindView(R.id.rv_square)
    RecyclerView rv_square;

    private long mClickTopTime;//返回顶部时间戳

    private List<UserBasicInfoBean> userBasicInfoBeanList = new ArrayList<>();
    private SquareAdapter squareAdapter;

    private String curryQuerySex;

    @Override
    protected int getContentViewId() {
        return R.layout.activity_square;
    }

    @Override
    protected void injectDagger() {
        activityComponent().inject(this);
    }

    private void initToolBar() {
        ImmersionBar.with(this).statusBarDarkFont(true).init();
        toolbar_center_title.setText("单身广场");
        tv_right.setVisibility(View.VISIBLE);
        tv_right.setText("性别切换");
        toolbar_center_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ((System.currentTimeMillis() - mClickTopTime) > 1000) {
                    mClickTopTime = System.currentTimeMillis();
                } else {
                    rv_square.smoothScrollToPosition(0);
                }
            }
        });
    }

    @Override
    protected void initViewsAndEvents(@Nullable Bundle savedInstanceState) {
        initToolBar();
        curryQuerySex = TextUtils.equals("女", Share.get().getSex()) ? "男" : "女";
        eventClick(iv_left).subscribe(o -> // 点击返回键
        {
            close();
        });
        eventClick(tv_right).subscribe(o -> // 点击性别切换
        {
            if (TextUtils.equals("男", curryQuerySex)) {
                curryQuerySex = "女";
            } else {
                curryQuerySex = "男";
            }
            querySquareUserBaseInfoList(Constants.MODE_REFRESH);
        });

//        rv_square.setNestedScrollingEnabled(false);
//        rv_square.setFocusable(false);
        rv_square.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
//        rv_square.addItemDecoration(new GridSpacingItemDecoration(3,
//                DensityUtil.dip2px(this, 10), false));
        squareAdapter = new SquareAdapter(this, userBasicInfoBeanList, R.layout.item_square);
        rv_square.setAdapter(squareAdapter);
        squareAdapter.setOnItemClickListener(new SuperAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int viewType, int position) {
                Bundle bundle = new Bundle();
                bundle.putString(Constants.USER_NAME, squareAdapter.getAllData().get(position).getUsername());
                startActivity(UserDetailInfoActivity.class, bundle);
            }
        });

        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull final RefreshLayout refreshLayout) {
                querySquareUserBaseInfoList(Constants.MODE_REFRESH);
            }
        });
        refreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout rl) {
//                mAdapter.addData(movies);
                querySquareUserBaseInfoList(Constants.MODE_LOAD_MORE);
//                rl.finishLoadMoreWithNoMoreData();
            }
        });
        querySquareUserBaseInfoList(Constants.MODE_REFRESH);
    }

    /**
     * 查询广场
     * 用户基本信息列表列表
     *
     * @param refreshType 刷新还是上拉加载
     */
    private void querySquareUserBaseInfoList(int refreshType) {
//        showProgressDialog("");
        BmobQuery<UserBasicInfoBean> categoryBmobQuery = new BmobQuery<>();
        categoryBmobQuery.addWhereEqualTo("sex", curryQuerySex);
        categoryBmobQuery.setLimit(10);//每次最多查询10条
        if (refreshType == Constants.MODE_LOAD_MORE) {
            categoryBmobQuery.setSkip(squareAdapter.getItemCount());// 忽略前多少条数据，查询后面的
        }
        categoryBmobQuery.findObjects(new FindListener<UserBasicInfoBean>() {
            @Override
            public void done(List<UserBasicInfoBean> result, BmobException e) {
                if (e == null) {
                    if (!result.isEmpty()) {
                        Collections.reverse(result);
                        List<UserBasicInfoBean> filterResult = new ArrayList<>();
                        for (int i = 0; i < result.size(); i++) {
                            if (UserBasicInfoBean.isCompleteBasicInfo(result.get(i))) {
                                filterResult.add(result.get(i));
                            }
                        }
                        if (refreshType == Constants.MODE_REFRESH) {
                            userBasicInfoBeanList.clear();
                            userBasicInfoBeanList.addAll(filterResult);
                            if (!userBasicInfoBeanList.isEmpty()) {
                                squareAdapter.replaceAll(userBasicInfoBeanList);
                            }
                        } else {
                            userBasicInfoBeanList.addAll(filterResult);
                            if (!userBasicInfoBeanList.isEmpty()) {
                                squareAdapter.addAll(filterResult);
                            }
                        }
                        Log.e(TAG, "done: 广场用户（" + curryQuerySex + "）查询成功" + result.size() + " 过滤后的为：" + filterResult.size());
                    } else {
                        refreshLayout.finishLoadMoreWithNoMoreData();
                    }
                    Log.e(TAG, "done: 广场用户（" + curryQuerySex + "）查询成功");
                } else {
                    Log.e(TAG, e.toString());
                }
//                hideProgressDialog();
                refreshLayout.finishRefresh();
                refreshLayout.finishLoadMore();
            }
        });
    }

    /**
     * 查询广场
     * 女生用户基本信息列表列表
     */
    private void querySquareWoMenUserBaseInfoList() {
        BmobQuery<UserBasicInfoBean> categoryBmobQuery = new BmobQuery<>();
        categoryBmobQuery.addWhereEqualTo("sex", "女");//女生
        categoryBmobQuery.findObjects(new FindListener<UserBasicInfoBean>() {
            @Override
            public void done(List<UserBasicInfoBean> result, BmobException e) {
                if (e == null) {
                    if (!result.isEmpty()) {
                        userBasicInfoBeanList.clear();
                        userBasicInfoBeanList.addAll(result);
//                        homeViewPager2Adapter.addAll(userBasicInfoBeanList);
                    }
                } else {
                    Log.e(TAG, e.toString());
                }
            }
        });
    }

    /**
     * 查询广场
     * 男用户基本信息列表列表
     */
    private void querySquareManUserBaseInfoList() {
        BmobQuery<UserBasicInfoBean> categoryBmobQuery = new BmobQuery<>();
        categoryBmobQuery.addWhereEqualTo("sex", "男");//男生
        categoryBmobQuery.findObjects(new FindListener<UserBasicInfoBean>() {
            @Override
            public void done(List<UserBasicInfoBean> result, BmobException e) {
                if (e == null) {
                    if (!result.isEmpty()) {
                        userBasicInfoBeanList.clear();
                        userBasicInfoBeanList.addAll(result);
//                        homeViewPager2Adapter.addAll(userBasicInfoBeanList);
                    }
                } else {
                    Log.e(TAG, e.toString());
                }
            }
        });
    }
}
