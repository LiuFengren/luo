package com.huolongluo.luo.huolongluo.ui.adapter;

import android.content.Context;

import com.huolongluo.luo.R;
import com.huolongluo.luo.huolongluo.bean.ItemBean;
import com.huolongluo.luo.superAdapter.recycler.BaseViewHolder;
import com.huolongluo.luo.superAdapter.recycler.IMultiItemViewType;
import com.huolongluo.luo.superAdapter.recycler.SuperAdapter;

import java.util.List;

/**
 * Created by Administrator on 2017/8/10 0010.
 */

public class MyAdapter extends SuperAdapter<ItemBean>
{

    /**
     * 只加载一种Item布局，就用这个构造方法
     */
    public MyAdapter(Context context, List list, int layoutResId)
    {
        super(context, list, layoutResId);
    }


    /**
     * 加载多种不同Item布局，就用这个构造方法
     */
    public MyAdapter(Context context, List list, IMultiItemViewType multiItemViewType)
    {
        super(context, list, multiItemViewType);
    }

    @Override
    public void onBind(int viewType, BaseViewHolder holder, int position, ItemBean item)
    {
        holder.setText(R.id.tv_title, item.getTitle());
        holder.setText(R.id.tv_content, item.getContent());
    }
}
