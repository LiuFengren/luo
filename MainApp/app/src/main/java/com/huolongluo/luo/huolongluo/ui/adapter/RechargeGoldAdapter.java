//package com.huolongluo.luo.huolongluo.ui.adapter;
//
//import android.content.Context;
//import android.util.Log;
//import android.view.View;
//
//import com.huolongluo.luo.R;
//import com.huolongluo.luo.huolongluo.bean.RechargeGoldListBean;
//import com.huolongluo.luo.superAdapter.recycler.BaseViewHolder;
//import com.huolongluo.luo.superAdapter.recycler.SuperAdapter;
//import com.jakewharton.rxbinding.view.RxView;
//
//import java.util.List;
//import java.util.concurrent.TimeUnit;
//
//import rx.android.schedulers.AndroidSchedulers;
//
///**
// * Created by 火龙裸 on 2018/6/10 0010.
// */
//
//public class RechargeGoldAdapter extends SuperAdapter<RechargeGoldListBean>
//{
//    public RechargeGoldAdapter(Context context, List<RechargeGoldListBean> list, int layoutResId)
//    {
//        super(context, list, layoutResId);
//    }
//
//    @Override
//    public void onBind(int viewType, BaseViewHolder holder, int position, RechargeGoldListBean item)
//    {
//        holder.setText(R.id.tv_gold_num, item.getGoldNum() <= 0 ? "0" : item.getGoldNum() + "");
//        holder.setText(R.id.tv_gift_gold_num, item.getGoldGiftNum() <= 0 ? "0" : item.getGoldGiftNum() + "");
//        holder.setText(R.id.tv_will_pay,item.getWillPay() <= 0 ? "0" : item.getWillPay() + "");
//
//        holder.setText(R.id.tv_remark, "多送" + item.getGoldGiftNum() + "金币");
//
//        if (item.isForVip()) {
//            holder.setVisibility(R.id.tv_remark_is_for_vip, View.VISIBLE);
//        } else {
//            holder.setVisibility(R.id.tv_remark_is_for_vip, View.GONE);
//        }
//
////        if (item.getGoldGiftNum() <= 0) {
////            holder.setVisibility(R.id.tv_zhuanshi_gift_num, View.INVISIBLE);
////        } else {
////            holder.setText(R.id.tv_zhuanshi_gift_num,"多赠" + item.getGoldGiftNum() + "金币");
////            holder.setVisibility(R.id.tv_zhuanshi_gift_num, View.VISIBLE);
////        }
//
//
//        // 点击item
////        RxView.clicks(holder.getItemView()).throttleFirst(500, TimeUnit.MILLISECONDS).observeOn(AndroidSchedulers.mainThread()).subscribe(v ->
////        {
////            Log.e("TAG", "onBind: 点击了：" + position + "  共：" + getItemCount());
////        });
//
//        // 点击任务弹出窗体
////        RxView.clicks(holder.getView(R.id.tv_task_name)).throttleFirst(500, TimeUnit.MILLISECONDS).observeOn(AndroidSchedulers.mainThread()).subscribe(v ->
////        {
////            EventBus.getDefault().post(new Event.Task(item.getContent()+""));
////        });
////        // 点击 提交 任务
////        RxView.clicks(holder.getView(R.id.tv_submit01)).throttleFirst(500, TimeUnit.MILLISECONDS).observeOn(AndroidSchedulers.mainThread()).subscribe(v ->
////        {
////            Intent intent = new Intent(mContext, SubMitTaskActivity.class);
////            intent.putExtra("tId", item.getId()+"");
////            intent.putExtra("position", position);
////            intent.putExtra("title", item.getContent());
////            mContext.startActivity(intent);
////        });
//
//    }
//}
