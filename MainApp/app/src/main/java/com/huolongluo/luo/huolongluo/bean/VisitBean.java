package com.huolongluo.luo.huolongluo.bean;

import cn.bmob.v3.BmobObject;

public class VisitBean extends BmobObject {
    private String username;//用户名
    private String nickName;//昵称
    private String headPic;//用户头像
    private String birtyday;//生日
    private String sex;//性别
    private String address;//现居
    private String visitToUserName;//访问了谁
    private boolean isUnLock;//是否解锁了该用户

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getHeadPic() {
        return headPic;
    }

    public void setHeadPic(String headPic) {
        this.headPic = headPic;
    }

    public String getBirtyday() {
        return birtyday;
    }

    public void setBirtyday(String birtyday) {
        this.birtyday = birtyday;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getVisitToUserName() {
        return visitToUserName;
    }

    public void setVisitToUserName(String visitToUserName) {
        this.visitToUserName = visitToUserName;
    }

    public boolean isUnLock() {
        return isUnLock;
    }

    public void setUnLock(boolean unLock) {
        isUnLock = unLock;
    }
}
