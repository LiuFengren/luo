package com.huolongluo.luo.huolongluo.bean;

import cn.bmob.v3.BmobObject;

public class VipPackageBean extends BmobObject {
    private String packageName;//套餐名称
    private double giftGold;//此套餐送多少金币
    private double price;//价格
    private String remark;//备注
    private boolean isChecked;//是否被选中

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public double getGiftGold() {
        return giftGold;
    }

    public void setGiftGold(double giftGold) {
        this.giftGold = giftGold;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
