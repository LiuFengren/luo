package com.huolongluo.luo.huolongluo.bean;

import cn.bmob.v3.BmobObject;

/**
 * 充值套餐
 * */
public class GoodsBean extends BmobObject {
    private double willPay;//价格
    private double goldNum;//购买的金币数量
    private double goldGiftNum;//获得赠送的金币数量
    private boolean checked;//是否被选中

    public double getWillPay() {
        return willPay;
    }

    public void setWillPay(double willPay) {
        this.willPay = willPay;
    }

    public double getGoldNum() {
        return goldNum;
    }

    public void setGoldNum(double goldNum) {
        this.goldNum = goldNum;
    }

    public double getGoldGiftNum() {
        return goldGiftNum;
    }

    public void setGoldGiftNum(double goldGiftNum) {
        this.goldGiftNum = goldGiftNum;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}
