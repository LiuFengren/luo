package com.huolongluo.luo.huolongluo.bean;

import cn.bmob.v3.BmobObject;

public class TiXianRecordBean extends BmobObject {
    private String username;//用户名
    private String jifenBalance;//提现后的积分余额
    private String tixianJiFen;//提现积分
    private String tiXianAccount;//提现账户名
    private int tixianState;//提现状态 0待审核，1审核通过（同时放款），-1审核未通过

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getJifenBalance() {
        return jifenBalance;
    }

    public void setJifenBalance(String jifenBalance) {
        this.jifenBalance = jifenBalance;
    }

    public String getTixianJiFen() {
        return tixianJiFen;
    }

    public void setTixianJiFen(String tixianJiFen) {
        this.tixianJiFen = tixianJiFen;
    }

    public String getTiXianAccount() {
        return tiXianAccount;
    }

    public void setTiXianAccount(String tiXianAccount) {
        this.tiXianAccount = tiXianAccount;
    }

    public int getTixianState() {
        return tixianState;
    }

    public void setTixianState(int tixianState) {
        this.tixianState = tixianState;
    }


}
