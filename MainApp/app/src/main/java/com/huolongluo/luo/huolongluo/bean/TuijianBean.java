package com.huolongluo.luo.huolongluo.bean;

import cn.bmob.v3.BmobObject;

public class TuijianBean extends BmobObject {
    private String userHeader;
    private String userNike;

    public String getUserHeader() {
        return userHeader;
    }

    public void setUserHeader(String userHeader) {
        this.userHeader = userHeader;
    }

    public String getUserNike() {
        return userNike;
    }

    public void setUserNike(String userNike) {
        this.userNike = userNike;
    }
}
