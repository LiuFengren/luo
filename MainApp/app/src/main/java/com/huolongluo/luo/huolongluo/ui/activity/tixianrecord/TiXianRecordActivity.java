package com.huolongluo.luo.huolongluo.ui.activity.tixianrecord;

import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gyf.immersionbar.ImmersionBar;
import com.huolongluo.luo.R;
import com.huolongluo.luo.huolongluo.base.BaseActivity;
import com.huolongluo.luo.huolongluo.bean.TiXianRecordBean;
import com.huolongluo.luo.huolongluo.constants.Constants;
import com.huolongluo.luo.huolongluo.share.Share;
import com.huolongluo.luo.huolongluo.ui.adapter.TiXianRecordAdapter;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnLoadMoreListener;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;

/**
 * <p>
 * Created by 火龙裸 on 2017/9/6.
 */

public class TiXianRecordActivity extends BaseActivity {
    private static final String TAG = "TiXianRecordActivity";
    @BindView(R.id.toolbar_center_title)
    TextView toolbar_center_title;
    @BindView(R.id.iv_left)
    ImageView iv_left;
    /********************* 以上为ToolBar ************************/
    @BindView(R.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    @BindView(R.id.rv_tixian)
    RecyclerView rv_tixian;

    private List<TiXianRecordBean> tiXianRecordBeanList = new ArrayList<>();
    private TiXianRecordAdapter tiXianRecordAdapter;

    @Override
    protected int getContentViewId() {
        return R.layout.activity_tixian_detail;
    }

    @Override
    protected void injectDagger() {
        activityComponent().inject(this);
    }

    private void initToolBar() {
        ImmersionBar.with(this).statusBarDarkFont(true).init();
        toolbar_center_title.setText("提现明细");
    }

    @Override
    protected void initViewsAndEvents(@Nullable Bundle savedInstanceState) {
        initToolBar();
        eventClick(iv_left).subscribe(o -> // 点击返回键
        {
            close();
        });

        rv_tixian.setNestedScrollingEnabled(false);
        rv_tixian.setFocusable(false);
        rv_tixian.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
//        rv_tixian.addItemDecoration(new GridSpacingItemDecoration(3,
//                DensityUtil.dip2px(this, 10), false));
        tiXianRecordAdapter = new TiXianRecordAdapter(this, tiXianRecordBeanList, R.layout.item_tixian_record);
        rv_tixian.setAdapter(tiXianRecordAdapter);

        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull final RefreshLayout refreshLayout) {
                queryTiXianRecord(Constants.MODE_REFRESH);
            }
        });
        refreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout rl) {
//                mAdapter.addData(movies);
                queryTiXianRecord(Constants.MODE_LOAD_MORE);
//                rl.finishLoadMoreWithNoMoreData();
            }
        });

        queryTiXianRecord(Constants.MODE_REFRESH);
    }

    /**
     * 查询谁访问了我
     *
     * @param refreshType 刷新还是上拉加载
     */
    private void queryTiXianRecord(int refreshType) {
        BmobQuery<TiXianRecordBean> categoryBmobQuery = new BmobQuery<>();
        categoryBmobQuery.addWhereEqualTo("username", Share.get().getUserName());
        categoryBmobQuery.setLimit(10);//每次最多查询10条
        if (refreshType == Constants.MODE_LOAD_MORE) {
            categoryBmobQuery.setSkip(tiXianRecordAdapter.getItemCount());// 忽略前多少条数据，查询后面的
        }
        categoryBmobQuery.findObjects(new FindListener<TiXianRecordBean>() {
            @Override
            public void done(List<TiXianRecordBean> result, BmobException e) {
                if (e == null) {
                    if (!result.isEmpty()) {
                        Collections.reverse(result);
                        if (refreshType == Constants.MODE_REFRESH) {
                            tiXianRecordBeanList.clear();
                            tiXianRecordBeanList.addAll(result);
                            if (!tiXianRecordBeanList.isEmpty()) {
                                tiXianRecordAdapter.replaceAll(tiXianRecordBeanList);
                            }
                        } else {
                            tiXianRecordBeanList.addAll(result);
                            if (!tiXianRecordBeanList.isEmpty()) {
                                tiXianRecordAdapter.addAll(result);
                            }
                        }
                    } else {
                        refreshLayout.finishLoadMoreWithNoMoreData();
                    }
                    Log.e(TAG, "done: 提现记录 查询成功" + result.size());
                } else {
                    Log.e(TAG, e.toString());
                }
                refreshLayout.finishRefresh();
                refreshLayout.finishLoadMore();
            }
        });
    }
}
