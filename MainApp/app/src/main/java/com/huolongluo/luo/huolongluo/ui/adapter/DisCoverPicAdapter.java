package com.huolongluo.luo.huolongluo.ui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.View;

import com.blankj.utilcode.util.ConvertUtils;
import com.huolongluo.luo.R;
import com.huolongluo.luo.superAdapter.recycler.BaseViewHolder;
import com.huolongluo.luo.superAdapter.recycler.IMultiItemViewType;
import com.huolongluo.luo.superAdapter.recycler.SuperAdapter;
import com.jakewharton.rxbinding.view.RxView;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.ImageViewerPopupView;
import com.lxj.xpopup.interfaces.OnSrcViewUpdateListener;
import com.lxj.xpopup.util.SmartGlideImageLoader;
import com.tencent.liteav.basic.ImageLoader;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by 火龙裸 on 2018/6/10 0010.
 */

public class DisCoverPicAdapter extends SuperAdapter<String> {

    /**
     * 单图片布局
     */
    public static final int TYPE_PIC_SINGLE = 1;
    /**
     * 多图片布局
     */
    public static final int TYPE_PIC_MULTIPLE = 2;

    public DisCoverPicAdapter(Context context, List<String> list, IMultiItemViewType<String> multiItemViewType) {
        super(context, list, multiItemViewType);
    }

    @Override
    public void onBind(int viewType, BaseViewHolder holder, int position, String item) {
        if (viewType == TYPE_PIC_SINGLE) {
            ImageLoader.loadImage(mContext, holder.getView(R.id.iv_user_pic_single), item);
            if ((position + 1) % 2 == 0) {
                holder.setVisibility(R.id.view_space, View.INVISIBLE);
            } else {
                holder.setVisibility(R.id.view_space, View.VISIBLE);
            }
        } else {
            ImageLoader.loadImage(mContext, holder.getView(R.id.iv_user_pic_multiple), item);
            if ((position + 1) % 3 == 0) {
                holder.setVisibility(R.id.view_space, View.INVISIBLE);
            } else {
                holder.setVisibility(R.id.view_space, View.VISIBLE);
            }
        }


        ArrayList<Object> list = new ArrayList<>();
        for (int i = 0; i < mList.size(); i++) {
            list.add(mList.get(i));
        }
        // 点击item
        RxView.clicks(holder.getItemView()).throttleFirst(500, TimeUnit.MILLISECONDS).observeOn(AndroidSchedulers.mainThread()).subscribe(v ->
        {
            new XPopup.Builder(mContext)
//                            .animationDuration(1000)
                    .isTouchThrough(true)
                    .asImageViewer(holder.getView(R.id.iv_userinfo_card_pic), position, list,
                            false, true, -1, -1, ConvertUtils.dp2px(10), true,
                            Color.rgb(32, 36, 46),
                            new OnSrcViewUpdateListener() {
                                @Override
                                public void onSrcViewUpdate(ImageViewerPopupView popupView, int position) {
//                                    RecyclerView rv = (RecyclerView) holder.itemView.getParent();
//                                    popupView.updateSrcView((ImageView) rv.getChildAt(position));
                                }
                            }, new SmartGlideImageLoader(true, R.mipmap.ic_launcher), null)
                    .show();
        });
    }
//    public DisCoverAdapter(Context context, List<String> list, int layoutResId) {
//        super(context, list, layoutResId);
//    }

    /*@Override
    public void onBind(int viewType, BaseViewHolder holder, int position, String item) {
//        AboutMeBean aboutMeBean = item.getAboutMe();
//
//        if (aboutMeBean != null && !aboutMeBean.getUserTopPic().isEmpty()) {
//            ImageLoader.loadImage(mContext, holder.getView(R.id.iv_user_pic), aboutMeBean.getUserTopPic().get(0));
////            Glide.with(mContext).load(item.getUserPic()).error(loadTransform(context, errorResId, radius)).into(imageView);
//        }

//        if (item.getUserTopPic() != null && !item.getUserTopPic().isEmpty()) {
//            ImageLoader.loadImage(mContext, holder.getView(R.id.iv_user_pic), item.getUserTopPic().get(0));
//        }
//
//        holder.setText(R.id.tv_nick_name, item.getNickName());
//        holder.setText(R.id.tv_address, item.getAddress());
//        holder.setText(R.id.tv_zuji, item.getZujiAddress());
//        if (!TextUtils.isEmpty(item.getBirthday())) {
//            int yearIndex = item.getBirthday().indexOf("-");
//            String year = "";
//            if (yearIndex != -1) {
//                year = item.getBirthday().substring(0, yearIndex);
//            }
//            holder.setText(R.id.tv_old, year + "年");
//        }
//        holder.setText(R.id.tv_height, item.getHeight() + "cm");
//        holder.setText(R.id.tv_xueli, item.getXueli());
//        holder.setText(R.id.tv_work, item.getWork());
//        if (item.getGiftGold() <= 0) {
//            holder.setVisibility(R.id.tv_vip_gift_num, View.INVISIBLE);
//        } else {
//            holder.setText(R.id.tv_vip_gift_num,"送" + item.getGiftGold() + "金币");
//            holder.setVisibility(R.id.tv_vip_gift_num, View.VISIBLE);
//        }
//
//        if (item.isChecked()) {
//            holder.getView(R.id.ll_chongzhi_type).setBackgroundResource(R.mipmap.icon_charge_item_selected);
//        } else {
//            holder.getView(R.id.ll_chongzhi_type).setBackgroundResource(0);
//        }
//
//        // 点击item
//        RxView.clicks(holder.getItemView()).throttleFirst(500, TimeUnit.MILLISECONDS).observeOn(AndroidSchedulers.mainThread()).subscribe(v ->
//        {
//            Log.e("TAG", "onBind: 点击了：" + position + "  共：" + getItemCount());
//
//            for (int i = 0; i < getItemCount(); i++) {
//                getAllData().get(i).setChecked(false);
//            }
//            item.setChecked(true);
//            for (int i = 0; i < getItemCount(); i++) {
//                Log.e("TAG", "onBind: 最终的选中情况：" + item.isChecked());
//            }
//            notifyDataSetChanged();
//        });

        // 点击任务弹出窗体
//        RxView.clicks(holder.getView(R.id.tv_task_name)).throttleFirst(500, TimeUnit.MILLISECONDS).observeOn(AndroidSchedulers.mainThread()).subscribe(v ->
//        {
//            EventBus.getDefault().post(new Event.Task(item.getContent()+""));
//        });
//        // 点击 提交 任务
//        RxView.clicks(holder.getView(R.id.tv_submit01)).throttleFirst(500, TimeUnit.MILLISECONDS).observeOn(AndroidSchedulers.mainThread()).subscribe(v ->
//        {
//            Intent intent = new Intent(mContext, SubMitTaskActivity.class);
//            intent.putExtra("tId", item.getId()+"");
//            intent.putExtra("position", position);
//            intent.putExtra("title", item.getContent());
//            mContext.startActivity(intent);
//        });

    }*/
}
