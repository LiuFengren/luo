package com.huolongluo.luo.huolongluo.bean;

import java.util.List;

import cn.bmob.v3.BmobObject;

public class DisCoverBean extends BmobObject {
    private String username;//用户名
    private String nickName;//昵称
    private String sex;//性别
    private String old;//年龄
    private String userHeaderImg;//头像
    private String dongtaiText;//动态文字
    private List<String> dongtaiPic;//动态携带的图片

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getOld() {
        return old;
    }

    public void setOld(String old) {
        this.old = old;
    }

    public String getUserHeaderImg() {
        return userHeaderImg;
    }

    public void setUserHeaderImg(String userHeaderImg) {
        this.userHeaderImg = userHeaderImg;
    }

    public String getDongtaiText() {
        return dongtaiText;
    }

    public void setDongtaiText(String dongtaiText) {
        this.dongtaiText = dongtaiText;
    }

    public List<String> getDongtaiPic() {
        return dongtaiPic;
    }

    public void setDongtaiPic(List<String> dongtaiPic) {
        this.dongtaiPic = dongtaiPic;
    }

    @Override
    public String toString() {
        return "DisCoverBean{" +
                "username='" + username + '\'' +
                ", nickName='" + nickName + '\'' +
                ", sex='" + sex + '\'' +
                ", old='" + old + '\'' +
                ", userHeaderImg='" + userHeaderImg + '\'' +
                ", dongtaiText='" + dongtaiText + '\'' +
                ", dongtaiPic=" + dongtaiPic +
                '}';
    }
}
