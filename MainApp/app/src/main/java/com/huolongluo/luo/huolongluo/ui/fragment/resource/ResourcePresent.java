package com.huolongluo.luo.huolongluo.ui.fragment.resource;

import android.content.Context;

import com.huolongluo.luo.base.ActivityContext;
import com.huolongluo.luo.huolongluo.net.okhttp.Api;

import javax.inject.Inject;

/**
 * Created by 火龙裸 on 2018/5/26 0026.
 */

public class ResourcePresent implements ResourceContract.Presenter
{
    private ResourceContract.View mView;
    private Context mContext;

    @Inject
    Api api;

    @Inject
    public ResourcePresent(@ActivityContext Context mContext)
    {
        this.mContext = mContext;
    }

    @Override
    public void attachView(ResourceContract.View view)
    {
        mView = view;
    }

    @Override
    public void detachView()
    {
        mView = null;
    }

//    @Override
//    public Subscription getUserOtherInfo(String uId, String isErp)
//    {
//        return api.getUserOtherInfo(uId, isErp, response -> mView.getUserOtherInfoSucce(response));
//    }
//
//    @Override
//    public Subscription getJuniorId(String uId)
//    {
//        return api.getJuniorId(uId, response -> mView.getJuniorIdSuccess(response));
//    }
//
//    @Override
//    public Subscription getUnreadCount(String uId, String isErp)
//    {
//        return api.getUnreadCount(uId, isErp, response -> mView.getUnreadCountSucce(response));
//    }
//
//    @Override
//    public Subscription loginEerp(String member_id, String ticket, String password, String type)
//    {
//        return api.loginEerp(member_id, ticket, password, type, new HttpOnNextListener2_2<LoginErpBean>()
//        {
//            @Override
//            public void onNext(LoginErpBean result)
//            {
//                mView.loginEerpSucce(result);
//            }
//        });
//    }
}
