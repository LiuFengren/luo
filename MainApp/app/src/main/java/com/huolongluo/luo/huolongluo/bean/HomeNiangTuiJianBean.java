package com.huolongluo.luo.huolongluo.bean;

import cn.bmob.v3.BmobObject;

public class HomeNiangTuiJianBean extends BmobObject {
    private String userId;//用户ID
    private String pic;//照片
    private String nickName;//昵称
    private int birthYear;//出生年份
    private String sex;//性别
    private String address;//住址
    private String zujiAddress;//祖籍地址
    private String xueli;//学历
    private String work;//工作

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public int getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(int birthYear) {
        this.birthYear = birthYear;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZujiAddress() {
        return zujiAddress;
    }

    public void setZujiAddress(String zujiAddress) {
        this.zujiAddress = zujiAddress;
    }

    public String getXueli() {
        return xueli;
    }

    public void setXueli(String xueli) {
        this.xueli = xueli;
    }

    public String getWork() {
        return work;
    }

    public void setWork(String work) {
        this.work = work;
    }
}
