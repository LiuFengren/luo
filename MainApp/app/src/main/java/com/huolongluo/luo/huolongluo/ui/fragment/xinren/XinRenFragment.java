package com.huolongluo.luo.huolongluo.ui.fragment.xinren;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.huolongluo.luo.R;
import com.huolongluo.luo.huolongluo.base.BaseActivity;
import com.huolongluo.luo.huolongluo.base.BaseFragment;
import com.huolongluo.luo.huolongluo.bean.TuijianBean;
import com.huolongluo.luo.huolongluo.ui.adapter.HomeNewUserAdapter;
import com.huolongluo.luo.util.ImmersedStatusbarUtils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

public class XinRenFragment extends BaseFragment implements XinRenContract.View {
    private static final String TAG = "XinRenFragment";
    @Inject
    XinRenPresent present;
//    @BindView(R.id.banner)
//    Banner banner;
//    @BindView(R.id.rv_guess_you_like)
//    RecyclerView rv_guess_you_like;
    @BindView(R.id.rv_xinren)
    RecyclerView rv_xinren;

    private HomeNewUserAdapter homeNewUserAdapter;
    private List<TuijianBean> xinRenBeanList = new ArrayList<>();//首页推荐的RecyclerView有几条Item

    public static XinRenFragment getInstance() {
        Bundle args = new Bundle();
        XinRenFragment fragment = new XinRenFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void initDagger() {
        ((BaseActivity) getActivity()).activityComponent().inject(this);
        present.attachView(this);
    }

//    private void initToolBar() {
//        int statusbarHeight = ImmersedStatusbarUtils.getStatusBarHeight(getActivity());
//        lin1.setPadding(0, statusbarHeight, 0, 0);
//    }

    @Override
    protected int getContentViewId() {
        return R.layout.fragment_home_xinren;
    }

    @Override
    protected void initViewsAndEvents(View rootView) {
//        initToolBar();
//        subscription = present.getGuessLiskList();
        rv_xinren.setNestedScrollingEnabled(false);
        rv_xinren.setFocusable(false);
        rv_xinren.setLayoutManager(new GridLayoutManager(getActivity(), 2) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        });
        //rv_sucai.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false));
        homeNewUserAdapter = new HomeNewUserAdapter(getActivity(), xinRenBeanList, R.layout.item_home_tuijian);
        rv_xinren.setAdapter(homeNewUserAdapter);
    }

//    @Override
//    public void getGuessLiskListSucce(GuessYouLikeBean result) {
//        Log.e(TAG, "getGuessLiskListSucce: 获取猜你喜欢列表成功：" + result.getUsers().size());
//    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        present.detachView();
    }
}
