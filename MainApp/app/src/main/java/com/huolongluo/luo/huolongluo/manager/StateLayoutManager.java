package com.huolongluo.luo.huolongluo.manager;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Looper;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.huolongluo.luo.R;
import com.huolongluo.luo.huolongluo.net.okhttp.HttpUtils;

/**
 * Create By 火龙裸
 */
public class StateLayoutManager extends FrameLayout {

    enum State {
        STATE_EMPTY,
        STATE_EMPTY_CLICK,
        STATE_LOADING,
        STATE_ERROR,
        STATE_NO_NET,
        STATE_CONTENT,
    }

    private View mEmptyView;
    private View mEmptyClickView;
    private View mLoadingView;
    private View mErrorView;
    private View mNoNetView;
    private View mContentView;

    public StateLayoutManager(@NonNull Context context) {
        this(context, null);
    }

    public StateLayoutManager(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    private void changeView(State state) {
        setViewGone();
        switch (state) {
            case STATE_LOADING:
                mLoadingView.setVisibility(VISIBLE);
                break;
            case STATE_EMPTY:
                mEmptyView.setVisibility(VISIBLE);
                break;
            case STATE_EMPTY_CLICK:
                mEmptyClickView.setVisibility(VISIBLE);
                break;
            case STATE_ERROR:
                mErrorView.setVisibility(VISIBLE);
                break;
            case STATE_NO_NET:
                mNoNetView.setVisibility(VISIBLE);
                break;
            case STATE_CONTENT:
                mContentView.setVisibility(VISIBLE);
                break;
        }
    }

    private void showView(final State state) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            changeView(state);
        } else {
            post(new Runnable() {
                @Override
                public void run() {
                    changeView(state);
                }
            });
        }
    }

    /**
     * 无数据
     */
    public void showEmpty() {
        showView(State.STATE_EMPTY);
    }

    /**
     * 无数据可以点击
     */
    public void showEmptyClick(final OnClickListener listener) {
        showView(State.STATE_EMPTY_CLICK);
        mEmptyClickView.findViewById(R.id.tv_retry).setOnClickListener(listener);
    }

    /**
     * 加载中
     */
    public void showLoading() {
        showView(State.STATE_LOADING);
    }

    /**
     * 显示正常
     */
    public void showContent() {
        showView(State.STATE_CONTENT);
    }

    /**
     * 展示错误布局
     */
    public void showError(final OnClickListener listener) {
        boolean isAvailable = HttpUtils.isNetworkConnected(getContext());
        if (isAvailable) { //有网络出错
            showView(State.STATE_ERROR);
            mErrorView.findViewById(R.id.tv_retry).setOnClickListener(listener);
        } else { //无网络出错
            showView(State.STATE_NO_NET);
            mNoNetView.findViewById(R.id.tv_no_net_retry).setOnClickListener(listener);
        }
    }

    /**
     * 加载中布局文案
     *
     * @param text
     */
    public void loadingText(String text) {
        View view = mLoadingView.findViewById(R.id.tv_loading);
        if (view != null) {
            TextView tv = (TextView) view;
            tv.setText(text);
        }
    }

    /**
     * 空布局文案
     *
     * @param text
     */
    public void emptyText(String text) {
        View view = mEmptyView.findViewById(R.id.tv_empty);
        if (view != null) {
            TextView tv = (TextView) view;
            tv.setText(text);
        }
    }

    /**
     * 空布局图案
     *
     * @param resId
     */
    public void emptyIv(int resId) {
        View view = mEmptyView.findViewById(R.id.iv_empty);
        if (view != null) {
            ImageView iv = (ImageView) view;
            iv.setImageResource(resId);
        }
    }

    /**
     * 加载错误文案
     *
     * @param text
     */
    public void errorText(String text) {
        View view = mErrorView.findViewById(R.id.tv_error);
        if (view != null) {
            TextView tv = view.findViewById(R.id.tv_error);
            tv.setText(text);
        }
    }

    private void setViewGone() {
        mLoadingView.setVisibility(GONE);
        mEmptyView.setVisibility(GONE);
        mErrorView.setVisibility(GONE);
        mNoNetView.setVisibility(GONE);
        mContentView.setVisibility(GONE);
        mEmptyClickView.setVisibility(GONE);
    }

    public static class Builder {
        private Context mContext;
        private LayoutInflater mInflater;
        private StateLayoutManager mStatusLayout;

        public Builder(Context context) {
            mContext = context;
            mStatusLayout = new StateLayoutManager(mContext);
            mInflater = LayoutInflater.from(mContext);
        }

        /**
         * 默认状态布局
         */
        private void initDefault() {
            if (mStatusLayout.mEmptyView == null) {//设置空布局
                mStatusLayout.mEmptyView = mInflater.inflate(R.layout.layout_empty, mStatusLayout, false);
                mStatusLayout.mEmptyView.setVisibility(GONE);
                mStatusLayout.addView(mStatusLayout.mEmptyView);
            }
            if (mStatusLayout.mLoadingView == null) { //设置等待布局
                mStatusLayout.mLoadingView = mInflater.inflate(R.layout.layout_loading, mStatusLayout, false);
                mStatusLayout.mLoadingView.setVisibility(GONE);
                mStatusLayout.addView(mStatusLayout.mLoadingView);
            }
            if (mStatusLayout.mErrorView == null) { //设置错误布局
                mStatusLayout.mErrorView = mInflater.inflate(R.layout.layout_error, mStatusLayout, false);
                mStatusLayout.mErrorView.setVisibility(GONE);
                mStatusLayout.addView(mStatusLayout.mErrorView);
            }
            if (mStatusLayout.mNoNetView == null) { //无网络布局
                mStatusLayout.mNoNetView = mInflater.inflate(R.layout.layout_no_net, mStatusLayout, false);
                mStatusLayout.mNoNetView.setVisibility(GONE);
                mStatusLayout.addView(mStatusLayout.mNoNetView);
            }
            if (mStatusLayout.mEmptyClickView == null) { //无数据可点击
                mStatusLayout.mEmptyClickView = mInflater.inflate(R.layout.layout_empty_to_click, mStatusLayout, false);
                mStatusLayout.mEmptyClickView.setVisibility(GONE);
                mStatusLayout.addView(mStatusLayout.mEmptyClickView);
            }
        }

        public Builder initPage(Object targetView) {
            ViewGroup content = null;
            if (targetView instanceof Activity) {
                mContext = (Context) targetView;
                content = ((Activity) targetView).findViewById(android.R.id.content);
            } else if (targetView instanceof Fragment) {
                mContext = ((Fragment) targetView).getActivity();
                content = (ViewGroup) ((Fragment) targetView).getView().getParent();
            } else if (targetView instanceof View) {
                mContext = ((View) targetView).getContext();
                content = (ViewGroup) ((View) targetView).getParent();
            }
            if (content != null) {
                int index = 0;
                View contentView;
                if (targetView instanceof View) {
                    contentView = (View) targetView;
                    index = content.indexOfChild(contentView);
                } else {
                    contentView = content.getChildAt(0);
                }
                mStatusLayout.mContentView = contentView;
                mStatusLayout.removeAllViews();
                content.removeView(contentView);

                ViewGroup.LayoutParams params = contentView.getLayoutParams();
                mStatusLayout.addView(contentView);
                content.addView(mStatusLayout, index, params);
                initDefault();
            }
            return this;
        }

        public StateLayoutManager create() {
            return mStatusLayout;
        }

    }

}
