package com.huolongluo.luo.util;

import android.text.TextUtils;
import android.util.Log;

import com.huolongluo.luo.huolongluo.share.Share;
import com.tencent.imsdk.v2.V2TIMCallback;
import com.tencent.imsdk.v2.V2TIMManager;
import com.tencent.imsdk.v2.V2TIMUserFullInfo;
import com.tencent.qcloud.tuicore.util.ErrorMessageConverter;
import com.tencent.qcloud.tuicore.util.ToastUtil;

public class TUIKitUtil {
    private static final String TAG = "TUIKitUtil";
    /**
     * 修改用户信息 头像、昵称之类的
     */
    public static void updateProfile(String headPic, String nickName) {
        V2TIMUserFullInfo v2TIMUserFullInfo = new V2TIMUserFullInfo();
        // 头像
        if (!TextUtils.isEmpty(headPic)) {
            v2TIMUserFullInfo.setFaceUrl(headPic);
//            UserInfo.getInstance().setAvatar(faceUrl);
        }

        // 昵称
        if (!TextUtils.isEmpty(nickName)) {
            v2TIMUserFullInfo.setNickname(TextUtils.isEmpty(Share.get().getNickName()) ? Share.get().getUserName() : Share.get().getNickName());
        }
//        UserInfo.getInstance().setName(nickName);
//
//        // 生日
//        if (birthday != 0) {
//            v2TIMUserFullInfo.setBirthday(birthday);
//        }
//
//        // 个性签名
//        v2TIMUserFullInfo.setSelfSignature(signature);
//
        // 性别
//        v2TIMUserFullInfo.setGender(gender);

        V2TIMManager.getInstance().setSelfInfo(v2TIMUserFullInfo, new V2TIMCallback() {
            @Override
            public void onError(int code, String desc) {
                Log.e(TAG, "modifySelfProfile err code = " + code + ", desc = " + ErrorMessageConverter.convertIMError(code, desc));
//                ToastUtil.toastShortMessage("Error code = " + code + ", desc = " + ErrorMessageConverter.convertIMError(code, desc));
            }

            @Override
            public void onSuccess() {
                Log.i("TAG", "modifySelfProfile success");
            }
        });
    }
}
