package com.huolongluo.luo.huolongluo.injection.component;

import android.app.Application;
import android.content.Context;

import com.huolongluo.luo.base.ApplicationContext;
import com.huolongluo.luo.huolongluo.base.BaseApp;
import com.huolongluo.luo.huolongluo.injection.model.ApiModule;
import com.huolongluo.luo.huolongluo.injection.model.ApplicationModule;
import com.huolongluo.luo.huolongluo.net.okhttp.OkHttpHelper;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Component;
import okhttp3.OkHttpClient;

/**
 * <p>
 * Created by 火龙裸 on 2020/3/12 0011.
 */

@Singleton
@Component(modules = {ApplicationModule.class, ApiModule.class})
public interface ApplicationComponent
{
    void inject(BaseApp application);

    @ApplicationContext
    Context context();

    Application application();

    @Named("api")
    OkHttpClient getOkHttpClient();

    @Named("apiCache")
    OkHttpClient getOkHttpClientCache();

    OkHttpHelper getOkHttpHelper();
}
