package com.huolongluo.luo.huolongluo.net.okhttp;

/**
 * Created by 火龙裸 on 2020/3/12 0011.
 */

public interface HttpOnNextListener<T>
{
    void onNext(T response);
}

