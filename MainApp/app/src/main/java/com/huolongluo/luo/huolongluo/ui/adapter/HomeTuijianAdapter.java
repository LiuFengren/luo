package com.huolongluo.luo.huolongluo.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.huolongluo.luo.R;
import com.huolongluo.luo.huolongluo.bean.TuijianBean;
import com.huolongluo.luo.superAdapter.recycler.BaseViewHolder;
import com.huolongluo.luo.superAdapter.recycler.SuperAdapter;
import com.jakewharton.rxbinding.view.RxView;

import java.util.List;
import java.util.concurrent.TimeUnit;

import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by 火龙裸 on 2018/6/10 0010.
 */

public class HomeTuijianAdapter extends SuperAdapter<TuijianBean>
{
    public HomeTuijianAdapter(Context context, List<TuijianBean> list, int layoutResId)
    {
        super(context, list, layoutResId);
    }

    @Override
    public void onBind(int viewType, BaseViewHolder holder, int position, TuijianBean item)
    {
//        WindowManager wm=(WindowManager)mContext.getSystemService(Context.WINDOW_SERVICE);
//        int i=(wm.getDefaultDisplay().getWidth()/2)- ScreenUtils.dp2px(mContext,10);
//        holder.getView(R.id.iv_shipin01).setLayoutParams(new RelativeLayout.LayoutParams(i,i));
//        if (!TextUtils.isEmpty(item.getPicture()))
//        {
//            String[] pictures = item.getPicture().split(",");
////            ImageLoadUtils.showImage(mContext, pictures[0], holder.getView(R.id.iv_shipin01));
//            Glide.with(mContext).load(pictures[0]).into((ImageView) holder.getView(R.id.iv_shipin01));
//        }
//        L.e("============视频地址=====首页=======" + item.getVideo());

//        holder.setText(R.id.tv_shipin_time01, DateUtils.format(item.getCreateTime(), "yyyy-MM-dd"));
        Glide.with(mContext).load(item.getUserHeader()).into((ImageView) holder.getView(R.id.iv_header));
        holder.setText(R.id.iv_header, item.getUserNike());
        // 点击item
        RxView.clicks(holder.getItemView()).throttleFirst(500, TimeUnit.MILLISECONDS).observeOn(AndroidSchedulers.mainThread()).subscribe(v ->
        {
            //播放视频
//            Intent intent = new Intent(mContext, DetailVideoActivity.class);
//            Bundle bundle = new Bundle();
//            bundle.putString(Constants.VIDEO_PATH, item.getVideo());
//            bundle.putSerializable("bean",item);
//            intent.putExtra(Constants.BUNDLE, bundle);
//            mContext.startActivity(intent);
        });
    }
}
