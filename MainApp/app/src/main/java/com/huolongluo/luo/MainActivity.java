package com.huolongluo.luo;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.widget.ViewPager2;

import com.blankj.utilcode.util.AppUtils;
import com.gyf.immersionbar.ImmersionBar;
import com.huolongluo.luo.huolongluo.base.BaseActivity;
import com.huolongluo.luo.huolongluo.bean.ConsumeBean;
import com.huolongluo.luo.huolongluo.bean.RechargeRecordBean;
import com.huolongluo.luo.huolongluo.bean.UserBasicInfoBean;
import com.huolongluo.luo.huolongluo.bean.UserWalletBean;
import com.huolongluo.luo.huolongluo.bean.VersionUpdateBean;
import com.huolongluo.luo.huolongluo.manager.DialogClickListener;
import com.huolongluo.luo.huolongluo.share.Event;
import com.huolongluo.luo.huolongluo.share.Share;
import com.huolongluo.luo.huolongluo.ui.activity.account.MyAccountActivity;
import com.huolongluo.luo.huolongluo.ui.activity.friendcontacts.FriendContactsActivity;
import com.huolongluo.luo.huolongluo.ui.fragment.discover.DiscoverFragment;
import com.huolongluo.luo.huolongluo.ui.fragment.home.HomeFragment;
import com.huolongluo.luo.huolongluo.ui.fragment.mine.MineFragment;
import com.huolongluo.luo.manager.AppManager;
import com.huolongluo.luo.manager.IMAudioVideoCallManager;
import com.huolongluo.luo.util.L;
import com.huolongluo.luo.util.TUIKitUtil;
import com.tencent.imsdk.v2.V2TIMConversationListener;
import com.tencent.imsdk.v2.V2TIMManager;
import com.tencent.imsdk.v2.V2TIMValueCallback;
import com.tencent.qcloud.tuicore.TUICore;
import com.tencent.qcloud.tuicore.TUICoreKeyEvent;
import com.tencent.qcloud.tuicore.component.UnreadCountTextView;
import com.tencent.qcloud.tuicore.interfaces.ITUINotification;
import com.tencent.qcloud.tuikit.tuichat.bean.message.TUIMessageBean;
import com.tencent.qcloud.tuikit.tuiconversation.ui.page.TUIConversationFragment;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.QueryListener;
import cn.bmob.v3.listener.UpdateListener;

public class MainActivity extends BaseActivity implements View.OnClickListener {
    private static final String TAG = "MainActivity";

    @BindView(R.id.main_view_pager)
    ViewPager2 main_view_pager;
    @BindView(R.id.msg_total_unread)
    UnreadCountTextView msg_total_unread;
    @BindView(R.id.iv_tab0)
    ImageView iv_tab0;
    @BindView(R.id.tv_tab0)
    TextView tv_tab0;
    @BindView(R.id.ll_tab0)
    LinearLayout ll_tab0;
    @BindView(R.id.iv_tab1)
    ImageView iv_tab1;
    @BindView(R.id.tv_tab1)
    TextView tv_tab1;
    @BindView(R.id.ll_tab1)
    LinearLayout ll_tab1;
    @BindView(R.id.iv_tab2)
    ImageView iv_tab2;
    @BindView(R.id.tv_tab2)
    TextView tv_tab2;
    @BindView(R.id.rl_tab2)
    RelativeLayout rl_tab2;
    @BindView(R.id.iv_tab3)
    ImageView iv_tab3;
    @BindView(R.id.tv_tab3)
    TextView tv_tab3;
    @BindView(R.id.ll_tab3)
    LinearLayout ll_tab3;
//    @BindView(R.id.iv_tab4)
//    ImageView iv_tab4;
//    @BindView(R.id.tv_tab4)
//    TextView tv_tab4;
//    @BindView(R.id.ll_tab4)
//    LinearLayout ll_tab4;

//    private View mLastTab;

    private List<Fragment> fragments = new ArrayList<>();

    //五个Fragment
//    private TUIConversationFragment tuiConversationFragment;//会话
//    private HomeFragment homeFragment;//联系人
    private HomeFragment homeFragment;//首页
    private DiscoverFragment discoverFragment;//发现
    private TUIConversationFragment tuiConversationFragment;//会话
    //    private ResourceFragment resourceFragment;//资源
    private MineFragment mineFragment;//发现
    private int currentFragment = 0;

    private String jsonResultGiftListBean;

    private String chatTargetUserId = "";//聊天的目标用户ID
    private volatile boolean isImCalling = false;//是否正在通话中
    private volatile boolean isCalling = false;//是否主叫
    public static volatile String callType = "";//通话类型
    private String[] mUserIDs;//(被叫的用户ID列表)为空说明是被叫，不为空说明当前设备是主叫

    @Override
    protected int getContentViewId() {
        return R.layout.activity_main;
    }

    @Override
    protected void injectDagger() {
        activityComponent().inject(this);
    }

    @Override
    protected void initViewsAndEvents(@Nullable Bundle savedInstanceState) {
        ImmersionBar.with(this).statusBarDarkFont(true).init();

        fragments.add(new HomeFragment());
        fragments.add(new DiscoverFragment());
        fragments.add(new TUIConversationFragment());
        fragments.add(new MineFragment());

        MainFragmentViewPagerAdapter fragmentAdapter = new MainFragmentViewPagerAdapter(this);
        fragmentAdapter.setFragmentList(fragments);

        // 关闭左右滑动切换页面
        main_view_pager.setUserInputEnabled(false);
        // 设置缓存数量为4 避免销毁重建
        main_view_pager.setOffscreenPageLimit(4);
        main_view_pager.setAdapter(fragmentAdapter);
        main_view_pager.setCurrentItem(0, false);

//        if (mLastTab == null) {
//            mLastTab = ll_tab1;
//        } else {
//            // 初始化时，强制切换tab到上一次的位置
//            View tmp = mLastTab;
//            mLastTab = null;
//            tabClick(tmp);
//            mLastTab = tmp;
//        }


//        setSelect(1);
        ll_tab0.setOnClickListener(this);
        ll_tab1.setOnClickListener(this);
        rl_tab2.setOnClickListener(this);
        ll_tab3.setOnClickListener(this);
//        eventClick(ll_tab1).subscribe(o -> // 会话
//        {
//            setSelect(0);
//        });
//        eventClick(ll_tab2).subscribe(o -> // 联系人
//        {
//            setSelect(1);
//        });
//        eventClick(rl_tab3).subscribe(o -> // 消息
//        {
//            setSelect(2);
//        });
//        eventClick(ll_tab4).subscribe(o -> // 资源
//        {
//            setSelect(3);
//        });
//        eventClick(ll_tab5).subscribe(o -> // 我的
//        {
//            setSelect(4);
//        });
        queryLatestVersionCode();

        /*TUICore.registerEvent(TUICoreKeyEvent.CLICK_GIFT, TUICoreKeyEvent.CLICK_GIFT, new ITUINotification() {
            @Override
            public void onNotifyEvent(String key, String subKey, Map<String, Object> param) {
                // TODO: 2022/4/13 查询礼物借款
                Toast.makeText(MainActivity.this, "MainActivity收到事件", Toast.LENGTH_SHORT).show();
//                TUIC2CChatFragment.getInstance().showGiftDialog();
                Log.e("TAG", "onNotifyEvent: 收到通知================》" + param.get("key1") + "  第二个参数：" + param.get("key2"));
//                showProgressDialog("");
//                GiftDialog giftDialog = new GiftDialog(MainActivity.this.getApplicationContext());ds
//                giftDialog.show();
//                startActivity(RegisterActivity.class);
//                TUIC2CChatFragment.getInstance().showGiftDialog();

                if (TextUtils.isEmpty(jsonResultGiftListBean)) {
                    queryGiftListBean();
                } else {
                    HashMap<String, Object> param1 = new HashMap<>();
                    param1.put(TUICoreKeyEvent.GIFT_LIST_BEAN, jsonResultGiftListBean);
                    TUICore.notifyEvent(TUICoreKeyEvent.GIFT_LIST_BEAN, TUICoreKeyEvent.GIFT_LIST_BEAN, param1);
                }
            }
        });*/
        //用户点击跳转到充值界面
        TUICore.registerEvent(TUICoreKeyEvent.NoGoldDialog_CKCK_CHONGZHI, TUICoreKeyEvent.NoGoldDialog_CKCK_CHONGZHI, new ITUINotification() {
            @Override
            public void onNotifyEvent(String key, String subKey, Map<String, Object> param) {
//                startActivity(RechargeGoldActivity.class);//跳转到充值界面
                startActivity(MyAccountActivity.class);//跳转到充值界面
            }
        });
        //用户打开好友联系人列表
        TUICore.registerEvent(TUICoreKeyEvent.CLICK_FRIEND_CONTACTS, TUICoreKeyEvent.CLICK_FRIEND_CONTACTS, new ITUINotification() {
            @Override
            public void onNotifyEvent(String key, String subKey, Map<String, Object> param) {
                startActivity(FriendContactsActivity.class);
            }
        });
        //当前用户发送消息
        TUICore.registerEvent(TUICoreKeyEvent.SEND_IM_MESSAGE, TUICoreKeyEvent.SEND_IM_MESSAGE, new ITUINotification() {
            @Override
            public void onNotifyEvent(String key, String subKey, Map<String, Object> param) {
                chatTargetUserId = (String) param.get(TUICoreKeyEvent.SEND_IM_MESSAGE_TARGET_USER_ID);
                HashMap<String, Object> param1 = new HashMap<>();

                if (TextUtils.equals(Share.get().getKeFuAccount(), chatTargetUserId) || Share.get().getIsAgent()) {
                    //给客服发消息，或者当前用户是代理
                    param1.put(TUICoreKeyEvent.SEND_IM_MESSAGE_HEYAN_RESULT, 1);
                    Log.e(TAG, "onNotifyEvent: 发送的是客服消息 或者当前用户是代理吗？" + Share.get().getIsAgent());
                } else {
                    double goldBalance = Double.parseDouble(Share.get().getGoldBalance());//金币余额
                    double messagePrice = Double.parseDouble(Share.get().getMessagePrice());//一条消息的价格
                    if (goldBalance >= messagePrice || Share.get().getIsVip()) {
                        //金币余额足够
                        param1.put(TUICoreKeyEvent.SEND_IM_MESSAGE_HEYAN_RESULT, 1);
                        if (!Share.get().getIsVip()) {
                            double toUpdateGold = goldBalance - messagePrice;
                            updateUserWallet(toUpdateGold);
                        }
                    } else {
                        //金币余额不足，请充值
                        param1.put(TUICoreKeyEvent.SEND_IM_MESSAGE_HEYAN_RESULT, 0);
                    }
                }
                TUIMessageBean msg = (TUIMessageBean) param.get(TUICoreKeyEvent.SEND_IM_MESSAGE_CONTENT);
                Log.e(TAG, "onNotifyEvent: 聊天 收到要发送的消息了，核验消息是否为空：" + (msg == null));
                param1.put(TUICoreKeyEvent.SEND_IM_MESSAGE_CONTENT, msg);

                TUICore.notifyEvent(TUICoreKeyEvent.SEND_IM_MESSAGE_HEYAN, TUICoreKeyEvent.SEND_IM_MESSAGE_HEYAN, param1);
            }
        });

        //用户点击了发起通话按钮
        TUICore.registerEvent(TUICoreKeyEvent.CLICK_AUDIO_OR_VIDEO, TUICoreKeyEvent.CLICK_AUDIO_OR_VIDEO, new ITUINotification() {
            @Override
            public void onNotifyEvent(String key, String subKey, Map<String, Object> param) {
                Log.e(TAG, "onNotifyEvent: 用户点击了发起音视频通话主叫");
                String callType = (String) param.get(TUICoreKeyEvent.CLICK_AUDIO_OR_VIDEO_TYPE);//获取通话类型
                String userid = (String) param.get(TUICoreKeyEvent.CLICK_AUDIO_OR_VIDEO_USERID);//被呼叫的用户ID
                double goldBalance = Double.parseDouble(Share.get().getGoldBalance());//金币余额
                double audioPrice = Double.parseDouble(Share.get().getAudioPrice());//一分钟语音消耗多少金币
                double videoPrice = Double.parseDouble(Share.get().getVideoPrice());//一分钟视频消耗多少金币
                if (TextUtils.equals("audio", callType)) {
                    //语音通话
                    if (goldBalance >= audioPrice) {
                        //金币余额足够
//                        int toUpdateGold = goldBalance - 20;
                        IMAudioVideoCallManager.clickStartCall(callType, userid);
                    } else {
                        //金币余额不足，请充值
                        HashMap<String, Object> param1 = new HashMap<>();
                        param1.put(TUICoreKeyEvent.SEND_IM_MESSAGE_HEYAN_RESULT, 0);
                        TUICore.notifyEvent(TUICoreKeyEvent.SEND_IM_MESSAGE_HEYAN, TUICoreKeyEvent.SEND_IM_MESSAGE_HEYAN, param1);
                    }
                } else {
                    //视频通话
                    if (goldBalance >= videoPrice) {
                        //金币余额足够
//                        int toUpdateGold = goldBalance - 40;
                        IMAudioVideoCallManager.clickStartCall(callType, userid);
                    } else {
                        //金币余额不足，请充值
                        HashMap<String, Object> param1 = new HashMap<>();
                        param1.put(TUICoreKeyEvent.SEND_IM_MESSAGE_HEYAN_RESULT, 0);
                        TUICore.notifyEvent(TUICoreKeyEvent.SEND_IM_MESSAGE_HEYAN, TUICoreKeyEvent.SEND_IM_MESSAGE_HEYAN, param1);
                    }
                }
            }
        });
        //用户建立起了通话
        TUICore.registerEvent(TUICoreKeyEvent.START_IM_CALLING, TUICoreKeyEvent.START_IM_CALLING, new ITUINotification() {
            @Override
            public void onNotifyEvent(String key, String subKey, Map<String, Object> param) {
                callType = (String) param.get(TUICoreKeyEvent.START_IM_CALLING_TYPE);//获取通话类型
                mUserIDs = (String[]) param.get(TUICoreKeyEvent.START_IM_CALLING_USERIDS);//(被叫的用户ID列表)为空说明是被叫，不为空说明当前设备是主叫
                Log.e(TAG, "onNotifyEvent: 用户间建立起通话||||||||||| 20金币每分钟 40金币每分钟 callType：" + callType + "  mUserIDs：" + Arrays.toString(mUserIDs));
                if (mUserIDs == null || mUserIDs.length <= 0) {
                    isCalling = false;
                } else {
                    isCalling = true;
                }
                isImCalling = true;
                IMAudioVideoCallManager.onCalling(isCalling);
            }
        });
        //用户结束通话
        TUICore.registerEvent(TUICoreKeyEvent.STOP_IM_CALLING, TUICoreKeyEvent.STOP_IM_CALLING, new ITUINotification() {
            @Override
            public void onNotifyEvent(String key, String subKey, Map<String, Object> param) {
                if (isImCalling) {
                    isImCalling = false;
                    String[] beCalledUserIDs = mUserIDs;
                    Log.e(TAG, "onNotifyEvent: 用户间结束通话|||||||||||");
                    IMAudioVideoCallManager.onStopCalling(isCalling, beCalledUserIDs);
                } else {
                    Log.e(TAG, "onNotifyEvent: 已经不在通话了，收到结束通话事件");
                }
                mUserIDs = null;
            }
        });

        /*AlipayConfig alipayConfig = new AlipayConfig();
        alipayConfig.setServerUrl(URL);
        alipayConfig.setAppId(APP_ID);
        alipayConfig.setPrivateKey(PRIVATE_KEY);
        alipayConfig.setFormat(FORMAT);
        alipayConfig.setCharset(CHARSET);
        alipayConfig.setAlipayPublicKey(ALIPAY_PUBLIC_KEY);
        alipayConfig.setSignType(SIGN_TYPE);
        //构造client
        AlipayClient alipayClient = new DefaultAlipayClient(alipayConfig);*/
    }

    /**
     * 更新用户钱包的金币数量
     *
     * @param toUpdateGold 将金币更新为多少
     */
    private void updateUserWallet(double toUpdateGold) {
        BmobQuery<UserWalletBean> categoryBmobQuery = new BmobQuery<>();
        categoryBmobQuery.addWhereEqualTo("username", Share.get().getUserName());
        categoryBmobQuery.findObjects(new FindListener<UserWalletBean>() {
            @Override
            public void done(List<UserWalletBean> result, BmobException e) {
                if (e == null) {
                    if (!result.isEmpty()) {
                        //更新UserWalletBean表里面id为6b6c11c537的数据，address内容更新为“北京朝阳”
                        UserWalletBean p2 = result.get(0);
                        Share.get().setUser_wallet_bean_object_id(p2.getObjectId());
                        p2.setGoldBalance(toUpdateGold + "");
                        p2.update(p2.getObjectId(), new UpdateListener() {
                            @Override
                            public void done(BmobException e) {
                                if (e == null) {
                                    Share.get().setGoldBalance(toUpdateGold + "");
                                    Log.e(TAG, "金币余额 更新成功:" + p2.getUpdatedAt());
//                                    UserWalletBean.updateUserIntegralBalance();
                                    UserWalletBean.updateChatTargetUserIdIntegralBalance(chatTargetUserId);
                                } else {
                                    Log.e(TAG, "金币余额 更新失败：" + e.getMessage());
                                }
                            }
                        });
                    } else {
                        Log.e(TAG, "更新金币余额 用户钱包不存在");
//                        createUserWallet();//给用户创建一个钱包
                        UserWalletBean.createUserWallet();
                    }
                } else {
                    Log.e(TAG, e.toString());
                }
            }
        });
    }

//    /**
//     * 给用户创建一个钱包
//     * */
//    private void createUserWallet() {
//        UserWalletBean p2 = new UserWalletBean();
//        p2.setUsername(Share.get().getUserName());
//        p2.setVip(false);
//        p2.setGoldBalance(100);
//        p2.setIntegralBalance(0);
//        p2.save(new SaveListener<String>() {
//            @Override
//            public void done(String objectId,BmobException e) {
//                if(e==null){
//                    Log.e(TAG, "给用户创建一个钱包 数据成功，返回objectId为："+objectId);
//                    Share.get().setGoldBalance(100);
//                }else{
//                    Log.e(TAG, "给用户创建一个钱包 创建数据失败：" + e.getMessage());
//                }
//            }
//        });
//    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ll_tab0:
                setSelect(0);// 首页
                break;
            case R.id.ll_tab1:
                setSelect(1);// 发现
                break;
            case R.id.rl_tab2:
                setSelect(2);// 会话
                break;
            case R.id.ll_tab3:// 我的
                setSelect(3);
                break;
        }
    }

//    /**
//     * 修改用户信息 头像、昵称之类的
//     */
//    private void updateProfile() {
//        V2TIMUserFullInfo v2TIMUserFullInfo = new V2TIMUserFullInfo();
//        // 头像
//        if (!TextUtils.isEmpty(Share.get().getHeadPic())) {
//            v2TIMUserFullInfo.setFaceUrl(Share.get().getHeadPic());
////            UserInfo.getInstance().setAvatar(faceUrl);
//        }
//
//        // 昵称
//        v2TIMUserFullInfo.setNickname(TextUtils.isEmpty(Share.get().getNickName()) ? Share.get().getUserName() : Share.get().getNickName());
////        UserInfo.getInstance().setName(nickName);
////
////        // 生日
////        if (birthday != 0) {
////            v2TIMUserFullInfo.setBirthday(birthday);
////        }
////
////        // 个性签名
////        v2TIMUserFullInfo.setSelfSignature(signature);
////
//        // 性别
////        v2TIMUserFullInfo.setGender(gender);
//
//        V2TIMManager.getInstance().setSelfInfo(v2TIMUserFullInfo, new V2TIMCallback() {
//            @Override
//            public void onError(int code, String desc) {
//                Log.e("TAG", "modifySelfProfile err code = " + code + ", desc = " + ErrorMessageConverter.convertIMError(code, desc));
//                ToastUtil.toastShortMessage("Error code = " + code + ", desc = " + ErrorMessageConverter.convertIMError(code, desc));
//            }
//
//            @Override
//            public void onSuccess() {
//                Log.i("TAG", "modifySelfProfile success");
//            }
//        });
//    }

    private void setSelect(int i) {
        currentFragment = i;
        L.e("===========切换到的 Fragment=========" + i);
//        FragmentManager fm = getSupportFragmentManager();
//        FragmentTransaction transaction = fm.beginTransaction();
//        hideFragment(transaction);

        resetImgs();

        //把图片设置为亮色
        //设置内容区域
        switch (i) {
            case 0:
                main_view_pager.setCurrentItem(0, false);
                iv_tab0.setImageResource(R.mipmap.icon_main_home_on);
                tv_tab0.setTextColor(ContextCompat.getColor(this, R.color.F6E6FFF));
                break;
            case 1:
                main_view_pager.setCurrentItem(1, false);
                iv_tab1.setImageResource(R.mipmap.icon_main_discover_on);
                tv_tab1.setTextColor(ContextCompat.getColor(this, R.color.F6E6FFF));
                break;
            case 2:
                main_view_pager.setCurrentItem(2, false);
                iv_tab2.setImageResource(R.mipmap.icon_main_msg_on);
                tv_tab2.setTextColor(ContextCompat.getColor(this, R.color.F6E6FFF));
                break;
            case 3:
                main_view_pager.setCurrentItem(3, false);
                iv_tab3.setImageResource(R.mipmap.icon_main_mine_on);
                tv_tab3.setTextColor(ContextCompat.getColor(this, R.color.F6E6FFF));
                break;
        }
    }

    private final V2TIMConversationListener unreadListener = new V2TIMConversationListener() {
        @Override
        public void onTotalUnreadMessageCountChanged(long totalUnreadCount) {
            if (msg_total_unread == null) {
                return;
            }
            if (totalUnreadCount > 0) {
                msg_total_unread.setVisibility(View.VISIBLE);
            } else {
                msg_total_unread.setVisibility(View.INVISIBLE);
            }
            String unreadStr = "" + totalUnreadCount;
            if (totalUnreadCount > 100) {
                unreadStr = "99+";
            }
            msg_total_unread.setText(unreadStr);
            // 华为离线推送角标
//            OfflineMessageDispatcher.updateBadge(MainActivity.this, (int) totalUnreadCount);
        }
    };

//    private final V2TIMFriendshipListener friendshipListener = new V2TIMFriendshipListener() {
//        @Override
//        public void onFriendApplicationListAdded(List<V2TIMFriendApplication> applicationList) {
//            refreshFriendApplicationUnreadCount();
//        }
//
//        @Override
//        public void onFriendApplicationListDeleted(List<String> userIDList) {
//            refreshFriendApplicationUnreadCount();
//        }
//
//        @Override
//        public void onFriendApplicationListRead() {
//            refreshFriendApplicationUnreadCount();
//        }
//    };

//    private void refreshFriendApplicationUnreadCount() {
//        V2TIMManager.getFriendshipManager().getFriendApplicationList(new V2TIMValueCallback<V2TIMFriendApplicationResult>() {
//            @Override
//            public void onSuccess(V2TIMFriendApplicationResult v2TIMFriendApplicationResult) {
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        int unreadCount = v2TIMFriendApplicationResult.getUnreadCount();
//                        if (unreadCount > 0) {
//                            mNewFriendUnread.setVisibility(View.VISIBLE);
//                        } else {
//                            mNewFriendUnread.setVisibility(View.GONE);
//                        }
//                        String unreadStr = "" + unreadCount;
//                        if (unreadCount > 100) {
//                            unreadStr = "99+";
//                        }
//                        mNewFriendUnread.setText(unreadStr);
//                    }
//                });
//            }
//
//            @Override
//            public void onError(int code, String desc) {
//
//            }
//        });
//    }

    private void registerUnreadListener() {
        V2TIMManager.getConversationManager().addConversationListener(unreadListener);
        V2TIMManager.getConversationManager().getTotalUnreadMessageCount(new V2TIMValueCallback<Long>() {
            @Override
            public void onSuccess(Long aLong) {
                runOnUiThread(() -> unreadListener.onTotalUnreadMessageCountChanged(aLong));
            }

            @Override
            public void onError(int code, String desc) {
            }
        });

//        V2TIMManager.getFriendshipManager().addFriendListener(friendshipListener);
//        refreshFriendApplicationUnreadCount();
    }

    @Override
    protected void onResume() {
        super.onResume();
//        updateProfile();
        TUIKitUtil.updateProfile(Share.get().getHeadPic(), Share.get().getNickName());
        registerUnreadListener();
        UserBasicInfoBean.createUserDetailInfoBean();
        UserWalletBean.queryUserWallet();

        //查询用户充值次数
        RechargeRecordBean.queryRechargeCount();

        ConsumeBean.queryConsumeConfig();
    }

    /**
     * 切换图片至暗色
     **/
    private void resetImgs() {
        tv_tab0.setTextColor(ContextCompat.getColor(this, R.color.F707070));
        tv_tab1.setTextColor(ContextCompat.getColor(this, R.color.F707070));
        tv_tab2.setTextColor(ContextCompat.getColor(this, R.color.F707070));
        tv_tab3.setTextColor(ContextCompat.getColor(this, R.color.F707070));
//        tv_tab5.setTextColor(ContextCompat.getColor(this, R.color.A4A4A4));
        iv_tab0.setImageResource(R.mipmap.icon_main_home_off);
        iv_tab1.setImageResource(R.mipmap.icon_main_discover_off);
        iv_tab2.setImageResource(R.mipmap.icon_main_msg_off);
        iv_tab3.setImageResource(R.mipmap.icon_main_mine_off);
//        iv_tab5.setImageResource(R.mipmap.tab_icon_lesson_inactive);
    }

    /*private void queryGiftListBean() {
        BmobQuery<GiftListBean> bmobQuery = new BmobQuery<>();
        bmobQuery.findObjects(new FindListener<GiftListBean>() {
            @Override
            public void done(List<GiftListBean> result, BmobException e) {
                if (e == null) {
                    Log.e("TAG", "done: 查询礼物成功：" + result.size());
                    Gson gson = new Gson();
                    jsonResultGiftListBean = gson.toJson(result);

                    HashMap<String, Object> param1 = new HashMap<>();
                    param1.put(TUICoreKeyEvent.GIFT_LIST_BEAN, jsonResultGiftListBean);
                    TUICore.notifyEvent(TUICoreKeyEvent.GIFT_LIST_BEAN, TUICoreKeyEvent.GIFT_LIST_BEAN, param1);
                } else {
                    Log.e("BMOB 查询礼物失败", e.toString());
                }
            }
        });
    }*/

    /**
     * 检查最新的版本号
     */
    private void queryLatestVersionCode() {
        BmobQuery<VersionUpdateBean> bmobQuery = new BmobQuery<>();
        bmobQuery.getObject("Yctd666L", new QueryListener<VersionUpdateBean>() {
            @Override
            public void done(VersionUpdateBean versionUpdateBean, BmobException e) {
                if (e == null) {
                    L.e("查询到最新版本号为：" + versionUpdateBean.getLatestVersionCode());
                    if (AppUtils.getAppVersionCode() >= Integer.parseInt(versionUpdateBean.getLatestVersionCode())) {
                        Log.e(TAG, "done: 当前已是最新版本");
                    } else {
                        if (versionUpdateBean.isForcedUpdate()) {
                            showConfirmNoCancelDialog("版本更新", "检测到新版本，是否立即更新", new DialogClickListener() {
                                @Override
                                public void onConfirm() {
                                    Intent intent = new Intent();
                                    intent.setAction("android.intent.action.VIEW");
                                    Uri content_uri = Uri.parse(versionUpdateBean.getDownLoadUrl());
                                    intent.setData(content_uri);
                                    startActivity(intent);
                                }

                                @Override
                                public void onCancel() {
                                }
                            });
                        } else {
                            showConfirmDialog("版本更新", "检测到新版本，需要更新后才可正常使用，是否立即更新", new DialogClickListener() {
                                @Override
                                public void onConfirm() {
                                    Intent intent = new Intent();
                                    intent.setAction("android.intent.action.VIEW");
                                    Uri content_uri = Uri.parse(versionUpdateBean.getDownLoadUrl());
                                    intent.setData(content_uri);
                                    startActivity(intent);
                                }

                                @Override
                                public void onCancel() {
                                }
                            });
                        }
                    }
                } else {
//                    ToastSimple.show(e.toString(), 2);
                    L.e(e.toString());
                }
            }
        });
    }

    private long mExitTime;//退出时间戳

    @Override
    public void onBackPressed() {
        if (currentFragment == 2) {
            EventBus.getDefault().post(new Event.clickWebViewBack());
            return;
        }
        if ((System.currentTimeMillis() - mExitTime) > 2000) {
            showToastMessage("再按一次退出程序", 2);
            mExitTime = System.currentTimeMillis();
        } else {
            AppManager.get().AppExit(this);
        }
    }
}