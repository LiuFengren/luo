package com.huolongluo.luo.huolongluo.injection.model;

import android.app.Application;
import android.content.Context;

import com.huolongluo.luo.base.ApplicationContext;

import dagger.Module;
import dagger.Provides;

/**
 * <p>
 * Created by 火龙裸 on 2022/3/12.
 */

@Module
public class ApplicationModule
{
    protected final Application mApplication;

    public ApplicationModule(Application mApplication)
    {
        this.mApplication = mApplication;
    }

    @Provides
    Application provodeApplication()
    {
        return mApplication;
    }

    @Provides
    @ApplicationContext
    Context provideContext()
    {
        return mApplication;
    }
}
