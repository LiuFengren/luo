package com.huolongluo.luo.wxapi;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import androidx.annotation.Nullable;

import com.huolongluo.luo.MainActivity;
import com.huolongluo.luo.huolongluo.base.BaseActivity;
import com.huolongluo.luo.manager.pay.wechat.WxConfig;
import com.huolongluo.luo.manager.pay.wechat.uikit.NetworkUtil;
import com.tencent.mm.opensdk.modelbase.BaseReq;
import com.tencent.mm.opensdk.modelbase.BaseResp;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;

public class WXPayEntryActivity extends BaseActivity implements IWXAPIEventHandler {
    private static final String TAG = "WXPayEntryActivity";

    private IWXAPI api;
    private MyHandler handler;

    @Override
    protected int getContentViewId() {
        return 0;
    }

    @Override
    protected void injectDagger() {
    }

    private static class MyHandler extends Handler {
        private final WeakReference<WXPayEntryActivity> wxEntryActivityWeakReference;

        public MyHandler(WXPayEntryActivity wxEntryActivity){
            wxEntryActivityWeakReference = new WeakReference<WXPayEntryActivity>(wxEntryActivity);
        }

        @Override
        public void handleMessage(Message msg) {
            int tag = msg.what;
            switch (tag) {
                case NetworkUtil.GET_TOKEN: {
                    Bundle data = msg.getData();
                    JSONObject json = null;
                    try {
                        json = new JSONObject(data.getString("result"));
                        String openId, accessToken, refreshToken, scope;
                        openId = json.getString("openid");
                        accessToken = json.getString("access_token");
                        refreshToken = json.getString("refresh_token");
                        scope = json.getString("scope");
                        Intent intent = new Intent(wxEntryActivityWeakReference.get(), MainActivity.class);
                        intent.putExtra("openId", openId);
                        intent.putExtra("accessToken", accessToken);
                        intent.putExtra("refreshToken", refreshToken);
                        intent.putExtra("scope", scope);
                        wxEntryActivityWeakReference.get().startActivity(intent);
                    } catch (JSONException e) {
                        Log.e(TAG, e.getMessage());
                    }
                }
            }
        }
    }

    @Override
    protected void initViewsAndEvents(@Nullable Bundle savedInstanceState) {
        api = WXAPIFactory.createWXAPI(this, WxConfig.APP_ID, false);
        handler = new MyHandler(this);
        try {
            Intent intent = getIntent();
            api.handleIntent(intent, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onReq(BaseReq baseReq) {

    }

    /**
     * 支付后的回调
     * */
    @Override
    public void onResp(BaseResp baseResp) {
        Log.e(TAG, "onResp: 回调类型：" + baseResp.getType() + "  结果码：" + baseResp.errCode);
    }
}
