package com.huolongluo.luo.huolongluo.ui.adapter;

import android.os.Bundle;
import android.text.TextUtils;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DiffUtil;

import com.huolongluo.luo.huolongluo.bean.ItemBean;

import java.util.List;

public class MyDiffUtil extends DiffUtil.Callback {

    private List<ItemBean> oldList;
    private List<ItemBean> newList;

    public MyDiffUtil(List<ItemBean> oldList, List<ItemBean> newList) {
        this.oldList = oldList;
        this.newList = newList;
    }

    @Override
    public int getOldListSize() {
        return oldList.size();
    }

    @Override
    public int getNewListSize() {
        return newList.size();
    }

    /**
     * 返回true，则会执行areContentsTheSame()方法，
     * 返回false，则继续比较数据
     * */
    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        //通过判断ID，来区分是否同一个Item
        return TextUtils.equals(oldList.get(oldItemPosition).getId(), newList.get(newItemPosition).getId());
    }

    /**
     * 返回true，则保存老的视图和数据（不做任何修改）
     * 返回false，说明数据有变化，则执行getChangePayload()方法
     * */
    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        ItemBean oldItems = oldList.get(oldItemPosition);
        ItemBean newItems = newList.get(newItemPosition);
        return TextUtils.equals(oldItems.getContent(), newItems.getContent());
    }


    /**
     * 这个方法非必须重写的方法。不重写这个方法的情况下，会执行父类的getChangePayload()方法而返回null，
     * 并且调用适配器当中三个参数的onBindViewHolder()方法，要是适配器当中三个参数的onBindViewHolder()方法
     * 也不重写，默认调用其super.onBindViewHolder(holder, position, payloads);
     * */
    @Nullable
    @Override
    public Object getChangePayload(int oldItemPosition, int newItemPosition) {
        ItemBean oldItems = oldList.get(oldItemPosition);
        ItemBean newItems = newList.get(newItemPosition);

        Bundle bundle = new Bundle();
        if (!TextUtils.equals(oldItems.getTitle(), newItems.getTitle())) {
            bundle.putString("title", newItems.getTitle());
        }

        if (!TextUtils.equals(oldItems.getContent(), newItems.getContent())) {
            bundle.putString("content", newItems.getContent());
        }

//        return super.getChangePayload(oldItemPosition, newItemPosition);
        return bundle;
    }
}
