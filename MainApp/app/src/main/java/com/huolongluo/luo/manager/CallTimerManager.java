package com.huolongluo.luo.manager;

import android.util.Log;

import com.huolongluo.luo.MainActivity;
import com.tencent.qcloud.tuicore.TUICore;
import com.tencent.qcloud.tuicore.TUICoreKeyEvent;

import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

/**
 * 倒计时管理类
 */
public class CallTimerManager {
    private static final String TAG = "CallTimerManager";
    public static int mOverMaxTime = 30;//最大超时时长为多少秒
    private static Timer overTimer = null;
    private static OverTimerTask overTimerTask = null;

//    private static CallTimerCallBack timerCallBack;

    static class OverTimerTask extends TimerTask {
        @Override
        public void run() {
            mOverMaxTime--;
            Log.e(TAG, "run: 倒计时=====》 " + mOverMaxTime);
            if (mOverMaxTime <= 0) {
//                stopOverTimer();

                HashMap<String, Object> param1 = new HashMap<>();
                param1.put(TUICoreKeyEvent.FORCE_HANG_UP_CALLING_TYPE, MainActivity.callType);
                TUICore.notifyEvent(TUICoreKeyEvent.FORCE_HANG_UP_CALLING, TUICoreKeyEvent.FORCE_HANG_UP_CALLING, param1);
            }
        }
    }

    /**
     * @param isCallBack 是否需要回调
     */
    public static void startOverTimer(int overMaxTime) {
        stopOverTimer();
        mOverMaxTime = overMaxTime;//重置倒计时剩余时间
//        timerCallBack = callTimerCallBack;
        overTimerTask = new OverTimerTask();
        overTimer = new Timer();
        overTimer.schedule(overTimerTask, 0, 1000);
    }

    /**
     * 取消超时倒计时计算
     *
     * @param isCallBack 是否需要回调
     */
    public static void stopOverTimer() {
        if (overTimer != null) {
            overTimer.cancel();
            overTimer.purge();
            overTimer = null;
        }
        if (overTimerTask != null) {
            overTimerTask.cancel();
            overTimerTask = null;
        }
        /*if (timerCallBack != null) {
            Log.e(TAG, "stopOverTimer: ====================================================回调倒计时结束接口方法 调用栈：" + Log.getStackTraceString(new Throwable()));
            timerCallBack.timerStop(mOverMaxTime);
            timerCallBack = null;
        }*/
    }

//    interface CallTimerCallBack {
//        void timerStop(int surplusTime);
//    }
}
