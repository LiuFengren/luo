package com.huolongluo.luo.huolongluo.ui.fragment.upwall;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.huolongluo.luo.R;
import com.huolongluo.luo.huolongluo.base.BaseActivity;
import com.huolongluo.luo.huolongluo.base.BaseFragment;
import com.huolongluo.luo.huolongluo.bean.UserBasicInfoBean;
import com.huolongluo.luo.huolongluo.constants.Constants;
import com.huolongluo.luo.huolongluo.ui.activity.userdetail.UserDetailInfoActivity;
import com.huolongluo.luo.huolongluo.ui.adapter.UpWallAdapter;
import com.huolongluo.luo.superAdapter.recycler.SuperAdapter;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnLoadMoreListener;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;

public class UpWallFragment extends BaseFragment {
    private static final String TAG = "UpWallFragment";

    @BindView(R.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    @BindView(R.id.rv_up_wall)
    RecyclerView rv_up_wall;

    private String upWallSex;//上墙性别
    private List<UserBasicInfoBean> userBasicInfoBeanList = new ArrayList<>();//关注或粉丝列表
    private UpWallAdapter upWallAdapter;

    public static UpWallFragment getInstance(String upWallSex) {
        Bundle args = new Bundle();
        UpWallFragment fragment = new UpWallFragment();
        fragment.setArguments(args);
        fragment.upWallSex = upWallSex;
        return fragment;
    }

    @Override
    protected void initDagger() {
        ((BaseActivity) getActivity()).activityComponent().inject(this);
    }

    @Override
    protected int getContentViewId() {
        return R.layout.fragment_up_wall;
    }

    @Override
    protected void initViewsAndEvents(View rootView) {
        Log.e(TAG, "initViewsAndEvents: 当前Fragment的数据为：" + upWallSex);
        rv_up_wall.setNestedScrollingEnabled(false);
        rv_up_wall.setFocusable(false);
        rv_up_wall.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
//        rv_up_wall.addItemDecoration(new GridSpacingItemDecoration(3,
//                DensityUtil.dip2px(this, 10), false));
        upWallAdapter = new UpWallAdapter(getActivity(), userBasicInfoBeanList, R.layout.item_square);
        rv_up_wall.setAdapter(upWallAdapter);

        upWallAdapter.setOnItemClickListener(new SuperAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int viewType, int position) {
                Bundle bundle = new Bundle();
                bundle.putString(Constants.USER_NAME, upWallAdapter.getAllData().get(position).getUsername());
                startActivity(UserDetailInfoActivity.class, bundle);
            }
        });

        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull final RefreshLayout refreshLayout) {
                queryUpWallUser(Constants.MODE_REFRESH);
            }
        });
        refreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout rl) {
//                mAdapter.addData(movies);
                queryUpWallUser(Constants.MODE_LOAD_MORE);
//                rl.finishLoadMoreWithNoMoreData();
            }
        });

//        insertFollowOrFans();
        queryUpWallUser(Constants.MODE_REFRESH);
    }

    private void queryUpWallUser(int refreshType) {
        /*showProgressDialog("");
        BmobQuery<UserBasicInfoBean> categoryBmobQuery = new BmobQuery<>();
//        categoryBmobQuery.addWhereEqualTo("sex", upWallSex);
        categoryBmobQuery.addWhereEqualTo("isUpWall", true);
        categoryBmobQuery.findObjects(new FindListener<UserBasicInfoBean>() {
            @Override
            public void done(List<UserBasicInfoBean> result, BmobException e) {
                if (e == null) {
                    if (!result.isEmpty()) {
                        userBasicInfoBeanList.clear();
                        userBasicInfoBeanList.addAll(result);
                        if (!userBasicInfoBeanList.isEmpty()) {
                            upWallAdapter.addAll(userBasicInfoBeanList);
                        }
                    }
                    Log.e(TAG, "done: 上墙用户（" + upWallSex + "）查询成功" + result.size());
                } else {
                    Log.e(TAG, e.toString());
                }
                hideProgressDialog();
            }
        });*/
        /******************/

        //--and条件1
        BmobQuery<UserBasicInfoBean> eq1 = new BmobQuery<UserBasicInfoBean>();
        eq1.addWhereEqualTo("sex", upWallSex);
        //--and条件2
        BmobQuery<UserBasicInfoBean> eq2 = new BmobQuery<UserBasicInfoBean>();
        eq2.addWhereEqualTo("isUpWall", true);

        //最后组装完整的and条件
        List<BmobQuery<UserBasicInfoBean>> andQuerys = new ArrayList<BmobQuery<UserBasicInfoBean>>();
        andQuerys.add(eq1);
        andQuerys.add(eq2);
        
        //查询符合整个and条件的人
        BmobQuery<UserBasicInfoBean> query = new BmobQuery<UserBasicInfoBean>();
        query.and(andQuerys);

        if (refreshType == Constants.MODE_LOAD_MORE) {
            query.setSkip(upWallAdapter.getItemCount());// 忽略前多少条数据，查询后面的
        }

        query.findObjects(new FindListener<UserBasicInfoBean>() {
            @Override
            public void done(List<UserBasicInfoBean> result, BmobException e) {
                if (e == null) {
                    if (!result.isEmpty()) {
                        List<UserBasicInfoBean> filterResult = new ArrayList<>();
                        for (int i = 0; i < result.size(); i++) {
                            if (UserBasicInfoBean.isCompleteBasicInfo(result.get(i))) {
                                filterResult.add(result.get(i));
                            }
                        }

                        if (refreshType == Constants.MODE_REFRESH) {
                            userBasicInfoBeanList.clear();
                            userBasicInfoBeanList.addAll(filterResult);
                            if (!userBasicInfoBeanList.isEmpty()) {
                                upWallAdapter.replaceAll(userBasicInfoBeanList);
                            }
                        } else {
                            userBasicInfoBeanList.addAll(filterResult);
                            if (!userBasicInfoBeanList.isEmpty()) {
                                upWallAdapter.addAll(filterResult);
                            }
                        }
                        Log.e(TAG, "done: 上墙用户" + upWallSex + "）查询成功" + result.size() + " 过滤后的为：" + filterResult.size());
                    } else {
                        refreshLayout.finishLoadMoreWithNoMoreData();
                    }
                    Log.e(TAG, "done: 上墙用户（" + upWallSex + "）查询成功");
                } else {
                    Log.e(TAG, e.toString());
                }
                refreshLayout.finishRefresh();
                refreshLayout.finishLoadMore();
            }
        });
    }
}
