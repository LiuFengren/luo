package com.huolongluo.luo.huolongluo.ui.fragment.resource;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.huolongluo.luo.BuildConfig;
import com.huolongluo.luo.R;
import com.huolongluo.luo.huolongluo.base.BaseFragment;
import com.huolongluo.luo.huolongluo.share.Event;
import com.huolongluo.luo.huolongluo.ui.activity.browser.SonicJavaScriptInterface;
import com.huolongluo.luo.huolongluo.ui.activity.browser.SonicRuntimeImpl;
import com.huolongluo.luo.huolongluo.ui.activity.browser.SonicSessionClientImpl;
import com.huolongluo.luo.manager.AppManager;
import com.huolongluo.luo.util.L;
import com.tencent.sonic.sdk.SonicConfig;
import com.tencent.sonic.sdk.SonicEngine;
import com.tencent.sonic.sdk.SonicSession;
import com.tencent.sonic.sdk.SonicSessionConfig;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;

public class ResourceFragment extends BaseFragment {

    @BindView(R.id.iv_left)
    ImageView ivLeft;
    @BindView(R.id.toolbar_center_title)
    TextView toolbar_center_title;
    @BindView(R.id.web_context)
    WebView web_context;

    private String urlTitle = "";
    public final static String PARAM_URL = "param_url";
    public final static String PARAM_MODE = "param_mode";
    private SonicSession sonicSession;
    //    private String url = "https://02191725.caomei16.xyz";
    private String url = "https://v.qq.com/";
    private boolean isLoadError = false;//是否加载网页失败

    private Intent intent;
    SonicSessionClientImpl sonicSessionClient = null;
    private WebView mWebView;

    public static ResourceFragment getInstance() {
        Bundle args = new Bundle();
        ResourceFragment fragment = new ResourceFragment();
        fragment.setArguments(args);
        return fragment;
    }
    
    @Override
    protected void initDagger() {
    }

    @Override
    protected int getContentViewId() {
        intent = getActivity().getIntent();

        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);

        // step 1: Initialize sonic engine if necessary, or maybe u can do this when application created
        if (!SonicEngine.isGetInstanceAllowed()) {
            SonicEngine.createInstance(new SonicRuntimeImpl(getActivity().getApplication()), new SonicConfig.Builder().build());
        }

        // step 2: Create SonicSession
        sonicSession = SonicEngine.getInstance().createSession(url, new SonicSessionConfig.Builder().build());
        if (null != sonicSession) {
            sonicSession.bindClient(sonicSessionClient = new SonicSessionClientImpl());
        } else {
            // this only happen when a same sonic session is already running,
            // u can comment following codes to feedback as a default mode.
//            throw new UnknownError("create session fail!");
            L.e("create session fail!");
        }
        return R.layout.fragment_resource;
    }

    @Override
    protected void initViewsAndEvents(View rootView) {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        ivLeft.setVisibility(View.GONE);

        // step 3: BindWebView for sessionClient and bindClient for SonicSession
        // in the real world, the init flow may cost a long time as startup
        // runtime、init configs....
//        setContentView(R.layout.activity_browser);
//        WebView webView = (WebView) findViewById(R.id.webview);
        web_context.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                if (sonicSession != null) {
                    sonicSession.getSessionClient().pageFinish(url);
                }
                if (!isLoadError) {
//                    web_context.setVisibility(View.VISIBLE);
                    showDefaultContent();
                }
                L.e("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                isLoadError = true;
                L.e("bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb");
                showDefaultError(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        isLoadError = false;
                        showDefaultListLoading();
                        web_context.reload();
                    }
                });
//                web_context.setVisibility(View.INVISIBLE);//避免出现默认的加载错误界面
//                tv_error_tips.setVisibility(View.VISIBLE);
            }

            @TargetApi(21)
            @Override
            public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {
                L.e("》》》》》》》》》》》》》》》》》  shouldInterceptRequest：" + request.getUrl());
                return shouldInterceptRequest(view, request.getUrl().toString());
            }

            @Override
            public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
                if (sonicSession != null) {
                    //step 6: Call sessionClient.requestResource when host allow the application
                    // to return the local data .
                    L.e("》》》》》》》》》》》》》》》》》  shouldInterceptRequest 不为空：" + url);

                    return (WebResourceResponse) sonicSession.getSessionClient().requestResource(url);
                }
                L.e("》》》》》》》》》》》》》》》》》  shouldInterceptRequest 为空：" + url);

                return null;
            }
        });
        web_context.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);
                if (TextUtils.isEmpty(urlTitle)) {//获取网页标题
                    toolbar_center_title.setText(title);
                } else {
                    toolbar_center_title.setText(urlTitle);
                }
            }
        });

        WebSettings webSettings = web_context.getSettings();
        if (BuildConfig.DEBUG) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                web_context.setWebContentsDebuggingEnabled(true);
            }
        }
//        web_context.addJavascriptInterface(new MyJavaScriptInterface(), "handlers");
//        web_context.loadUrl("javascript:android.callAndroid($(\"#username\").val(), $(\"#password\").val())");

        // step 4: bind javascript
        // note:if api level lower than 17(android 4.2), addJavascriptInterface has security
        // issue, please use x5 or see https://developer.android.com/reference/android/webkit/
        // WebView.html#addJavascriptInterface(java.lang.Object, java.lang.String)
        webSettings.setJavaScriptEnabled(true);
        web_context.removeJavascriptInterface("searchBoxJavaBridge_");
        intent.putExtra(SonicJavaScriptInterface.PARAM_LOAD_URL_TIME, System.currentTimeMillis());
        web_context.addJavascriptInterface(new SonicJavaScriptInterface(sonicSessionClient, intent), "sonic");

        // init webview settings
        webSettings.setAllowContentAccess(true);
        webSettings.setDatabaseEnabled(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setAppCacheEnabled(true);
        webSettings.setSavePassword(false);
        webSettings.setSaveFormData(false);
        webSettings.setUseWideViewPort(true);
        webSettings.setLoadWithOverviewMode(true);


        // step 5: webview is ready now, just tell session client to bind
        if (sonicSessionClient != null) {
            sonicSessionClient.bindWebView(web_context);
            sonicSessionClient.clientReady();
        } else { // default mode
            web_context.loadUrl(url);
        }
    }

//    class MyJavaScriptInterface {
//        /**
//         * 获取环境信息
//         */
//        @android.webkit.JavascriptInterface
//        public String getEnvironment() {
//            L.e("getEnvironment");
////            Map<String, String> map = new HashMap<>();
////            map.put("device_type", "1");
////            map.put("device_model", AppUtil.getTem_Model());
////            map.put("device_no", AppUtil.getIMEI(XnovalAppliction.getContext()));
////            map.put("os_version", AppUtil.getOsVersion());
////            map.put("version", AppUtil.getAppVersion(XnovalAppliction.getContext()) + "");
////            map.put("token", Share.get().getToken());
////            map.put("timestamp", "");
//
//            String token = Share.get().getRealUser().getToken();
//            CommonBean commonBean = new CommonBean("1", AppUtil.getTem_Model(),
//                    AppUtil.getIMEI(XnovalAppliction.getContext()), AppUtil.getOsVersion(),
//                    AppUtil.getVerName(XnovalAppliction.getContext()) + "",
//                    token, "");
//
//            GsonBuilder gsonBuilder = new GsonBuilder();
//            gsonBuilder.setPrettyPrinting();
//            gsonBuilder.enableComplexMapKeySerialization();
//            Gson gson = gsonBuilder.create();
//
//            return gson.toJson(commonBean);
//        }
//
//        /**
//         * H5执行了某项操作
//         * 打开书架: { "type": 1 }
//         * 跳转到登录页: { "type": 2 }
//         * 跳转到用户资料设置页面: { "type": 3 }
//         * 打开应用商店页面: { "type": 4 }
//         * 观看视频: { "type": 5 }
//         * 跳转到充值界面: { "type": 6 }
//         */
//        @android.webkit.JavascriptInterface
//        public void dispatchAction(String result) {
//            L.e("dispatchAction：" + result);
//            JSBean jsBean = new Gson().fromJson(result, JSBean.class);
//            L.e("当前type为：" + jsBean.getType());
////            L.e("==== " + jsBean.getType() + "   " + jsBean.getPayload().getId() + "   " + jsBean.getPayload().getUrl());
//            switch (jsBean.getType()) {
//                case 1:
//                    L.e("开始切换");
//                    EventBus.getDefault().post(new Event.switchToLibraryFragment());
//                    break;
//                case 2:
//                    startActivity(LoginActivity.class);
//                    break;
//                case 3:
//                    if (CheckLoginUtil.checkIsLogin()) {
//                        startActivity(MyInfoActivity.class);
//                    } else {
//                        startActivity(LoginActivity.class);
//                    }
//                    break;
//                case 4:
//                    break;
//                case 5:
//                    EventBus.getDefault().post(new Event.playVideoAd(jsBean.getMission_id() + ""));
//                    break;
//                case 6://跳转到充值界面
//                    startActivity(RechargeActivity.class);
//                    break;
//            }
//        }
//
//        /**
//         * AES 加密
//         */
//        @android.webkit.JavascriptInterface
//        public String aesEncrypt(String result) {
//            L.e("aesEncrypt：" + result);// TODO: 2020/6/26 本地对result加密 ,然后return回去
////            AesUtil.encrypt(result);
//            return AesUtil.encrypt(result);
//        }
//
//        /**
//         * AES 解密
//         */
//        @android.webkit.JavascriptInterface
//        public String aesDecrypt(String result) {
//            L.e("aesDecrypt：" + result);// TODO: 2020/6/26 本地对result解密 ,然后return回去
////            AesUtil.decrypt(result);
//            return AesUtil.decrypt(result);
//        }
//
//        /**
//         * 显示耗时等待菊花
//         */
//        @android.webkit.JavascriptInterface
//        public void showLoading() {
//            showWaitingDialog(true);
//        }
//
//        /**
//         * 关闭耗时等待菊花
//         */
//        @android.webkit.JavascriptInterface
//        public void hideLoading() {
//            hideWaitingDialog();
//        }
//
//        /**
//         * 错误代码提示
//         */
//        @android.webkit.JavascriptInterface
//        public void handleError(String message) {
//            // TODO: 2020/6/30 弹一个Dialog，内容为message
//            Toast.makeText(mActivity, message, Toast.LENGTH_SHORT).show();
//        }
//    }

    private long timeMillis;//退出时间戳

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void clickWebViewBack(Event.clickWebViewBack event) {
        if (web_context.canGoBack()) {
            web_context.goBack();
        } else {
            if ((System.currentTimeMillis() - timeMillis) > 2000) {
                Toast.makeText(getActivity(), "再按一次退出程序", Toast.LENGTH_SHORT).show();
                timeMillis = System.currentTimeMillis();
            } else {
                AppManager.get().AppExit(getActivity());
            }
        }
    }


    @Override
    public void onDestroyView() {
        if (null != sonicSession) {
            sonicSession.destroy();
            sonicSession = null;
        }
        web_context.loadUrl("javascript:localStorage.clear()");
        web_context.loadDataWithBaseURL(null, "", "text/html", "utf-8", null);
        web_context.clearCache(true);
        web_context.clearHistory();
        web_context.destroy();
        web_context = null;
        super.onDestroyView();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            web_context.reload();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }
}
