package com.huolongluo.luo.huolongluo.bean;

import android.text.TextUtils;
import android.util.Log;

import com.huolongluo.luo.huolongluo.share.Share;
import com.huolongluo.luo.util.TUIKitUtil;
import com.huolongluo.luo.util.ToastSimple;
import com.luck.picture.lib.entity.LocalMedia;

import java.io.Serializable;
import java.util.List;

import cn.bmob.v3.BmobObject;
import cn.bmob.v3.datatype.BmobFile;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.SaveListener;
import cn.bmob.v3.listener.UpdateListener;
import cn.bmob.v3.listener.UploadBatchListener;

public class UserBasicInfoBean extends BmobObject implements Serializable {
    private static final String TAG = "UserBasicInfoBean";
    private String username;//用户名
    private List<String> userTopPic;//用户置顶图片
    private String nickName;//昵称
    private String wechat;//微信
    private String headPic;//头像
    private boolean isVip;//是否VIP
    private String address;//住址
    private String zujiAddress;//祖籍地址
    private String xueli;//学历
    private String work;//工作
    private int height;//身高
    private int weight;//体重
    private String birthday;//生日
    private String biYeSchool;//毕业院校
    private String yearInCome;//年收入
    private String sex;//性别
    private boolean isUpWall;//是否上墙
    private boolean isHeYanWeChat;//微信号是否已核验通过

    private int layoutType;//在首页Item的布局。0正常布局，1进入单身广场布局，2推荐上墙布局

    public String getUsername() {
        if (username == null) {
            return "";
        }
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<String> getUserTopPic() {
        return userTopPic;
    }

    public void setUserTopPic(List<String> userTopPic) {
        this.userTopPic = userTopPic;
    }

    public String getNickName() {
        if (nickName == null) {
            return "";
        }
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getWechat() {
        if (wechat == null) {
            return "";
        }
        return wechat;
    }

    public void setWechat(String wechat) {
        this.wechat = wechat;
    }

    public String getHeadPic() {
        if (headPic == null) {
            return "";
        }
        return headPic;
    }

    public void setHeadPic(String headPic) {
        this.headPic = headPic;
    }

    public boolean isVip() {
        return isVip;
    }

    public void setVip(boolean vip) {
        isVip = vip;
    }

    public String getAddress() {
        if (address == null) {
            return "";
        }
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZujiAddress() {
        if (zujiAddress == null) {
            return "";
        }
        return zujiAddress;
    }

    public void setZujiAddress(String zujiAddress) {
        this.zujiAddress = zujiAddress;
    }

    public String getXueli() {
        if (xueli == null) {
            return "";
        }
        return xueli;
    }

    public void setXueli(String xueli) {
        this.xueli = xueli;
    }

    public String getWork() {
        if (work == null) {
            return "";
        }
        return work;
    }

    public void setWork(String work) {
        this.work = work;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getBirthday() {
        if (birthday == null) {
            return "";
        }
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getBiYeSchool() {
        if (biYeSchool == null) {
            return "";
        }
        return biYeSchool;
    }

    public void setBiYeSchool(String biYeSchool) {
        this.biYeSchool = biYeSchool;
    }

    public String getYearInCome() {
        if (yearInCome == null) {
            return "";
        }
        return yearInCome;
    }

    public void setYearInCome(String yearInCome) {
        this.yearInCome = yearInCome;
    }

    public String getSex() {
        if (sex == null) {
            return "";
        }
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public boolean isUpWall() {
        return isUpWall;
    }

    public void setUpWall(boolean upWall) {
        isUpWall = upWall;
    }

    public boolean isHeYanWeChat() {
        return isHeYanWeChat;
    }

    public void setHeYanWeChat(boolean heYanWeChat) {
        isHeYanWeChat = heYanWeChat;
    }

    public int getLayoutType() {
        return layoutType;
    }

    public void setLayoutType(int layoutType) {
        this.layoutType = layoutType;
    }

    @Override
    public String toString() {
        return "UserBasicInfoBean{" +
                "username='" + username + '\'' +
                ", userTopPic=" + userTopPic +
                ", nickName='" + nickName + '\'' +
                ", wechat='" + wechat + '\'' +
                ", headPic='" + headPic + '\'' +
                ", isVip=" + isVip +
                ", address='" + address + '\'' +
                ", zujiAddress='" + zujiAddress + '\'' +
                ", xueli='" + xueli + '\'' +
                ", work='" + work + '\'' +
                ", height=" + height +
                ", weight=" + weight +
                ", birthday='" + birthday + '\'' +
                ", biYeSchool='" + biYeSchool + '\'' +
                ", yearInCome='" + yearInCome + '\'' +
                ", sex='" + sex + '\'' +
                ", isUpWall=" + isUpWall +
                ", isHeYanWeChat=" + isHeYanWeChat +
                ", layoutType=" + layoutType +
                '}';
    }

    /**
     * 自身用户基本信息是否已完善
     */
    public static boolean isCompleteMyBasicInfo() {
        if (TextUtils.isEmpty(Share.get().getBmobCurrentUserWechat())
                || TextUtils.isEmpty(Share.get().getBirthDay())
                || TextUtils.isEmpty(Share.get().getSex())
                || (Share.get().getHeight() == 0)
                || (Share.get().getWeight() == 0)
                || TextUtils.isEmpty(Share.get().getAddress())
                || TextUtils.isEmpty(Share.get().getZujiAddress())
                || TextUtils.isEmpty(Share.get().getXueli())
                || TextUtils.isEmpty(Share.get().getBiyeSchool())
                || TextUtils.isEmpty(Share.get().getWork())
                || TextUtils.isEmpty(Share.get().getYearInCome())) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * 判断某个用户基本信息资料是否完整
     */
    public static boolean isCompleteBasicInfo(UserBasicInfoBean userBasicInfoBean) {
        if (TextUtils.isEmpty(userBasicInfoBean.getWechat())
                || TextUtils.isEmpty(userBasicInfoBean.getBirthday())
                || TextUtils.isEmpty(userBasicInfoBean.getSex())
                || (userBasicInfoBean.getHeight() == 0)
                || (userBasicInfoBean.getWeight() == 0)
                || TextUtils.isEmpty(userBasicInfoBean.getAddress())
                || TextUtils.isEmpty(userBasicInfoBean.getZujiAddress())
                || TextUtils.isEmpty(userBasicInfoBean.getXueli())
                || TextUtils.isEmpty(userBasicInfoBean.getBiYeSchool())
                || TextUtils.isEmpty(userBasicInfoBean.getWork())
                || TextUtils.isEmpty(userBasicInfoBean.getYearInCome())) {
            return false;
        } else {
            return true;
        }
    }

    public static void updateLocalSpUserBasicInfo(UserBasicInfoBean userBasicInfoBean) {
        if (userBasicInfoBean != null) {
            Share.get().setUserName(userBasicInfoBean.getUsername());
            Share.get().setHeadPic(userBasicInfoBean.getHeadPic());
            Share.get().setBirthDay(userBasicInfoBean.getBirthday());
            Share.get().setNickName(userBasicInfoBean.getNickName());
            Share.get().setBmobCurrentUserWechat(userBasicInfoBean.getWechat());
            Share.get().setHeight(userBasicInfoBean.getHeight());
            Share.get().setWeight(userBasicInfoBean.getWeight());
            Share.get().setAddress(userBasicInfoBean.getAddress());
            Share.get().setZujiAddress(userBasicInfoBean.getZujiAddress());
            Share.get().setXueli(userBasicInfoBean.getXueli());
            Share.get().setBiyeSchool(userBasicInfoBean.getBiYeSchool());
            Share.get().setWork(userBasicInfoBean.getWork());
            Share.get().setYearInCome(userBasicInfoBean.getYearInCome());
        }
    }

    /**
     * 添加一行用户详情数据到数据库
     */
    public static void createUserDetailInfoBean() {
        if (!TextUtils.isEmpty(Share.get().getUserName())) {
            UserBasicInfoBean userBasic = new UserBasicInfoBean();
//            UserDetailInfoBean userDetailInfo = new UserDetailInfoBean();
            userBasic.setUsername(Share.get().getUserName());
            userBasic.setNickName(Share.get().getNickName());
            userBasic.setWechat("");
            userBasic.setHeadPic(Share.get().getHeadPic());
            userBasic.setBirthday("");
            userBasic.setSex(Share.get().getSex());
            userBasic.setAddress("");
            userBasic.setZujiAddress("");
            userBasic.setXueli("");
            userBasic.setWork("");
            userBasic.setHeight(0);
            userBasic.setWeight(0);
            userBasic.setBiYeSchool("");
            userBasic.setYearInCome("");
            userBasic.setHeYanWeChat(false);

            userBasic.save(new SaveListener<String>() {
                @Override
                public void done(String objectId, BmobException e) {
                    if (e == null) {
                        Log.e(TAG, "添加用户详情数据成功，返回objectId为：" + objectId);
                    } else {
                        Log.e(TAG, "done: 创建用户详情数据失败：" + e.getMessage());
                    }
                }
            });
        }
    }


    public static void saveUserHeadPic(List<LocalMedia> selectList, UserBasicInfoBean p2) {
        if (p2 == null || selectList.isEmpty()) {
            return;
        }

        final String[] filePaths = new String[selectList.size()];
        for (int i = 0; i < selectList.size(); i++) {
//            filePaths[i] = selectList.get(i).getPath();
            filePaths[i] = selectList.get(i).getRealPath();
        }

        //批量上传文件
        BmobFile.uploadBatch(filePaths, new UploadBatchListener() {
            @Override
            public void onSuccess(List<BmobFile> files, List<String> urls) {
                //1、files-上传完成后的BmobFile集合，是为了方便大家对其上传后的数据进行操作，例如你可以将该文件保存到表中
                //2、urls-上传文件的完整url地址
                if (urls.size() == filePaths.length) {//如果数量相等，则代表文件全部上传完成
                    //do something
                    Log.e(TAG, "onSuccess: 图片上传完成：" + urls.size() + " || " + urls.toString());
                    Share.get().setHeadPic(urls.get(0));
                    UserBasicInfoBean.updateUserHeaderPic(p2);
                }
            }

            @Override
            public void onError(int statuscode, String errormsg) {
                ToastSimple.show("错误码" + statuscode + ",错误描述：" + errormsg);
                Log.e(TAG, "错误码" + statuscode + ",错误描述：" + errormsg);
            }

            @Override
            public void onProgress(int curIndex, int curPercent, int total, int totalPercent) {
                //1、curIndex--表示当前第几个文件正在上传
                //2、curPercent--表示当前上传文件的进度值（百分比）
                //3、total--表示总的上传文件数
                //4、totalPercent--表示总的上传进度（百分比）
            }
        });
    }

    public static void updateUserHeaderPic(UserBasicInfoBean p2) {
        p2.setHeadPic(Share.get().getHeadPic());
        p2.update(p2.getObjectId(), new UpdateListener() {
            @Override
            public void done(BmobException e) {
                if (e == null) {
                    Log.e(TAG, "头像更新成功:" + p2.getUpdatedAt());
                    TUIKitUtil.updateProfile(Share.get().getHeadPic(), Share.get().getNickName());
                } else {
                    Log.e(TAG, "头像更新失败：" + e.getMessage());
                }
            }
        });
    }
}
