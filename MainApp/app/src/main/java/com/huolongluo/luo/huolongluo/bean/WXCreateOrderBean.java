package com.huolongluo.luo.huolongluo.bean;

public class WXCreateOrderBean {

    private String code;
    private String message;
    private String prepayId;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPrepayId() {
        return prepayId;
    }

    public void setPrepayId(String prepayId) {
        this.prepayId = prepayId;
    }

    @Override
    public String toString() {
        return "WXCreateOrderBean{" +
                "code='" + code + '\'' +
                ", message='" + message + '\'' +
                ", prepayId='" + prepayId + '\'' +
                '}';
    }
}
