package com.huolongluo.luo.huolongluo.share;

import com.cocosw.favor.FavorAdapter;
import com.huolongluo.luo.base.s;

/**
 * Created by 火龙裸先生 on 2020/3/12 0011.
 */

public class Share {
    public static ShareData get() {
        return new FavorAdapter.Builder(s.app()).build().create(ShareData.class);
    }
}
