package com.huolongluo.luo.huolongluo.ui.activity.publish;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;

import com.gyf.immersionbar.ImmersionBar;
import com.huolongluo.luo.R;
import com.huolongluo.luo.huolongluo.base.BaseActivity;
import com.huolongluo.luo.huolongluo.bean.DisCoverBean;
import com.huolongluo.luo.huolongluo.share.Share;
import com.huolongluo.luo.huolongluo.ui.adapter.GridImageAdapter;
import com.huolongluo.luo.util.ToastSimple;
import com.huolongluo.luo.util.engine.MyGlideEngine;
import com.huolongluo.luo.widget.FullyGridLayoutManager;
import com.jakewharton.rxbinding.widget.RxTextView;
import com.luck.picture.lib.animators.AnimationType;
import com.luck.picture.lib.basic.PictureSelectionModel;
import com.luck.picture.lib.basic.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.config.PictureSelectionConfig;
import com.luck.picture.lib.config.SelectLimitType;
import com.luck.picture.lib.config.SelectMimeType;
import com.luck.picture.lib.config.SelectModeConfig;
import com.luck.picture.lib.decoration.GridSpacingItemDecoration;
import com.luck.picture.lib.engine.ImageEngine;
import com.luck.picture.lib.engine.UriToFileTransformEngine;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.entity.MediaExtraInfo;
import com.luck.picture.lib.interfaces.OnExternalPreviewEventListener;
import com.luck.picture.lib.interfaces.OnKeyValueResultCallbackListener;
import com.luck.picture.lib.interfaces.OnResultCallbackListener;
import com.luck.picture.lib.interfaces.OnSelectLimitTipsListener;
import com.luck.picture.lib.language.LanguageConfig;
import com.luck.picture.lib.style.PictureSelectorStyle;
import com.luck.picture.lib.utils.DensityUtil;
import com.luck.picture.lib.utils.MediaUtils;
import com.luck.picture.lib.utils.SandboxTransformUtils;
import com.luck.picture.lib.utils.ToastUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import cn.bmob.v3.datatype.BmobFile;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.SaveListener;
import cn.bmob.v3.listener.UploadBatchListener;

/**
 * <p>
 * Created by 火龙裸 on 2017/9/6.
 */

public class PublishActivity extends BaseActivity
{
    private static final String TAG = "PublishActivity";
    @BindView(R.id.iv_left)
    ImageView iv_left;
    @BindView(R.id.toolbar_center_title)
    TextView toolbar_center_title;
    @BindView(R.id.tv_right)
    TextView tv_right;
    @BindView(R.id.et_content)
    EditText et_content;
    @BindView(R.id.rv_publish)
    RecyclerView rv_publish;

    private boolean isHasLiftDelete;
    private boolean needScaleSmall = false;
    private boolean needScaleBig = true;

    private GridImageAdapter gridImageAdapter;

    private int maxSelectVideoNum = 1; // 1视频
    private int maxSelectNum = 9;
    private int animationMode = AnimationType.DEFAULT_ANIMATION;
    private ImageEngine imageEngine;
    private PictureSelectorStyle selectorStyle;
    private int chooseMode = SelectMimeType.ofImage();
    private int language = LanguageConfig.UNKNOWN_LANGUAGE;
    private List<LocalMedia> selectList = new ArrayList<>();
    private int resultMode = 0;//LAUNCHER_RESULT = 0、ACTIVITY_RESULT = 1、CALLBACK_RESULT = 2
    private ActivityResultLauncher<Intent> launcherResult;

    private String content = "";
    private List<String> pictureUrls = new ArrayList<>();
    private String picturePath = "";
    private String videoPath = "";

    @Override
    protected int getContentViewId()
    {
        return R.layout.activity_publish;
    }

    @Override
    protected void injectDagger()
    {
        activityComponent().inject(this);
    }

    private void initToolBar()
    {
        ImmersionBar.with(this).statusBarDarkFont(true).init();
        toolbar_center_title.setText("");
        iv_left.setVisibility(View.VISIBLE);
        tv_right.setVisibility(View.VISIBLE);
        tv_right.setText("发布");
    }

    @Override
    protected void initViewsAndEvents(@Nullable Bundle savedInstanceState) {
        initToolBar();
        
        // 点击返回键
        eventClick(iv_left).subscribe(o -> close());
        RxTextView.textChanges(et_content).map(CharSequence::toString).subscribe(s -> content = s.trim());
        eventClick(tv_right).subscribe(o -> {
            if (content.isEmpty())
            {
                ToastSimple.show("请输入内容！", 2);
                return;
            }
            showProgressDialog("");
            if (!selectList.isEmpty())
            {
                Log.e(TAG, "initViewsAndEvents: 选择的图片数量：" + selectList.size() + " " + selectList.get(0).getFileName());
                publish();
            } else {
                createDongTai();
            }

//            if (!videoPath.isEmpty())
//            {
//                String filePath = selectList.get(0).getPath();
//                String keyPath = System.currentTimeMillis() + "_video";
//                getQiniuToken(filePath, keyPath);
//                return;
//            }
            if (selectList.size() == 1)
            {
                String filePath = selectList.get(0).getCompressPath();
                String keyPath = System.currentTimeMillis() + "_picture";
                //单张图片文件
//                getQiniuToken(filePath, keyPath);
            }
            else
            {
                for (int i = 0; i < selectList.size(); i++)
                {
                    String filePath = selectList.get(i).getCompressPath();
                    String keyPath = System.currentTimeMillis() + "_picture" + i;
//                    getQiniuToken(filePath, keyPath);
                }
            }

        });


        // 注册需要写在onCreate或Fragment onAttach里，否则会报java.lang.IllegalStateException异常
        launcherResult = createActivityResultLauncher();

        rv_publish.setNestedScrollingEnabled(false);
        rv_publish.setFocusable(false);
        FullyGridLayoutManager manager = new FullyGridLayoutManager(PublishActivity.this, 3, GridLayoutManager.VERTICAL, false);
        rv_publish.setLayoutManager(manager);
        RecyclerView.ItemAnimator itemAnimator = rv_publish.getItemAnimator();
        if (itemAnimator != null) {
            ((SimpleItemAnimator) itemAnimator).setSupportsChangeAnimations(false);
        }
        rv_publish.addItemDecoration(new GridSpacingItemDecoration(3,
                DensityUtil.dip2px(this, 10), false));
        gridImageAdapter = new GridImageAdapter(PublishActivity.this, selectList);
        gridImageAdapter.setSelectMax(maxSelectNum);
        rv_publish.setAdapter(gridImageAdapter);
        if (savedInstanceState != null && savedInstanceState.getParcelableArrayList("selectorList") != null) {
            selectList.clear();
            selectList.addAll(savedInstanceState.getParcelableArrayList("selectorList"));
        }

        imageEngine = MyGlideEngine.createGlideEngine();

        gridImageAdapter.setOnItemClickListener(new GridImageAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                // 预览图片、视频、音频
                PictureSelector.create(PublishActivity.this)
                        .openPreview()
                        .setImageEngine(imageEngine)
                        .setSelectorUIStyle(selectorStyle)
                        .setLanguage(language)
                        .isPreviewFullScreenMode(true)
                        .setExternalPreviewEventListener(new MyExternalPreviewEventListener(gridImageAdapter))
                        .startActivityPreview(position, true, gridImageAdapter.getData());
            }

            @Override
            public void openPicture() {
                        // 进入相册0
                        PictureSelectionModel selectionModel = PictureSelector.create(PublishActivity.this)
                                .openGallery(chooseMode)
                                .setSelectorUIStyle(selectorStyle)
                                .setImageEngine(imageEngine)
//                                .setCropEngine(getCropFileEngine())
//                                .setCompressEngine(getCompressFileEngine())
                                .setSandboxFileEngine(new MeSandboxFileEngine())
                                .setCameraInterceptListener(null)
                                .setRecordAudioInterceptListener(null)
                                .setSelectLimitTipsListener(new MeOnSelectLimitTipsListener())
                                .setEditMediaInterceptListener(null)
                                .setPermissionDescriptionListener(null)
                                .setPreviewInterceptListener(null)
                                .setPermissionDeniedListener(null)
                                .setAddBitmapWatermarkListener(null)
                                .setVideoThumbnailListener(null)
//                              .setQueryFilterListener(new OnQueryFilterListener() {
//                                    @Override
//                                    public boolean onFilter(String absolutePath) {
//                                        return PictureMimeType.isUrlHasVideo(absolutePath);
//                                    }
//                                })
                                //.setExtendLoaderEngine(getExtendLoaderEngine())
                                .setInjectLayoutResourceListener(null)
                                .setSelectionMode(SelectModeConfig.MULTIPLE)
                                .setLanguage(language)
                                .setQuerySortOrder("")
                                .setOutputCameraDir("")
                                .setOutputAudioDir("")
                                .setQuerySandboxDir("")
                                .isDisplayTimeAxis(true)
                                .isOnlyObtainSandboxDir(false)
                                .isPageStrategy(true)
                                .isOriginalControl(false)
                                .isDisplayCamera(true)
                                .isOpenClickSound(false)
                                .setSkipCropMimeType("")
                                .isFastSlidingSelect(true)
                                //.setOutputCameraImageFileName("luck.jpeg")
                                //.setOutputCameraVideoFileName("luck.mp4")
                                .isWithSelectVideoImage(true)
                                .isPreviewFullScreenMode(true)
                                .isPreviewZoomEffect(true)
                                .isPreviewImage(true)
                                .isPreviewVideo(true)
                                .isPreviewAudio(true)
                                //.setQueryOnlyMimeType(PictureMimeType.ofGIF())
                                .isMaxSelectEnabledMask(true)
                                .isDirectReturnSingle(false)
                                .setMaxSelectNum(maxSelectNum)
                                .setMaxVideoSelectNum(maxSelectVideoNum)
                                .setRecyclerAnimationMode(animationMode)
                                .isGif(false)
                                .setSelectedData(gridImageAdapter.getData());
                        forSelectResult(selectionModel);
            }
        });

//        gridImageAdapter.setItemLongClickListener(new GridImageAdapter.OnItemLongClickListener() {
//            @Override
//            public void onItemLongClick(RecyclerView.ViewHolder holder, int position, View v) {
//                int itemViewType = holder.getItemViewType();
//                if (itemViewType != GridImageAdapter.TYPE_CAMERA) {
//                    mItemTouchHelper.startDrag(holder);
//                }
//            }
//        });
//        // 绑定拖拽事件
//        mItemTouchHelper.attachToRecyclerView(rv_publish);
    }



    public void publish() {
        final String[] filePaths = new String[selectList.size()];
        for (int i = 0; i < selectList.size(); i++) {
//            filePaths[i] = selectList.get(i).getPath();
            filePaths[i] = selectList.get(i).getRealPath();
        }
        Log.e(TAG, "publish: 选择了图片：" + selectList.size() + "张 " + selectList.get(0).getPath() + "  最终文件为：" + filePaths.length);

        //批量上传文件
        BmobFile.uploadBatch(filePaths, new UploadBatchListener() {
            @Override
            public void onSuccess(List<BmobFile> files,List<String> urls) {
                //1、files-上传完成后的BmobFile集合，是为了方便大家对其上传后的数据进行操作，例如你可以将该文件保存到表中
                //2、urls-上传文件的完整url地址
                if(urls.size()==filePaths.length){//如果数量相等，则代表文件全部上传完成
                    //do something
                    Log.e(TAG, "onSuccess: 图片上传完成：" + urls.size() + " || " + urls.toString());
                    pictureUrls.addAll(urls);
                    createDongTai();
                }
            }

            @Override
            public void onError(int statuscode, String errormsg) {
                ToastSimple.show("错误码"+statuscode +",错误描述："+errormsg);
                Log.e(TAG, "错误码"+statuscode +",错误描述："+errormsg);
                hideProgressDialog();
            }

            @Override
            public void onProgress(int curIndex, int curPercent, int total,int totalPercent) {
                //1、curIndex--表示当前第几个文件正在上传
                //2、curPercent--表示当前上传文件的进度值（百分比）
                //3、total--表示总的上传文件数
                //4、totalPercent--表示总的上传进度（百分比）
            }
        });
    }

    private void createDongTai() {
        DisCoverBean disCoverBean = new DisCoverBean();
        disCoverBean.setUsername(Share.get().getUserName());
        disCoverBean.setNickName(Share.get().getNickName());
        disCoverBean.setSex(Share.get().getSex());
        disCoverBean.setUserHeaderImg(Share.get().getHeadPic());
        disCoverBean.setDongtaiText(content);
        disCoverBean.setDongtaiPic(pictureUrls);
        disCoverBean.save(new SaveListener<String>() {
            @Override
            public void done(String objectId, BmobException e) {
                if(e==null){
                    Log.e(TAG, "添加动态数据成功，返回objectId为："+objectId);
                    close();
                }else{
                    Log.e(TAG, "添加动态数据失败：" + e.getMessage());
                }
                hideProgressDialog();
            }
        });
    }

//    private final ItemTouchHelper mItemTouchHelper = new ItemTouchHelper(new ItemTouchHelper.Callback() {
//        @Override
//        public boolean isLongPressDragEnabled() {
//            return true;
//        }
//
//        @Override
//        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
//        }
//
//        @Override
//        public int getMovementFlags(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder) {
//            int itemViewType = viewHolder.getItemViewType();
//            if (itemViewType != GridImageAdapter.TYPE_CAMERA) {
//                viewHolder.itemView.setAlpha(0.7f);
//            }
//            return makeMovementFlags(ItemTouchHelper.DOWN | ItemTouchHelper.UP
//                    | ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT, 0);
//        }
//
//        @Override
//        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
//            try {
//                //得到item原来的position
//                int fromPosition = viewHolder.getAbsoluteAdapterPosition();
//                //得到目标position
//                int toPosition = target.getAbsoluteAdapterPosition();
//                int itemViewType = target.getItemViewType();
//                if (itemViewType != GridImageAdapter.TYPE_CAMERA) {
//                    if (fromPosition < toPosition) {
//                        for (int i = fromPosition; i < toPosition; i++) {
//                            Collections.swap(gridImageAdapter.getData(), i, i + 1);
//                        }
//                    } else {
//                        for (int i = fromPosition; i > toPosition; i--) {
//                            Collections.swap(gridImageAdapter.getData(), i, i - 1);
//                        }
//                    }
//                    gridImageAdapter.notifyItemMoved(fromPosition, toPosition);
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            return true;
//        }
//
//        @Override
//        public void onChildDraw(@NonNull Canvas c, @NonNull RecyclerView recyclerView,
//                                @NonNull RecyclerView.ViewHolder viewHolder, float dx, float dy, int actionState, boolean isCurrentlyActive) {
//            int itemViewType = viewHolder.getItemViewType();
//            if (itemViewType != GridImageAdapter.TYPE_CAMERA) {
//                if (needScaleBig) {
//                    needScaleBig = false;
//                    AnimatorSet animatorSet = new AnimatorSet();
//                    animatorSet.playTogether(
//                            ObjectAnimator.ofFloat(viewHolder.itemView, "scaleX", 1.0F, 1.1F),
//                            ObjectAnimator.ofFloat(viewHolder.itemView, "scaleY", 1.0F, 1.1F));
//                    animatorSet.setDuration(50);
//                    animatorSet.setInterpolator(new LinearInterpolator());
//                    animatorSet.start();
//                    animatorSet.addListener(new AnimatorListenerAdapter() {
//                        @Override
//                        public void onAnimationEnd(Animator animation) {
//                            needScaleSmall = true;
//                        }
//                    });
//                }
//                int targetDy = tvDeleteText.getTop() - viewHolder.itemView.getBottom();
//                if (dy >= targetDy) {
//                    //拖到删除处
//                    mDragListener.deleteState(true);
//                    if (isHasLiftDelete) {
//                        //在删除处放手，则删除item
//                        viewHolder.itemView.setVisibility(View.INVISIBLE);
//                        gridImageAdapter.delete(viewHolder.getAbsoluteAdapterPosition());
//                        resetState();
//                        return;
//                    }
//                } else {
//                    //没有到删除处
//                    if (View.INVISIBLE == viewHolder.itemView.getVisibility()) {
//                        //如果viewHolder不可见，则表示用户放手，重置删除区域状态
//                        mDragListener.dragState(false);
//                    }
//                    mDragListener.deleteState(false);
//                }
//                super.onChildDraw(c, recyclerView, viewHolder, dx, dy, actionState, isCurrentlyActive);
//            }
//        }
//
//        @Override
//        public void onSelectedChanged(@Nullable RecyclerView.ViewHolder viewHolder, int actionState) {
//            int itemViewType = viewHolder != null ? viewHolder.getItemViewType() : GridImageAdapter.TYPE_CAMERA;
//            if (itemViewType != GridImageAdapter.TYPE_CAMERA) {
//                if (ItemTouchHelper.ACTION_STATE_DRAG == actionState) {
//                    mDragListener.dragState(true);
//                }
//                super.onSelectedChanged(viewHolder, actionState);
//            }
//        }
//
//        @Override
//        public long getAnimationDuration(@NonNull RecyclerView recyclerView, int animationType, float animateDx, float animateDy) {
//            isHasLiftDelete = true;
//            return super.getAnimationDuration(recyclerView, animationType, animateDx, animateDy);
//        }
//
//        @Override
//        public void clearView(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder) {
//            int itemViewType = viewHolder.getItemViewType();
//            if (itemViewType != GridImageAdapter.TYPE_CAMERA) {
//                viewHolder.itemView.setAlpha(1.0F);
//                if (needScaleSmall) {
//                    needScaleSmall = false;
//                    AnimatorSet animatorSet = new AnimatorSet();
//                    animatorSet.playTogether(
//                            ObjectAnimator.ofFloat(viewHolder.itemView, "scaleX", 1.1F, 1.0F),
//                            ObjectAnimator.ofFloat(viewHolder.itemView, "scaleY", 1.1F, 1.0F));
//                    animatorSet.setInterpolator(new LinearInterpolator());
//                    animatorSet.setDuration(50);
//                    animatorSet.start();
//                    animatorSet.addListener(new AnimatorListenerAdapter() {
//                        @Override
//                        public void onAnimationEnd(Animator animation) {
//                            needScaleBig = true;
//                        }
//                    });
//                }
//                super.clearView(recyclerView, viewHolder);
//                gridImageAdapter.notifyItemChanged(viewHolder.getAbsoluteAdapterPosition());
//                resetState();
//            }
//        }
//    });

    /**
     * 重置
     */
//    private void resetState() {
//        isHasLiftDelete = false;
//        mDragListener.deleteState(false);
//        mDragListener.dragState(false);
//    }

//    private final DragListener mDragListener = new DragListener() {
//        @Override
//        public void deleteState(boolean isDelete) {
//            if (isDelete) {
//                if (!TextUtils.equals(getString(R.string.app_let_go_drag_delete), tvDeleteText.getText())) {
//                    tvDeleteText.setText(getString(R.string.app_let_go_drag_delete));
//                    tvDeleteText.setCompoundDrawablesRelativeWithIntrinsicBounds(0, R.drawable.ic_dump_delete, 0, 0);
//                }
//            } else {
//                if (!TextUtils.equals(getString(R.string.app_drag_delete), tvDeleteText.getText())) {
//                    tvDeleteText.setText(getString(R.string.app_drag_delete));
//                    tvDeleteText.setCompoundDrawablesRelativeWithIntrinsicBounds(0, R.drawable.ic_normal_delete, 0, 0);
//                }
//            }
//
//        }
//
//        @Override
//        public void dragState(boolean isStart) {
//            if (isStart) {
//                if (tvDeleteText.getAlpha() == 0F) {
//                    ObjectAnimator alphaAnimator = ObjectAnimator.ofFloat(tvDeleteText, "alpha", 0F, 1F);
//                    alphaAnimator.setInterpolator(new LinearInterpolator());
//                    alphaAnimator.setDuration(120);
//                    alphaAnimator.start();
//                }
//            } else {
//                if (tvDeleteText.getAlpha() == 1F) {
//                    ObjectAnimator alphaAnimator = ObjectAnimator.ofFloat(tvDeleteText, "alpha", 1F, 0F);
//                    alphaAnimator.setInterpolator(new LinearInterpolator());
//                    alphaAnimator.setDuration(120);
//                    alphaAnimator.start();
//                }
//            }
//        }
//    };

    private void forSelectResult(PictureSelectionModel model) {
        switch (resultMode) {
            case 1:
                model.forResult(PictureConfig.CHOOSE_REQUEST);
                break;
            case 2:
                model.forResult(new MeOnResultCallbackListener());
                break;
            default:
                model.forResult(launcherResult);
                break;
        }
    }

    /**
     * 创建一个ActivityResultLauncher
     *
     * @return
     */
    private ActivityResultLauncher<Intent> createActivityResultLauncher() {
        return registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult result) {
                        int resultCode = result.getResultCode();
                        if (resultCode == RESULT_OK) {
                            ArrayList<LocalMedia> selectList = PictureSelector.obtainSelectorList(result.getData());
                            analyticalSelectResults(selectList);
                        } else if (resultCode == RESULT_CANCELED) {
                            Log.i(TAG, "onActivityResult PictureSelector Cancel");
                        }
                    }
                });
    }

    /**
     * 选择结果
     */
    private class MeOnResultCallbackListener implements OnResultCallbackListener<LocalMedia> {
        @Override
        public void onResult(ArrayList<LocalMedia> result) {
            analyticalSelectResults(result);
        }

        @Override
        public void onCancel() {
            Log.i(TAG, "PictureSelector Cancel");
        }
    }

    /**
     * 处理选择结果
     *
     * @param result
     */
    private void analyticalSelectResults(ArrayList<LocalMedia> result) {
        selectList.clear();
        selectList.addAll(result);
        for (LocalMedia media : result) {
            if (media.getWidth() == 0 || media.getHeight() == 0) {
                if (PictureMimeType.isHasImage(media.getMimeType())) {
                    MediaExtraInfo imageExtraInfo = MediaUtils.getImageSize(this, media.getPath());
                    media.setWidth(imageExtraInfo.getWidth());
                    media.setHeight(imageExtraInfo.getHeight());
                } else if (PictureMimeType.isHasVideo(media.getMimeType())) {
                    MediaExtraInfo videoExtraInfo = MediaUtils.getVideoSize(this, media.getPath());
                    media.setWidth(videoExtraInfo.getWidth());
                    media.setHeight(videoExtraInfo.getHeight());
                }
            }
            Log.i(TAG, "文件名: " + media.getFileName());
            Log.i(TAG, "是否压缩:" + media.isCompressed());
            Log.i(TAG, "压缩:" + media.getCompressPath());
            Log.i(TAG, "初始路径:" + media.getPath());
            Log.i(TAG, "绝对路径:" + media.getRealPath());
            Log.i(TAG, "是否裁剪:" + media.isCut());
            Log.i(TAG, "裁剪:" + media.getCutPath());
            Log.i(TAG, "是否开启原图:" + media.isOriginal());
            Log.i(TAG, "原图路径:" + media.getOriginalPath());
            Log.i(TAG, "沙盒路径:" + media.getSandboxPath());
            Log.i(TAG, "水印路径:" + media.getWatermarkPath());
            Log.i(TAG, "视频缩略图:" + media.getVideoThumbnailPath());
            Log.i(TAG, "原始宽高: " + media.getWidth() + "x" + media.getHeight());
            Log.i(TAG, "裁剪宽高: " + media.getCropImageWidth() + "x" + media.getCropImageHeight());
            Log.i(TAG, "文件大小: " + media.getSize());
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                boolean isMaxSize = result.size() == gridImageAdapter.getSelectMax();
                int oldSize = gridImageAdapter.getData().size();
                gridImageAdapter.notifyItemRangeRemoved(0, isMaxSize ? oldSize + 1 : oldSize);
                gridImageAdapter.getData().clear();

                gridImageAdapter.getData().addAll(result);
                gridImageAdapter.notifyItemRangeInserted(0, result.size());
            }
        });
    }

    /**
     * 创建相机自定义输出目录
     *
     * @return
     */
//    private String getSandboxCameraOutputPath() {
//        if (cb_custom_sandbox.isChecked()) {
//            File externalFilesDir = this.getExternalFilesDir("");
//            File customFile = new File(externalFilesDir.getAbsolutePath(), "Sandbox");
//            if (!customFile.exists()) {
//                customFile.mkdirs();
//            }
//            return customFile.getAbsolutePath() + File.separator;
//        } else {
//            return "";
//        }
//    }
//
//    /**
//     * 创建音频自定义输出目录
//     *
//     * @return
//     */
//    private String getSandboxAudioOutputPath() {
//        if (cb_custom_sandbox.isChecked()) {
//            File externalFilesDir = this.getExternalFilesDir("");
//            File customFile = new File(externalFilesDir.getAbsolutePath(), "Sound");
//            if (!customFile.exists()) {
//                customFile.mkdirs();
//            }
//            return customFile.getAbsolutePath() + File.separator;
//        } else {
//            return "";
//        }
//    }

    /**
     * 创建自定义输出目录
     *
     * @return
     */
    private String getSandboxPath() {
        File externalFilesDir = this.getExternalFilesDir("");
        File customFile = new File(externalFilesDir.getAbsolutePath(), "Sandbox");
        if (!customFile.exists()) {
            customFile.mkdirs();
        }
        return customFile.getAbsolutePath() + File.separator;
    }

    /**
     * 创建自定义输出目录
     *
     * @return
     */
    private String getSandboxMarkDir() {
        File externalFilesDir = this.getExternalFilesDir("");
        File customFile = new File(externalFilesDir.getAbsolutePath(), "Mark");
        if (!customFile.exists()) {
            customFile.mkdirs();
        }
        return customFile.getAbsolutePath() + File.separator;
    }

    /**
     * 创建自定义输出目录
     *
     * @return
     */
    private String getVideoThumbnailDir() {
        File externalFilesDir = this.getExternalFilesDir("");
        File customFile = new File(externalFilesDir.getAbsolutePath(), "Thumbnail");
        if (!customFile.exists()) {
            customFile.mkdirs();
        }
        return customFile.getAbsolutePath() + File.separator;
    }

    /**
     * 外部预览监听事件
     */
    private static class MyExternalPreviewEventListener implements OnExternalPreviewEventListener {
        private final GridImageAdapter adapter;

        public MyExternalPreviewEventListener(GridImageAdapter adapter) {
            this.adapter = adapter;
        }

        @Override
        public void onPreviewDelete(int position) {
            adapter.remove(position);
            adapter.notifyItemRemoved(position);
        }

        @Override
        public boolean onLongPressDownload(LocalMedia media) {
            return false;
        }
    }

    /**
     * 自定义沙盒文件处理
     */
    private static class MeSandboxFileEngine implements UriToFileTransformEngine {

        @Override
        public void onUriToFileAsyncTransform(Context context, String srcPath, String mineType, OnKeyValueResultCallbackListener call) {
            if (call != null) {
                call.onCallback(srcPath, SandboxTransformUtils.copyPathToSandbox(context, srcPath, mineType));
            }
        }
    }

    /**
     * 拦截自定义提示
     */
    private static class MeOnSelectLimitTipsListener implements OnSelectLimitTipsListener {

        @Override
        public boolean onSelectLimitTips(Context context, PictureSelectionConfig config, int limitType) {
            if (limitType == SelectLimitType.SELECT_NOT_SUPPORT_SELECT_LIMIT) {
                ToastUtils.showToast(context, "暂不支持的选择类型");
                return true;
            }
            return false;
        }
    }

    /**
     * @describe：拖拽监听事件
     */
    public interface DragListener {
        /**
         * 是否将 item拖动到删除处，根据状态改变颜色
         *
         * @param isDelete
         */
        void deleteState(boolean isDelete);

        /**
         * 是否于拖拽状态
         *
         * @param isStart
         */
        void dragState(boolean isStart);
    }
}
