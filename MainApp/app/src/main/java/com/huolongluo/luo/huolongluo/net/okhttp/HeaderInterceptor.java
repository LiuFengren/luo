package com.huolongluo.luo.huolongluo.net.okhttp;

import com.huolongluo.luo.huolongluo.bean.WXCreateOrderEntity;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by 火龙裸 on 2018/8/17.
 */

public class HeaderInterceptor implements Interceptor
{
    @Override
    public Response intercept(Chain chain) throws IOException
    {
        Request original = chain.request();
        Request.Builder requestBuilder = original.newBuilder()//
                .header("Accept-Language", "zh-CN")//
                .header("Accept", "application/json");//
//                .header("Authorization", WXCreateOrderEntity.getAuthorizationHeader());////令牌
        Request request = requestBuilder.build();
        return chain.proceed(request);
    }
}
