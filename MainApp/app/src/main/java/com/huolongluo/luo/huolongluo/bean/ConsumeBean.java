package com.huolongluo.luo.huolongluo.bean;

import android.text.TextUtils;
import android.util.Log;

import com.huolongluo.luo.huolongluo.share.Share;

import cn.bmob.v3.BmobObject;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.QueryListener;

/**
 * 消费和分佣配置
 */
public class ConsumeBean extends BmobObject {
    private static final String TAG = "ConsumeBean";
    private String messagePrice;//一条消息消耗多少金币
    private String audioPrice;//一分钟语音消耗多少金币
    private String videoPrice;//一分钟视频消耗多少金币
    private String unlockWechatPrice;//解锁微信消耗多少金币
    private String upWallPrice;//上墙消耗多少金币

    private String profitIntegralUnit;//消耗对方一个金币可分佣多少积分

    private String newUserWillGetGold;//新注册用户将获得多少金币

    private String tiXianGetJiFenUnit;//他人提现后，邀请的人将获得多少积分比

    public String getMessagePrice() {
        if (TextUtils.isEmpty(messagePrice)) {
            return "0";
        }
        return messagePrice;
    }

    public void setMessagePrice(String messagePrice) {
        this.messagePrice = messagePrice;
    }

    public String getAudioPrice() {
        if (TextUtils.isEmpty(audioPrice)) {
            return "0";
        }
        return audioPrice;
    }

    public void setAudioPrice(String audioPrice) {
        this.audioPrice = audioPrice;
    }

    public String getVideoPrice() {
        if (TextUtils.isEmpty(videoPrice)) {
            return "0";
        }
        return videoPrice;
    }

    public void setVideoPrice(String videoPrice) {
        this.videoPrice = videoPrice;
    }

    public String getUnlockWechatPrice() {
        if (TextUtils.isEmpty(unlockWechatPrice)) {
            return "0";
        }
        return unlockWechatPrice;
    }

    public void setUnlockWechatPrice(String unlockWechatPrice) {
        this.unlockWechatPrice = unlockWechatPrice;
    }

    public String getUpWallPrice() {
        if (TextUtils.isEmpty(upWallPrice)) {
            return "0";
        }
        return upWallPrice;
    }

    public void setUpWallPrice(String upWallPrice) {
        this.upWallPrice = upWallPrice;
    }

    public String getProfitIntegralUnit() {
        if (TextUtils.isEmpty(profitIntegralUnit)) {
            return "0";
        }
        return profitIntegralUnit;
    }

    public void setProfitIntegralUnit(String profitIntegralUnit) {
        this.profitIntegralUnit = profitIntegralUnit;
    }

    public String getNewUserWillGetGold() {
        if (TextUtils.isEmpty(newUserWillGetGold)) {
            return "100";
        }
        return newUserWillGetGold;
    }

    public void setNewUserWillGetGold(String newUserWillGetGold) {
        this.newUserWillGetGold = newUserWillGetGold;
    }

    public String getTiXianGetJiFenUnit() {
        if (TextUtils.isEmpty(tiXianGetJiFenUnit)) {
            return "0.1";
        }
        return tiXianGetJiFenUnit;
    }

    public void setTiXianGetJiFenUnit(String tiXianGetJiFenUnit) {
        this.tiXianGetJiFenUnit = tiXianGetJiFenUnit;
    }

    @Override
    public String toString() {
        return "ConsumeBean{" +
                "messagePrice='" + messagePrice + '\'' +
                ", audioPrice='" + audioPrice + '\'' +
                ", videoPrice='" + videoPrice + '\'' +
                ", unlockWechatPrice='" + unlockWechatPrice + '\'' +
                ", upWallPrice='" + upWallPrice + '\'' +
                ", profitIntegralUnit='" + profitIntegralUnit + '\'' +
                ", newUserWillGetGold='" + newUserWillGetGold + '\'' +
                ", tiXianGetJiFenUnit='" + tiXianGetJiFenUnit + '\'' +
                '}';
    }

    /**
     * 查询消费分佣配置
     */
    public static void queryConsumeConfig() {
        //查找ConsumeBean表里面id为6b6c11c537的数据
        BmobQuery<ConsumeBean> bmobQuery = new BmobQuery<ConsumeBean>();
        bmobQuery.getObject("lMmzZZZb", new QueryListener<ConsumeBean>() {
            @Override
            public void done(ConsumeBean object, BmobException e) {
                if (e == null) {
                    Log.e(TAG, "查询消费分佣配置 查询消费分佣配置成功");
                    Share.get().setMessagePrice(object.getMessagePrice());
                    Share.get().setAudioPrice(object.getAudioPrice());
                    Share.get().setVideoPrice(object.getVideoPrice());
                    Share.get().setUpWallPrice(object.getUpWallPrice());
                    Share.get().setUnLockWechatPrice(object.getUnlockWechatPrice());
                    Share.get().setProfitIntegralUnit(object.getProfitIntegralUnit());
                    Share.get().setNewUserWillGetGold(object.getNewUserWillGetGold());
                    Share.get().setTixianGetJifenUnit(object.getTiXianGetJiFenUnit());
                } else {
                    Log.e(TAG, "查询消费分佣配置 查询失败：" + e.getMessage());
                }
            }
        });
    }
}
