package com.huolongluo.luo.huolongluo.ui.activity.follow;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gyf.immersionbar.ImmersionBar;
import com.huolongluo.luo.R;
import com.huolongluo.luo.huolongluo.base.BaseActivity;
import com.huolongluo.luo.huolongluo.bean.FollowBean;
import com.huolongluo.luo.huolongluo.constants.Constants;
import com.huolongluo.luo.huolongluo.share.Share;
import com.huolongluo.luo.huolongluo.ui.activity.userdetail.UserDetailInfoActivity;
import com.huolongluo.luo.huolongluo.ui.adapter.FollowAdapter;
import com.huolongluo.luo.superAdapter.recycler.SuperAdapter;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnLoadMoreListener;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;

/**
 * <p>
 * Created by 火龙裸 on 2017/9/6.
 */

public class FollowActivity extends BaseActivity {
    private static final String TAG = "FollowActivity";
    @BindView(R.id.toolbar_center_title)
    TextView toolbar_center_title;
    @BindView(R.id.iv_left)
    ImageView iv_left;
    /********************* 以上为ToolBar ************************/
    @BindView(R.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    @BindView(R.id.rv_follow)
    RecyclerView rv_follow;

    private int followType = 1;//1代表从”我的关注“点击进来的，2表示从”谁关注我“点击进来的

    private List<FollowBean> followBeanList = new ArrayList<>();
    private FollowAdapter followAdapter;

    private String currentUserWalletBeanObjectId = "";
    private String unLockUserObjectId = "";

    @Override
    protected int getContentViewId() {
        return R.layout.activity_follow;
    }

    @Override
    protected void injectDagger() {
        activityComponent().inject(this);
    }

    private void initToolBar() {
        ImmersionBar.with(this).statusBarDarkFont(true).init();
        if (followType == 1) {
            toolbar_center_title.setText("我的关注");
        } else {
            toolbar_center_title.setText("谁关注我");
        }
    }

    @Override
    protected void initViewsAndEvents(@Nullable Bundle savedInstanceState) {
        if (getBundle() != null) {
            followType = getBundle().getInt(Constants.FOLLOW_TYPE);
        }
        initToolBar();
        eventClick(iv_left).subscribe(o -> // 点击返回键
        {
            close();
        });

        rv_follow.setNestedScrollingEnabled(false);
        rv_follow.setFocusable(false);
        rv_follow.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
//        rv_follow.addItemDecoration(new GridSpacingItemDecoration(3,
//                DensityUtil.dip2px(this, 10), false));
        followAdapter = new FollowAdapter(this, followBeanList, R.layout.item_follow, followType);
        rv_follow.setAdapter(followAdapter);

        followAdapter.setOnItemClickListener(new SuperAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int viewType, int position) {
                Bundle bundle = new Bundle();
                if (followType == 1) {
                    bundle.putString(Constants.USER_NAME, followAdapter.getAllData().get(position).getFollowToUserName());
                } else {
                    bundle.putString(Constants.USER_NAME, followAdapter.getAllData().get(position).getUsername());
                }
                startActivity(UserDetailInfoActivity.class, bundle);
            }
        });

        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull final RefreshLayout refreshLayout) {
                queryFollowBean(Constants.MODE_REFRESH);
            }
        });
        refreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout rl) {
//                mAdapter.addData(movies);
                queryFollowBean(Constants.MODE_LOAD_MORE);
//                rl.finishLoadMoreWithNoMoreData();
            }
        });

        queryFollowBean(Constants.MODE_REFRESH);
//        //查询个人钱包余额，是否会员
//        queryUserWallet();
    }

//    private void queryUserWallet() {
//        BmobQuery<UserWalletBean> categoryBmobQuery = new BmobQuery<>();
//        categoryBmobQuery.addWhereEqualTo("username", Share.get().getUserName());
//        categoryBmobQuery.findObjects(new FindListener<UserWalletBean>() {
//            @Override
//            public void done(List<UserWalletBean> result, BmobException e) {
//                if (e == null) {
//                    if (!result.isEmpty()) {
//                        UserWalletBean userWalletBean = result.get(0);
//                        currentUserWalletBeanObjectId = userWalletBean.getObjectId();
//                        Share.get().setIsVip(userWalletBean.isVip());
//                        Share.get().setGoldBalance(userWalletBean.getGoldBalance());
//                        Share.get().setIntegralBalance(userWalletBean.getIntegralBalance());
//                    } else {
//                        createUserWallet();//给用户创建一个钱包
//                    }
//                } else {
//                    Log.e(TAG, e.toString());
//                }
//            }
//        });
//    }
//
//    /**
//     * 给用户创建一个钱包
//     */
//    private void createUserWallet() {
//        UserWalletBean p2 = new UserWalletBean();
//        p2.setUsername(Share.get().getUserName());
//        p2.setVip(false);
//        p2.setGoldBalance(0);
//        p2.setIntegralBalance(0);
//        p2.save(new SaveListener<String>() {
//            @Override
//            public void done(String objectId, BmobException e) {
//                if (e == null) {
//                    Log.e(TAG, "给用户创建一个钱包 数据成功，返回objectId为：" + objectId);
//                } else {
//                    Log.e(TAG, "给用户创建一个钱包 创建数据失败：" + e.getMessage());
//                }
//            }
//        });
//    }

    /**
     * 查询谁访问了我
     *
     * @param refreshType 刷新还是加载更多
     */
    private void queryFollowBean(int refreshType) {
        BmobQuery<FollowBean> categoryBmobQuery = new BmobQuery<>();
        if (followType == 1) {
            categoryBmobQuery.addWhereEqualTo("username", Share.get().getUserName());
        } else {
            categoryBmobQuery.addWhereEqualTo("followToUserName", Share.get().getUserName());
        }
        categoryBmobQuery.setLimit(10);//每次最多查询10条
        if (refreshType == Constants.MODE_LOAD_MORE) {
            categoryBmobQuery.setSkip(followAdapter.getItemCount());// 忽略前多少条数据，查询后面的
        }
        categoryBmobQuery.findObjects(new FindListener<FollowBean>() {
            @Override
            public void done(List<FollowBean> result, BmobException e) {
                if (e == null) {
                    if (!result.isEmpty()) {
                        Collections.reverse(result);
                        if (refreshType == Constants.MODE_REFRESH) {
                            followBeanList.clear();
                            followBeanList.addAll(result);
                            if (!followBeanList.isEmpty()) {
                                followAdapter.replaceAll(followBeanList);
                            }
                        } else {
                            followBeanList.addAll(result);
                            if (!followBeanList.isEmpty()) {
                                followAdapter.addAll(result);
                            }
                        }
                    } else {
                        refreshLayout.finishLoadMoreWithNoMoreData();
                    }
                    Log.e(TAG, "done: 到访用户 查询成功" + result.size());
                } else {
                    Log.e(TAG, e.toString());
                }
                refreshLayout.finishRefresh();
                refreshLayout.finishLoadMore();
            }
        });
    }
}
