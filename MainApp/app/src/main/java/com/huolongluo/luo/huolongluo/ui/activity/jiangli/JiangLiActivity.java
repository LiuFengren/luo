package com.huolongluo.luo.huolongluo.ui.activity.jiangli;

import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import com.gyf.immersionbar.ImmersionBar;
import com.huolongluo.luo.R;
import com.huolongluo.luo.huolongluo.base.BaseActivity;
import com.huolongluo.luo.huolongluo.bean.TiXianRecordBean;
import com.huolongluo.luo.huolongluo.bean.UserBean;
import com.huolongluo.luo.huolongluo.bean.UserWalletBean;
import com.huolongluo.luo.huolongluo.manager.DialogClickListener;
import com.huolongluo.luo.huolongluo.share.Event;
import com.huolongluo.luo.huolongluo.share.Share;
import com.huolongluo.luo.huolongluo.ui.activity.bindtixian.BindTiXianActivity;
import com.huolongluo.luo.huolongluo.ui.activity.howtoupwall.HowToUpWallActivity;
import com.huolongluo.luo.huolongluo.ui.activity.jifentip.JiFenTipActivity;
import com.huolongluo.luo.huolongluo.ui.activity.tixianrecord.TiXianRecordActivity;
import com.huolongluo.luo.util.Arith;
import com.huolongluo.luo.util.ImmersedStatusbarUtils;
import com.huolongluo.luo.util.L;
import com.huolongluo.luo.util.StringUtil;
import com.huolongluo.luo.util.TelNumMatch;
import com.huolongluo.luo.util.ToastSimple;
import com.jakewharton.rxbinding.widget.RxTextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import butterknife.BindView;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.SaveListener;
import cn.bmob.v3.listener.UpdateListener;

/**
 * <p>
 * Created by 火龙裸 on 2017/9/6.
 */

public class JiangLiActivity extends BaseActivity {
    private static final String TAG = "JiangLiActivity";
    @BindView(R.id.toolbar_center_title)
    TextView toolbar_center_title;
    @BindView(R.id.iv_left)
    ImageView iv_left;
    @BindView(R.id.tv_right)
    TextView tv_right;
    /********************* 以上为ToolBar ************************/
    @BindView(R.id.iv_gold_tips)
    ImageView iv_gold_tips;
    @BindView(R.id.tv_jifen_num)
    TextView tv_jifen_num;
    @BindView(R.id.tv_can_tixian_jifen_num)
    TextView tv_can_tixian_jifen_num;
    @BindView(R.id.card_view_bind_account)
    CardView card_view_bind_account;
    @BindView(R.id.tv_bind_alipay)
    TextView tv_bind_alipay;
    @BindView(R.id.tv_tixian)
    TextView tv_tixian;

    private String currentUserWalletBeanObjectId = "";
    private double jifenBalance = 0;//积分余额数量
    private double canTiXianJiFenNum = 0;//可体现积分数量


    @Override
    protected int getContentViewId() {
        return R.layout.activity_jiangli;
    }

    @Override
    protected void injectDagger() {
        activityComponent().inject(this);
    }

    private void initToolBar() {
        ImmersionBar.with(this).statusBarDarkFont(true).init();
        toolbar_center_title.setText("我的奖励");
        tv_right.setVisibility(View.VISIBLE);
        tv_right.setText("提现明细");
    }

    @Override
    protected void initViewsAndEvents(@Nullable Bundle savedInstanceState) {
        EventBus.getDefault().register(this);
        initToolBar();

        tv_jifen_num.setText(Share.get().getIntegralBalance() + "");

        double integralBalance = Double.parseDouble(Share.get().getIntegralBalance());

        canTiXianJiFenNum = Arith.mul(integralBalance / 50, 50);//每50积分代表5元，最低提现5元
        tv_can_tixian_jifen_num.setText(canTiXianJiFenNum + "");
        if (TextUtils.isEmpty(Share.get().getTiXianAccount())) {
            tv_bind_alipay.setText("未绑定");
        } else {
            tv_bind_alipay.setText(Share.get().getTiXianAccount());
        }

        eventClick(iv_left).subscribe(o -> // 点击返回键
        {
            close();
        });
        eventClick(tv_right).subscribe(o -> // 提现明细
        {
            startActivity(TiXianRecordActivity.class);
        });
        eventClick(iv_gold_tips).subscribe(o -> // 提现提示
        {
            startActivity(JiFenTipActivity.class);
        });
        eventClick(card_view_bind_account).subscribe(o -> // 绑定提现账户
        {
            startActivity(BindTiXianActivity.class);
        });
        eventClick(tv_tixian).subscribe(o -> // 提现按钮
        {
            if (TextUtils.equals("未绑定", tv_bind_alipay.getText().toString()) || TextUtils.isEmpty(tv_bind_alipay.getText().toString())) {
                View view = LayoutInflater.from(JiangLiActivity.this).inflate(R.layout.toast_text, null);
                TextView tv_toast_message = view.findViewById(R.id.tv_toast_message);
                tv_toast_message.setText("提现账户名不能为空");
                ToastSimple.makeText(Gravity.CENTER, 2, view).show();
            } else {
                if (canTiXianJiFenNum >= 50) {
                    Log.e(TAG, "initViewsAndEvents: 开始提现");
                    createTiXianApply();
                } else {
                    View view = LayoutInflater.from(JiangLiActivity.this).inflate(R.layout.toast_text, null);
                    TextView tv_toast_message = view.findViewById(R.id.tv_toast_message);
                    tv_toast_message.setText("积分总额最低50为一单位可提现");
                    ToastSimple.makeText(Gravity.CENTER, 2, view).show();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        queryUserWallet();
    }

    private void queryUserWallet() {
        BmobQuery<UserWalletBean> categoryBmobQuery = new BmobQuery<>();
        categoryBmobQuery.addWhereEqualTo("username", Share.get().getUserName());
        categoryBmobQuery.findObjects(new FindListener<UserWalletBean>() {
            @Override
            public void done(List<UserWalletBean> result, BmobException e) {
                if (e == null) {
                    Log.e(TAG, "done: 查询用户钱包信息 成功:" + result.size());
                    if (!result.isEmpty()) {
                        UserWalletBean userWalletBean = result.get(0);
                        currentUserWalletBeanObjectId = userWalletBean.getObjectId();
                        jifenBalance = Double.parseDouble(userWalletBean.getIntegralBalance());
                        canTiXianJiFenNum = jifenBalance / 50 * 50;//每50积分代表5元，最低提现5元

                        Share.get().setUser_wallet_bean_object_id(currentUserWalletBeanObjectId);
                        Share.get().setIsVip(userWalletBean.isVip());
                        Share.get().setGoldBalance(userWalletBean.getGoldBalance());
                        Share.get().setIntegralBalance(jifenBalance + "");

                        if (tv_jifen_num != null) {
                            tv_jifen_num.setText(jifenBalance + "");
                        }
                        if (tv_can_tixian_jifen_num != null) {
                            tv_can_tixian_jifen_num.setText(canTiXianJiFenNum + "");
                        }
                    } else {
//                        createUserWallet();//给用户创建一个钱包
                        UserWalletBean.createUserWallet();
                    }
                } else {
                    Log.e(TAG, e.toString());
                }
            }
        });
    }

//    /**
//     * 给用户创建一个钱包
//     */
//    private void createUserWallet() {
//        UserWalletBean p2 = new UserWalletBean();
//        p2.setUsername(Share.get().getUserName());
//        p2.setVip(false);
//        p2.setGoldBalance(100);
//        p2.setIntegralBalance(0);
//        p2.save(new SaveListener<String>() {
//            @Override
//            public void done(String objectId, BmobException e) {
//                if (e == null) {
//                    Log.e(TAG, "给用户创建一个钱包 数据成功，返回objectId为：" + objectId);
//                    Share.get().setGoldBalance(100);
//                } else {
//                    Log.e(TAG, "给用户创建一个钱包 创建数据失败：" + e.getMessage());
//                }
//            }
//        });
//    }

    /**
     * 更新钱包 积分
     * */
    private void updateUserWallet(String objectId, double inviteWillGetJiFenNum){
        UserWalletBean p2 = new UserWalletBean();
        p2.setIntegralBalance(jifenBalance + "");
        p2.update(objectId, new UpdateListener() {
            @Override
            public void done(BmobException e) {
                if(e==null){
                    Log.e(TAG, "done: 更新钱包积分成功");
                    UserWalletBean.updateUserInviteBalance(inviteWillGetJiFenNum);
                }else{
                    Log.e(TAG, "done: 更新钱包积分失败：" + e.getMessage());
                }
            }

        });
    }

    /**
     * 发起一条提现申请记录
     * */
    private void createTiXianApply() {
        TiXianRecordBean tiXianRecordBean = new TiXianRecordBean();
        tiXianRecordBean.setUsername(Share.get().getUserName());
        tiXianRecordBean.setTiXianAccount(Share.get().getTiXianAccount());
        tiXianRecordBean.setTixianJiFen(canTiXianJiFenNum + "");
        tiXianRecordBean.setJifenBalance((Arith.sub(jifenBalance, canTiXianJiFenNum)) + "");
        tiXianRecordBean.setTixianState(0);
        tiXianRecordBean.save(new SaveListener<String>() {
            @Override
            public void done(String objectId,BmobException e) {
                if(e==null){
                    Log.e(TAG, "提现申请 添加数据成功，返回objectId为："+objectId);
                    double tiXianGetJiFenUnit = 0.1;
                    if (!TextUtils.isEmpty(Share.get().getTixianGetJifenUnit())) {
                        tiXianGetJiFenUnit = Double.parseDouble(Share.get().getTixianGetJifenUnit());
                    }

                    double inviteWillGetJiFenNum = Arith.mul(canTiXianJiFenNum, tiXianGetJiFenUnit);//邀请人将获得多少积分
                    jifenBalance = jifenBalance - canTiXianJiFenNum;
                    canTiXianJiFenNum = 0;
                    tv_jifen_num.setText(jifenBalance + "");
                    tv_can_tixian_jifen_num.setText(canTiXianJiFenNum + "");
                    updateUserWallet(currentUserWalletBeanObjectId, inviteWillGetJiFenNum);
                    showConfirmDialog("提现", canTiXianJiFenNum + "积分提现申请发起成功，请耐心等待。", new DialogClickListener() {
                        @Override
                        public void onConfirm() {
                        }

                        @Override
                        public void onCancel() {
                        }
                    });
                }else{
                    Log.e(TAG, "提现申请 创建数据失败：" + e.getMessage());
                    View view = LayoutInflater.from(JiangLiActivity.this).inflate(R.layout.toast_text, null);
                    TextView tv_toast_message = view.findViewById(R.id.tv_toast_message);
                    tv_toast_message.setText("提现申请发起错误：" + e.getMessage());
                    ToastSimple.makeText(Gravity.CENTER, 2, view).show();
                }
            }
        });
    }

    /**
     * 刷新用户提现账户名
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void editTiXian(Event.editTiXian event) {
        tv_bind_alipay.setText(event.editTiXianAccount);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
