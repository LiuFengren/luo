package com.huolongluo.luo.huolongluo.ui.adapter;

import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;

import com.huolongluo.luo.huolongluo.bean.ItemBean;

public class MyItemDiffItemCallBack extends DiffUtil.ItemCallback<ItemBean> {

    @Override
    public boolean areItemsTheSame(@NonNull ItemBean oldItem, @NonNull ItemBean newItem) {
        return TextUtils.equals(oldItem.getId(), newItem.getId());
    }

    @Override
    public boolean areContentsTheSame(@NonNull ItemBean oldItem, @NonNull ItemBean newItem) {
        if (!TextUtils.equals(oldItem.getTitle(), newItem.getTitle())) {
            return false;
        }

        if (!TextUtils.equals(oldItem.getContent(), newItem.getContent())) {
            return false;
        }

        return true;
    }
}
