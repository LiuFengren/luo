package com.huolongluo.luo.huolongluo.share;

import com.huolongluo.luo.huolongluo.bean.DisCoverBean;
import com.huolongluo.luo.huolongluo.bean.UserBasicInfoBean;

import java.util.List;

/**
 * Created by 火龙裸先生 on 2020/3/12 0011.
 */

public class Event {

    public static class CardPayCallBack {
        public String payType;
        public String data;
        public boolean isCallBackToService;

        public CardPayCallBack(String payType, String data, boolean isCallBackToService) {
            this.payType = payType;
            this.data = data;
            this.isCallBackToService = isCallBackToService;
        }
    }

    public static class RefreshOrderNotPay {
    }

    /**
     * 点击了Walfare页面的返回键
     */
    public static class clickWebViewBack {
    }

    public static class updataUserInfoDetail {
        public int updataCardType;
        public String updataCardText;
        public List<String> updataCardPic;

        public updataUserInfoDetail(int updataCardType, String updataCardText, List<String> updataCardPic) {
            this.updataCardType = updataCardType;
            this.updataCardText = updataCardText;
            this.updataCardPic = updataCardPic;
        }
    }

    public static class updataUserTopPic {
        public List<String> picUrl;

        public updataUserTopPic(List<String> picUrl) {
            this.picUrl = picUrl;
        }
    }

    public static class updateUserBaseInfo {
        public UserBasicInfoBean result;

        public updateUserBaseInfo(UserBasicInfoBean result) {
            this.result = result;
        }
    }

    public static class editNickName {
        public String editNickName;

        public editNickName(String editNickName) {
            this.editNickName = editNickName;
        }
    }

    public static class editWeChat {
        public String editWeChat;

        public editWeChat(String editWeChat) {
            this.editWeChat = editWeChat;
        }
    }

    public static class editSchool {
        public String editSchoolName;

        public editSchool(String editSchoolName) {
            this.editSchoolName = editSchoolName;
        }
    }

    public static class editJob {
        public String editJobName;

        public editJob(String editJobName) {
            this.editJobName = editJobName;
        }
    }

    public static class editTiXian {
        public String editTiXianRealFullName;
        public String editTiXianAccount;

        public editTiXian(String editTiXianRealFullName, String editTiXianAccount) {
            this.editTiXianRealFullName = editTiXianRealFullName;
            this.editTiXianAccount = editTiXianAccount;
        }
    }

    public static class clickDongTaiMore {
        public int position;
        public DisCoverBean disCoverBean;

        public clickDongTaiMore(int position, DisCoverBean disCoverBean) {
            this.position = position;
            this.disCoverBean = disCoverBean;
        }
    }

    /**
     * 微信快捷登录
     */
    public static class toWxLogin {
        public String phone;
        public String psw;

        public toWxLogin(String phone, String psw) {
            this.phone = phone;
            this.psw = psw;
        }
    }

}
