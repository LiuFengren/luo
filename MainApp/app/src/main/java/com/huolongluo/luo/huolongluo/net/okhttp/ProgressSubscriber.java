package com.huolongluo.luo.huolongluo.net.okhttp;

import android.content.Context;

import com.huolongluo.luo.huolongluo.manager.DialogManager2;
import com.huolongluo.luo.huolongluo.net.BaseResponse;
import com.huolongluo.luo.huolongluo.net.BaseResponse2;
import com.huolongluo.luo.util.L;
import com.huolongluo.luo.util.ToastSimple;

import java.net.ConnectException;
import java.net.SocketTimeoutException;

import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;

/**
 * 用于在Http请求开始时，自动显示一个ProgressDialog
 * <br>
 * HttpOnNextListener 无自定义出错处理 ，HttpOnNextListener2 有自定义出错处理
 * <br>
 * 在Http请求结束是，关闭ProgressDialog
 * <br>
 * 调用者自己对请求数据进行处理
 * <p>
 * Created by 火龙裸 on 2020/3/12.
 */

public class ProgressSubscriber<T> extends Subscriber<T> {
    //    回调接口  
    private HttpOnNextListener mSubscriberOnNextListener;
    private HttpOnNextListener2 mSubscriberOnNextListener2;
    private HttpOnNextListener2_2 mSubscriberOnNextListener2_2;
    private Context mContext;
    private boolean isShowDialog;
//    private LoadingPopupView loadingPopup;

    public ProgressSubscriber(HttpOnNextListener mSubscriberOnNextListener, Context mContext, boolean isShowDialog) {
        this.mSubscriberOnNextListener = mSubscriberOnNextListener;
        this.mContext = mContext;
        this.isShowDialog = isShowDialog;
    }

    public ProgressSubscriber(HttpOnNextListener2 mSubscriberOnNextListener2, Context mContext, boolean isShowDialog) {
        this.mSubscriberOnNextListener2 = mSubscriberOnNextListener2;
        this.mContext = mContext;
        this.isShowDialog = isShowDialog;
    }

    public ProgressSubscriber(HttpOnNextListener2_2 mSubscriberOnNextListener2_2, Context mContext, boolean isShowDialog)
    {
        this.mSubscriberOnNextListener2_2 = mSubscriberOnNextListener2_2;
        this.mContext = mContext;
        this.isShowDialog = isShowDialog;
    }


    /**
     * 完成，隐藏ProgressDialog
     */
    @Override
    public void onCompleted() {
        DialogManager2.INSTANCE.dismiss();
        L.d("--- onCompleted");
    }

    @Override
    public void onError(Throwable e) {
        if (e instanceof HttpException) {
            ToastSimple.show("服务繁忙，请稍后重试", 2);
        } else if (e instanceof SocketTimeoutException) {
            ToastSimple.show("网络出问题了，请稍后重试", 2);
        } else if (e instanceof ConnectException) {
            ToastSimple.show("连接出问题了，请稍后重试", 2);
        } else {
            L.e("错误: " + e.getMessage());
        }

        if (null != mSubscriberOnNextListener2) {
            mSubscriberOnNextListener2.onError(e);
        } else if (null != mSubscriberOnNextListener2_2) {
            mSubscriberOnNextListener2_2.onError(e);
        }
        DialogManager2.INSTANCE.dismiss();
    }

    @Override
    public void onStart() {
        if (isShowDialog) {
//            DialogManager.INSTANCE.showProgressDialog(mContext, "加载中");
            DialogManager2.INSTANCE.showProgressDialog(mContext, "加载中");
        }
    }

    @Override
    public void onNext(T t) {
        L.d(t.toString());

        if (t instanceof BaseResponse) {
            BaseResponse response = (BaseResponse) t;

            if (response.getCode() == 0 && response.isSuccess()) {
                if (null != mSubscriberOnNextListener) {
                    // noinspection unchecked
                    mSubscriberOnNextListener.onNext(response.getData());
                }

                if (null != mSubscriberOnNextListener2) {
                    // noinspection unchecked
                    mSubscriberOnNextListener2.onNext(response.getData());
                }
            } else {
                switch (response.getCode()) {
                    case 10001:
                    case 10002:
                    case 10003:
                        ToastSimple.show(response.getMsg(), 2);
                        break;
                    case 3001:
                        ToastSimple.show("请重新登录授权", 2);
                        break;
                    case 3002:
                        ToastSimple.show("参数错误", 2);
                        break;
                    case 3003:
//                        ToastSimple.show("Token已过期，请重新登录饿了么", 3); //token认证失败
                        ToastSimple.show(response.getMsg(), 3); //token认证失败
                        break;
                    case 3004:
                        ToastSimple.show(response.getMsg(), 2);
                        break;
                    case 2001:
                        ToastSimple.show("服务器错误", 2);
                        break;
                    default:
                        ToastSimple.show(response.getMsg(), 2);
                        break;
                }
                if (null != mSubscriberOnNextListener2) {
                    // noinspection unchecked
                    mSubscriberOnNextListener2.onFail(response);
                }
            }
        } else if (t instanceof BaseResponse2) {

            BaseResponse2 response2 = (BaseResponse2) t;

            if (response2.getDm_error() == 0) {
                if (null != mSubscriberOnNextListener) {
                    // noinspection unchecked
                    mSubscriberOnNextListener.onNext(response2.getData());
                }

                if (null != mSubscriberOnNextListener2_2) {
                    // noinspection unchecked
                    mSubscriberOnNextListener2_2.onNext(response2.getData());
                }
            } else {
                mSubscriberOnNextListener2_2.onFail(response2);
            }
        } else {
            if (null != mSubscriberOnNextListener) {
                // noinspection unchecked
                mSubscriberOnNextListener.onNext(t.toString());
            }
        }
    }
}
