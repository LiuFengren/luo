package com.huolongluo.luo.huolongluo.net.okhttp;

import com.huolongluo.luo.huolongluo.net.BaseResponse2;

/**
 * <p> Erp接口专用
 * Created by 火龙裸 on 2017/10/16.
 */

public abstract class HttpOnNextListener2_2<T>
{
    public abstract void onNext(T result);

    public void onFail(BaseResponse2 errResponse)
    {
    }

    public void onError(Throwable error)
    {
    }
}
