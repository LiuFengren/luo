package com.huolongluo.luo.huolongluo.ui.fragment.home;

import android.content.Context;

import com.huolongluo.luo.base.ActivityContext;
import com.huolongluo.luo.huolongluo.bean.GuessYouLikeBean;
import com.huolongluo.luo.huolongluo.net.okhttp.Api;
import com.huolongluo.luo.huolongluo.net.okhttp.HttpOnNextListener2_2;

import javax.inject.Inject;

import rx.Subscription;

/**
 * Created by 火龙裸 on 2018/5/26 0026.
 */

public class HomePresent implements HomeContract.Presenter
{
    private HomeContract.View mView;
    private Context mContext;

    @Inject
    Api api;

    @Inject
    public HomePresent(@ActivityContext Context mContext)
    {
        this.mContext = mContext;
    }

    @Override
    public void attachView(HomeContract.View view)
    {
        mView = view;
    }

    @Override
    public void detachView()
    {
        mView = null;
    }

//    @Override
//    public Subscription getGuessLiskList() {
//        return api.getGuessLiskList(new HttpOnNextListener2_2<GuessYouLikeBean>() {
//            @Override
//            public void onNext(GuessYouLikeBean result) {
//                mView.getGuessLiskListSucce(result);
//            }
//        });
//    }

//    @Override
//    public Subscription getNewsList(int pageNo, int pageSize)
//    {
//        return api.getNewsList(pageNo, pageSize, response -> mView.getNewsListSucce(response));
//    }
//
//    @Override
//    public Subscription getTaskList(String aId, String uId, String pageNo, String pageSize,String period,String star)
//    {
//        return api.getTaskList(aId, uId, pageNo, pageSize,period,star, response -> mView.getTaskListSucce(response));
//    }
//
//    @Override
//    public Subscription getDynamic(String type, String content, String uName, String uId, String isErp, int pageNo, int pageSize)
//    {
//        return api.getDynamic(type, content, uName, uId, isErp, pageNo, pageSize,"", new HttpOnNextListener2<List<DynamicListBean>>()
//        {
//            @Override
//            public void onNext(List<DynamicListBean> response)
//            {
//                mView.getDynamicSucce(type, response);
//            }
//        });
//    }
//
//    @Override
//    public Subscription getPicture(String type)
//    {
//        return api.getPicture(type, response -> mView.getPictureSucce(response));
//    }
//
//    @Override
//    public Subscription getRank(String id)
//    {
//        return api.getRank(id, new HttpOnNextListener2_2<RankBean>()
//        {
//            @Override
//            public void onNext(RankBean result)
//            {
//                mView.getRankSucce(result);
//            }
//        });
//    }
//
//    @Override
//    public Subscription getQuantity(String id)
//    {
//        return api.getQuantity(id, new HttpOnNextListener2_2<QuantityBean>()
//        {
//            @Override
//            public void onNext(QuantityBean result)
//            {
//                mView.getQuantitySucce(result);
//            }
//
//            @Override
//            public void onError(Throwable error)
//            {
//                super.onError(error);
//                mView.getQuantityErr();
//            }
//
//            @Override
//            public void onFail(BaseResponse2 errResponse)
//            {
//                super.onFail(errResponse);
//                mView.getQuantityErr();
//            }
//        });
//    }
//
//    @Override
//    public Subscription getUpTime(String id)
//    {
//        return api.getUpTime(id, new HttpOnNextListener2_2<UpTimeBean>()
//        {
//            @Override
//            public void onNext(UpTimeBean result)
//            {
//                mView.getUpTimeSucce(result);
//            }
//        });
//    }
//
//    @Override
//    public Subscription loginEerp(String member_id, String ticket, String password, String type)
//    {
//        return api.loginEerp(member_id, ticket, password, type, new HttpOnNextListener2_2<LoginErpBean>()
//        {
//            @Override
//            public void onNext(LoginErpBean result)
//            {
//                mView.loginEerpSucce(result);
//            }
//        });
//    }
}
