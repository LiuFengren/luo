package com.huolongluo.luo.huolongluo.ui.fragment.selectlabel;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.huolongluo.luo.R;
import com.huolongluo.luo.huolongluo.base.BaseActivity;
import com.huolongluo.luo.huolongluo.base.BaseFragment;

public class SelectLabelFragment extends BaseFragment {
    private static final String TAG = "SelectLabelFragment";

    private String labels;

    public static SelectLabelFragment getInstance(String labels) {
        Bundle args = new Bundle();
        SelectLabelFragment fragment = new SelectLabelFragment();
        fragment.setArguments(args);
        fragment.labels = labels;
        return fragment;
    }
    
    @Override
    protected void initDagger() {
        ((BaseActivity) getActivity()).activityComponent().inject(this);
    }

    @Override
    protected int getContentViewId() {
        return R.layout.fragment_select_label;
    }

    @Override
    protected void initViewsAndEvents(View rootView) {
        Log.e(TAG, "initViewsAndEvents: 当前Fragment的数据为：" + labels);
    }
}
