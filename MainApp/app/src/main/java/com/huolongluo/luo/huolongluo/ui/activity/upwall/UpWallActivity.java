package com.huolongluo.luo.huolongluo.ui.activity.upwall;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import com.gyf.immersionbar.ImmersionBar;
import com.huolongluo.luo.R;
import com.huolongluo.luo.huolongluo.base.BaseActivity;
import com.huolongluo.luo.huolongluo.share.Event;
import com.huolongluo.luo.huolongluo.ui.activity.howtoupwall.HowToUpWallActivity;
import com.huolongluo.luo.huolongluo.ui.adapter.FriendContactsViewPager2Adapter;
import com.huolongluo.luo.huolongluo.ui.fragment.friendcontact.FriendContactsFragment;
import com.huolongluo.luo.huolongluo.ui.fragment.upwall.UpWallFragment;
import com.huolongluo.luo.util.ToastSimple;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * <p>
 * Created by 火龙裸 on 2017/9/6.
 */

public class UpWallActivity extends BaseActivity {
    private static final String TAG = "UpWallActivity";
    @BindView(R.id.toolbar_center_title)
    TextView toolbar_center_title;
    @BindView(R.id.iv_left)
    ImageView iv_left;
    @BindView(R.id.tv_right)
    TextView tv_right;
    /********************* 以上为ToolBar ************************/
    @BindView(R.id.ll_who_to_up_wall)
    LinearLayout ll_who_to_up_wall;
    @BindView(R.id.tb_up_wall_title)
    TabLayout tb_up_wall_title;
    @BindView(R.id.vp_up_wall)
    ViewPager2 vp_up_wall;

    private List<String> list_title = new ArrayList<>(); // tab名称列表
    private List<Fragment> list_fragment = new ArrayList<>(); // 定义要装fragment的列表

    @Override
    protected int getContentViewId() {
        return R.layout.activity_up_wall;
    }

    @Override
    protected void injectDagger() {
        activityComponent().inject(this);
    }

    private void initToolBar() {
        ImmersionBar.with(this).statusBarDarkFont(true).init();
        toolbar_center_title.setText("上墙用户");
    }

    @Override
    protected void initViewsAndEvents(@Nullable Bundle savedInstanceState) {
        initToolBar();
        eventClick(iv_left).subscribe(o -> // 点击返回键
        {
            close();
        });
        eventClick(ll_who_to_up_wall).subscribe(o -> // 点击怎么上墙
        {
            startActivity(HowToUpWallActivity.class);
        });

        list_title.clear();
        list_fragment.clear();

        list_title.add("上墙男生");
        list_title.add("上墙女生");
        list_fragment.add(UpWallFragment.getInstance("男"));
        list_fragment.add(UpWallFragment.getInstance("女"));

        vp_up_wall.setAdapter(new FriendContactsViewPager2Adapter(getSupportFragmentManager(), getLifecycle(), list_fragment));

        //关联TabLayout 添加attach()
        new TabLayoutMediator(tb_up_wall_title, vp_up_wall, new TabLayoutMediator.TabConfigurationStrategy() {
            @Override
            public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {
                tab.setText(list_title.get(position));
            }
        }).attach();

        vp_up_wall.registerOnPageChangeCallback(callback);
        vp_up_wall.setUserInputEnabled(false);//禁止左右滑动
    }

    private ViewPager2.OnPageChangeCallback callback = new ViewPager2.OnPageChangeCallback() {
        @Override
        public void onPageSelected(int position) {
            super.onPageSelected(position);
            Log.e(TAG, "onPageSelected: 改变选中了：" + position);
        }
    };
}
