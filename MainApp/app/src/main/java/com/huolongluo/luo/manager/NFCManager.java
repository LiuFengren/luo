package com.huolongluo.luo.manager;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.MifareClassic;
import android.nfc.tech.Ndef;
import android.nfc.tech.NfcA;
import android.text.TextUtils;

import com.huolongluo.luo.util.L;
import com.huolongluo.luo.util.ToastSimple;

import java.io.IOException;


public class NFCManager
{
    private NfcAdapter nfcAdapter;
    private PendingIntent mPendingIntent;
    private IntentFilter[] mFilters;
    private String[][] mTechLists;

    private Activity mActivity;

    //修改密码
    private byte[] myKeyA = {'q', '<', '^', 'a', '-', 'y'};

    public void init(Activity activity)
    {
        mActivity = activity;

        nfcAdapter = NfcAdapter.getDefaultAdapter(activity);

        if (null == nfcAdapter)
        {
//            ToastSimple.show("设备不支持NFC功能！", 2);
            L.d("设备不支持NFC功能！");
            return;
        }

        if (!nfcAdapter.isEnabled())
        {
            ToastSimple.show("设备NFC功能没有打开！", 2);
            enableDialog(activity);
            return;
        }

        //        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);

        mPendingIntent = PendingIntent.getActivity(activity, 0, new Intent(activity, activity.getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);

        // 三种Activity NDEF_DISCOVERED ,TECH_DISCOVERED,TAG_DISCOVERED
        // 指明的先后顺序非常重要， 当Android设备检测到有NFC Tag靠近时，会根据Action申明的顺序给对应的Activity
        // 发送含NFC消息的 Intent.
        IntentFilter ndef = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
        IntentFilter tech = new IntentFilter(NfcAdapter.ACTION_TECH_DISCOVERED);
        IntentFilter tag = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);

        try
        {
            ndef.addDataType("*/*");
        }
        catch (IntentFilter.MalformedMimeTypeException e)
        {
            throw new RuntimeException("fail", e);
        }
        mFilters = new IntentFilter[]{ndef, tech, tag};
        mTechLists = new String[][]{new String[]{Ndef.class.getName(), MifareClassic.class.getName(), NfcA.class.getName()}};

    }

    private void enableDialog(final Context context)
    {
        AlertDialog.Builder ab = new AlertDialog.Builder(context);
        ab.setTitle("提醒");
        ab.setMessage("设备NFC开关未打开，是否现在去打开？");
        ab.setNeutralButton("否", (dialog, which) -> dialog.dismiss());
        ab.setNegativeButton("是", (dialog, which) ->
        {
            context.startActivity(new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS));
            dialog.dismiss();
        });
        ab.create().show();
    }

    public void onPause()
    {
        if (nfcAdapter != null)
        {
            nfcAdapter.disableForegroundDispatch(mActivity);
        }
    }

    public void onResume()
    {
        if (null != nfcAdapter)
        {
            mPendingIntent = PendingIntent.getActivity(mActivity, 0, new Intent(mActivity, mActivity.getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
            nfcAdapter.enableForegroundDispatch(mActivity, mPendingIntent, mFilters, mTechLists);
        }
    }

    public void onNewIntent(Intent intent, boolean isReadCard)
    {
        Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);

        if (null == tag)
        {
            L.d("tag 为空");
            return;
        }

        int before = (int) Long.parseLong(bytesToHexString(tag.getId()), 16);

        int r24 = before >> 24 & 0x000000FF;
        int r8 = before >> 8 & 0x0000FF00;
        int l8 = before << 8 & 0x00FF0000;
        int l24 = before << 24 & 0xFF000000;

        String metaInfo = "ID(dec):" + Long.parseLong(Integer.toHexString((r24 | r8 | l8 | l24)), 16);

        L.d(metaInfo);


        String[] techList = tag.getTechList();
        boolean haveMifareUltralight = false;

        for (String tech : techList)
        {
            if (tech.contains("MifareClassic"))
            {
                haveMifareUltralight = true;
                break;
            }
        }

        if (!haveMifareUltralight)
        {
            ToastSimple.show("设备不支持MifareClassic", 2);
            return;
        }

        if (isReadCard)
        {
            readTag(tag);
//            if (null != data)
//            {
//                L.d("output " + data);
//            }
        }
        else
        {
            writeTag(tag);
        }
    }

    private void readTag(Tag tag)
    {
        try
        {
            MifareClassic mfc = MifareClassic.get(tag);
            // Enable I/O operations to the tag from this TagTechnology object.
            mfc.connect();

            // Authenticate a sector with key A
            // 只验证第一扇区
//            boolean auth = mfc.authenticateSectorWithKeyA(1, MifareClassic.KEY_DEFAULT);
//            boolean auth = isKeyMifareClassicEnable(mfc, 1);
            boolean auth = mfc.authenticateSectorWithKeyA(1, myKeyA);
            if (auth)
            {
                int bIndex = mfc.sectorToBlock(1);
                byte[] dataCard = mfc.readBlock(bIndex);
                byte[] dataPay = mfc.readBlock(bIndex + 1);


                String cardText = new String(dataCard);
                String payText = new String(dataPay);

                if (TextUtils.isEmpty(cardText) || TextUtils.isEmpty(payText))
                {
                    return;
                }

                if (null != onReadTagListener)
                {
                    onReadTagListener.readTag(cardText, payText);
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private String readTag1(Tag tag)
    {
        MifareClassic mfc = MifareClassic.get(tag);
        for (String tech : tag.getTechList())
        {
            L.d(tech);
        }

        boolean auth;

        // 读取 TAG
        try
        {
            String metaInfo = "";
            // Enable I/O operations to the tag from this TagTechnology object.
            mfc.connect();

            int type = mfc.getType(); // 获取TAG的类型
            int sectorCount = mfc.getSectorCount(); // 获取TAG中包含的扇区数
            String types = "";
            switch (type)
            {
                case MifareClassic.TYPE_CLASSIC:
                    types = "TYPE_CLASSIC";
                    break;
                case MifareClassic.TYPE_PLUS:
                    types = "TYPE_PLUS";
                    break;
                case MifareClassic.TYPE_PRO:
                    types = "TYPE_PRO";
                    break;
                case MifareClassic.TYPE_UNKNOWN:
                    types = "TYPE_UNKNOWN";
                    break;
            }
            String aa = "卡片类型：" + types + "\n共 " + sectorCount + " 个扇区\n共 " + mfc.getBlockCount() + " 个块\n存储空间：" + mfc.getSize() + " B\n";

            L.d(aa);
            for (int j = 0; j < sectorCount; j++)
            {
                // Authenticate a sector with key A
                auth = isKeyMifareClassicEnable(mfc, j);
//                auth = mfc.authenticateSectorWithKeyA(j, MifareClassic.KEY_DEFAULT);
                int bCount;
                int bIndex;
                if (auth)
                {
                    metaInfo += "Sector " + j + " :验证成功\n";
                    // 读取扇中的块
                    bCount = mfc.getBlockCountInSector(j);
                    bIndex = mfc.sectorToBlock(j);

                    for (int i = 0; i < bCount; i++)
                    {
                        byte[] data = mfc.readBlock(bIndex);
                        metaInfo += "Block " + bIndex + " : " + bytesToHexString(data) + " " + new String(data) + "\n";
                        bIndex++;
                    }
                }
                else
                {
                    metaInfo += "Sector " + j + " ：验证失败\n";
                }

                L.d(metaInfo);
                metaInfo = "";
            }
            return metaInfo;
        }
        catch (Exception e)
        {
            ToastSimple.show(e.getMessage(), 2);
            e.printStackTrace();
        }
        finally
        {
            if (null != mfc)
            {
                try
                {
                    mfc.close();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                    ToastSimple.show(e.getMessage(), 2);
                }
            }
        }
        return null;
    }

    /**
     * 验证扇sector的密码.
     *
     * @param mfc
     * @param sectorIndex
     * @return
     */
    private Boolean isKeyMifareClassicEnable(MifareClassic mfc, int sectorIndex)
    {
        boolean auth = false;
        try
        {
            auth = mfc.authenticateSectorWithKeyA(sectorIndex, MifareClassic.KEY_DEFAULT);
            if (!auth)
            {
                auth = mfc.authenticateSectorWithKeyA(sectorIndex, myKeyA);
                L.d("myKeyA");
            }
            if (!auth)
            {
                auth = mfc.authenticateSectorWithKeyA(sectorIndex, MifareClassic.KEY_NFC_FORUM);
                L.d("KEY_NFC_FORUM");
            }
            L.d("KEY_DEFAULT");
        }
        catch (IOException e)
        {
            L.e("IOException while authenticateSectorWithKey MifareClassic...", e);
        }
        return auth;
    }

    private void writeTag(Tag tag)
    {
        MifareClassic mfc = MifareClassic.get(tag);

        try
        {
            mfc.connect();
            boolean auth = false;
            byte[] KeyValue = null;
            if (isKeyMifareClassicEnable(mfc, 1))
            {
                KeyValue = mfc.readBlock(7);
                L.d("keyValue: " + bytesToHexString(KeyValue));
            }
            mfc.close();

            mfc.connect();
            short sectorAddress = 1;
//            auth = mfc.authenticateSectorWithKeyA(sectorAddress, MifareClassic.KEY_DEFAULT);
            if (isKeyMifareClassicEnable(mfc, sectorAddress))
            {
                // the last block of the sector is used for KeyA and KeyB cannot be overwritted  
                mfc.writeBlock(4, "000QY10CRtsqjFV3".getBytes());
                mfc.writeBlock(5, "Gv5WRtM9ubHuowE3".getBytes());

                //修改键值，keyA存放在每扇最后一块
                for (int i = 0; i < 6; i++)
                {
                    KeyValue[i] = myKeyA[i];
                }

                mfc.writeBlock(7, KeyValue);
                mfc.close();
                ToastSimple.show("写入成功", 2);
            }
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block  
            e.printStackTrace();
        }
        finally
        {
            try
            {
                mfc.close();
            }
            catch (IOException e)
            {
                // TODO Auto-generated catch block  
                e.printStackTrace();
            }
        }
    }

    // 字符序列转换为16进制字符串
    private String bytesToHexString(byte[] src)
    {
        StringBuilder stringBuilder = new StringBuilder("");
        if (src == null || src.length <= 0)
        {
            return null;
        }
        char[] buffer = new char[2];
        for (int i = 0; i < src.length; i++)
        {
            buffer[0] = Character.forDigit((src[i] >>> 4) & 0x0F, 16);
            buffer[1] = Character.forDigit(src[i] & 0x0F, 16);

            stringBuilder.append(buffer);

        }
        return stringBuilder.toString();
    }

    private OnReadTagListener onReadTagListener;

    public interface OnReadTagListener
    {
        void readTag(String cardText, String payText);
    }

    public void setOnReadTagListener(OnReadTagListener onReadTagListener)
    {
        this.onReadTagListener = onReadTagListener;
    }
}
