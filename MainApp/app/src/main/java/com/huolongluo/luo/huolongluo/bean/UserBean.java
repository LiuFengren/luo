package com.huolongluo.luo.huolongluo.bean;

import android.util.Log;

import com.huolongluo.luo.huolongluo.share.Event;
import com.huolongluo.luo.huolongluo.share.Share;
import com.tencent.imsdk.v2.V2TIMCallback;
import com.tencent.qcloud.tuicore.TUILogin;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.BmobUser;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.SaveListener;

/**
 * 登录时返回的用户信息表
 */
public class UserBean extends BmobUser {
    private static final String TAG = "UserBean";
    //    private String username;
//    private String password;
    private String sex;//性别
    private String deviceId;//设备ID
    private String inviteCode;//邀请码
    private boolean disable;//是否被禁用
    private String kefu;//当前用户所对应的客服账号
    private boolean isAgent;//是否代理
    private String tixianRealFullName;//提现 真实全名
    private String tixianAccount;//提现 账号

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getInviteCode() {
        return inviteCode;
    }

    public void setInviteCode(String inviteCode) {
        this.inviteCode = inviteCode;
    }

    public boolean isDisable() {
        return disable;
    }

    public void setDisable(boolean disable) {
        this.disable = disable;
    }

    public String getKefu() {
        return kefu;
    }

    public void setKefu(String kefu) {
        this.kefu = kefu;
    }

    public boolean isAgent() {
        return isAgent;
    }

    public void setAgent(boolean agent) {
        isAgent = agent;
    }

    public String getTixianRealFullName() {
        return tixianRealFullName;
    }

    public void setTixianRealFullName(String tixianRealFullName) {
        this.tixianRealFullName = tixianRealFullName;
    }

    public String getTixianAccount() {
        return tixianAccount;
    }

    public void setTixianAccount(String tixianAccount) {
        this.tixianAccount = tixianAccount;
    }

    public static void clearLocalSpUserBasicInfo() {
        Share.get().setHeadPic("");

        Share.get().setUserName("");
        Share.get().setImUserSig("");

        Share.get().setBmobCurrentUserObject("");
        Share.get().setUser_wallet_bean_object_id("");

        Share.get().setBirthDay("");
        Share.get().setSex("");
        Share.get().setNickName("");
        Share.get().setBmobCurrentUserWechat("");
        Share.get().setHeight(0);
        Share.get().setWeight(0);
        Share.get().setAddress("");
        Share.get().setZujiAddress("");
        Share.get().setXueli("");
        Share.get().setBiyeSchool("");
        Share.get().setWork("");
        Share.get().setYearInCome("");

        Share.get().setIsVip(false);
        Share.get().setIsAgent(false);
        Share.get().setRechargeCount(0);

        Share.get().setTiXianRealFullName("");
        Share.get().setTiXianAccount("");
        Share.get().setMyInviteUserName("");

        Share.get().setWxRefreshToken("");

        Share.get().setIsLogin(false);

        BmobUser.logOut();//Bmob退出登录

        //退出IM
        TUILogin.logout(new V2TIMCallback() {
            @Override
            public void onSuccess() {
                Log.e(TAG, "logout im success");
            }

            @Override
            public void onError(int code, String desc) {
                Log.e(TAG, "logout im fail: " + code + "=" + desc);
            }
        });
    }

    public static void registerAndLoginBmob(String phone, String psw, String sex) {
        BmobQuery<UserBean> categoryBmobQuery = new BmobQuery<>();
        categoryBmobQuery.addWhereEqualTo("username", phone);
        categoryBmobQuery.findObjects(new FindListener<UserBean>() {
            @Override
            public void done(List<UserBean> result, BmobException e) {
                if (e == null) {
                    if (!result.isEmpty()) {
                        //用户存在，进行登录
                        EventBus.getDefault().post(new Event.toWxLogin(phone, psw));
                    } else {
                        //用户不存在，注册并登录
                        registerBmob(phone, psw, sex);
                    }
                } else {
                    Log.e(TAG, e.toString());
                }
            }
        });
    }

    private static void registerBmob(String phone, String psw, String sex) {
        UserBean user = new UserBean();
        user.setUsername(phone);
        user.setPassword(psw);
        user.setSex(sex);
        user.setDeviceId(Share.get().getDeviceId());
        user.signUp(new SaveListener<UserBean>() {
            @Override
            public void done(UserBean user, BmobException e) {
                if (e == null) {
                    //注册成功，进行登录
                    EventBus.getDefault().post(new Event.toWxLogin(phone, psw));
                } else {
                    Log.e(TAG, "微信信息注册Bmob失败了：" + e.toString());
                }
            }
        });
    }
}
