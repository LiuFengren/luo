package com.huolongluo.luo.huolongluo.bean;

import cn.bmob.v3.BmobObject;

public class LabelsBean extends BmobObject {
    private String labelType;//标签类型
    private String labelContent;//对应的标签内容,"，"分割

    public String getLabelType() {
        return labelType;
    }

    public void setLabelType(String labelType) {
        this.labelType = labelType;
    }

    public String getLabelContent() {
        return labelContent;
    }

    public void setLabelContent(String labelContent) {
        this.labelContent = labelContent;
    }
}
