package com.huolongluo.luo.huolongluo.bean;

import cn.bmob.v3.BmobObject;

public class GiftListBean extends BmobObject {
    private String giftName;//礼物名称
    private String giftPic;//礼物图片
    private String giftGoldNum;//礼物的金币价格

    public String getGiftName() {
        return giftName;
    }

    public void setGiftName(String giftName) {
        this.giftName = giftName;
    }

    public String getGiftPic() {
        return giftPic;
    }

    public void setGiftPic(String giftPic) {
        this.giftPic = giftPic;
    }

    public String getGiftGoldNum() {
        return giftGoldNum;
    }

    public void setGiftGoldNum(String giftGoldNum) {
        this.giftGoldNum = giftGoldNum;
    }

    @Override
    public String toString() {
        return "GiftListBean{" +
                "giftName='" + giftName + '\'' +
                ", giftPic='" + giftPic + '\'' +
                ", giftGoldNum='" + giftGoldNum + '\'' +
                '}';
    }
}
