package com.huolongluo.luo.huolongluo.ui.activity.aboutus;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.AppUtils;
import com.gyf.immersionbar.ImmersionBar;
import com.huolongluo.luo.R;
import com.huolongluo.luo.huolongluo.base.BaseActivity;
import com.huolongluo.luo.huolongluo.bean.UserBean;
import com.huolongluo.luo.huolongluo.bean.VersionUpdateBean;
import com.huolongluo.luo.huolongluo.manager.DialogClickListener;
import com.huolongluo.luo.huolongluo.ui.activity.accountset.AccountSetActivity;
import com.huolongluo.luo.huolongluo.ui.activity.browser.BrowserActivity;
import com.huolongluo.luo.huolongluo.ui.activity.login.LoginActivity;
import com.huolongluo.luo.manager.AppManager;
import com.huolongluo.luo.util.L;
import com.huolongluo.luo.util.ToastSimple;

import butterknife.BindView;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.QueryListener;

/**
 * <p>
 * Created by 火龙裸 on 2017/9/6.
 */

public class AboutUsActivity extends BaseActivity {
    @BindView(R.id.toolbar_center_title)
    TextView toolbar_center_title;
    @BindView(R.id.iv_left)
    ImageView iv_left;
    @BindView(R.id.tv_right)
    TextView tv_right;
    /********************* 以上为ToolBar ************************/
    @BindView(R.id.tv_service_agreement)
    TextView tv_service_agreement;
    @BindView(R.id.tv_servier_policy)
    TextView tv_servier_policy;
    @BindView(R.id.tv_recharge_protocol)
    TextView tv_recharge_protocol;

    @Override
    protected int getContentViewId() {
        return R.layout.activity_about_us;
    }

    @Override
    protected void injectDagger() {
        activityComponent().inject(this);
    }

    private void initToolBar() {
        ImmersionBar.with(this).statusBarDarkFont(true).init();
        toolbar_center_title.setText("关于我们");
    }

    @Override
    protected void initViewsAndEvents(@Nullable Bundle savedInstanceState) {
        initToolBar();
        eventClick(iv_left).subscribe(o -> // 点击返回键
        {
            close();
        });

        eventClick(tv_service_agreement).subscribe(o -> // 点击用户服务协议
        {
            Intent intent = new Intent(AboutUsActivity.this, BrowserActivity.class);
            intent.putExtra(BrowserActivity.PARAM_TITLE, "用户服务协议");
            intent.putExtra(BrowserActivity.PARAM_URL, "http://www.huolongluo.top/service_agreement.html");
            intent.putExtra(BrowserActivity.PARAM_MODE, BrowserActivity.MODE_SONIC);
            startActivity(intent);
        });
        eventClick(tv_servier_policy).subscribe(o -> // 点击隐私政策
        {
            Intent intent = new Intent(AboutUsActivity.this, BrowserActivity.class);
            intent.putExtra(BrowserActivity.PARAM_TITLE, "隐私政策");
            intent.putExtra(BrowserActivity.PARAM_URL, "http://www.huolongluo.top/servier_policy.html");
            intent.putExtra(BrowserActivity.PARAM_MODE, BrowserActivity.MODE_SONIC);
            startActivity(intent);
        });
        eventClick(tv_recharge_protocol).subscribe(o -> // 点击用户充值协议
        {
            Intent intent = new Intent(AboutUsActivity.this, BrowserActivity.class);
            intent.putExtra(BrowserActivity.PARAM_TITLE, "用户充值协议");
            intent.putExtra(BrowserActivity.PARAM_URL, "http://www.huolongluo.top/recharge_protocol.html");
            intent.putExtra(BrowserActivity.PARAM_MODE, BrowserActivity.MODE_SONIC);
            startActivity(intent);
        });
    }
}
