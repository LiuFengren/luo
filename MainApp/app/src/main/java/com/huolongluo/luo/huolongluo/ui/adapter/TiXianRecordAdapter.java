package com.huolongluo.luo.huolongluo.ui.adapter;

import android.content.Context;

import com.huolongluo.luo.R;
import com.huolongluo.luo.huolongluo.bean.TiXianRecordBean;
import com.huolongluo.luo.superAdapter.recycler.BaseViewHolder;
import com.huolongluo.luo.superAdapter.recycler.SuperAdapter;

import java.util.List;

/**
 * Created by 火龙裸 on 2018/6/10 0010.
 */

public class TiXianRecordAdapter extends SuperAdapter<TiXianRecordBean> {
    public TiXianRecordAdapter(Context context, List<TiXianRecordBean> list, int layoutResId) {
        super(context, list, layoutResId);
    }

    @Override
    public void onBind(int viewType, BaseViewHolder holder, int position, TiXianRecordBean item) {

        holder.setText(R.id.tv_tixian_iifen, item.getTixianJiFen() + "");
        holder.setText(R.id.tv_tixian_account, item.getTiXianAccount());
        holder.setText(R.id.tv_tixian_time, item.getUpdatedAt());

        switch (item.getTixianState()) {
            case 0:
                holder.setText(R.id.tv_tixian_state, "待审核");
                holder.setBackgroundColor(R.id.tv_tixian_state, R.color.F4EA9A5);
                break;
            case 1:
                holder.setText(R.id.tv_tixian_state, "已提现");
                holder.setBackgroundColor(R.id.tv_tixian_state, R.color.F6E6FFF);
                break;
            case -1:
                holder.setText(R.id.tv_tixian_state, "审核未通过");
                holder.setBackgroundColor(R.id.tv_tixian_state, R.color.F6898E);
                break;
        }

    }
}
