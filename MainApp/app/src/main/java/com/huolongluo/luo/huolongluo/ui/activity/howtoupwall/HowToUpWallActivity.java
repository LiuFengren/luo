package com.huolongluo.luo.huolongluo.ui.activity.howtoupwall;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.gyf.immersionbar.ImmersionBar;
import com.huolongluo.luo.R;
import com.huolongluo.luo.huolongluo.base.BaseActivity;
import com.huolongluo.luo.huolongluo.bean.GoldToUpWallBean;
import com.huolongluo.luo.huolongluo.bean.UserWalletBean;
import com.huolongluo.luo.huolongluo.manager.DialogClickListener;
import com.huolongluo.luo.huolongluo.share.Share;
import com.huolongluo.luo.huolongluo.ui.activity.account.MyAccountActivity;
import com.huolongluo.luo.huolongluo.ui.activity.login.LoginActivity;
import com.huolongluo.luo.util.Arith;
import com.huolongluo.luo.util.ToastSimple;

import java.util.List;

import butterknife.BindView;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.SaveListener;
import cn.bmob.v3.listener.UpdateListener;

/**
 * <p>
 * Created by 火龙裸 on 2017/9/6.
 */

public class HowToUpWallActivity extends BaseActivity {
    private static final String TAG = "HowToUpWallActivity";
    @BindView(R.id.toolbar_center_title)
    TextView toolbar_center_title;
    @BindView(R.id.iv_left)
    ImageView iv_left;
    /********************* 以上为ToolBar ************************/
    @BindView(R.id.tv_how_up_wall)
    TextView tv_how_up_wall;
    @BindView(R.id.tv_to_up_wall)
    TextView tv_to_up_wall;

    private String currentUserWalletBeanObjectId = "";

    @Override
    protected int getContentViewId() {
        return R.layout.activity_how_to_upwall;
    }

    @Override
    protected void injectDagger() {
        activityComponent().inject(this);
    }

    private void initToolBar() {
        ImmersionBar.with(this).statusBarDarkFont(true).init();
        toolbar_center_title.setText("上墙介绍");
    }

    @Override
    protected void initViewsAndEvents(@Nullable Bundle savedInstanceState) {
        initToolBar();

        tv_how_up_wall.setText("首页，需要认认真真完善个人介绍和资料，让大家能了解你，展示自己的个人魅力，一定会有人欣赏你，并推荐你。其次抑或是通过点击下方按钮，消耗" + Share.get().getUpWallPrice() + "金币，亦可获得上墙机会（连续多次消耗金币，系统则连续对应数周为你提供专属流量曝光）。");

        eventClick(iv_left).subscribe(o -> // 点击返回键
        {
            close();
        });
        eventClick(tv_to_up_wall).subscribe(o ->
        {
            double goldBalance = Double.parseDouble(Share.get().getGoldBalance());//金币余额
            double upWallPrice = Double.parseDouble(Share.get().getUpWallPrice());//上墙价格
            if (goldBalance - upWallPrice >= 0) {
                //更新用户钱包里的金币余额
                updateUserWalletBean(Arith.sub(goldBalance, upWallPrice));
            } else {
                View view = LayoutInflater.from(HowToUpWallActivity.this).inflate(R.layout.toast_text, null);
                TextView tv_toast_message = view.findViewById(R.id.tv_toast_message);
                tv_toast_message.setText("金币余额不足，请先充值");
                ToastSimple.makeText(Gravity.CENTER, 2, view).show();
                startActivity(MyAccountActivity.class);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        //查询个人钱包余额，是否会员
        queryUserWallet();
    }

    private void queryUserWallet() {
        BmobQuery<UserWalletBean> categoryBmobQuery = new BmobQuery<>();
        categoryBmobQuery.addWhereEqualTo("username", Share.get().getUserName());
        categoryBmobQuery.findObjects(new FindListener<UserWalletBean>() {
            @Override
            public void done(List<UserWalletBean> result, BmobException e) {
                if (e == null) {
                    if (!result.isEmpty()) {
                        UserWalletBean userWalletBean = result.get(0);
                        currentUserWalletBeanObjectId = userWalletBean.getObjectId();
                        Share.get().setUser_wallet_bean_object_id(currentUserWalletBeanObjectId);
                        Share.get().setIsVip(userWalletBean.isVip());
                        Share.get().setGoldBalance(userWalletBean.getGoldBalance());
                        Share.get().setIntegralBalance(userWalletBean.getIntegralBalance());
                    } else {
//                        createUserWallet();//给用户创建一个钱包
                        UserWalletBean.createUserWallet();
                    }
                } else {
                    Log.e(TAG, e.toString());
                }
            }
        });
    }

//    /**
//     * 给用户创建一个钱包
//     */
//    private void createUserWallet() {
//        UserWalletBean p2 = new UserWalletBean();
//        p2.setUsername(Share.get().getUserName());
//        p2.setVip(false);
//        p2.setGoldBalance(100);
//        p2.setIntegralBalance(0);
//        p2.save(new SaveListener<String>() {
//            @Override
//            public void done(String objectId, BmobException e) {
//                if (e == null) {
//                    Log.e(TAG, "给用户创建一个钱包 数据成功，返回objectId为：" + objectId);
//                    Share.get().setGoldBalance(100);
//                } else {
//                    Log.e(TAG, "给用户创建一个钱包 创建数据失败：" + e.getMessage());
//                }
//            }
//        });
//    }

    /**
     * 更新用户钱包里的金币余额
     *
     * @param goldBalance 要更新为多少金币
     */
    private void updateUserWalletBean(double goldBalance) {
        if (!TextUtils.isEmpty(currentUserWalletBeanObjectId)) {
            //更新GoldToUpWallBean表里面id为6b6c11c537的数据，address内容更新为“北京朝阳”
            showProgressDialog("");
            UserWalletBean p2 = new UserWalletBean();
            p2.setGoldBalance(goldBalance + "");
            p2.update(currentUserWalletBeanObjectId, new UpdateListener() {
                @Override
                public void done(BmobException e) {
                    if (e == null) {
                        Log.e(TAG, "更新用户钱包里的金币余额 成功:" + p2.getUpdatedAt());
                        Share.get().setGoldBalance(goldBalance + "");

                        updataGoldToUpWallBean();
                    } else {
                        Log.e(TAG, "更新用户钱包里的金币余额 失败：" + e.getMessage());
                        View view = LayoutInflater.from(HowToUpWallActivity.this).inflate(R.layout.toast_text, null);
                        TextView tv_toast_message = view.findViewById(R.id.tv_toast_message);
                        tv_toast_message.setText(e.getMessage());
                        ToastSimple.makeText(Gravity.CENTER, 1, view).show();
                        hideProgressDialog();
                    }
                }
            });
        } else {
            Log.e(TAG, "updateUserWalletBean: 要更新的金币余额 数据objectId为空");
        }
    }

    private void updataGoldToUpWallBean() {
        GoldToUpWallBean goldToUpWallBean = new GoldToUpWallBean();
        goldToUpWallBean.setUsername(Share.get().getUserName());
        goldToUpWallBean.save(new SaveListener<String>() {
            @Override
            public void done(String objectId, BmobException e) {
                hideProgressDialog();
                if (e == null) {
                    Log.e(TAG, "添加数据成功，返回objectId为：" + objectId);
                    showConfirmNoCancelDialog("上墙申请", "上墙申请成功，将为你提供专属流量曝光", new DialogClickListener() {
                        @Override
                        public void onConfirm() {
                        }

                        @Override
                        public void onCancel() {
                        }
                    });
                } else {
                    ToastSimple.show("上墙失败：" + e.getMessage());
                    showConfirmNoCancelDialog("上墙申请", "上墙申请提交失败，失败导致平白消耗的金币，将退回至你的账户", new DialogClickListener() {
                        @Override
                        public void onConfirm() {
                        }

                        @Override
                        public void onCancel() {
                        }
                    });
                }
            }
        });
    }
}
