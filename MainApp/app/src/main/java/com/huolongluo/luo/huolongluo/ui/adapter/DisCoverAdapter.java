package com.huolongluo.luo.huolongluo.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.huolongluo.luo.R;
import com.huolongluo.luo.huolongluo.bean.DisCoverBean;
import com.huolongluo.luo.huolongluo.constants.Constants;
import com.huolongluo.luo.huolongluo.share.Event;
import com.huolongluo.luo.huolongluo.ui.activity.userdetail.UserDetailInfoActivity;
import com.huolongluo.luo.superAdapter.recycler.BaseViewHolder;
import com.huolongluo.luo.superAdapter.recycler.IMultiItemViewType;
import com.huolongluo.luo.superAdapter.recycler.SuperAdapter;
import com.huolongluo.luo.util.TextDrawAbleUtils;
import com.jakewharton.rxbinding.view.RxView;
import com.luck.picture.lib.decoration.GridSpacingItemDecoration;
import com.luck.picture.lib.utils.DensityUtil;
import com.tencent.liteav.trtccalling.model.util.ImageLoader;

import org.greenrobot.eventbus.EventBus;

import java.util.List;
import java.util.concurrent.TimeUnit;

import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by 火龙裸 on 2018/6/10 0010.
 */

public class DisCoverAdapter extends SuperAdapter<DisCoverBean> {
    private static final String TAG = "DisCoverAdapter";

    public DisCoverAdapter(Context context, List<DisCoverBean> list, int layoutResId) {
        super(context, list, layoutResId);
    }

    @Override
    public void onBind(int viewType, BaseViewHolder holder, int position, DisCoverBean item) {
        ImageLoader.loadImage(mContext, holder.getView(R.id.iv_user), item.getUserHeaderImg());
        holder.setText(R.id.tv_user_name, TextUtils.isEmpty(item.getNickName()) ? item.getUsername() : item.getNickName());
        if (TextUtils.equals("男", item.getSex())) {
            TextDrawAbleUtils.setTextDrawable(mContext, R.mipmap.ic_register_male_checked, holder.getView(R.id.tv_user_name), 3);
        } else {
            TextDrawAbleUtils.setTextDrawable(mContext, R.mipmap.ic_register_female_checked, holder.getView(R.id.tv_user_name), 3);
        }
        holder.setText(R.id.tv_time, item.getUpdatedAt());
        holder.setText(R.id.tv_content, item.getDongtaiText());

        // 点击头像
        RxView.clicks(holder.getView(R.id.iv_user)).throttleFirst(500, TimeUnit.MILLISECONDS).observeOn(AndroidSchedulers.mainThread()).subscribe(v ->
        {
            Intent intent = new Intent(mContext, UserDetailInfoActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(Constants.USER_NAME, item.getUsername());
            intent.putExtra("bundle", bundle);
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            mContext.startActivity(intent);
        });
        // 点击动态更多
        RxView.clicks(holder.getView(R.id.iv_relay)).throttleFirst(500, TimeUnit.MILLISECONDS).observeOn(AndroidSchedulers.mainThread()).subscribe(v ->
        {
            EventBus.getDefault().post(new Event.clickDongTaiMore(position, item));
        });

        RecyclerView rv_dongtai_pic = holder.getView(R.id.rv_dongtai_pic);
        rv_dongtai_pic.setNestedScrollingEnabled(false);
        rv_dongtai_pic.setFocusable(false);

        if (item.getDongtaiPic().size() <= 2) {
            rv_dongtai_pic.setLayoutManager(new GridLayoutManager(mContext, 2) {
                @Override
                public boolean canScrollVertically() {
                    return false;
                }
            });

//            rv_dongtai_pic.addItemDecoration(new GridSpacingItemDecoration(2,
//                    DensityUtil.dip2px(mContext, 10), false));

//            if (rv_dongtai_pic.getItemDecorationCount() <= 0) {
//                RecyclerView.ItemDecoration itemDecoration = new GridSpacingItemDecoration(2,
//                        DensityUtil.dip2px(mContext, 10), false);
//                rv_dongtai_pic.addItemDecoration(itemDecoration);
//            }
        } else {
            rv_dongtai_pic.setLayoutManager(new GridLayoutManager(mContext, 3) {
                @Override
                public boolean canScrollVertically() {
                    return false;
                }
            });

//            rv_dongtai_pic.addItemDecoration(new GridSpacingItemDecoration(3,
//                    DensityUtil.dip2px(mContext, 10), false));

//            if (rv_dongtai_pic.getItemDecorationCount() <= 0) {
//                RecyclerView.ItemDecoration itemDecoration = new GridSpacingItemDecoration(3,
//                        DensityUtil.dip2px(mContext, 10), false);
//                rv_dongtai_pic.addItemDecoration(itemDecoration);
//            }
        }


        DisCoverPicAdapter disCoverPicAdapter = new DisCoverPicAdapter(mContext, item.getDongtaiPic(), new IMultiItemViewType<String>() {
            @Override
            public int getItemViewType(int position, String s) {
                if (item.getDongtaiPic().size() > 2) {
                    return DisCoverPicAdapter.TYPE_PIC_MULTIPLE;
                } else {
                    return DisCoverPicAdapter.TYPE_PIC_SINGLE;
                }
            }

            @Override
            public int getLayoutId(int viewType) {
                if (viewType == DisCoverPicAdapter.TYPE_PIC_SINGLE) {
                    return R.layout.item_discover_pic1;
                } else {
                    return R.layout.item_discover_pic2;
                }
            }
        });
        rv_dongtai_pic.setAdapter(disCoverPicAdapter);

        disCoverPicAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int viewType, int position) {
                Log.e(TAG, "onItemClick: 点击图片：" + item.getDongtaiPic().get(position));
            }
        });
    }
//    public DisCoverAdapter(Context context, List<DisCoverBean> list, int layoutResId) {
//        super(context, list, layoutResId);
//    }

    /*@Override
    public void onBind(int viewType, BaseViewHolder holder, int position, DisCoverBean item) {
//        AboutMeBean aboutMeBean = item.getAboutMe();
//
//        if (aboutMeBean != null && !aboutMeBean.getUserTopPic().isEmpty()) {
//            ImageLoader.loadImage(mContext, holder.getView(R.id.iv_user_pic), aboutMeBean.getUserTopPic().get(0));
////            Glide.with(mContext).load(item.getUserPic()).error(loadTransform(context, errorResId, radius)).into(imageView);
//        }

//        if (item.getUserTopPic() != null && !item.getUserTopPic().isEmpty()) {
//            ImageLoader.loadImage(mContext, holder.getView(R.id.iv_user_pic), item.getUserTopPic().get(0));
//        }
//
//        holder.setText(R.id.tv_nick_name, item.getNickName());
//        holder.setText(R.id.tv_address, item.getAddress());
//        holder.setText(R.id.tv_zuji, item.getZujiAddress());
//        if (!TextUtils.isEmpty(item.getBirthday())) {
//            int yearIndex = item.getBirthday().indexOf("-");
//            String year = "";
//            if (yearIndex != -1) {
//                year = item.getBirthday().substring(0, yearIndex);
//            }
//            holder.setText(R.id.tv_old, year + "年");
//        }
//        holder.setText(R.id.tv_height, item.getHeight() + "cm");
//        holder.setText(R.id.tv_xueli, item.getXueli());
//        holder.setText(R.id.tv_work, item.getWork());
//        if (item.getGiftGold() <= 0) {
//            holder.setVisibility(R.id.tv_vip_gift_num, View.INVISIBLE);
//        } else {
//            holder.setText(R.id.tv_vip_gift_num,"送" + item.getGiftGold() + "金币");
//            holder.setVisibility(R.id.tv_vip_gift_num, View.VISIBLE);
//        }
//
//        if (item.isChecked()) {
//            holder.getView(R.id.ll_chongzhi_type).setBackgroundResource(R.mipmap.icon_charge_item_selected);
//        } else {
//            holder.getView(R.id.ll_chongzhi_type).setBackgroundResource(0);
//        }
//
//        // 点击item
//        RxView.clicks(holder.getItemView()).throttleFirst(500, TimeUnit.MILLISECONDS).observeOn(AndroidSchedulers.mainThread()).subscribe(v ->
//        {
//            Log.e("TAG", "onBind: 点击了：" + position + "  共：" + getItemCount());
//
//            for (int i = 0; i < getItemCount(); i++) {
//                getAllData().get(i).setChecked(false);
//            }
//            item.setChecked(true);
//            for (int i = 0; i < getItemCount(); i++) {
//                Log.e("TAG", "onBind: 最终的选中情况：" + item.isChecked());
//            }
//            notifyDataSetChanged();
//        });

        // 点击任务弹出窗体
//        RxView.clicks(holder.getView(R.id.tv_task_name)).throttleFirst(500, TimeUnit.MILLISECONDS).observeOn(AndroidSchedulers.mainThread()).subscribe(v ->
//        {
//            EventBus.getDefault().post(new Event.Task(item.getContent()+""));
//        });
//        // 点击 提交 任务
//        RxView.clicks(holder.getView(R.id.tv_submit01)).throttleFirst(500, TimeUnit.MILLISECONDS).observeOn(AndroidSchedulers.mainThread()).subscribe(v ->
//        {
//            Intent intent = new Intent(mContext, SubMitTaskActivity.class);
//            intent.putExtra("tId", item.getId()+"");
//            intent.putExtra("position", position);
//            intent.putExtra("title", item.getContent());
//            mContext.startActivity(intent);
//        });

    }*/
}
