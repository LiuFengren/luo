package com.huolongluo.luo.huolongluo.bean;

import cn.bmob.v3.BmobObject;

public class VersionUpdateBean extends BmobObject {
    private String latestVersionCode;//最新版本号
    private String downLoadUrl;//下载地址
    private boolean isForcedUpdate;//是否强制更新

    public String getLatestVersionCode() {
        return latestVersionCode;
    }

    public void setLatestVersionCode(String latestVersionCode) {
        this.latestVersionCode = latestVersionCode;
    }

    public String getDownLoadUrl() {
        return downLoadUrl;
    }

    public void setDownLoadUrl(String downLoadUrl) {
        this.downLoadUrl = downLoadUrl;
    }

    public boolean isForcedUpdate() {
        return isForcedUpdate;
    }

    public void setForcedUpdate(boolean forcedUpdate) {
        isForcedUpdate = forcedUpdate;
    }
}
