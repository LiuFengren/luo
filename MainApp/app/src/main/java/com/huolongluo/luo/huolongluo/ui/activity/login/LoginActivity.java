package com.huolongluo.luo.huolongluo.ui.activity.login;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.LinkMovementMethod;
import android.text.method.PasswordTransformationMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.gyf.immersionbar.ImmersionBar;
import com.huolongluo.luo.MainActivity;
import com.huolongluo.luo.R;
import com.huolongluo.luo.huolongluo.base.BaseActivity;
import com.huolongluo.luo.huolongluo.base.BaseApp;
import com.huolongluo.luo.huolongluo.bean.UserBean;
import com.huolongluo.luo.huolongluo.bean.UserLoginBean;
import com.huolongluo.luo.huolongluo.manager.DialogClickListener;
import com.huolongluo.luo.huolongluo.share.Event;
import com.huolongluo.luo.huolongluo.share.Share;
import com.huolongluo.luo.huolongluo.ui.activity.browser.BrowserActivity;
import com.huolongluo.luo.huolongluo.ui.activity.register.RegisterActivity;
import com.huolongluo.luo.huolongluo.ui.signature.GenerateTestUserSig;
import com.huolongluo.luo.manager.pay.wechat.WxConfig;
import com.huolongluo.luo.util.L;
import com.huolongluo.luo.util.ToastSimple;
import com.jakewharton.rxbinding.widget.RxTextView;
import com.tencent.imsdk.v2.V2TIMCallback;
import com.tencent.mm.opensdk.modelmsg.SendAuth;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.tencent.qcloud.tuicore.TUILogin;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import cn.bmob.v3.BmobUser;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.LogInListener;
import cn.bmob.v3.listener.SaveListener;

/**
 * <p>
 * Created by 火龙裸 on 2017/9/6.
 */

public class LoginActivity extends BaseActivity {
    private static final String TAG = "LoginActivity";
//    private CustomVideoView video_view;

    @BindView(R.id.et_phone)
    EditText et_phone;
    @BindView(R.id.et_psw)
    EditText et_psw;
    @BindView(R.id.tv_register)
    TextView tv_register;
    @BindView(R.id.tv_forget_psw)
    TextView tv_forget_psw;
    @BindView(R.id.iv_close_psw)
    ImageView iv_close_psw;
    @BindView(R.id.btn_login)
    Button btn_login;
    @BindView(R.id.iv_wx_login)
    ImageView iv_wx_login;
    @BindView(R.id.ck_app_use_tip)
    CheckBox ck_app_use_tip;
    @BindView(R.id.tv_app_use_tip)
    TextView tv_app_use_tip;

    private boolean isHidden = false; // 是否隐藏密码

    private String phone;//输入的手机号码
    private String psw;//输入的密码

    @Override
    protected int getContentViewId() {
        return R.layout.activity_login;
    }

    @Override
    protected void injectDagger() {
    }

    @Override
    protected void initViewsAndEvents(@Nullable Bundle savedInstanceState) {
        EventBus.getDefault().register(this);
        ImmersionBar.with(this).statusBarDarkFont(true).init();
//        iv_login_bg = findViewById(R.id.iv_login_bg);
//        startBgAnimation();//开始背景动画
//        initVideoView();

        eventClick(tv_register).subscribe(o ->
        {
            startActivity(RegisterActivity.class);
        });
        eventClick(tv_forget_psw).subscribe(o ->
        {
            showConfirmNoCancelDialog("忘记密码", "请联系客服。WX：huolongluoS", new DialogClickListener() {
                @Override
                public void onConfirm() {
                    ClipboardManager cm = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);//创建普通字符型ClipData
                    ClipData mClipData = ClipData.newPlainText("simple text", "huolongluoS");//将ClipData内容放到系统剪贴板里。
                    cm.setPrimaryClip(mClipData);

                    View view = LayoutInflater.from(LoginActivity.this).inflate(R.layout.toast_text, null);
                    TextView tv_toast_message = view.findViewById(R.id.tv_toast_message);
                    tv_toast_message.setText("微信号已复制");
                    ToastSimple.makeText(Gravity.CENTER, 1, view).show();
                }

                @Override
                public void onCancel() {
                }
            });
        });

        eventClick(iv_close_psw).subscribe(o ->
        {
            if (isHidden) {
                et_psw.setTransformationMethod(PasswordTransformationMethod.getInstance()); // 隐藏密码
                iv_close_psw.setImageResource(R.mipmap.password_icon_unhide);
            } else {
                et_psw.setTransformationMethod(HideReturnsTransformationMethod.getInstance()); // 明文密码
                iv_close_psw.setImageResource(R.mipmap.password_icon_hide);
            }
            isHidden = !isHidden;
            et_psw.setSelection(et_psw.getText().toString().trim().length()); // 将光标移至文字末尾
        });

        eventClick(btn_login).subscribe(o ->
        {
            if (TextUtils.isEmpty(phone)) {
                View view = LayoutInflater.from(this).inflate(R.layout.toast_text, null);
                TextView tv_toast_message = view.findViewById(R.id.tv_toast_message);
                tv_toast_message.setText("请输入登录手机号码");
                ToastSimple.makeText(Gravity.CENTER, 1, view).show();
            } else if (TextUtils.isEmpty(psw)) {
                View view = LayoutInflater.from(this).inflate(R.layout.toast_text, null);
                TextView tv_toast_message = view.findViewById(R.id.tv_toast_message);
                tv_toast_message.setText("请输入登录密码");
                ToastSimple.makeText(Gravity.CENTER, 1, view).show();
            } else if (!ck_app_use_tip.isChecked()) {
                showConfirmNoCancelDialog("提示", "请阅读并且同意《用户服务协议》，和《隐私政策》后方可登录使用。", new DialogClickListener() {
                    @Override
                    public void onConfirm() {
                    }

                    @Override
                    public void onCancel() {
                    }
                });
            } else {
                toLogin(phone, psw);
            }
        });

        IWXAPI api = WXAPIFactory.createWXAPI(this, WxConfig.APP_ID, false);

        eventClick(iv_wx_login).subscribe(o ->
        {
            if (!ck_app_use_tip.isChecked()) {
                showConfirmNoCancelDialog("提示", "请阅读并且同意《用户服务协议》，和《隐私政策》后方可登录使用。", new DialogClickListener() {
                    @Override
                    public void onConfirm() {
                    }

                    @Override
                    public void onCancel() {
                    }
                });
            } else {
                SendAuth.Req req = new SendAuth.Req();
                req.scope = "snsapi_userinfo,SCOPE";
                req.state = "wechat_sdk_demo_test";
                api.sendReq(req);
            }
        });

        RxTextView.afterTextChangeEvents(et_phone).subscribe(event ->
        {
            phone = event.view().getText().toString().trim();
            psw = et_psw.getText().toString().trim();
        });
        RxTextView.afterTextChangeEvents(et_psw).subscribe(event ->
        {
            psw = event.view().getText().toString().trim();
            phone = et_phone.getText().toString().trim();
        });

//        eventClick(ck_app_use_tip).subscribe(o ->
//        {
//            ck_app_use_tip.setChecked(!(ck_app_use_tip.isChecked()));
//        });

        //服务协议
        ClickableSpan span_service_agreement = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
//                String text = ((TextView) widget).getText().toString().substring(9, 15);
//                Toast.makeText(LoginActivity.this, text, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(LoginActivity.this, BrowserActivity.class);
                intent.putExtra(BrowserActivity.PARAM_TITLE, "用户服务协议");
                intent.putExtra(BrowserActivity.PARAM_URL, "http://www.huolongluo.top/service_agreement.html");
                intent.putExtra(BrowserActivity.PARAM_MODE, BrowserActivity.MODE_SONIC);
                startActivity(intent);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false); //去除下划线
            }
        };
        //隐私政策
        ClickableSpan span_servier_policy = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
//                String text = ((TextView) widget).getText().toString().substring(18, 22);
//                Toast.makeText(LoginActivity.this, text, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(LoginActivity.this, BrowserActivity.class);
                intent.putExtra(BrowserActivity.PARAM_TITLE, "隐私政策");
                intent.putExtra(BrowserActivity.PARAM_URL, "http://www.huolongluo.top/servier_policy.html");
                intent.putExtra(BrowserActivity.PARAM_MODE, BrowserActivity.MODE_SONIC);
                startActivity(intent);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false); //去除下划线
            }
        };
        SpannableString spannableString = new SpannableString("登录 即代表你同意用户服务协议 和 隐私政策");
        spannableString.setSpan(span_service_agreement, 9, 15, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        spannableString.setSpan(span_servier_policy, 18, 22, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        spannableString.setSpan(new ForegroundColorSpan(Color.parseColor("#F5A623")), 9, 15, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        spannableString.setSpan(new ForegroundColorSpan(Color.parseColor("#F5A623")), 18, 22, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        tv_app_use_tip.setMovementMethod(LinkMovementMethod.getInstance());
        tv_app_use_tip.setHighlightColor(Color.parseColor("#00000000"));//去除点击富文本后，出现背景色
        tv_app_use_tip.setText(spannableString);
    }

    private void toSaveUserLoginInfo(UserBean user) {
        L.e("===================开始保存用户登录信息");
        UserLoginBean userLoginBean = new UserLoginBean();
        userLoginBean.setUsername(user.getUsername());
        userLoginBean.setPassword(psw);
        userLoginBean.setInviteCode(user.getInviteCode());
        userLoginBean.setDeviceId(user.getDeviceId());
        userLoginBean.save(new SaveListener<String>() {
            @Override
            public void done(String objectId, BmobException e) {
                if (e != null) {
                    L.e("创建登录数据失败：" + e.getMessage());
                } else {
                    L.e("保存用户登录信息成功");
                }
            }
        });
    }

    private void toLoginIM(String userPhone, String userPsw) {
        // 在用户 UI 点击登录的时候登录 UI 组件：
        // 获取userSig函数
        String userSig = GenerateTestUserSig.genTestUserSig(userPhone);
        TUILogin.login(userPhone, userSig, new V2TIMCallback() {
            @Override
            public void onError(final int code, final String desc) {
                hideProgressDialog();
                L.e("登录IM失败：" + desc);
                View view = LayoutInflater.from(LoginActivity.this).inflate(R.layout.toast_text, null);
                TextView tv_toast_message = view.findViewById(R.id.tv_toast_message);
                tv_toast_message.setText("登录失败:" + desc);
                ToastSimple.makeText(Gravity.CENTER, 1, view).show();
            }

            @Override
            public void onSuccess() {
                hideProgressDialog();
                View view = LayoutInflater.from(LoginActivity.this).inflate(R.layout.toast_text, null);
                TextView tv_toast_message = view.findViewById(R.id.tv_toast_message);
                tv_toast_message.setText("登录成功:" + userPhone);

                Share.get().setUserName(userPhone);
                Share.get().setImUserSig(userSig);
                Share.get().setIsLogin(true);

                ToastSimple.makeText(Gravity.CENTER, 1, view).show();
                startActivity(MainActivity.class);
                close();
            }
        });
    }

    private void toLogin(String userPhone, String userPsw) {
        showProgressDialog("");

        BaseApp.get(this).init(0);

        //此处替换为你的用户名密码
        BmobUser.loginByAccount(userPhone, userPsw, new LogInListener<UserBean>() {
            @Override
            public void done(UserBean user, BmobException e) {
                if (e == null) {
                    if (user.isDisable()) {
                        hideProgressDialog();
                        View view = LayoutInflater.from(LoginActivity.this).inflate(R.layout.toast_text, null);
                        TextView tv_toast_message = view.findViewById(R.id.tv_toast_message);
                        tv_toast_message.setText("服务器异常:0");
                        ToastSimple.makeText(Gravity.CENTER, 1, view).show();
                    } else {
                        Log.e(TAG, "done: 当前登录的用户性别：" + user.getSex());
                        Share.get().setSex(user.getSex());
                        Share.get().setBmobCurrentUserObject(user.getObjectId());
                        Share.get().setMyInviteUserName(user.getInviteCode());
                        Share.get().setIsAgent(user.isAgent());
                        Share.get().setTiXianRealFullName(user.getTixianRealFullName());
                        Share.get().setTiXianAccount(user.getTixianAccount());
                        if (!TextUtils.isEmpty(user.getKefu())) {
                            Share.get().setKeFuAccount(user.getKefu());
                        }
                                toSaveUserLoginInfo(user);
//                            UserListBean userListBean = BmobUser.getCurrentUser(UserListBean.class);
                        toLoginIM(userPhone, userPsw);
                    }
                } else {
                    hideProgressDialog();
                    L.e("登录失败：" + e.toString());
                    View view = LayoutInflater.from(LoginActivity.this).inflate(R.layout.toast_text, null);
                    TextView tv_toast_message = view.findViewById(R.id.tv_toast_message);
                    tv_toast_message.setText("登录失败：" + e.toString());
                    ToastSimple.makeText(Gravity.CENTER, 1, view).show();
                }
            }
        });
    }

    /**
     * 背景微动画
     */
//    private void startBgAnimation() {
//        Animation animation = AnimationUtils.loadAnimation(LoginActivity.this, R.anim.seal_login_bg_translate_anim);
//        iv_login_bg.startAnimation(animation);
//    }

    /*private void initVideoView() {
        //加载视频资源控件
        video_view = (CustomVideoView) findViewById(R.id.video_view);
        //设置播放加载路径
        video_view.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.login));
        //循环播放
        video_view.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                video_view.start();
                mediaPlayer.setOnInfoListener(new MediaPlayer.OnInfoListener() {
                    @Override
                    public boolean onInfo(MediaPlayer mp, int what, int extra) {
                        return false;
                    }
                });
            }
        });
        //播放
        video_view.start();
    }

    @Override
    protected void onRestart() {
        //播放
        super.onRestart();
        video_view.start();
    }

    //防止锁屏或者切出的时候，音乐视频在播放
    @Override
    protected void onStop() {
        super.onStop();
        video_view.stopPlayback();
    }*/
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void toWxLogin(Event.toWxLogin event) {
        toLogin(event.phone, event.psw);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
