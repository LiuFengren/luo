package com.huolongluo.luo.huolongluo.ui.activity.invite;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.gyf.immersionbar.ImmersionBar;
import com.huolongluo.luo.R;
import com.huolongluo.luo.huolongluo.base.BaseActivity;
import com.huolongluo.luo.huolongluo.bean.InviteBean;
import com.huolongluo.luo.huolongluo.manager.DialogClickListener;
import com.huolongluo.luo.huolongluo.share.Share;
import com.huolongluo.luo.huolongluo.ui.activity.login.LoginActivity;
import com.huolongluo.luo.util.L;
import com.huolongluo.luo.util.ToastSimple;

import butterknife.BindView;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.CountListener;

/**
 * <p>
 * Created by 火龙裸 on 2017/9/6.
 */

public class InviteActivity extends BaseActivity {
    @BindView(R.id.toolbar_center_title)
    TextView toolbar_center_title;
    @BindView(R.id.iv_left)
    ImageView iv_left;
//    @BindView(R.id.tv_right)
//    TextView tv_right;
    /********************* 以上为ToolBar ************************/
    @BindView(R.id.iv_tip)
    ImageView iv_tip;
    @BindView(R.id.tv_invite)
    TextView tv_invite;
    @BindView(R.id.tv_my_invited_num)
    TextView tv_my_invited_num;
    @BindView(R.id.tv_upgrade)
    TextView tv_upgrade;

    private int inviteCount = 0;

    @Override
    protected int getContentViewId() {
        return R.layout.activity_invite;
    }

    @Override
    protected void injectDagger() {
        activityComponent().inject(this);
    }

    private void initToolBar() {
        ImmersionBar.with(this).statusBarDarkFont(true).init();
        toolbar_center_title.setText("邀请有奖");
    }

    @Override
    protected void initViewsAndEvents(@Nullable Bundle savedInstanceState) {
        initToolBar();
        eventClick(iv_left).subscribe(o -> // 点击返回键
        {
            close();
        });
//        eventClick(iv_tip).subscribe(o -> // 提示
//        {
//            showConfirmDialog("奖励规则", "自邀请用户的注册当日起，至注册90天内，奖励有效；你邀请的用户若注册时间超过");
//        });
        eventClick(tv_invite).subscribe(o -> // 邀请
        {
            ClipboardManager cm =(ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);//创建普通字符型ClipData
            ClipData mClipData = ClipData.newPlainText("simple text", Share.get().getUserName());//将ClipData内容放到系统剪贴板里。
            cm.setPrimaryClip(mClipData);

            showConfirmNoCancelDialog("邀请", "邀请码已复制，快粘贴分享给你的好友吧", new DialogClickListener() {
                @Override
                public void onConfirm() {
                }

                @Override
                public void onCancel() {
                }
            });
        });
        eventClick(tv_upgrade).subscribe(o -> // 立即升级
        {
            if (inviteCount < 15) {
                View view = LayoutInflater.from(InviteActivity.this).inflate(R.layout.toast_text, null);
                TextView tv_toast_message = view.findViewById(R.id.tv_toast_message);
                tv_toast_message.setText("邀请人数暂未达标");
                ToastSimple.makeText(Gravity.CENTER, 1, view).show();
            }
        });
        queryInviteCount();
    }

    /**
     * 查询我邀请的数量
     * */
    private void queryInviteCount() {
        BmobQuery<InviteBean> query = new BmobQuery<InviteBean>();
        query.addWhereEqualTo("beInvitedUsername", Share.get().getUserName());
        query.count(InviteBean.class, new CountListener() {
            @Override
            public void done(Integer count, BmobException e) {
                if(e==null){
                    L.d("关注 count对象个数为："+count);
                    inviteCount = count;
                    tv_my_invited_num.setText("当前邀请人数：" + count);
                    tv_upgrade.setClickable(false);
                    tv_upgrade.setBackgroundResource(R.drawable.bg_cccccc_radius30);
                    if (count >= 15) {
                        tv_upgrade.setClickable(true);
                        tv_upgrade.setBackgroundResource(R.drawable.bg_f33f24_radius30);
                    }
                }else{
                    Log.i("bmob","失败："+e.getMessage()+","+e.getErrorCode());
                }
            }
        });
    }
}
