package com.huolongluo.luo.huolongluo.net.okhttp;

import android.content.Context;

import com.huolongluo.luo.base.ActivityContext;
import com.huolongluo.luo.huolongluo.bean.GuessYouLikeBean;
import com.huolongluo.luo.huolongluo.bean.WXCreateOrderBean;
import com.huolongluo.luo.huolongluo.bean.WXCreateOrderEntity;
import com.huolongluo.luo.huolongluo.bean.WXCreateOrderEntity2;
import com.huolongluo.luo.huolongluo.net.UrlConstants;
import com.huolongluo.luo.huolongluo.share.Share;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Named;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * <p>
 * Created by 火龙裸 on 2017/8/21.
 */

public class Api
{
    private ApiService mApiService;
    private Context mContext;

    public Api(@Named("api") OkHttpClient mOkHttpClient, @ActivityContext Context context)
    {
        mContext = context;

        Retrofit retrofit = new Retrofit.Builder()//
                .addConverterFactory(JacksonConverterFactory.create())//
                .client(mOkHttpClient)//
                .baseUrl(UrlConstants.DOMAIN)//
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())//
                .build();
        mApiService = retrofit.create(ApiService.class);
    }

    /**
     * .do接口统一请求参数
     *
     * @return
     */
    private Map<String, String> common()
    {
        Map<String, String> map = new HashMap<>();
        map.put("meid", "");
        map.put("vv", "202102021805");
        map.put("deviceId", "202102021805");
        map.put("smid", "Due0%2F7S6lZZKdg7o9jKVOMAaACPIxUfW25WyOfZK3RYNjGtAdgrWlkAK2Kowa4x5wlIwyrQwMm3iOQ4HhQOga9Kw");
        map.put("conn", "wifi");
        map.put("icc", "");
        map.put("ua", "XiaomiM2104K10AC");
        map.put("sid", "20gxWMb0PYNKuit8t28Xh74QCmTEBSIDKhM2uUQ4OalKBQ1MLneAi3i3");
        map.put("uid", "14980156");
        map.put("oaid", "394c42334184a47b");
        map.put("ram", "7909974016");
        map.put("cc", "TG000014054");
        map.put("dev_name", "Xiaomi");
        map.put("ndid", "");
        map.put("ver", "dev");
        map.put("evid", "6137343431366636346338343564636566656661373665656532623762303934");
        map.put("cpu", "%5BMali-G77_MC9%5D%5BARMv8_638_MT6891%5D");
        map.put("mtid", "d9c471b76710fca876003681b785db87");
        map.put("msid", "");
        map.put("cv", "ZYJT3.5.82_Android");
        map.put("proto", "8");
        map.put("mtxid", "020000000000");
        map.put("logid", "247000702%2C247000803%2C247000601");
        map.put("osversion", "android_30");
        map.put("aid", "72a73b89ed4cafb5");
        map.put("source_info", "");

        return map;
    }

    private Observable handle(Observable observable)
    {
        return observable.unsubscribeOn(Schedulers.newThread()).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread());
    }
    

    public Subscription register(Map<String, String> options, HttpOnNextListener next)
    {
        return handle(mApiService.register(options, Share.get().getUserName())).subscribe(new ProgressSubscriber(next, mContext, true));
    }

    public Subscription getGuessLiskList(Map<String, String> map, HttpOnNextListener2_2<GuessYouLikeBean> next) {
        return handle(mApiService.getGuessLiskList(map)).subscribe(new ProgressSubscriber(next, mContext, true));
    }

    public Subscription wxCreateOrder(String AuthorizationHeader, WXCreateOrderEntity wxCreateOrderEntity, HttpOnNextListener<WXCreateOrderBean> next) {
        return handle(mApiService.wxCreateOrder(AuthorizationHeader, wxCreateOrderEntity)).subscribe(new ProgressSubscriber(next, mContext, true));
    }

    public Subscription wxCreateOrder2(WXCreateOrderEntity2 wxCreateOrderEntity2, HttpOnNextListener<WXCreateOrderBean> next) {
        return handle(mApiService.wxCreateOrder2(wxCreateOrderEntity2)).subscribe(new ProgressSubscriber(next, mContext, true));
    }
}
