package com.huolongluo.luo.huolongluo.ui.activity.account;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alipay.sdk.app.PayTask;
import com.gyf.immersionbar.ImmersionBar;
import com.huolongluo.luo.R;
import com.huolongluo.luo.huolongluo.base.BaseActivity;
import com.huolongluo.luo.huolongluo.bean.GoodsBean;
import com.huolongluo.luo.huolongluo.bean.RechargeRecordBean;
import com.huolongluo.luo.huolongluo.bean.UserWalletBean;
import com.huolongluo.luo.huolongluo.bean.WXCreateOrderBean;
import com.huolongluo.luo.huolongluo.bean.WXCreateOrderEntity;
import com.huolongluo.luo.huolongluo.bean.WXCreateOrderEntity2;
import com.huolongluo.luo.huolongluo.share.Share;
import com.huolongluo.luo.huolongluo.ui.activity.browser.BrowserActivity;
import com.huolongluo.luo.huolongluo.ui.activity.rechargerecord.RechargeRecordActivity;
import com.huolongluo.luo.huolongluo.ui.adapter.RechargeGoodsAdapter;
import com.huolongluo.luo.manager.pay.alipay.AuthResult;
import com.huolongluo.luo.manager.pay.alipay.PayResult;
import com.huolongluo.luo.manager.pay.alipay.util.AliPayConfig;
import com.huolongluo.luo.manager.pay.alipay.util.OrderInfoUtil2_0;
import com.huolongluo.luo.manager.pay.wechat.WxConfig;
import com.huolongluo.luo.util.Arith;
import com.huolongluo.luo.util.ToastSimple;
import com.luck.picture.lib.decoration.GridSpacingItemDecoration;
import com.luck.picture.lib.utils.DensityUtil;
import com.tencent.mm.opensdk.modelpay.PayReq;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.tencent.qcloud.tuicore.util.MD5Utils;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;
import okhttp3.HttpUrl;

/**
 * <p>
 * Created by 火龙裸 on 2017/9/6.
 */

public class MyAccountActivity extends BaseActivity implements MyAccountContract.View {
    private static final String TAG = "MyAccountActivity";
    @Inject
    MyAccountPresent present;
    @BindView(R.id.toolbar_center_title)
    TextView toolbar_center_title;
    @BindView(R.id.iv_left)
    ImageView iv_left;
    @BindView(R.id.tv_right)
    TextView tv_right;
    /********************* 以上为ToolBar ************************/
    @BindView(R.id.tv_my_zhuanshi_num)
    TextView tv_my_zhuanshi_num;
    @BindView(R.id.rv_chongzhi_zhuanshi)
    RecyclerView rv_chongzhi_zhuanshi;
    @BindView(R.id.ll_wx_pay)
    LinearLayout ll_wx_pay;
    @BindView(R.id.ll_ali_pay)
    LinearLayout ll_ali_pay;
    @BindView(R.id.iv_wx_pay)
    ImageView iv_wx_pay;
    @BindView(R.id.iv_ali_pay)
    ImageView iv_ali_pay;
    @BindView(R.id.btn_recharge)
    Button btn_recharge;
    @BindView(R.id.tv_chongzhi_xieyi)
    TextView tv_chongzhi_xieyi;

    private RechargeGoodsAdapter rechargeGoodsAdapter;
    private List<GoodsBean> goodsBeanList = new ArrayList<>();//商品列表

    private int payType = 1;//1表示支付宝支付，2表示微信支付
    private GoodsBean payGoodsBean = null;

    private IWXAPI api;

    @Override
    protected int getContentViewId() {
        return R.layout.activity_my_account;
    }

    @Override
    protected void injectDagger() {
        activityComponent().inject(this);
        present.attachView(this);
    }

    private void initToolBar() {
        ImmersionBar.with(this).statusBarDarkFont(true).init();
        toolbar_center_title.setText("我的账户");
        tv_right.setVisibility(View.VISIBLE);
        tv_right.setText("充值记录");
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void initViewsAndEvents(@Nullable Bundle savedInstanceState) {
        api = WXAPIFactory.createWXAPI(this, WxConfig.APP_ID, false);
        initToolBar();

        rv_chongzhi_zhuanshi.setNestedScrollingEnabled(false);
        rv_chongzhi_zhuanshi.setFocusable(false);
        rv_chongzhi_zhuanshi.setLayoutManager(new GridLayoutManager(MyAccountActivity.this, 3) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        });
        rv_chongzhi_zhuanshi.addItemDecoration(new GridSpacingItemDecoration(3,
                DensityUtil.dip2px(MyAccountActivity.this, 10), false));
        rechargeGoodsAdapter = new RechargeGoodsAdapter(MyAccountActivity.this, goodsBeanList, R.layout.item_chognzhi_zhuanshi);
        rv_chongzhi_zhuanshi.setAdapter(rechargeGoodsAdapter);


        eventClick(iv_left).subscribe(o -> // 点击返回键
        {
            close();
        });
        eventClick(tv_right).subscribe(o -> // 点击充值记录
        {
            startActivity(RechargeRecordActivity.class);
        });

        // 支付宝支付
        ll_ali_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                payType = 1;
                iv_wx_pay.setImageResource(R.mipmap.ic_cancel_ra_default);
                iv_ali_pay.setImageResource(R.mipmap.ic_cancel_ra_checked);
            }
        });
        // 微信支付
        ll_wx_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                payType = 2;
                iv_wx_pay.setImageResource(R.mipmap.ic_cancel_ra_checked);
                iv_ali_pay.setImageResource(R.mipmap.ic_cancel_ra_default);
            }
        });

        eventClick(btn_recharge).subscribe(o -> // 确认支付
        {
            payGoodsBean = null;
            for (int i = 0; i < rechargeGoodsAdapter.getItemCount(); i++) {
                if (rechargeGoodsAdapter.getAllData().get(i).isChecked()) {
                    payGoodsBean = rechargeGoodsAdapter.getAllData().get(i);
                    break;
                }
            }
            if (payGoodsBean == null) {
                return;
            }
            if (payType == 1) {
                //支付宝支付
                startAliPay(payGoodsBean.getWillPay() + "", "充值" + payGoodsBean.getGoldNum() + "金币");
            } else {
                //微信支付
                /*int wxSdkVersion = api.getWXAppSupportAPI();
                if (wxSdkVersion >= Build.OFFLINE_PAY_SDK_INT) {
                    api.sendReq(new JumpToOfflinePay.Req());
                }else {
                    Toast.makeText(MyAccountActivity.this, "not supported", Toast.LENGTH_LONG).show();
                }*/
                Log.e(TAG, "initViewsAndEvents: 当前系统时间戳：" + System.currentTimeMillis());

                /*PayReq request = new PayReq();
                request.appId = "wx1ef7f6f771a3d5d7";//应用ID
                request.partnerId = "1626026426";//商户号
                request.prepayId= "1101000000140415649af9fc314aa427";//预支付交易会话ID
                request.packageValue = "Sign=WXPay";//订单详情扩展字符串 固定值Sign=WXPay
                request.nonceStr= "1101000000140429eb40476f8896f4c9";//随机字符串 不长于32位。推荐随机数生成算法。
                request.timeStamp= "1398746574";//时间戳
                request.sign= "7FFECB600D7157C5AA49810D2D8F28BC2811827B";//签名，使用字段appId、timeStamp、nonceStr、prepayid计算得出的签名值 注意：取值RSA格式
                api.sendReq(request);*/

                long currentSystemTime = System.currentTimeMillis();
                WXCreateOrderEntity wxCreateOrderEntity = new WXCreateOrderEntity();
                WXCreateOrderEntity.AmountDTO amountDTO = new WXCreateOrderEntity.AmountDTO();
                amountDTO.setTotal(100);
                amountDTO.setCurrency("CNY");
                wxCreateOrderEntity.setAmount(amountDTO);
                wxCreateOrderEntity.setAppid(WxConfig.APP_ID);
                wxCreateOrderEntity.setMchid(WxConfig.PARTNER_ID);
                wxCreateOrderEntity.setOut_trade_no(currentSystemTime + "");
                wxCreateOrderEntity.setDescription("充值" + payGoodsBean.getGoldNum() + "金币");
                wxCreateOrderEntity.setNotify_url("https://www.weixin.qq.com/wxpay/pay.php");

                try {
                    String header = WXCreateOrderEntity.getAuthorizationHeader(this,"POST", HttpUrl.parse("https://www.weixin.qq.com/wxpay/pay.php"), "充值" + payGoodsBean.getGoldNum() + "金币", System.currentTimeMillis() + "", currentSystemTime);
                    Log.e(TAG, "initViewsAndEvents: 请求头为：" + header);
                    subscription = present.wxCreateOrder(header, wxCreateOrderEntity);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (SignatureException e) {
                    e.printStackTrace();
                } catch (InvalidKeyException e) {
                    e.printStackTrace();
                }

                /*WXCreateOrderEntity2 wxCreateOrderEntity2 = new WXCreateOrderEntity2();
                wxCreateOrderEntity2.setAppid(WxConfig.APP_ID);
                wxCreateOrderEntity2.setMch_id(WxConfig.PARTNER_ID);
                String nonce_str = System.currentTimeMillis() + "";//随机字符串
                wxCreateOrderEntity2.setNonce_str(nonce_str);
                wxCreateOrderEntity2.setSign(WXCreateOrderEntity2.getMD5_Sign("充值" + payGoodsBean.getGoldNum() + "金币", nonce_str));
                wxCreateOrderEntity2.setOut_trade_no(nonce_str);
                wxCreateOrderEntity2.setBody("充值" + payGoodsBean.getGoldNum() + "金币");
                double centWillPay = Arith.mul(payGoodsBean.getWillPay(), 100);
                Log.e(TAG, "initViewsAndEvents: 支付多少分钱：" + centWillPay);
                wxCreateOrderEntity2.setTotal_fee((int) centWillPay);
                wxCreateOrderEntity2.setSpbill_create_ip("192.168.123");
                wxCreateOrderEntity2.setTrade_type("APP");
                wxCreateOrderEntity2.setNotify_url("https://www.weixin.qq.com/wxpay/pay.php");
                subscription = present.wxCreateOrder2(wxCreateOrderEntity2);*/
            }
        });

        eventClick(tv_chongzhi_xieyi).subscribe(o -> //用户充值协议
        {
            Intent intent = new Intent(MyAccountActivity.this, BrowserActivity.class);
            intent.putExtra(BrowserActivity.PARAM_TITLE, "用户充值协议");
            intent.putExtra(BrowserActivity.PARAM_URL, "http://huolongluo.top/recharge_protocol.html");
            intent.putExtra(BrowserActivity.PARAM_MODE, BrowserActivity.MODE_SONIC);
            startActivity(intent);
        });

        queryGoods();
        //查询个人钱包余额，是否会员
        queryUserWallet();
    }

    /**
     * 开始调起支付宝支付
     * */
    private void startAliPay(String amount, String body) {
        if (TextUtils.isEmpty(AliPayConfig.APPID) || (TextUtils.isEmpty(AliPayConfig.RSA2_PRIVATE) && TextUtils.isEmpty(AliPayConfig.RSA_PRIVATE))) {
//            Log.e(TAG, "错误: 需要配置PARTNER |APP_ID| RSA_PRIVATE| TARGET_ID");
            ToastSimple.show("配置错误", 2);
            Log.e(TAG, "配置错误");
            return;
        }

        /*
         * 这里只是为了方便直接向商户展示支付宝的整个支付流程；所以Demo中加签过程直接放在客户端完成；
         * 真实App里，privateKey等数据严禁放在客户端，加签过程务必要放在服务端完成；
         * 防止商户私密数据泄露，造成不必要的资金损失，及面临各种安全风险；
         *
         * orderInfo 的获取必须来自服务端；
         */
        boolean rsa2 = (AliPayConfig.RSA2_PRIVATE.length() > 0);
        Map<String, String> params = OrderInfoUtil2_0.buildOrderParamMap(amount, body);
        String orderParam = OrderInfoUtil2_0.buildOrderParam(params);

        String privateKey = rsa2 ? AliPayConfig.RSA2_PRIVATE : AliPayConfig.RSA_PRIVATE;
        String sign = OrderInfoUtil2_0.getSign(params, privateKey, rsa2);
        final String orderInfo = orderParam + "&" + sign;

        final Runnable payRunnable = new Runnable() {
            @Override
            public void run() {
                PayTask alipay = new PayTask(MyAccountActivity.this);
                Map<String, String> result = alipay.payV2(orderInfo, true);
                Log.i("msp", result.toString());

                Message msg = new Message();
                msg.what = AliPayConfig.SDK_PAY_FLAG;
                msg.obj = result;
                mHandler.sendMessage(msg);
            }
        };

        // 必须异步调用
        Thread payThread = new Thread(payRunnable);
        payThread.start();
    }

    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {
        @SuppressWarnings("unused")
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case AliPayConfig.SDK_PAY_FLAG: {
                    @SuppressWarnings("unchecked")
                    PayResult payResult = new PayResult((Map<String, String>) msg.obj);
                    /**
                     * 对于支付结果，请商户依赖服务端的异步通知结果。同步通知结果，仅作为支付结束的通知。
                     */
                    String resultInfo = payResult.getResult();// 同步返回需要验证的信息
                    String resultStatus = payResult.getResultStatus();
                    // 判断resultStatus 为9000则代表支付成功
                    if (TextUtils.equals(resultStatus, "9000")) {
                        // 该笔订单是否真实支付成功，需要依赖服务端的异步通知。
                        Log.e(TAG, "handleMessage: 支付成功：" + payResult);
                        ToastSimple.show("支付成功:" + payResult);
                        double currentGoldBalance =  Double.parseDouble(Share.get().getGoldBalance());//用户当前金币余额
                        double rechargeGold = Arith.add(payGoodsBean.getGoldNum(), payGoodsBean.getGoldGiftNum());//充值了多少金币(包括赠送的)
                        double goldBalanceResult = Arith.add(currentGoldBalance, rechargeGold);
                        UserWalletBean.updateRechargeGoldBalance(goldBalanceResult);
                        RechargeRecordBean.addRechargeRecord("充值" + payGoodsBean.getGoldNum() + "金币", payGoodsBean.getWillPay() + "", "支付宝");
                        tv_my_zhuanshi_num.setText(goldBalanceResult + "");
                    } else {
                        // 该笔订单真实的支付结果，需要依赖服务端的异步通知。
                        ToastSimple.show( "支付失败：" + payResult.getMemo());
                    }
                    break;
                }
                case AliPayConfig.SDK_AUTH_FLAG: {
                    @SuppressWarnings("unchecked")
                    AuthResult authResult = new AuthResult((Map<String, String>) msg.obj, true);
                    String resultStatus = authResult.getResultStatus();

                    // 判断resultStatus 为“9000”且result_code
                    // 为“200”则代表授权成功，具体状态码代表含义可参考授权接口文档
                    if (TextUtils.equals(resultStatus, "9000") && TextUtils.equals(authResult.getResultCode(), "200")) {
                        // 获取alipay_open_id，调支付时作为参数extern_token 的value
                        // 传入，则支付账户为该授权账户
                        Log.e(TAG, "handleMessage: 授权成功：" + authResult);
                        ToastSimple.show( "授权成功：" + authResult);
                    } else {
                        // 其他状态值则为授权失败
                        Log.e(TAG, "handleMessage: 授权失败：" + authResult);
                        ToastSimple.show( "授权失败：" + authResult);
                    }
                    break;
                }
                default:
                    break;
            }
        };
    };

    private void queryUserWallet() {
        BmobQuery<UserWalletBean> categoryBmobQuery = new BmobQuery<>();
        categoryBmobQuery.addWhereEqualTo("username", Share.get().getUserName());
        categoryBmobQuery.findObjects(new FindListener<UserWalletBean>() {
            @Override
            public void done(List<UserWalletBean> result, BmobException e) {
                if (e == null) {
                    if (!result.isEmpty()) {
                        UserWalletBean userWalletBean = result.get(0);
                        Share.get().setIsVip(userWalletBean.isVip());
                        Share.get().setGoldBalance(userWalletBean.getGoldBalance());
                        Share.get().setIntegralBalance(userWalletBean.getIntegralBalance());
                        if (tv_my_zhuanshi_num != null) {
                            tv_my_zhuanshi_num.setText(userWalletBean.getGoldBalance() + "");
                        }
                    } else {
                        UserWalletBean.createUserWallet();
                    }
                } else {
                    Log.e(TAG, e.toString());
                }
            }
        });
    }

//    /**
//     * 给用户创建一个钱包
//     * */
//    private void createUserWallet() {
//        UserWalletBean p2 = new UserWalletBean();
//        p2.setUsername(Share.get().getUserName());
//        p2.setVip(false);
//        p2.setGoldBalance(100);
//        p2.setIntegralBalance(0);
//        p2.save(new SaveListener<String>() {
//            @Override
//            public void done(String objectId,BmobException e) {
//                if(e==null){
//                    Log.e(TAG, "给用户创建一个钱包 数据成功，返回objectId为："+objectId);
//                    Share.get().setGoldBalance(100);
//                }else{
//                    Log.e(TAG, "给用户创建一个钱包 创建数据失败：" + e.getMessage());
//                }
//            }
//        });
//    }

    /**
     * 查询多条数据
     * 商品列表
     */
    private void queryGoods() {
//        showProgressDialog("");
        BmobQuery<GoodsBean> bmobQuery = new BmobQuery<>();
        bmobQuery.findObjects(new FindListener<GoodsBean>() {
            @Override
            public void done(List<GoodsBean> result, BmobException e) {
                if (e == null) {
                    goodsBeanList.clear();
                    goodsBeanList.addAll(result);
                    if (!goodsBeanList.isEmpty()) {
                        goodsBeanList.get(0).setChecked(true);
                        Log.e(TAG, "done: 商品查询成功" + result.size() + " 金额：" + result.get(0).getWillPay());

                        rechargeGoodsAdapter.replaceAll(goodsBeanList);

                        /*rv_chongzhi_zhuanshi.setNestedScrollingEnabled(false);
                        rv_chongzhi_zhuanshi.setFocusable(false);
                        rv_chongzhi_zhuanshi.setLayoutManager(new GridLayoutManager(MyAccountActivity.this, 3) {
                            @Override
                            public boolean canScrollVertically() {
                                return false;
                            }
                        });
//                    rv_chongzhi_zhuanshi.setLayoutManager(new LinearLayoutManager(MyAccountActivity.this, LinearLayoutManager.VERTICAL, false) {
//                        @Override
//                        public boolean canScrollVertically() {
//                            return false;
//                        }
//                    });
                        rv_chongzhi_zhuanshi.addItemDecoration(new GridSpacingItemDecoration(3,
                                DensityUtil.dip2px(MyAccountActivity.this, 10), false));
                        rechargeGoodsAdapter = new RechargeGoodsAdapter(MyAccountActivity.this, goodsBeanList, R.layout.item_chognzhi_zhuanshi);ds
                        rv_chongzhi_zhuanshi.setAdapter(rechargeGoodsAdapter);*/
                    }
                } else {
                    Log.e(TAG, e.toString());
                }
//                hideProgressDialog();
            }
        });
    }

    /**
     * 创建微信支付订单成功
     * */
    @Override
    public void wxCreateOrderSuccess(WXCreateOrderBean wxCreateOrderBean) {
        Log.e(TAG, "wxCreateOrderSuccess: 创建微信支付订单成功：" + wxCreateOrderBean.toString());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        present.detachView();
    }
}
