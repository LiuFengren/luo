package com.huolongluo.luo.huolongluo.ui.activity.registernext;

import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;

import com.huolongluo.luo.R;
import com.huolongluo.luo.huolongluo.base.BaseActivity;
import com.huolongluo.luo.huolongluo.bean.InviteBean;
import com.huolongluo.luo.huolongluo.bean.UserBean;
import com.huolongluo.luo.huolongluo.share.Share;
import com.huolongluo.luo.util.ImmersedStatusbarUtils;
import com.huolongluo.luo.util.L;
import com.huolongluo.luo.util.StringUtil;
import com.huolongluo.luo.util.TelNumMatch;
import com.huolongluo.luo.util.ToastSimple;
import com.jakewharton.rxbinding.widget.RxTextView;

import butterknife.BindView;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.SaveListener;

/**
 * <p>
 * Created by 火龙裸 on 2017/9/6.
 */

public class RegisterNextActivity extends BaseActivity {
    @BindView(R.id.toolbar_center_title)
    TextView toolbar_center_title;
    @BindView(R.id.iv_left)
    ImageView iv_left;
    @BindView(R.id.my_toolbar)
    Toolbar my_toolbar;
    @BindView(R.id.lin1)
    LinearLayout lin1;
    /********************* 以上为ToolBar ************************/
    @BindView(R.id.et_phone)
    EditText et_phone;
    @BindView(R.id.et_psw)
    EditText et_psw;
    @BindView(R.id.et_invite_code)
    TextView et_invite_code;
    @BindView(R.id.iv_close_psw)
    ImageView iv_close_psw;
    @BindView(R.id.btn_register)
    Button btn_register;

    private boolean isHidden = false; // 是否隐藏密码

    private String phone;//输入的手机号码
    private String psw;//输入的密码
    private String inviteCode;//输入的邀请码

    @Override
    protected int getContentViewId() {
        return R.layout.activity_register_next;
    }

    @Override
    protected void injectDagger() {
        activityComponent().inject(this);
    }

    private void initToolBar() {
        lin1.setVisibility(View.VISIBLE);
        //全屏。
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        ImmersedStatusbarUtils.initAfterSetContentView(this, lin1); // 沉浸式
//        my_toolbar.setTitle("");
        toolbar_center_title.setText("");
        iv_left.setVisibility(View.VISIBLE);
        iv_left.setImageResource(R.mipmap.nav_icon_back_green);
        setSupportActionBar(my_toolbar);
    }

    @Override
    protected void initViewsAndEvents(@Nullable Bundle savedInstanceState) {
        initToolBar();
        eventClick(iv_left).subscribe(o -> // 点击返回键
        {
            close();
        });
        eventClick(iv_close_psw).subscribe(o ->
        {
            if (isHidden) {
                et_psw.setTransformationMethod(PasswordTransformationMethod.getInstance()); // 隐藏密码
                iv_close_psw.setImageResource(R.mipmap.password_icon_unhide);
            } else {
                et_psw.setTransformationMethod(HideReturnsTransformationMethod.getInstance()); // 明文密码
                iv_close_psw.setImageResource(R.mipmap.password_icon_hide);
            }
            isHidden = !isHidden;
            et_psw.setSelection(et_psw.getText().toString().trim().length()); // 将光标移至文字末尾
        });
        eventClick(btn_register).subscribe(o ->
        {
            if (TextUtils.isEmpty(phone)) {
                View view = LayoutInflater.from(this).inflate(R.layout.toast_text, null);
                TextView tv_toast_message = view.findViewById(R.id.tv_toast_message);
                tv_toast_message.setText("请先填写手机号码");
                ToastSimple.makeText(Gravity.CENTER, 1, view).show();
            } else if (!StringUtil.judgePhoneNums(phone)) {
                View view = LayoutInflater.from(this).inflate(R.layout.toast_text, null);
                TextView tv_toast_message = view.findViewById(R.id.tv_toast_message);
                tv_toast_message.setText("请输入正确的手机号");
                ToastSimple.makeText(Gravity.CENTER, 1, view).show();
            }
            /*else if (TextUtils.isEmpty(inviteCode)) {
                View view = LayoutInflater.from(this).inflate(R.layout.toast_text, null);
                TextView tv_toast_message = view.findViewById(R.id.tv_toast_message);
                tv_toast_message.setText("请输入邀请码");
                ToastSimple.makeText(Gravity.CENTER, 1, view).show();
            }*/
            else if (TextUtils.isEmpty(psw) || !StringUtil.pswIsLegal(psw) || StringUtil.isContainChinese(psw)) {
                View view = LayoutInflater.from(this).inflate(R.layout.toast_text, null);
                TextView tv_toast_message = view.findViewById(R.id.tv_toast_message);
                tv_toast_message.setText("密码需为8-20位字母或数字");
                ToastSimple.makeText(Gravity.CENTER, 1, view).show();
            } else {
                if (TelNumMatch.isValidPhoneNumber(phone)) {
                    showProgressDialog("");
                    UserBean user = new UserBean();
                    user.setUsername(phone);
                    user.setPassword(psw);
                    user.setInviteCode(inviteCode);
                    user.setDeviceId(Share.get().getDeviceId());

                    user.signUp(new SaveListener<UserBean>() {
                        @Override
                        public void done(UserBean user, BmobException e) {
                            if (e == null) {
                                View view = LayoutInflater.from(RegisterNextActivity.this).inflate(R.layout.toast_text, null);
                                TextView tv_toast_message = view.findViewById(R.id.tv_toast_message);
                                tv_toast_message.setText("注册成功");
                                ToastSimple.makeText(Gravity.CENTER, 2, view).show();
                                insertInviteBean();
                            } else {
                                L.e("注册失败了：" + e.toString());
                                View view = LayoutInflater.from(RegisterNextActivity.this).inflate(R.layout.toast_text, null);
                                TextView tv_toast_message = view.findViewById(R.id.tv_toast_message);
                                tv_toast_message.setText("注册失败");
                                ToastSimple.makeText(Gravity.CENTER, 1, view).show();
                            }
                            hideProgressDialog();
                        }
                    });
                } else {
                    View view = LayoutInflater.from(this).inflate(R.layout.toast_text, null);
                    TextView tv_toast_message = view.findViewById(R.id.tv_toast_message);
                    tv_toast_message.setText("手机号码不正确");
                    ToastSimple.makeText(Gravity.CENTER, 1, view).show();
                }
            }
        });
        RxTextView.afterTextChangeEvents(et_phone).subscribe(event ->
        {
            phone = event.view().getText().toString().trim();
            psw = et_psw.getText().toString().trim();
            inviteCode = et_invite_code.getText().toString().trim();
        });
        RxTextView.afterTextChangeEvents(et_psw).subscribe(event ->
        {
            psw = event.view().getText().toString().trim();
            phone = et_phone.getText().toString().trim();
            inviteCode = et_invite_code.getText().toString().trim();
        });
        RxTextView.afterTextChangeEvents(et_invite_code).subscribe(event ->
        {
            inviteCode = event.view().getText().toString().trim();
            phone = et_phone.getText().toString().trim();
            psw = et_psw.getText().toString().trim();
        });
    }

    /**
     * 插入一条邀请数据
     */
    private void insertInviteBean() {
        if (!TextUtils.isEmpty(inviteCode)) {
            InviteBean inviteBean = new InviteBean();
            inviteBean.setUsername(phone);
            inviteBean.setBeInvitedUsername(inviteCode);
//            inviteBean.setBeInvitedSex("女");
            inviteBean.save(new SaveListener<String>() {
                @Override
                public void done(String objectId, BmobException e) {
                    if (e == null) {
                        L.e("添加一条邀请数据成功，返回objectId为：" + objectId);
                    } else {
                        L.e("创建数据失败：" + e.getMessage());
                    }
                }
            });
        }
    }
}
