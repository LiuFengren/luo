package com.huolongluo.luo.util;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.TextView;

/**
 * Created by 火龙裸 on 2018/7/25.
 * <p>
 * TextView 的 drawableLeft drawableRight drawableTop drawableBottom
 */
public class TextDrawAbleUtils
{
    public static void setTextDrawable(Context context, int drawableId, TextView textView)
    {
        setTextDrawable(context, drawableId, textView, 1);
    }

    /**
     * @param context    context
     * @param drawableId 图片资源id
     * @param textView   需要设置的文本控件
     * @param direction  方向:1left,2top,3right,4bottom，默认在左边
     */
    public static void setTextDrawable(Context context, int drawableId, TextView textView, int direction)
    {
        Drawable drawable = context.getResources().getDrawable(drawableId);
        drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
        switch (direction)
        {
            case 1:
                textView.setCompoundDrawables(drawable, null, null, null);
                break;
            case 2:
                textView.setCompoundDrawables(null, drawable, null, null);
                break;
            case 3:
                textView.setCompoundDrawables(null, null, drawable, null);
                break;
            case 4:
                textView.setCompoundDrawables(null, null, null, drawable);
                break;
        }
    }
}
