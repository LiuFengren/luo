package com.huolongluo.luo.huolongluo.ui.fragment.mine;

import com.huolongluo.luo.base.BasePresenter;
import com.huolongluo.luo.base.BaseView;

/**
 * Created by 火龙裸 on 2018/5/26 0026.
 */

public interface MineContract
{
    interface View extends BaseView
    {
//        void getUserOtherInfoSucce(UserOtherInfoBean result);
//
//        void getJuniorIdSuccess(List<String> result);
//
//        void getUnreadCountSucce(UnReadCountBean result);
//
//        void loginEerpSucce(LoginErpBean result);

    }

    interface Presenter extends BasePresenter<View>
    {
//        Subscription getUserOtherInfo(String uId, String isErp);
//
//        Subscription getJuniorId(String uId);
//
//        Subscription getUnreadCount(String uId, String isErp);
//
//        Subscription loginEerp(String member_id, String ticket, String password, String type);


    }
}
