package com.huolongluo.luo.huolongluo.ui.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import com.huolongluo.luo.R;
import com.huolongluo.luo.huolongluo.bean.RechargeRecordBean;
import com.huolongluo.luo.superAdapter.recycler.BaseViewHolder;
import com.huolongluo.luo.superAdapter.recycler.SuperAdapter;
import com.huolongluo.luo.util.TextDrawAbleUtils;
import com.tencent.liteav.trtccalling.model.util.ImageLoader;

import java.util.List;

/**
 * Created by 火龙裸 on 2018/6/10 0010.
 */
public class RechargeRecordAdapter extends SuperAdapter<RechargeRecordBean> {

    public RechargeRecordAdapter(Context context, List<RechargeRecordBean> list, int layoutResId) {
        super(context, list, layoutResId);
    }

    @Override
    public void onBind(int viewType, BaseViewHolder holder, int position, RechargeRecordBean item) {
//        if (this.followType == 1) {
//            ImageLoader.loadImage(mContext, holder.getView(R.id.iv_header_pic), item.getFollowToUserPic());
//            holder.setText(R.id.tv_nick_name, TextUtils.isEmpty(item.getFollowUserNickName()) ? item.getFollowToUserName() : item.getFollowUserNickName());
//            if (TextUtils.equals("男", item.getFollowToUserSex())) {
//                TextDrawAbleUtils.setTextDrawable(mContext, R.mipmap.ic_home_man, holder.getView(R.id.tv_nick_name), 3);
//            } else {
//                TextDrawAbleUtils.setTextDrawable(mContext, R.mipmap.ic_home_woman, holder.getView(R.id.tv_nick_name), 3);
//            }
//            if (!TextUtils.isEmpty(item.getFolloToUserBirthDay())) {
//                holder.setText(R.id.tv_birth_yeay, item.getFolloToUserBirthDay());
//                holder.setVisibility(R.id.tv_birth_yeay, View.VISIBLE);
//            } else {
//                holder.setVisibility(R.id.tv_birth_yeay, View.GONE);
//            }
//            holder.setText(R.id.tv_user_address, "现居" + item.getFollowToUserAddress());
//        } else {
//            ImageLoader.loadImage(mContext, holder.getView(R.id.iv_header_pic), item.getHeadPic());
//            holder.setText(R.id.tv_nick_name, TextUtils.isEmpty(item.getUserNickName()) ? item.getUsername() : item.getUserNickName());
//            if (TextUtils.equals("男", item.getUserSex())) {
//                TextDrawAbleUtils.setTextDrawable(mContext, R.mipmap.ic_home_man, holder.getView(R.id.tv_nick_name), 3);
//            } else {
//                TextDrawAbleUtils.setTextDrawable(mContext, R.mipmap.ic_home_woman, holder.getView(R.id.tv_nick_name), 3);
//            }
//            if (!TextUtils.isEmpty(item.getUserBirthDay())) {
//                holder.setText(R.id.tv_birth_yeay, item.getUserBirthDay());
//                holder.setVisibility(R.id.tv_birth_yeay, View.VISIBLE);
//            } else {
//                holder.setVisibility(R.id.tv_birth_yeay, View.GONE);
//            }
//            holder.setText(R.id.tv_user_address, "现居" + item.getAddress());
//        }


//        ImageLoader.loadImage(mContext, holder.getView(R.id.iv_header_pic), TextUtils.isEmpty(item.getHeadPic()) ? Share.get().getHeadPic() : item.getHeadPic());
//        holder.setText(R.id.tv_nick_name, TextUtils.isEmpty(item.getNickName()) ? item.getUsername() : item.getNickName());
//        if (TextUtils.equals("男", item.getSex())) {
//            TextDrawAbleUtils.setTextDrawable(mContext, R.mipmap.ic_home_man, holder.getView(R.id.tv_nick_name), 3);
//        } else {
//            TextDrawAbleUtils.setTextDrawable(mContext, R.mipmap.ic_home_woman, holder.getView(R.id.tv_nick_name), 3);
//        }
//        holder.setText(R.id.tv_birth_yeay, item.getBirtyday());
//        holder.setText(R.id.tv_user_address, "现居" + item.getAddress());
//        holder.setText(R.id.tv_visit_time, "到访时间：" + item.getUpdatedAt());
//
//        if (item.isUnLock()) {
//            holder.getView(R.id.tv_unlock).setVisibility(View.VISIBLE);
//        } else {
//            holder.getView(R.id.tv_unlock).setVisibility(View.GONE);
//        }

//        if (contactsType == 0) {
//            holder.setText(R.id.tv_nick_name, TextUtils.isEmpty(item.getFollowUserNickName()) ? item.getFollowToUserName() : item.getFollowUserNickName());
//            if (!TextUtils.isEmpty(item.getFolloToUserBirthDay())) {
//                int yearIndex = item.getFolloToUserBirthDay().indexOf("-");
//                String year = "";
//                if (yearIndex != -1) {
//                    year = item.getFolloToUserBirthDay().substring(0, yearIndex);
//                }
//                holder.setText(R.id.tv_birth_yeay,year + "年");
//            }
//            holder.setText(R.id.tv_user_height,item.getFollowToUserHeight() + "CM");
//            holder.setText(R.id.tv_user_address,"现居" + item.getFollowToUserAddress());
//            if (TextUtils.equals("女", item.getFollowToUserSex())) {
//                TextDrawAbleUtils.setTextDrawable(mContext, R.mipmap.ic_home_woman, holder.getView(R.id.tv_nick_name), 3);
//            } else {
//                TextDrawAbleUtils.setTextDrawable(mContext, R.mipmap.ic_home_man, holder.getView(R.id.tv_nick_name), 3);
//            }
//        } else {
//            holder.setText(R.id.tv_nick_name, TextUtils.isEmpty(item.getUserNickName()) ? item.getUsername() : item.getFollowToUserName());
//            if (!TextUtils.isEmpty(item.getUserBirthDay())) {
//                int yearIndex = item.getUserBirthDay().indexOf("-");
//                String year = "";
//                if (yearIndex != -1) {
//                    year = item.getUserBirthDay().substring(0, yearIndex);
//                }
//                holder.setText(R.id.tv_birth_yeay,year + "年");
//            }
//            holder.setText(R.id.tv_user_height,item.getUserHeight() + "CM");
//            holder.setText(R.id.tv_user_address,"现居" + item.getAddress());
//            if (TextUtils.equals("女", item.getUserSex())) {
//                TextDrawAbleUtils.setTextDrawable(mContext, R.mipmap.ic_home_woman, holder.getView(R.id.tv_nick_name), 3);
//            } else {
//                TextDrawAbleUtils.setTextDrawable(mContext, R.mipmap.ic_home_man, holder.getView(R.id.tv_nick_name), 3);
//            }
//        }
//        if (item.getGiftGold() <= 0) {
//            holder.setVisibility(R.id.tv_vip_gift_num, View.INVISIBLE);
//        } else {
//            holder.setText(R.id.tv_vip_gift_num,"送" + item.getGiftGold() + "金币");
//            holder.setVisibility(R.id.tv_vip_gift_num, View.VISIBLE);
//        }
//
//        if (item.isChecked()) {
//            holder.getView(R.id.ll_chongzhi_type).setBackgroundResource(R.mipmap.icon_charge_item_selected);
//        } else {
//            holder.getView(R.id.ll_chongzhi_type).setBackgroundResource(0);
//        }
//
//        // 点击item
//        RxView.clicks(holder.getItemView()).throttleFirst(500, TimeUnit.MILLISECONDS).observeOn(AndroidSchedulers.mainThread()).subscribe(v ->
//        {
//            Log.e("TAG", "onBind: 点击了：" + position + "  共：" + getItemCount());
//
//            for (int i = 0; i < getItemCount(); i++) {
//                getAllData().get(i).setChecked(false);
//            }
//            item.setChecked(true);
//            for (int i = 0; i < getItemCount(); i++) {
//                Log.e("TAG", "onBind: 最终的选中情况：" + item.isChecked());
//            }
//            notifyDataSetChanged();
//        });

        // 点击任务弹出窗体
//        RxView.clicks(holder.getView(R.id.tv_task_name)).throttleFirst(500, TimeUnit.MILLISECONDS).observeOn(AndroidSchedulers.mainThread()).subscribe(v ->
//        {
//            EventBus.getDefault().post(new Event.Task(item.getContent()+""));
//        });
//        // 点击 提交 任务
//        RxView.clicks(holder.getView(R.id.tv_submit01)).throttleFirst(500, TimeUnit.MILLISECONDS).observeOn(AndroidSchedulers.mainThread()).subscribe(v ->
//        {
//            Intent intent = new Intent(mContext, SubMitTaskActivity.class);
//            intent.putExtra("tId", item.getId()+"");
//            intent.putExtra("position", position);
//            intent.putExtra("title", item.getContent());
//            mContext.startActivity(intent);
//        });

    }
}
