package com.huolongluo.luo.huolongluo.ui.fragment.friendcontact;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.huolongluo.luo.R;
import com.huolongluo.luo.huolongluo.base.BaseActivity;
import com.huolongluo.luo.huolongluo.base.BaseFragment;
import com.huolongluo.luo.huolongluo.bean.FollowBean;
import com.huolongluo.luo.huolongluo.constants.Constants;
import com.huolongluo.luo.huolongluo.share.Share;
import com.huolongluo.luo.huolongluo.ui.adapter.FriendContactsAdapter;
import com.huolongluo.luo.superAdapter.recycler.SuperAdapter;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnLoadMoreListener;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;
import com.tencent.imsdk.v2.V2TIMConversation;
import com.tencent.qcloud.tuicore.TUIConstants;
import com.tencent.qcloud.tuicore.TUICore;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;

public class FriendContactsFragment extends BaseFragment {
    private static final String TAG = "FriendContactsFragment";

    @BindView(R.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    @BindView(R.id.rv_friend_contacts)
    RecyclerView rv_friend_contacts;

    private int contactsType;//0表示关注，1表示粉丝
    private List<FollowBean> followBeanList = new ArrayList<>();//关注或粉丝列表
    private FriendContactsAdapter friendContactsAdapter;

    public static FriendContactsFragment getInstance(int contactsType) {
        Bundle args = new Bundle();
        FriendContactsFragment fragment = new FriendContactsFragment();
        fragment.setArguments(args);
        fragment.contactsType = contactsType;
        return fragment;
    }

    @Override
    protected void initDagger() {
        ((BaseActivity) getActivity()).activityComponent().inject(this);
    }

    @Override
    protected int getContentViewId() {
        return R.layout.fragment_friend_contacts;
    }

    @Override
    protected void initViewsAndEvents(View rootView) {
        Log.e(TAG, "initViewsAndEvents: 当前Fragment的数据为：" + contactsType);
        rv_friend_contacts.setNestedScrollingEnabled(false);
        rv_friend_contacts.setFocusable(false);
        rv_friend_contacts.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
//        rv_friend_contacts.addItemDecoration(new GridSpacingItemDecoration(3,
//                DensityUtil.dip2px(this, 10), false));
        friendContactsAdapter = new FriendContactsAdapter(getActivity(), followBeanList, R.layout.item_friend_contacts, contactsType);
        rv_friend_contacts.setAdapter(friendContactsAdapter);

        friendContactsAdapter.setOnItemClickListener(new SuperAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int viewType, int position) {
                FollowBean followBean = friendContactsAdapter.getAllData().get(position);
                if (contactsType == 0) {
                    String chatName = TextUtils.isEmpty(followBean.getFollowUserNickName()) ? followBean.getFollowToUserName() : followBean.getFollowUserNickName();
                    startChatActivity(followBean.getFollowToUserName(), V2TIMConversation.V2TIM_C2C, chatName, "");
                } else {
                    String chatName = TextUtils.isEmpty(followBean.getUserNickName()) ? followBean.getUsername() : followBean.getUserNickName();
                    startChatActivity(followBean.getUsername(), V2TIMConversation.V2TIM_C2C, chatName, "");
                }
            }
        });

        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull final RefreshLayout refreshLayout) {
                queryFollowOrFans(Constants.MODE_REFRESH);
            }
        });
        refreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout rl) {
                queryFollowOrFans(Constants.MODE_LOAD_MORE);
            }
        });

        queryFollowOrFans(Constants.MODE_REFRESH);
    }

    /**
     * 启动聊天界面去发送消息
     *
     * @param chatId    用户ID
     * @param chatType  V2TIMConversation.V2TIM_C2C单聊；
     * @param chatName  聊天窗口顶部显示的名称文字
     * @param groupType 单聊的话，直接传空字符串
     */
    public static void startChatActivity(String chatId, int chatType, String chatName, String groupType) {
        if (TextUtils.isEmpty(chatId)) {
            return;
        }
        Bundle bundle = new Bundle();
        bundle.putString(TUIConstants.TUIChat.CHAT_ID, chatId);
        bundle.putString(TUIConstants.TUIChat.CHAT_NAME, chatName);
        bundle.putInt(TUIConstants.TUIChat.CHAT_TYPE, chatType);
        if (chatType == V2TIMConversation.V2TIM_C2C) {
            //启动单聊
            TUICore.startActivity(TUIConstants.TUIChat.C2C_CHAT_ACTIVITY_NAME, bundle);
        } else if (chatType == V2TIMConversation.V2TIM_GROUP) {
            //启动群聊
            bundle.putString(TUIConstants.TUIChat.GROUP_TYPE, groupType);
            TUICore.startActivity(TUIConstants.TUIChat.GROUP_CHAT_ACTIVITY_NAME, bundle);
        }
    }

//    private void insertFollowOrFans() {
//        FollowBean post = new FollowBean();
//        post.setUsername("帖子标题");
//        post.setFollowToUserName("帖子内容");
//        post.setFollowToUserName("帖子内容");
//
//        FollowBean userBasicInfoBean = new FollowBean();
//        userBasicInfoBean.setAddress("深圳南昌");
//        post.setUserBasicInfo(userBasicInfoBean);
//
//        FollowBean followBaseInfo = new FollowBean();
//        followBaseInfo.setAddress("南昌深圳");
//        post.setFollowToUserBaseInfo(userBasicInfoBean);
//        post.save(new SaveListener<String>() {
//            @Override
//            public void done(String s, BmobException e) {
//                if (e == null) {
//                    Log.e(TAG, "发布帖子成功：" + s);
//                } else {
//                    Log.e("BMOB", e.toString());
//                    Log.e(TAG,  e.getMessage());
//                }
//            }
//        });
//    }

    private void queryFollowOrFans(int refreshType) {
        BmobQuery<FollowBean> categoryBmobQuery = new BmobQuery<>();
        if (contactsType == 0) {
            categoryBmobQuery.addWhereEqualTo("username", Share.get().getUserName());//查询自己关注了谁
        } else {
            categoryBmobQuery.addWhereEqualTo("followToUserName", Share.get().getUserName());//查询自己被谁关注了
        }
        Log.e(TAG, "queryFollowOrFans: 开始查询自己关注 或者被关注：" + contactsType);

        categoryBmobQuery.setLimit(10);//每次最多查询10条
        if (refreshType == Constants.MODE_LOAD_MORE) {
            categoryBmobQuery.setSkip(friendContactsAdapter.getItemCount());// 忽略前多少条数据，查询后面的
        }
        categoryBmobQuery.findObjects(new FindListener<FollowBean>() {
            @Override
            public void done(List<FollowBean> result, BmobException e) {
                if (e == null) {
                    if (!result.isEmpty()) {
                        if (refreshType == Constants.MODE_REFRESH) {
                            followBeanList.clear();
                            followBeanList.addAll(result);
                            if (!followBeanList.isEmpty()) {
                                friendContactsAdapter.replaceAll(followBeanList);
                            }
                        } else {
                            followBeanList.addAll(result);
                            if (!followBeanList.isEmpty()) {
                                friendContactsAdapter.addAll(result);
                            }
                        }
                    } else {
                        Log.e(TAG, "done: 自己关注 或者被关注 用户数量为 0");
                        refreshLayout.finishLoadMoreWithNoMoreData();
                    }
                } else {
                    Log.e(TAG, e.toString());
                }
                refreshLayout.finishRefresh();
                refreshLayout.finishLoadMore();
            }
        });
    }
}
