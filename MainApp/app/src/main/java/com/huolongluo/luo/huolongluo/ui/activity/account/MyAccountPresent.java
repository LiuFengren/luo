package com.huolongluo.luo.huolongluo.ui.activity.account;

import android.content.Context;

import com.huolongluo.luo.base.ActivityContext;
import com.huolongluo.luo.huolongluo.bean.WXCreateOrderBean;
import com.huolongluo.luo.huolongluo.bean.WXCreateOrderEntity;
import com.huolongluo.luo.huolongluo.bean.WXCreateOrderEntity2;
import com.huolongluo.luo.huolongluo.net.okhttp.Api;
import com.huolongluo.luo.huolongluo.net.okhttp.HttpOnNextListener;

import javax.inject.Inject;

import rx.Subscription;

/**
 * <p>
 * Created by 火龙裸 on 2017/9/6.
 */

public class MyAccountPresent implements MyAccountContract.Presenter
{
    private MyAccountContract.View mView;
    private Context mContext;

    @Inject
    Api api;

    @Inject
    public MyAccountPresent(@ActivityContext Context mContext)
    {
        this.mContext = mContext;
    }

    @Override
    public void attachView(MyAccountContract.View view)
    {
        mView = view;
    }

    @Override
    public void detachView()
    {
        mView = null;
    }

    @Override
    public Subscription wxCreateOrder(String AuthorizationHeader, WXCreateOrderEntity wxCreateOrderEntity) {
        return api.wxCreateOrder(AuthorizationHeader, wxCreateOrderEntity, new HttpOnNextListener<WXCreateOrderBean>() {
            @Override
            public void onNext(WXCreateOrderBean response) {
                mView.wxCreateOrderSuccess(response);
            }
        });
    }

    @Override
    public Subscription wxCreateOrder2(WXCreateOrderEntity2 wxCreateOrderEntity2) {
        return api.wxCreateOrder2(wxCreateOrderEntity2, new HttpOnNextListener<WXCreateOrderBean>() {
            @Override
            public void onNext(WXCreateOrderBean response) {
                mView.wxCreateOrderSuccess(response);
            }
        });
    }
}
