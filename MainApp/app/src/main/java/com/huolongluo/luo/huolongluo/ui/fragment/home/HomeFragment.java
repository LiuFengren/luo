package com.huolongluo.luo.huolongluo.ui.fragment.home;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.CompositePageTransformer;
import androidx.viewpager2.widget.MarginPageTransformer;
import androidx.viewpager2.widget.ViewPager2;

import com.huolongluo.luo.R;
import com.huolongluo.luo.huolongluo.base.BaseActivity;
import com.huolongluo.luo.huolongluo.base.BaseFragment;
import com.huolongluo.luo.huolongluo.bean.ConsumeBean;
import com.huolongluo.luo.huolongluo.bean.RechargeRecordBean;
import com.huolongluo.luo.huolongluo.bean.UserBasicInfoBean;
import com.huolongluo.luo.huolongluo.constants.Constants;
import com.huolongluo.luo.huolongluo.share.Share;
import com.huolongluo.luo.huolongluo.ui.activity.square.SquareActivity;
import com.huolongluo.luo.huolongluo.ui.activity.upwall.UpWallActivity;
import com.huolongluo.luo.huolongluo.ui.activity.userdetail.UserDetailInfoActivity;
import com.huolongluo.luo.huolongluo.ui.adapter.HomeViewPager2Adapter2;
import com.huolongluo.luo.manager.pay.alipay.PayDemoActivity;
import com.huolongluo.luo.superAdapter.recycler.IMultiItemViewType;
import com.huolongluo.luo.superAdapter.recycler.SuperAdapter;
import com.huolongluo.luo.widget.ScaleInTransformer;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.inject.Inject;

import butterknife.BindView;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;

public class HomeFragment extends BaseFragment implements HomeContract.View {
    private static final String TAG = "HomeFragment";
    @Inject
    HomePresent present;
    @BindView(R.id.vp_home_fragment)
    ViewPager2 vp_home_fragment;

    private HomeViewPager2Adapter2 homeViewPager2Adapter;
    private List<UserBasicInfoBean> userBasicInfoBeanList = new ArrayList<>();

    public static HomeFragment getInstance() {
        Bundle args = new Bundle();
        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void initDagger() {
        ((BaseActivity) getActivity()).activityComponent().inject(this);
        present.attachView(this);
    }

    @Override
    protected int getContentViewId() {
        return R.layout.fragment_home;
    }

    @Override
    protected void initViewsAndEvents(View rootView) {
//        homeViewPager2Adapter = new HomeViewPager2Adapter(getActivity(), userBasicInfoBeanList, R.layout.item_home);
        homeViewPager2Adapter = new HomeViewPager2Adapter2(getActivity(), userBasicInfoBeanList, new IMultiItemViewType<UserBasicInfoBean>() {
            @Override
            public int getItemViewType(int position, UserBasicInfoBean userBasicInfoBean) {
                if (userBasicInfoBean.getLayoutType() == 0) {
                    return HomeViewPager2Adapter2.TYPE_NORMAL_PIC;
                } else if (userBasicInfoBean.getLayoutType() == 1) {
                    return HomeViewPager2Adapter2.TYPE_SQUARE_PIC;
                } else {
                    return HomeViewPager2Adapter2.TYPE_UP_WALL;
                }
            }

            @Override
            public int getLayoutId(int viewType) {
                if (viewType == HomeViewPager2Adapter2.TYPE_NORMAL_PIC) {
                    return R.layout.item_home;
                } else if (viewType == HomeViewPager2Adapter2.TYPE_SQUARE_PIC) {
                    return R.layout.item_home2;
                } else {
                    return R.layout.item_home3;
                }
            }
        });
        homeViewPager2Adapter.setOnItemClickListener(new SuperAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int viewType, int position) {
                if (homeViewPager2Adapter.getAllData().get(position).getLayoutType() == 1) {
                    startActivity(new Intent(mContext, SquareActivity.class));
                } else if (homeViewPager2Adapter.getAllData().get(position).getLayoutType() == 2) {
                    Log.e(TAG, "onItemClick: 进入推荐上墙界面");
                    startActivity(UpWallActivity.class);
                } else {
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.USER_NAME, homeViewPager2Adapter.getAllData().get(position).getUsername());
                    startActivity(UserDetailInfoActivity.class, bundle);
                }
            }
        });
        vp_home_fragment.setAdapter(homeViewPager2Adapter);
        //设置竖直滑动
        vp_home_fragment.setOrientation(ViewPager2.ORIENTATION_HORIZONTAL);
        //页面切换监听
        vp_home_fragment.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
            }
        });

        //设置Transformer
        CompositePageTransformer compositePageTransformer = new CompositePageTransformer();
        //添加边距Transformer
        compositePageTransformer.addTransformer(new MarginPageTransformer(60));
        //添加缩放效果的Transformer
        compositePageTransformer.addTransformer(new ScaleInTransformer());
        vp_home_fragment.setPageTransformer(compositePageTransformer);

        //预加载页面数量
        vp_home_fragment.setOffscreenPageLimit(1);
        //一屏多页
        RecyclerView recyclerView = (RecyclerView) vp_home_fragment.getChildAt(0);//获取ViewPager2中的RecyclerView
//        recyclerView.setPadding(60, 0, 60, 0);//设置Padding
//        recyclerView.setClipToPadding(false);//为false意思是，view内部的padding区也可以显示view
        if (recyclerView != null && recyclerView instanceof RecyclerView) {
            recyclerView.setPadding(100, 50, 100, 150);//设置Padding
            recyclerView.setClipToPadding(false);//为false意思是，view内部的padding区也可以显示view
        }

//        queryAllBackSex();//查询异性
//        queryAllUserBaseInfo();
        TextView startPayAct = rootView.findViewById(R.id.startPayAct);
        startPayAct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startPayAct();
            }
        });
    }

    public void startPayAct() {
        startActivity(PayDemoActivity.class);
    }

    private void queryAllBackSex() {
        showProgressDialog("");
        /*String backSex = TextUtils.equals("女", Share.get().getSex()) ? "男" : "女";//异性
        Log.e(TAG, "queryAllBackSex: 开始查询：" + backSex + "  当前用户性别：" + Share.get().getSex());
        BmobQuery<UserBasicInfoBean> categoryBmobQuery = new BmobQuery<>();
        categoryBmobQuery.addWhereEqualTo("sex", backSex);//异性
        categoryBmobQuery.addWhereEqualTo("isUpWall", true);//上墙用户
        categoryBmobQuery.findObjects(new FindListener<UserBasicInfoBean>() {
            @Override
            public void done(List<UserBasicInfoBean> result, BmobException e) {
                if (e == null) {
                    if (!result.isEmpty()) {
                        Log.e(TAG, "done: 上墙异性用户数量为：" + result.size());
                        userBasicInfoBeanList.clear();
                        userBasicInfoBeanList.addAll(result);

                        int index1 = getRandomInt(0, userBasicInfoBeanList.size() - 1);
                        Log.e(TAG, "done: 广场下标为：" + index1);
                        UserBasicInfoBean squarePic = new UserBasicInfoBean();
                        squarePic.setLayoutType(1);
                        userBasicInfoBeanList.add(index1, squarePic);//添加广场Item布局

                        int index2 = getRandomInt(0, userBasicInfoBeanList.size() - 1);
                        Log.e(TAG, "done: 上墙下标为：" + index2);
                        UserBasicInfoBean upWall = new UserBasicInfoBean();
                        upWall.setLayoutType(2);
                        userBasicInfoBeanList.add(index2, upWall);

                        homeViewPager2Adapter.addAll(userBasicInfoBeanList);
                    } else {
                        Log.e(TAG, "done: 首页查询到的上墙用户数量为 0");
                    }
                } else {
                    Log.e(TAG, e.toString());
                }
            }
        });*/
        /*********************/


        String backSex = TextUtils.equals("女", Share.get().getSex()) ? "男" : "女";//异性
        //--and条件1
        BmobQuery<UserBasicInfoBean> eq1 = new BmobQuery<UserBasicInfoBean>();
        eq1.addWhereEqualTo("sex", backSex);
        //--and条件2
        BmobQuery<UserBasicInfoBean> eq2 = new BmobQuery<UserBasicInfoBean>();
        eq2.addWhereEqualTo("isUpWall", true);
        //--and条件3
//        BmobQuery<UserBasicInfoBean> eq3 = new BmobQuery<UserBasicInfoBean>();
//        eq3.addWhereEqualTo("nickName", null);

        //最后组装完整的and条件
        List<BmobQuery<UserBasicInfoBean>> andQuerys = new ArrayList<BmobQuery<UserBasicInfoBean>>();
        andQuerys.add(eq1);
        andQuerys.add(eq2);
//        andQuerys.add(eq3);

        //查询符合整个and条件的人
        BmobQuery<UserBasicInfoBean> query = new BmobQuery<UserBasicInfoBean>();
        query.and(andQuerys);
        query.findObjects(new FindListener<UserBasicInfoBean>() {
            @Override
            public void done(List<UserBasicInfoBean> result, BmobException e) {
                if (e == null) {
                    if (!result.isEmpty()) {
                        List<UserBasicInfoBean> filterResult = new ArrayList<>();
                        for (int i = 0; i < result.size(); i++) {
                            if (UserBasicInfoBean.isCompleteBasicInfo(result.get(i))) {
                                filterResult.add(result.get(i));
                            }
                        }

                        userBasicInfoBeanList.clear();
                        userBasicInfoBeanList.addAll(filterResult);

                        Log.e(TAG, "done: 上墙用户（" + backSex + "）查询成功" + result.size() + " 过滤后的为：" + filterResult.size());
                        if (!userBasicInfoBeanList.isEmpty()) {
                            int index1 = getRandomInt(0, userBasicInfoBeanList.size() - 1);
                            Log.e(TAG, "done: 广场Item布局下标为：" + index1);
                            UserBasicInfoBean squarePic = new UserBasicInfoBean();
                            squarePic.setLayoutType(1);
                            userBasicInfoBeanList.add(index1, squarePic);//添加广场Item布局

                            int index2 = getRandomInt(0, userBasicInfoBeanList.size() - 1);
                            Log.e(TAG, "done: 上墙Item布局下标为：" + index2);
                            UserBasicInfoBean upWall = new UserBasicInfoBean();
                            upWall.setLayoutType(2);
                            userBasicInfoBeanList.add(index2, upWall);

                            homeViewPager2Adapter.addAll(userBasicInfoBeanList);
                        }
                    } else {
                        Log.e(TAG, "done: 首页查询到的上墙用户数量为 0");
                    }
                } else {
                    Log.e(TAG, e.toString());
                }
                hideProgressDialog();
            }
        });
    }

    //查询多条数据
    private void queryAllUserBaseInfo() {
        BmobQuery<UserBasicInfoBean> bmobQuery = new BmobQuery<>();
        bmobQuery.findObjects(new FindListener<UserBasicInfoBean>() {
            @Override
            public void done(List<UserBasicInfoBean> result, BmobException e) {
                if (e == null) {
                    if (!result.isEmpty()) {
                        userBasicInfoBeanList.clear();
                        userBasicInfoBeanList.addAll(result);

                        int index = getRandomInt(0, userBasicInfoBeanList.size() - 1);
                        Log.e(TAG, "done: 下标为：" + index);
                        userBasicInfoBeanList.add(index, null);

                        homeViewPager2Adapter.addAll(userBasicInfoBeanList);
                    }
                } else {
                    Log.e(TAG, e.toString());
                }
//                hideProgressDialog();
            }
        });
    }

    /*
     * 生成[min, max]之间的随机整数
     * @param min 最小整数
     * @param max 最大整数
     */
    private static int getRandomInt(int min, int max) {
        return new Random().nextInt(max) % (max - min + 1) + min;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (userBasicInfoBeanList.isEmpty()) {
            queryAllBackSex();//查询异性
        }
        //查询用户充值次数
        RechargeRecordBean.queryRechargeCount();
        ConsumeBean.queryConsumeConfig();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        present.detachView();
    }
}
