package com.huolongluo.luo.huolongluo.ui.fragment.discover;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.huolongluo.luo.R;
import com.huolongluo.luo.huolongluo.base.BaseFragment;
import com.huolongluo.luo.huolongluo.bean.ConsumeBean;
import com.huolongluo.luo.huolongluo.bean.DisCoverBean;
import com.huolongluo.luo.huolongluo.bean.JuBaoBean;
import com.huolongluo.luo.huolongluo.bean.RechargeRecordBean;
import com.huolongluo.luo.huolongluo.bean.UserBasicInfoBean;
import com.huolongluo.luo.huolongluo.constants.Constants;
import com.huolongluo.luo.huolongluo.share.Event;
import com.huolongluo.luo.huolongluo.share.Share;
import com.huolongluo.luo.huolongluo.ui.activity.publish.PublishActivity;
import com.huolongluo.luo.huolongluo.ui.adapter.DisCoverAdapter;
import com.huolongluo.luo.util.ToastSimple;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.interfaces.OnSelectListener;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnLoadMoreListener;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.SaveListener;

public class DiscoverFragment extends BaseFragment {
    private static final String TAG = "DiscoverFragment";

    @BindView(R.id.tv_discover_title)
    TextView tv_discover_title;
    @BindView(R.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    @BindView(R.id.rv_dongtai)
    RecyclerView rv_dongtai;
    @BindView(R.id.iv_fabu)
    ImageView iv_fabu;
    private long mClickTopTime;//返回顶部时间戳

    private List<DisCoverBean> dongTaiBeanList = new ArrayList<>();
    private DisCoverAdapter disCoverAdapter;

    private String curryQuerySex;

    public static DiscoverFragment getInstance() {
        Bundle args = new Bundle();
        DiscoverFragment fragment = new DiscoverFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void initDagger() {
    }

    @Override
    protected int getContentViewId() {
        return R.layout.fragment_discover;
    }

    @Override
    protected void initViewsAndEvents(View rootView) {
        EventBus.getDefault().register(this);
        curryQuerySex = TextUtils.equals("女", Share.get().getSex()) ? "男" : "女";
        tv_discover_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ((System.currentTimeMillis() - mClickTopTime) > 1000) {
                    mClickTopTime = System.currentTimeMillis();
                } else {
                    rv_dongtai.smoothScrollToPosition(0);
                }
            }
        });

        iv_fabu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (UserBasicInfoBean.isCompleteMyBasicInfo()) {
                    startActivity(PublishActivity.class);
                } else {
                    View toastView = LayoutInflater.from(getActivity()).inflate(R.layout.toast_text, null);
                    TextView tv_toast_message = toastView.findViewById(R.id.tv_toast_message);
                    tv_toast_message.setText("请先完善用户个人基本信息");
                    ToastSimple.makeText(Gravity.CENTER, 2, toastView).show();
                }
            }
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
//        linearLayoutManager.setStackFromEnd(true);
//        linearLayoutManager.setReverseLayout(true);
        rv_dongtai.setLayoutManager(linearLayoutManager);
        disCoverAdapter = new DisCoverAdapter(getActivity(), dongTaiBeanList, R.layout.item_discover);
        rv_dongtai.setAdapter(disCoverAdapter);

        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull final RefreshLayout refreshLayout) {
                queryDisCoverList(Constants.MODE_REFRESH);
            }
        });
        refreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout rl) {
                queryDisCoverList(Constants.MODE_LOAD_MORE);
            }
        });
        queryDisCoverList(Constants.MODE_REFRESH);
    }

    /**
     * 查询某个时间之前的10条数据
     * */
    private void queryDisCoverList(int refreshType) {
        BmobQuery<DisCoverBean> categoryBmobQuery = new BmobQuery<>();
        categoryBmobQuery.addWhereEqualTo("sex", curryQuerySex);
        categoryBmobQuery.setLimit(10);//每次最多查询10条
        if (refreshType == Constants.MODE_LOAD_MORE) {
            categoryBmobQuery.setSkip(disCoverAdapter.getItemCount());// 忽略前多少条数据，查询后面的
        }
        categoryBmobQuery.findObjects(new FindListener<DisCoverBean>() {
            @Override
            public void done(List<DisCoverBean> result, BmobException e) {
                if (e == null) {
                    if (!result.isEmpty()) {
                        Collections.reverse(result);
                        if (refreshType == Constants.MODE_REFRESH) {
                            dongTaiBeanList.clear();
                            disCoverAdapter.clear();
                            dongTaiBeanList.addAll(result);
                            Log.e(TAG, "done: 最终的数据为：" + dongTaiBeanList.toString());
                            if (!dongTaiBeanList.isEmpty()) {
                                disCoverAdapter.replaceAll(dongTaiBeanList);
                            }
                        } else {
                            dongTaiBeanList.addAll(result);
                            if (!dongTaiBeanList.isEmpty()) {
                                disCoverAdapter.addAll(result);
                            }
                        }
                    } else {
                        refreshLayout.finishLoadMoreWithNoMoreData();
                    }
                    Log.e(TAG, "done: 动态列表（" + curryQuerySex + "）查询成功" + result.size());
                } else {
                    Log.e(TAG, e.toString());
                }
//                hideProgressDialog();
                refreshLayout.finishRefresh();
                refreshLayout.finishLoadMore();
            }
        });
    }

    /**
     * 点击动态更多
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void clickDongTaiMore(Event.clickDongTaiMore event) {
        Log.e(TAG, "clickDongTaiMore: 点击了动态：" + event.disCoverBean.getDongtaiText());
        new XPopup.Builder(getContext())
                .isDestroyOnDismiss(true) //对于只使用一次的弹窗，推荐设置这个
//                .isDarkTheme(true)
                .hasShadowBg(true)
//                        .isViewMode(true)
//                            .hasBlurBg(true)
//                            .isDestroyOnDismiss(true) //对于只使用一次的弹窗，推荐设置这个
                .asBottomList("", new String[]{"举报", "不看TA的动态"},
                        new OnSelectListener() {
                            @Override
                            public void onSelect(int position, String text) {
                                Log.e(TAG, "click " + text);
                                if (position == 0) {
                                    juBao(event.disCoverBean);
                                } else {
                                    dongTaiBeanList.remove(event.position);
                                    disCoverAdapter.remove(event.position);
                                }
                            }
                        }).show();
    }

    private void juBao(DisCoverBean jubaoBean) {
        new XPopup.Builder(getContext())
                .isDestroyOnDismiss(true) //对于只使用一次的弹窗，推荐设置这个
//                .isDarkTheme(true)
                .hasShadowBg(true)
//                        .isViewMode(true)
//                            .hasBlurBg(true)
//                            .isDestroyOnDismiss(true) //对于只使用一次的弹窗，推荐设置这个
                .asBottomList("", new String[]{"政治敏感", "暴力色情低俗", "不友善的内容", "造谣诽谤", "广告营销", "其他"},
                        new OnSelectListener() {
                            @Override
                            public void onSelect(int position, String text) {
                                Log.e(TAG, "click " + text);
                                JuBaoBean juBaoBean = new JuBaoBean();
                                juBaoBean.setUsername(Share.get().getUserName());
                                juBaoBean.setJubaoUserName(jubaoBean.getUsername());
                                juBaoBean.setJubaoDongTaiObjectId(jubaoBean.getObjectId());
                                juBaoBean.setJubaoType(text);
                                juBaoBean.save(new SaveListener<String>() {
                                    @Override
                                    public void done(String objectId, BmobException e) {
                                        if (e == null) {
                                            Log.e(TAG, "done: 举报数据添加成功：" + objectId);
                                            ToastSimple.show("操作成功");
                                        } else {
                                            Log.e(TAG, "done: 举报数据添加失败：" + e.getMessage());
                                            ToastSimple.show("操作失败：" + e.getMessage());
                                        }
                                    }
                                });
                            }
                        }).show();
    }

    private void queryUserBasicInfo() {
        //查找UserBasicInfoBean表里面username为自己的数据
        BmobQuery<UserBasicInfoBean> categoryBmobQuery = new BmobQuery<>();
        categoryBmobQuery.addWhereEqualTo("username", Share.get().getUserName());
        categoryBmobQuery.findObjects(new FindListener<UserBasicInfoBean>() {
            @Override
            public void done(List<UserBasicInfoBean> result, BmobException e) {
                if (e == null) {
                    if (!result.isEmpty()) {
                        Log.e(TAG, "查询成功 当前用户的objectId>>>>>：" + result.get(0).getObjectId());
                        UserBasicInfoBean userBasicInfoBean = result.get(0);
                        UserBasicInfoBean.updateLocalSpUserBasicInfo(userBasicInfoBean);
                    } else {
                        Log.e(TAG, "done: 查找UserBasicInfoBean表里面username为自己的数据 为空");
                    }
                } else {
                    Log.e(TAG, e.toString());
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        //查询用户个人信息
        queryUserBasicInfo();

        //查询用户充值次数
        RechargeRecordBean.queryRechargeCount();
        ConsumeBean.queryConsumeConfig();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }
}
