package com.huolongluo.luo.huolongluo.ui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ConvertUtils;
import com.bumptech.glide.Glide;
import com.huolongluo.luo.R;
import com.huolongluo.luo.superAdapter.recycler.BaseViewHolder;
import com.huolongluo.luo.superAdapter.recycler.SuperAdapter;
import com.jakewharton.rxbinding.view.RxView;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.ImageViewerPopupView;
import com.lxj.xpopup.interfaces.OnSrcViewUpdateListener;
import com.lxj.xpopup.util.SmartGlideImageLoader;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import rx.android.schedulers.AndroidSchedulers;

public class UserInfoDetailCardPicAdapter extends SuperAdapter<String> {

    public UserInfoDetailCardPicAdapter(Context context, List<String> list, int layoutResId) {
        super(context, list, layoutResId);
    }

    @Override
    public void onBind(int viewType, BaseViewHolder holder, int position, String item) {
        Glide.with(mContext).load(item).into((ImageView) holder.getView(R.id.iv_userinfo_card_pic));

        ArrayList<Object> list = new ArrayList<>();
        for (int i = 0; i < mList.size(); i++) {
            list.add(mList.get(i));
        }

        // 点击item
        RxView.clicks(holder.getItemView()).throttleFirst(500, TimeUnit.MILLISECONDS).observeOn(AndroidSchedulers.mainThread()).subscribe(v ->
        {
            new XPopup.Builder(mContext)
//                            .animationDuration(1000)
                    .isTouchThrough(true)
                    .asImageViewer(holder.getView(R.id.iv_userinfo_card_pic), position, list,
                            false, true, -1, -1, ConvertUtils.dp2px(10), true,
                            Color.rgb(32, 36, 46),
                            new OnSrcViewUpdateListener() {
                                @Override
                                public void onSrcViewUpdate(ImageViewerPopupView popupView, int position) {
//                                    RecyclerView rv = (RecyclerView) holder.itemView.getParent();
//                                    popupView.updateSrcView((ImageView) rv.getChildAt(position));
                                }
                            }, new SmartGlideImageLoader(true, R.mipmap.ic_launcher), null)
                    .show();
        });
    }
}
