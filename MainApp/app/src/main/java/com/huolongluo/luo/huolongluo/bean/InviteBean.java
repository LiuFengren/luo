package com.huolongluo.luo.huolongluo.bean;

import cn.bmob.v3.BmobObject;

public class InviteBean extends BmobObject {
    private String username;//用户名
    private String beInvitedUsername;//被谁邀请的 那个人的用户名
//    private String beInvitedSex;//被谁邀请的 那个人的性别

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getBeInvitedUsername() {
        return beInvitedUsername;
    }

    public void setBeInvitedUsername(String beInvitedUsername) {
        this.beInvitedUsername = beInvitedUsername;
    }

//    public String getBeInvitedSex() {
//        return beInvitedSex;
//    }
//
//    public void setBeInvitedSex(String beInvitedSex) {
//        this.beInvitedSex = beInvitedSex;
//    }
}
