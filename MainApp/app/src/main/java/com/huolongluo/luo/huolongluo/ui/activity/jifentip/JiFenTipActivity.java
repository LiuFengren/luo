package com.huolongluo.luo.huolongluo.ui.activity.jifentip;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.gyf.immersionbar.ImmersionBar;
import com.huolongluo.luo.R;
import com.huolongluo.luo.huolongluo.base.BaseActivity;

import butterknife.BindView;

/**
 * <p>
 * Created by 火龙裸 on 2017/9/6.
 */

public class JiFenTipActivity extends BaseActivity {
    private static final String TAG = "JiFenTipActivity";
    @BindView(R.id.toolbar_center_title)
    TextView toolbar_center_title;
    @BindView(R.id.iv_left)
    ImageView iv_left;

    /********************* 以上为ToolBar ************************/

    @Override
    protected int getContentViewId() {
        return R.layout.activity_jifen_tip;
    }

    @Override
    protected void injectDagger() {
        activityComponent().inject(this);
    }

    private void initToolBar() {
        ImmersionBar.with(this).statusBarDarkFont(true).init();
        toolbar_center_title.setText("积分常见问题");
    }

    @Override
    protected void initViewsAndEvents(@Nullable Bundle savedInstanceState) {
        initToolBar();
        eventClick(iv_left).subscribe(o -> // 点击返回键
        {
            close();
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
