package com.huolongluo.luo.huolongluo.base;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.huolongluo.luo.R;
import com.huolongluo.luo.base.BaseView;
import com.huolongluo.luo.huolongluo.injection.component.ActivityComponent;
import com.huolongluo.luo.huolongluo.injection.component.DaggerActivityComponent;
import com.huolongluo.luo.huolongluo.injection.model.ActivityModule;
import com.huolongluo.luo.huolongluo.manager.DialogClickListener;
import com.huolongluo.luo.huolongluo.manager.DialogManager2;
import com.huolongluo.luo.huolongluo.manager.StateLayoutManager;
import com.huolongluo.luo.manager.AppManager;
import com.huolongluo.luo.util.ToastSimple;
import com.jakewharton.rxbinding.view.RxView;

import java.util.concurrent.TimeUnit;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by 火龙裸先生 on 2017/8/10.
 * Class Note:
 * 1 所有的activity继承于这个类
 */
public abstract class BaseActivity extends AppCompatActivity implements BaseView
{
    private Context mContext = null; //context
    private ActivityComponent mActivityComponent;

    public Subscription subscription;
    Unbinder unbinder;

    @Nullable
    private StateLayoutManager mStatusLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        mContext = this;

        AppManager.get().addActivity(this);

        if (getContentViewId() != 0)
        {
            setContentView(getContentViewId());
        }

        unbinder = ButterKnife.bind(this);

        //设置多状态布局
        View contentView = findViewById(R.id.content_view);
        if (contentView != null) {
            mStatusLayout = new StateLayoutManager.Builder(mContext)
                    .initPage(contentView)
                    .create();
        }

        // dagger2注解
        injectDagger();

        initViewsAndEvents(savedInstanceState);
    }

    public ActivityComponent activityComponent()
    {
        if (null == mActivityComponent)
        {
            mActivityComponent = DaggerActivityComponent.builder().applicationComponent(BaseApp.get(this).getAppComponent()).activityModule(new
                    ActivityModule(this)).build();
        }
        return mActivityComponent;
    }


    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        AppManager.get().finishActivity(this);

        unbinder.unbind();
        unSubscription();
    }

    public void unSubscription()
    {
        if (null != subscription && !subscription.isUnsubscribed())
        {
            subscription.unsubscribe();
        }
    }

    public void startActivity(Class<?> clazz)
    {
        Intent intent = new Intent(this, clazz);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }

    public void startActivity(Class<?> clazz, Bundle bundle)
    {
        Intent intent = new Intent(this, clazz);
        if (null != bundle)
        {
            intent.putExtra("bundle", bundle);
        }
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }

    public Observable<Void> eventClick(View view)
    {
        return eventClick(view, 1000);
    }

    public Observable<Void> eventClick(View view, int milliseconds)
    {
        return RxView.clicks(view).throttleFirst(milliseconds, TimeUnit.MILLISECONDS).observeOn(AndroidSchedulers.mainThread());
    }

    public Bundle getBundle()
    {
        return getIntent().getBundleExtra("bundle");
    }

    /**
     * bind layout resource file
     */
    protected abstract int getContentViewId();

    /**
     * Dagger2 use in your application module(not used in 'base' module)
     */
    protected abstract void injectDagger();

    /**
     * init views and events here
     */
    protected abstract void initViewsAndEvents(@Nullable Bundle savedInstanceState);


    /**
     * implements methods in BaseView
     */
    @Override
    public void showToastMessage(String msg, double seconds)
    {
        ToastSimple.show(msg, seconds);
    }

    @Override
    public void close()
    {
        finish();
    }

    @Override
    public void showProgressDialog(String msg)
    {
        DialogManager2.INSTANCE.showProgressDialog(mContext, msg);
    }

//    @Override
//    public void showProgressDialog(String msg, int progress)
//    {
//        DialogManager.INSTANCE.showProgressDialog(mContext, msg, progress);
//    }

    @Override
    public void hideProgressDialog()
    {
        DialogManager2.INSTANCE.dismiss();
    }

//    @Override
//    public void showErrorMessage(String msg, String content)
//    {
//        DialogManager.INSTANCE.showErrorDialog(mContext, msg, content, SweetAlertDialog::dismissWithAnimation);
//    }


    @Override
    public void showConfirmDialog(String title, String content, DialogClickListener dialogClickListener) {
        DialogManager2.INSTANCE.showConfirmDialog(mContext, title, content, dialogClickListener);
    }

    @Override
    public void showConfirmNoCancelDialog(String title, String content, DialogClickListener dialogClickListener) {
        DialogManager2.INSTANCE.showConfirmNoCancelDialog(mContext, title, content, dialogClickListener);
    }

    /**
     * 加载中布局
     */
    @Override
    public void showDefaultListLoading() {
        if (mStatusLayout != null) {
            mStatusLayout.showLoading();
        }
    }

    /**
     * 展示正常布局
     */
    @Override
    public void showDefaultContent() {
        if (mStatusLayout != null) {
            mStatusLayout.showContent();
        }
    }

    /**
     * 展示空布局 默认文案
     */
    @Override
    public void showDefaultEmpty() {
        if (mStatusLayout != null) {
            mStatusLayout.showEmpty();
        }
    }

    /**
     * 展示空布局
     *
     * @param text 文案
     */
    @Override
    public void showDefaultEmpty(@Nullable String text) {
        if (mStatusLayout != null) {
            mStatusLayout.showEmpty();
            mStatusLayout.emptyText(text);
        }
    }

    @Override
    public void showDefaultEmpty(@Nullable String text, int resId) {
        if (mStatusLayout != null) {
            mStatusLayout.showEmpty();
            mStatusLayout.emptyText(text);
            mStatusLayout.emptyIv(resId);
        }
    }

    /**
     * 展示空布局 带有点击事件
     */
    @Override
    public void showDefaultEmptyClick(View.OnClickListener listener) {
        if (mStatusLayout != null) {
            mStatusLayout.showEmptyClick(listener);
        }
    }

    /**
     * 展示错误布局
     *
     * @param listener
     */
    @Override
    public void showDefaultError(View.OnClickListener listener) {
        if (mStatusLayout != null) {
            mStatusLayout.showError(listener);
        }
    }
}
