package com.huolongluo.luo.huolongluo.ui.activity.feedback;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.gyf.immersionbar.ImmersionBar;
import com.huolongluo.luo.R;
import com.huolongluo.luo.huolongluo.base.BaseActivity;
import com.huolongluo.luo.huolongluo.bean.FeedBackBean;
import com.huolongluo.luo.huolongluo.manager.DialogClickListener;
import com.huolongluo.luo.huolongluo.share.Share;
import com.huolongluo.luo.util.ToastSimple;

import butterknife.BindView;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.SaveListener;

/**
 * <p>
 * Created by 火龙裸 on 2017/9/6.
 */

public class FeedBackActivity extends BaseActivity {
    private static final String TAG = "FeedBackActivity";
    @BindView(R.id.toolbar_center_title)
    TextView toolbar_center_title;
    @BindView(R.id.iv_left)
    ImageView iv_left;
    @BindView(R.id.tv_right)
    TextView tv_right;
    /********************* 以上为ToolBar ************************/
    @BindView(R.id.et_feed_back_content)
    EditText et_feed_back_content;
    @BindView(R.id.tv_sure)
    TextView tv_sure;

    @Override
    protected int getContentViewId() {
        return R.layout.activity_feed_back;
    }

    @Override
    protected void injectDagger() {
        activityComponent().inject(this);
    }

    private void initToolBar() {
        ImmersionBar.with(this).statusBarDarkFont(true).init();
        toolbar_center_title.setText("问题反馈");
    }

    @Override
    protected void initViewsAndEvents(@Nullable Bundle savedInstanceState) {
        initToolBar();
        eventClick(iv_left).subscribe(o -> // 点击返回键
        {
            close();
        });
        eventClick(tv_sure).subscribe(o -> // 点击确定
        {
            String etFeedBackContent = et_feed_back_content.getText().toString().trim();
            if (TextUtils.isEmpty(etFeedBackContent) || etFeedBackContent.length() < 10) {

                View view = LayoutInflater.from(FeedBackActivity.this).inflate(R.layout.toast_text, null);
                TextView tv_toast_message = view.findViewById(R.id.tv_toast_message);
                tv_toast_message.setText("请填写至少10个字的问题描述");
                ToastSimple.makeText(Gravity.CENTER, 1, view).show();

                return;
            }

            createFeedBack(etFeedBackContent);
        });
    }

    private void createFeedBack(String feedBackContent) {
        FeedBackBean feedBackBean = new FeedBackBean();
        feedBackBean.setUsername(Share.get().getUserName());
        feedBackBean.setFeedBackContent(feedBackContent);
        feedBackBean.save(new SaveListener<String>() {
            @Override
            public void done(String objectId, BmobException e) {
                if (e == null) {
                    Log.e(TAG, "问题反馈数据提交成功");
                    showConfirmNoCancelDialog("问题反馈", "您的问题已提交，我们会尽快处理并答复给你", new DialogClickListener() {
                        @Override
                        public void onConfirm() {
                            close();
                        }

                        @Override
                        public void onCancel() {
                            close();
                        }
                    });
                } else {
                    ToastSimple.show("问题反馈数据提交失败：" + e.getMessage());
                }
            }
        });
    }
}
