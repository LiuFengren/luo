package com.huolongluo.luo.huolongluo.ui.activity.vip;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alipay.sdk.app.PayTask;
import com.gyf.immersionbar.ImmersionBar;
import com.huolongluo.luo.R;
import com.huolongluo.luo.huolongluo.base.BaseActivity;
import com.huolongluo.luo.huolongluo.bean.RechargeRecordBean;
import com.huolongluo.luo.huolongluo.bean.UserWalletBean;
import com.huolongluo.luo.huolongluo.bean.VipPackageBean;
import com.huolongluo.luo.huolongluo.share.Share;
import com.huolongluo.luo.huolongluo.ui.adapter.VIPPackageAdapter;
import com.huolongluo.luo.manager.pay.alipay.AuthResult;
import com.huolongluo.luo.manager.pay.alipay.PayResult;
import com.huolongluo.luo.manager.pay.alipay.util.AliPayConfig;
import com.huolongluo.luo.manager.pay.alipay.util.OrderInfoUtil2_0;
import com.huolongluo.luo.manager.pay.wechat.WxConfig;
import com.huolongluo.luo.superAdapter.recycler.SuperAdapter;
import com.huolongluo.luo.util.Arith;
import com.huolongluo.luo.util.ToastSimple;
import com.luck.picture.lib.decoration.GridSpacingItemDecoration;
import com.luck.picture.lib.utils.DensityUtil;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;

/**
 * <p>
 * Created by 火龙裸 on 2017/9/6.
 */

public class VipActivity extends BaseActivity {
    private static final String TAG = "VipActivity";
    @BindView(R.id.toolbar_center_title)
    TextView toolbar_center_title;
    @BindView(R.id.iv_left)
    ImageView iv_left;
    @BindView(R.id.tv_right)
    TextView tv_right;
    /********************* 以上为ToolBar ************************/
    @BindView(R.id.iv_header)
    ImageView iv_header;
    @BindView(R.id.rv_vip_package)
    RecyclerView rv_vip_package;
    @BindView(R.id.tv_vip_year_get_gold)
    TextView tv_vip_year_get_gold;
    @BindView(R.id.ll_wx_pay)
    LinearLayout ll_wx_pay;
    @BindView(R.id.ll_ali_pay)
    LinearLayout ll_ali_pay;

    private VIPPackageAdapter vipPackageAdapter;
    private List<VipPackageBean> vipPackageBeanList = new ArrayList<>();//商品列表

    private int payType = 1;//1表示微信支付，2表示支付宝支付
    private VipPackageBean payVipPackageBean = null;

    private IWXAPI api;

    @Override
    protected int getContentViewId() {
        return R.layout.activity_vip;
    }

    @Override
    protected void injectDagger() {
        activityComponent().inject(this);
    }

    private void initToolBar() {
        ImmersionBar.with(this).statusBarDarkFont(true).init();
        toolbar_center_title.setText("开通VIP");
    }

    @Override
    protected void initViewsAndEvents(@Nullable Bundle savedInstanceState) {
        api = WXAPIFactory.createWXAPI(this, WxConfig.APP_ID, false);
        initToolBar();
        eventClick(iv_left).subscribe(o -> // 点击返回键
        {
            close();
        });

        /*// 微信支付
        ll_wx_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                payType = 1;
            }
        });
        // 支付宝支付
        ll_ali_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                payType = 2;
            }
        });*/

        rv_vip_package.setNestedScrollingEnabled(false);
        rv_vip_package.setFocusable(false);
        rv_vip_package.setLayoutManager(new GridLayoutManager(this, 3) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        });
        rv_vip_package.addItemDecoration(new GridSpacingItemDecoration(3,
                DensityUtil.dip2px(this, 10), false));
        vipPackageAdapter = new VIPPackageAdapter(this, vipPackageBeanList, R.layout.item_vip_package);
        rv_vip_package.setAdapter(vipPackageAdapter);

        vipPackageAdapter.setOnItemClickListener(new SuperAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int viewType, int position) {
                for (int i = 0; i < vipPackageAdapter.getItemCount(); i++) {
                    vipPackageAdapter.getAllData().get(i).setChecked(false);
                }
                vipPackageAdapter.getAllData().get(position).setChecked(true);
                for (int i = 0; i < vipPackageAdapter.getItemCount(); i++) {
                    Log.e("TAG", "onBind: 最终的选中情况：" + vipPackageAdapter.getAllData().get(i).isChecked());
                }
                vipPackageAdapter.notifyDataSetChanged();
            }
        });

        eventClick(ll_wx_pay).subscribe(o -> // 微信支付
        {
            ToastSimple.show("服务器异常");
            /*int wxSdkVersion = api.getWXAppSupportAPI();
            if (wxSdkVersion >= Build.OFFLINE_PAY_SDK_INT) {
                api.sendReq(new JumpToOfflinePay.Req());
            }else {
                Toast.makeText(VipActivity.this, "not supported", Toast.LENGTH_LONG).show();
            }*/
        });
        eventClick(ll_ali_pay).subscribe(o -> // 支付宝支付
        {
            payVipPackageBean = null;
            for (int i = 0; i < vipPackageAdapter.getItemCount(); i++) {
                if (vipPackageAdapter.getAllData().get(i).isChecked()) {
                    payVipPackageBean = vipPackageAdapter.getAllData().get(i);
                    break;
                }
            }
            if (payVipPackageBean == null) {
                return;
            }
            startAliPay(payVipPackageBean.getPrice() + "", payVipPackageBean.getPackageName());
        });

        queryGoods();
    }

    /**
     * 查询多条数据
     * 商品列表
     */
    private void queryGoods() {
//        showProgressDialog("");
        BmobQuery<VipPackageBean> bmobQuery = new BmobQuery<>();
        bmobQuery.findObjects(new FindListener<VipPackageBean>() {
            @Override
            public void done(List<VipPackageBean> result, BmobException e) {
                if (e == null) {
                    vipPackageBeanList.clear();
                    vipPackageBeanList.addAll(result);
                    if (!vipPackageBeanList.isEmpty()) {
                        vipPackageBeanList.get(vipPackageBeanList.size() - 1).setChecked(true);
                        vipPackageAdapter.addAll(vipPackageBeanList);
                        Log.e(TAG, "done: VIP商品查询成功" + result.size() + " 金额：" + result.get(0).getPackageName());
                        for (int i = 0; i < vipPackageBeanList.size(); i++) {
                            if (TextUtils.equals("年会员", vipPackageBeanList.get(i).getPackageName())) {
                                if (tv_vip_year_get_gold != null) {
                                    tv_vip_year_get_gold.setText("年会员开通立送" + vipPackageBeanList.get(i).getGiftGold() + "金币");
                                }
                                break;
                            }
                        }
                    }
                } else {
                    Log.e(TAG, e.toString());
                }
            }
        });
    }

    /**
     * 开始调起支付宝支付
     */
    private void startAliPay(String amount, String body) {
        if (TextUtils.isEmpty(AliPayConfig.APPID) || (TextUtils.isEmpty(AliPayConfig.RSA2_PRIVATE) && TextUtils.isEmpty(AliPayConfig.RSA_PRIVATE))) {
//            Log.e(TAG, "错误: 需要配置PARTNER |APP_ID| RSA_PRIVATE| TARGET_ID");
            ToastSimple.show("配置错误", 2);
            Log.e(TAG, "配置错误");
            return;
        }

        /*
         * 这里只是为了方便直接向商户展示支付宝的整个支付流程；所以Demo中加签过程直接放在客户端完成；
         * 真实App里，privateKey等数据严禁放在客户端，加签过程务必要放在服务端完成；
         * 防止商户私密数据泄露，造成不必要的资金损失，及面临各种安全风险；
         *
         * orderInfo 的获取必须来自服务端；
         */
        boolean rsa2 = (AliPayConfig.RSA2_PRIVATE.length() > 0);
        Map<String, String> params = OrderInfoUtil2_0.buildOrderParamMap(amount, body);
        String orderParam = OrderInfoUtil2_0.buildOrderParam(params);

        String privateKey = rsa2 ? AliPayConfig.RSA2_PRIVATE : AliPayConfig.RSA_PRIVATE;
        String sign = OrderInfoUtil2_0.getSign(params, privateKey, rsa2);
        final String orderInfo = orderParam + "&" + sign;

        final Runnable payRunnable = new Runnable() {
            @Override
            public void run() {
                PayTask alipay = new PayTask(VipActivity.this);
                Map<String, String> result = alipay.payV2(orderInfo, true);
                Log.i("msp", result.toString());

                Message msg = new Message();
                msg.what = AliPayConfig.SDK_PAY_FLAG;
                msg.obj = result;
                mHandler.sendMessage(msg);
            }
        };

        // 必须异步调用
        Thread payThread = new Thread(payRunnable);
        payThread.start();
    }

    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {
        @SuppressWarnings("unused")
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case AliPayConfig.SDK_PAY_FLAG: {
                    @SuppressWarnings("unchecked")
                    PayResult payResult = new PayResult((Map<String, String>) msg.obj);
                    /**
                     * 对于支付结果，请商户依赖服务端的异步通知结果。同步通知结果，仅作为支付结束的通知。
                     */
                    String resultInfo = payResult.getResult();// 同步返回需要验证的信息
                    String resultStatus = payResult.getResultStatus();
                    // 判断resultStatus 为9000则代表支付成功
                    if (TextUtils.equals(resultStatus, "9000")) {
                        // 该笔订单是否真实支付成功，需要依赖服务端的异步通知。
                        Log.e(TAG, "handleMessage: 支付成功：" + payResult);
                        ToastSimple.show("支付成功:" + payResult);
                        double currentGoldBalance = Double.parseDouble(Share.get().getGoldBalance());//用户当前金币余额
                        double rechargeGold = payVipPackageBean.getGiftGold();//充值了多少金币(包括赠送的)
                        double goldBalanceResult = Arith.add(currentGoldBalance, rechargeGold);
                        UserWalletBean.updateRechargeGoldBalance(goldBalanceResult);
                        RechargeRecordBean.addRechargeRecord(payVipPackageBean.getPackageName(), payVipPackageBean.getPrice() + "", "支付宝");
                    } else {
                        // 该笔订单真实的支付结果，需要依赖服务端的异步通知。
                        ToastSimple.show("支付失败：" + payResult.getMemo());
                    }
                    break;
                }
                case AliPayConfig.SDK_AUTH_FLAG: {
                    @SuppressWarnings("unchecked")
                    AuthResult authResult = new AuthResult((Map<String, String>) msg.obj, true);
                    String resultStatus = authResult.getResultStatus();

                    // 判断resultStatus 为“9000”且result_code
                    // 为“200”则代表授权成功，具体状态码代表含义可参考授权接口文档
                    if (TextUtils.equals(resultStatus, "9000") && TextUtils.equals(authResult.getResultCode(), "200")) {
                        // 获取alipay_open_id，调支付时作为参数extern_token 的value
                        // 传入，则支付账户为该授权账户
                        Log.e(TAG, "handleMessage: 授权成功：" + authResult);
                        ToastSimple.show("授权成功：" + authResult);
                    } else {
                        // 其他状态值则为授权失败
                        Log.e(TAG, "handleMessage: 授权失败：" + authResult);
                        ToastSimple.show("授权失败：" + authResult);
                    }
                    break;
                }
                default:
                    break;
            }
        }

        ;
    };
}
