package com.huolongluo.luo.huolongluo.bean;

import android.util.Log;

import com.huolongluo.luo.huolongluo.share.Share;

import cn.bmob.v3.BmobObject;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.CountListener;
import cn.bmob.v3.listener.SaveListener;

public class RechargeRecordBean extends BmobObject {
    private static final String TAG = "RechargeRecordBean";
    private String username;//用户名
    private String rechargeName;//充值名称/
    private String payMoney;//充值的金额
    private String payway;//支付方式(1微信，2支付宝)

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRechargeName() {
        return rechargeName;
    }

    public void setRechargeName(String rechargeName) {
        this.rechargeName = rechargeName;
    }

    public String getPayMoney() {
        return payMoney;
    }

    public void setPayMoney(String payMoney) {
        this.payMoney = payMoney;
    }

    public String getPayway() {
        return payway;
    }

    public void setPayway(String payway) {
        this.payway = payway;
    }

    /**
     * 查询用户充值次数
     */
    public static void queryRechargeCount() {
        BmobQuery<RechargeRecordBean> query = new BmobQuery<RechargeRecordBean>();
        query.addWhereEqualTo("username", Share.get().getUserName());
        query.count(RechargeRecordBean.class, new CountListener() {
            @Override
            public void done(Integer count, BmobException e) {
                if (e == null) {
                    Share.get().setRechargeCount(count);
                    Log.e(TAG, "查询用户充值次数 的count对象个数为：" + count);
                } else {
                    Log.e(TAG, "查询用户充值次数失败：" + e.getMessage() + "," + e.getErrorCode());
                }
            }
        });
    }

    /**
     * 添加一行充值/消费记录
     * @param rechargeName 充值名称
     * @param payMoney 充值的金额
     * @param payway 支付方式(1微信，2支付宝)
     * */
    public static void addRechargeRecord(String rechargeName, String payMoney, String payway) {
        RechargeRecordBean p2 = new RechargeRecordBean();
        p2.setUsername(Share.get().getUserName());
        p2.setRechargeName(rechargeName);
        p2.setPayMoney(payMoney);
        p2.setPayway(payway);
        p2.save(new SaveListener<String>() {
            @Override
            public void done(String objectId,BmobException e) {
                if(e==null){
                    Log.d(TAG, "添加充值记录成功："+objectId);
                }else{
                    Log.e(TAG, "添加充值记录失败：" + e.getMessage());
                }
            }
        });
    }
}
