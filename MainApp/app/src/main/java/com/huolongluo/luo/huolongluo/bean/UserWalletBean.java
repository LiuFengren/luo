package com.huolongluo.luo.huolongluo.bean;

import android.text.TextUtils;
import android.util.Log;

import com.huolongluo.luo.huolongluo.share.Share;
import com.huolongluo.luo.util.Arith;

import java.util.List;

import cn.bmob.v3.BmobObject;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.SaveListener;
import cn.bmob.v3.listener.UpdateListener;

/**
 * 用户钱包
 */
public class UserWalletBean extends BmobObject {
    private static final String TAG = "UserWalletBean";

    private String username;//用户名
    private boolean isVip;//是否VIP
    private String goldBalance;//金币余额
    private String integralBalance;//积分余额
//    private String tiXianAccount;//提现账户名

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isVip() {
        return isVip;
    }

    public void setVip(boolean vip) {
        isVip = vip;
    }

    public String getGoldBalance() {
        if (TextUtils.isEmpty(goldBalance)) {
            return "0";
        }
        return goldBalance;
    }

    public void setGoldBalance(String goldBalance) {
        this.goldBalance = goldBalance;
    }

    public String getIntegralBalance() {
        if (TextUtils.isEmpty(integralBalance)) {
            return "0";
        }
        return integralBalance;
    }

    public void setIntegralBalance(String integralBalance) {
        this.integralBalance = integralBalance;
    }

    /**
     * 查询用户钱包余额，是否会员
     */
    public static void queryUserWallet() {
        BmobQuery<UserWalletBean> categoryBmobQuery = new BmobQuery<>();
        categoryBmobQuery.addWhereEqualTo("username", Share.get().getUserName());
        categoryBmobQuery.findObjects(new FindListener<UserWalletBean>() {
            @Override
            public void done(List<UserWalletBean> result, BmobException e) {
                if (e == null) {
                    if (!result.isEmpty()) {
                        UserWalletBean userWalletBean = result.get(0);
                        Share.get().setUser_wallet_bean_object_id(userWalletBean.getObjectId());
                        Share.get().setIsVip(userWalletBean.isVip());
                        Share.get().setGoldBalance(userWalletBean.getGoldBalance());
                        Share.get().setIntegralBalance(userWalletBean.getIntegralBalance());
                    } else {
//                        createUserWallet();//给用户创建一个钱包
                        UserWalletBean.createUserWallet();
                    }
                } else {
                    Log.e(TAG, e.toString());
                }
            }
        });
    }

    /**
     * 新用户，创建用户钱包
     * 给用户创建一个钱包
     */
    public static void createUserWallet() {
        UserWalletBean p2 = new UserWalletBean();
        p2.setUsername(Share.get().getUserName());
        p2.setVip(false);
        p2.setGoldBalance(Share.get().getNewUserWillGetGold());
        p2.setIntegralBalance("0");
        p2.save(new SaveListener<String>() {
            @Override
            public void done(String objectId, BmobException e) {
                if (e == null) {
                    Log.e(TAG, "给用户创建一个钱包 数据成功，返回objectId为：" + objectId);
                    Share.get().setUser_wallet_bean_object_id(objectId);
                    Share.get().setGoldBalance(Share.get().getNewUserWillGetGold());
                } else {
                    Log.e(TAG, "给用户创建一个钱包 创建数据失败：" + e.getMessage());
                }
            }
        });
    }

    /**
     * 用户充值金币
     * 更新用户金币余额
     *
     * @param goldBalance 将金币更新为多少
     */
    public static void updateRechargeGoldBalance(double goldBalance) {
//        double currentGoldBalance =  Double.parseDouble(Share.get().getGoldBalance());//用户当前金币余额
        UserWalletBean p2 = new UserWalletBean();
        p2.setGoldBalance(goldBalance + "");
        p2.update(Share.get().getUser_wallet_bean_object_id(), new UpdateListener() {
            @Override
            public void done(BmobException e) {
                if (e == null) {
                    Log.e(TAG, "更新用户金币余额成功:" + p2.getUpdatedAt());
                    //消耗了多少金币
                    Share.get().setGoldBalance(goldBalance + "");
                } else {
                    Log.e(TAG, "更新用户金币余额成失败：" + e.getMessage());
                }
            }
        });
    }

    /**
     * 音视频通话后，更新用户的金币余额
     *
     * @param goldBalance     将金币更新为多少
     * @param beCalledUserIDs (被叫的用户ID列表)为空说明是被叫，不为空说明当前设备是主叫
     */
    public static void updateUserGoldBalance(double goldBalance, String[] beCalledUserIDs) {
        //更新Person表里面id为6b6c11c537的数据，address内容更新为“北京朝阳”
        UserWalletBean p2 = new UserWalletBean();
        p2.setGoldBalance(goldBalance + "");
        p2.update(Share.get().getUser_wallet_bean_object_id(), new UpdateListener() {
            @Override
            public void done(BmobException e) {
                if (e == null) {
                    Log.e(TAG, "更新用户金币余额成功:" + p2.getUpdatedAt());
                    //消耗了多少金币
                    double consumeGoldNum = Double.parseDouble(Share.get().getGoldBalance()) - goldBalance;
                    Share.get().setGoldBalance(goldBalance + "");
                    if (beCalledUserIDs != null && beCalledUserIDs.length > 0) {
                        updateChatTargetUserIdIntegralBalance(beCalledUserIDs[0], consumeGoldNum);
                    }
                } else {
                    Log.e(TAG, "更新用户金币余额成失败：" + e.getMessage());
                }
            }
        });
    }

    /******************************************* 分佣 ***********************************************/
    /**
     * 用户提现，分佣给邀请人 每次分佣指定比例积分
     */
    public static void updateUserInviteBalance(double inviteWillGetJiFenNum) {
        if (TextUtils.isEmpty(Share.get().getMyInviteUserName())) {
            return;
        }
        BmobQuery<UserWalletBean> categoryBmobQuery = new BmobQuery<>();
        categoryBmobQuery.addWhereEqualTo("username", Share.get().getMyInviteUserName());
        categoryBmobQuery.findObjects(new FindListener<UserWalletBean>() {
            @Override
            public void done(List<UserWalletBean> result, BmobException e) {
                if (e == null) {
                    if (!result.isEmpty()) {
                        UserWalletBean userWalletBean = result.get(0);
                        //邀请人的积分余额
                        double integral = Double.parseDouble(userWalletBean.getIntegralBalance());
                        Log.e(TAG, "查询成功 当前用户的objectId>>>>>：" + userWalletBean.getObjectId());

                        UserWalletBean walletBean = new UserWalletBean();
                        walletBean.setIntegralBalance((Arith.add(integral, inviteWillGetJiFenNum)) + "");
                        walletBean.update(userWalletBean.getObjectId(), new UpdateListener() {
                            @Override
                            public void done(BmobException e) {
                                if (e == null) {
                                    Log.e(TAG, "done: 更新邀请人积分成功：" + walletBean.getUpdatedAt());
                                } else {
                                    Log.e(TAG, "done: 更新邀请人积分失败：" + e.getMessage());
                                }
                            }
                        });

                    } else {
                        Log.e(TAG, "done: 查找邀请自己的那个人的钱包数据 为空");
                    }
                } else {
                    Log.e(TAG, e.toString());
                }
            }
        });
    }

    /**
     * 消息收益
     * 分佣给聊天的对方 每次分佣4积分
     * 只有代理，才有在聊天界面，对方发消息过来，才会有积分收益
     *
     * @param chatTargetUserId 聊天的目标用户ID
     */
    public static void updateChatTargetUserIdIntegralBalance(String chatTargetUserId) {
        //当前用户是否代理
        if (Share.get().getIsAgent() || Share.get().getIsVip()) {
            Log.e(TAG, "updateChatTargetUserIdIntegralBalance: 因为当前用户是代理 或者是VIP，不给对方分佣 " + Share.get().getIsVip());
            return;
        }
        if (Share.get().getRechargeCount() <= 0) {
            Log.e(TAG, "updateChatTargetUserIdIntegralBalance: 因为当前用户从来没充值过，不给对方分佣");
            return;
        }
        BmobQuery<UserWalletBean> categoryBmobQuery = new BmobQuery<>();
        categoryBmobQuery.addWhereEqualTo("username", chatTargetUserId);
        categoryBmobQuery.findObjects(new FindListener<UserWalletBean>() {
            @Override
            public void done(List<UserWalletBean> result, BmobException e) {
                if (e == null) {
                    if (!result.isEmpty()) {
                        UserWalletBean userWalletBean = result.get(0);
                        //接收消息的人的积分余额
                        double integral = Double.parseDouble(userWalletBean.getIntegralBalance());
                        Log.e(TAG, "查询成功 当前用户的objectId>>>>>：" + userWalletBean.getObjectId());

                        UserWalletBean walletBean = new UserWalletBean();
                        double profitIntegralUnit = 0.5;
                        double messagePrice = 10;
                        if (!TextUtils.isEmpty(Share.get().getProfitIntegralUnit())) {
                            profitIntegralUnit = Double.parseDouble(Share.get().getProfitIntegralUnit());
                        }
                        if (!TextUtils.isEmpty(Share.get().getMessagePrice())) {
                            messagePrice = Double.parseDouble(Share.get().getMessagePrice());
                        }
                        walletBean.setIntegralBalance((Arith.add(integral, (Arith.mul(messagePrice, profitIntegralUnit)))) + "");
                        walletBean.update(userWalletBean.getObjectId(), new UpdateListener() {
                            @Override
                            public void done(BmobException e) {
                                if (e == null) {
                                    Log.e(TAG, "done: 更新邀请人积分成功：" + walletBean.getUpdatedAt());
                                } else {
                                    Log.e(TAG, "done: 更新邀请人积分失败：" + e.getMessage());
                                }
                            }
                        });

                    } else {
                        Log.e(TAG, "done: 查找邀请自己的那个人的钱包数据 为空");
                    }
                } else {
                    Log.e(TAG, e.toString());
                }
            }
        });
    }

    /**
     * 音视频通话收益
     * 分佣给聊天的对方 每次分佣4积分
     * 只有代理，才有在聊天界面，对方发消息过来，才会有积分收益
     *
     * @param chatTargetUserId 聊天的目标用户ID
     * @param consumeGoldNum   消耗的金币数量
     */
    public static void updateChatTargetUserIdIntegralBalance(String chatTargetUserId, double consumeGoldNum) {
        //当前用户是否代理
        if (Share.get().getIsAgent() || Share.get().getIsVip()) {
            Log.e(TAG, "updateChatTargetUserIdIntegralBalance: 因为当前用户是代理，或者是VIP，不给对方分佣 " + Share.get().getIsVip());
            return;
        }
        if (Share.get().getRechargeCount() <= 0) {
            Log.e(TAG, "updateChatTargetUserIdIntegralBalance: 因为当前用户从来没充值过，不给对方分佣");
            return;
        }
        BmobQuery<UserWalletBean> categoryBmobQuery = new BmobQuery<>();
        categoryBmobQuery.addWhereEqualTo("username", chatTargetUserId);
        categoryBmobQuery.findObjects(new FindListener<UserWalletBean>() {
            @Override
            public void done(List<UserWalletBean> result, BmobException e) {
                if (e == null) {
                    if (!result.isEmpty()) {
                        UserWalletBean userWalletBean = result.get(0);
                        //音视频通话 被叫人的积分余额
                        double integral = Double.parseDouble(userWalletBean.getIntegralBalance());
                        Log.e(TAG, "查询成功 当前用户的objectId>>>>>：" + userWalletBean.getObjectId());

                        UserWalletBean walletBean = new UserWalletBean();
                        double profitIntegralUnit = 0.5;
                        if (!TextUtils.isEmpty(Share.get().getProfitIntegralUnit())) {
                            profitIntegralUnit = Double.parseDouble(Share.get().getProfitIntegralUnit());
                        }

                        double resultIntegralBalance = (Arith.add(integral, Arith.mul(consumeGoldNum, profitIntegralUnit)));
                        walletBean.setIntegralBalance(resultIntegralBalance + "");
                        walletBean.update(userWalletBean.getObjectId(), new UpdateListener() {
                            @Override
                            public void done(BmobException e) {
                                if (e == null) {
                                    Log.e(TAG, "done: 更新音视频通话代理人积分成功：" + walletBean.getUpdatedAt() + "  更新了多少积分：" + resultIntegralBalance);
                                } else {
                                    Log.e(TAG, "done: 更新音视频通话代理人积分失败：" + e.getMessage());
                                }
                            }
                        });
                    } else {
                        Log.e(TAG, "done: 查找邀请自己的那个人的钱包数据 为空");
                    }
                } else {
                    Log.e(TAG, e.toString());
                }
            }
        });
    }

    /**
     * 微信被解锁人收益
     * 更新被解锁人的积分余额
     */
    public static void updateUnlockWechatTargetIdIntegralBalance(UserBasicInfoBean userBasicInfoBean) {
        //当前用户是否代理
//        if (!Share.get().getIsAgent()) {
//            return;
//        }
        if (Share.get().getIsAgent()) {
            Log.e(TAG, "updateUnlockWechatTargetIdIntegralBalance: 因为当前用户是代理，不给对方分佣");
            return;
        }
        if (Share.get().getRechargeCount() <= 0) {
            Log.e(TAG, "updateUnlockWechatTargetIdIntegralBalance: 因为当前用户从来没充值过，不给对方分佣");
            return;
        }

        BmobQuery<UserWalletBean> categoryBmobQuery = new BmobQuery<>();
        categoryBmobQuery.addWhereEqualTo("username", userBasicInfoBean.getUsername());
        categoryBmobQuery.findObjects(new FindListener<UserWalletBean>() {
            @Override
            public void done(List<UserWalletBean> result, BmobException e) {
                if (e == null) {
                    if (!result.isEmpty()) {
                        UserWalletBean userWalletBean = result.get(0);
                        //被解锁微信的人的积分余额
                        double integral = Double.parseDouble(userWalletBean.getIntegralBalance());
                        Log.e(TAG, "查询成功 当前用户的objectId>>>>>：" + userWalletBean.getObjectId());

                        UserWalletBean walletBean = new UserWalletBean();
                        double unlockWechatPrice = Double.parseDouble(Share.get().getUnLockWechatPrice());//解锁微信价格
                        double profitIntegralUnit = Double.parseDouble(Share.get().getProfitIntegralUnit());//消耗对方一个金币可分佣多少积分
                        walletBean.setIntegralBalance((Arith.add(integral, Arith.mul(unlockWechatPrice, profitIntegralUnit))) + "");
                        walletBean.update(userWalletBean.getObjectId(), new UpdateListener() {
                            @Override
                            public void done(BmobException e) {
                                if (e == null) {
                                    Log.e(TAG, "done: 更新微信解锁人积分成功：" + walletBean.getUpdatedAt());
                                } else {
                                    Log.e(TAG, "done: 更新微信解锁人积分失败：" + e.getMessage());
                                }
                            }
                        });
                    } else {
                        Log.e(TAG, "done: 查找微信解锁自己的那个人的数据 为空");
                    }
                } else {
                    Log.e(TAG, e.toString());
                }
            }
        });
    }
}
