package com.huolongluo.luo.huolongluo.ui.activity.bindtixian;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.gyf.immersionbar.ImmersionBar;
import com.huolongluo.luo.R;
import com.huolongluo.luo.huolongluo.base.BaseActivity;
import com.huolongluo.luo.huolongluo.bean.UserBean;
import com.huolongluo.luo.huolongluo.share.Event;
import com.huolongluo.luo.huolongluo.share.Share;
import com.huolongluo.luo.util.ToastSimple;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import cn.bmob.v3.BmobUser;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.UpdateListener;

/**
 * <p>
 * Created by 火龙裸 on 2017/9/6.
 */

public class BindTiXianActivity extends BaseActivity {
    private static final String TAG = "MyAccountActivity";
    @BindView(R.id.toolbar_center_title)
    TextView toolbar_center_title;
    @BindView(R.id.iv_left)
    ImageView iv_left;
    @BindView(R.id.tv_right)
    TextView tv_right;
    /********************* 以上为ToolBar ************************/
    @BindView(R.id.et_tixian_real_full_name)
    EditText et_tixian_real_full_name;
    @BindView(R.id.et_tixian_account)
    EditText et_tixian_account;
    @BindView(R.id.tv_sure)
    TextView tv_sure;

    @Override
    protected int getContentViewId() {
        return R.layout.activity_bind_tixian;
    }

    @Override
    protected void injectDagger() {
        activityComponent().inject(this);
    }

    private void initToolBar() {
        ImmersionBar.with(this).statusBarDarkFont(true).init();
        toolbar_center_title.setText("提现账户名");
//        tv_right.setVisibility(View.VISIBLE);
//        tv_right.setText("充值记录");
    }

    @Override
    protected void initViewsAndEvents(@Nullable Bundle savedInstanceState) {
        initToolBar();
        eventClick(iv_left).subscribe(o -> // 点击返回键
        {
            close();
        });

        if (!TextUtils.isEmpty(Share.get().getTiXianRealFullName())) {
            et_tixian_real_full_name.setText(Share.get().getTiXianRealFullName());
        }
        if (!TextUtils.isEmpty(Share.get().getTiXianAccount())) {
            et_tixian_account.setText(Share.get().getTiXianAccount());
        }

        eventClick(tv_sure).subscribe(o -> // 点击确定
        {
            String etTiXianRealFullName = et_tixian_real_full_name.getText().toString().trim();
            String etTiXianAccount = et_tixian_account.getText().toString().trim();
            if (TextUtils.isEmpty(etTiXianRealFullName) || TextUtils.isEmpty(etTiXianAccount)) {
                ToastSimple.show("户名和提现账号不能为空");
                return;
            }

            if (BmobUser.isLogin()) {
                UserBean user = BmobUser.getCurrentUser(UserBean.class);
                Log.e(TAG, "initViewsAndEvents: 登录状态，用户名为：" + user.getUsername());
                if (TextUtils.equals(Share.get().getUserName(), user.getUsername())) {
                    user.setTixianRealFullName(etTiXianRealFullName);
                    user.setTixianAccount(etTiXianAccount);
                    user.update(new UpdateListener() {
                        @Override
                        public void done(BmobException e) {
                            if (e == null) {
                                View view = LayoutInflater.from(BindTiXianActivity.this).inflate(R.layout.toast_text, null);
                                TextView tv_toast_message = view.findViewById(R.id.tv_toast_message);
                                tv_toast_message.setText("绑定成功");
                                ToastSimple.makeText(Gravity.CENTER, 2, view).show();

                                Share.get().setTiXianRealFullName(etTiXianRealFullName);
                                Share.get().setTiXianAccount(etTiXianAccount);

                                EventBus.getDefault().post(new Event.editTiXian(etTiXianRealFullName, etTiXianAccount));
                                close();

                            } else {
                                View view = LayoutInflater.from(BindTiXianActivity.this).inflate(R.layout.toast_text, null);
                                TextView tv_toast_message = view.findViewById(R.id.tv_toast_message);
                                tv_toast_message.setText(e.getMessage());
                                ToastSimple.makeText(Gravity.CENTER, 2, view).show();
                                Log.e(TAG, "绑定失败：" + e.getMessage());
                            }
                        }
                    });
                } else {
                    View view = LayoutInflater.from(BindTiXianActivity.this).inflate(R.layout.toast_text, null);
                    TextView tv_toast_message = view.findViewById(R.id.tv_toast_message);
                    tv_toast_message.setText("登录失效，请先重新登录");
                    ToastSimple.makeText(Gravity.CENTER, 2, view).show();
                }
//                Snackbar.make(view, "当前用户：" + user.getUsername() + "-" + user.getAge(), Snackbar.LENGTH_LONG).show();
//                String username = (String) BmobUser.getObjectByKey("username");
//                Integer age = (Integer) BmobUser.getObjectByKey("age");
//                Snackbar.make(view, "当前用户属性：" + username + "-" + age, Snackbar.LENGTH_LONG).show();
            } else {
                Log.e(TAG, "登录失效，请先重新登录");
                View view = LayoutInflater.from(BindTiXianActivity.this).inflate(R.layout.toast_text, null);
                TextView tv_toast_message = view.findViewById(R.id.tv_toast_message);
                tv_toast_message.setText("登录失效，请先重新登录");
                ToastSimple.makeText(Gravity.CENTER, 2, view).show();
            }
        });

    }
}
