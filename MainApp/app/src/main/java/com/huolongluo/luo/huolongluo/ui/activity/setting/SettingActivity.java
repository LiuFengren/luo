package com.huolongluo.luo.huolongluo.ui.activity.setting;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.AppUtils;
import com.gyf.immersionbar.ImmersionBar;
import com.huolongluo.luo.R;
import com.huolongluo.luo.huolongluo.base.BaseActivity;
import com.huolongluo.luo.huolongluo.bean.UserBean;
import com.huolongluo.luo.huolongluo.bean.VersionUpdateBean;
import com.huolongluo.luo.huolongluo.manager.DialogClickListener;
import com.huolongluo.luo.huolongluo.ui.activity.aboutus.AboutUsActivity;
import com.huolongluo.luo.huolongluo.ui.activity.accountset.AccountSetActivity;
import com.huolongluo.luo.huolongluo.ui.activity.login.LoginActivity;
import com.huolongluo.luo.manager.AppManager;
import com.huolongluo.luo.util.L;
import com.huolongluo.luo.util.ToastSimple;

import butterknife.BindView;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.QueryListener;

/**
 * <p>
 * Created by 火龙裸 on 2017/9/6.
 */

public class SettingActivity extends BaseActivity {
    @BindView(R.id.toolbar_center_title)
    TextView toolbar_center_title;
    @BindView(R.id.iv_left)
    ImageView iv_left;
    @BindView(R.id.tv_right)
    TextView tv_right;
    /********************* 以上为ToolBar ************************/
    @BindView(R.id.ll_set_account)
    LinearLayout ll_set_account;
    @BindView(R.id.ll_set_about_us)
    LinearLayout ll_set_about_us;
    @BindView(R.id.tv_version_name)
    TextView tv_version_name;
    @BindView(R.id.ll_set_up_version)
    LinearLayout ll_set_up_version;
    @BindView(R.id.btn_logout)
    Button btn_logout;

    @Override
    protected int getContentViewId() {
        return R.layout.activity_setting;
    }

    @Override
    protected void injectDagger() {
        activityComponent().inject(this);
    }

    private void initToolBar() {
        ImmersionBar.with(this).statusBarDarkFont(true).init();
        toolbar_center_title.setText("设置");
    }

    @Override
    protected void initViewsAndEvents(@Nullable Bundle savedInstanceState) {
        initToolBar();
        tv_version_name.setText("当前版本：" + AppUtils.getAppVersionName());
        eventClick(iv_left).subscribe(o -> // 点击返回键
        {
            close();
        });
        eventClick(ll_set_account).subscribe(o -> // 点击账号设置
        {
            startActivity(AccountSetActivity.class);
        });
        eventClick(ll_set_about_us).subscribe(o -> // 点击关于我们
        {
            startActivity(AboutUsActivity.class);
        });
        eventClick(ll_set_up_version).subscribe(o -> // 点击版本更新
        {
            queryLatestVersionCode();
        });


        eventClick(btn_logout).subscribe(o -> // 退出登录
        {
            showConfirmDialog("提示", "确认退出登录吗？", new DialogClickListener() {
                @Override
                public void onConfirm() {
                    UserBean.clearLocalSpUserBasicInfo();
                    AppManager.get().finishAllToActivity(SettingActivity.this, LoginActivity.class);
                }

                @Override
                public void onCancel() {
                }
            });
        });
    }

    /**
     * 检查最新的版本号
     */
    private void queryLatestVersionCode() {
        BmobQuery<VersionUpdateBean> bmobQuery = new BmobQuery<>();
        bmobQuery.getObject("Yctd666L", new QueryListener<VersionUpdateBean>() {
            @Override
            public void done(VersionUpdateBean versionUpdateBean, BmobException e) {
                if (e == null) {
                    L.e("查询到最新版本号为：" + versionUpdateBean.getLatestVersionCode());
                    if (AppUtils.getAppVersionCode() >= Integer.parseInt(versionUpdateBean.getLatestVersionCode())) {
                        ToastSimple.show("当前已是最新版本");
                    } else {
                        if (versionUpdateBean.isForcedUpdate()) {
                            showConfirmNoCancelDialog("版本更新", "检测到新版本，是否立即更新", new DialogClickListener() {
                                @Override
                                public void onConfirm() {
                                    Intent intent = new Intent();
                                    intent.setAction("android.intent.action.VIEW");
                                    Uri content_uri = Uri.parse(versionUpdateBean.getDownLoadUrl());
                                    intent.setData(content_uri);
                                    startActivity(intent);
                                }

                                @Override
                                public void onCancel() {
                                }
                            });
                        } else {
                            showConfirmDialog("版本更新", "检测到新版本，是否立即更新", new DialogClickListener() {
                                @Override
                                public void onConfirm() {
                                    Intent intent = new Intent();
                                    intent.setAction("android.intent.action.VIEW");
                                    Uri content_uri = Uri.parse(versionUpdateBean.getDownLoadUrl());
                                    intent.setData(content_uri);
                                    startActivity(intent);
                                }

                                @Override
                                public void onCancel() {
                                }
                            });
                        }
                    }
                } else {
                    ToastSimple.show(e.toString(), 2);
                    L.e(e.toString());
                }
            }
        });
    }
}
