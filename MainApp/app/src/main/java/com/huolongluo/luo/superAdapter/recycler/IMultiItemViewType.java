package com.huolongluo.luo.superAdapter.recycler;

/**
 * Interface for multiple types.
 * Created by 火龙裸 on 2017/8/21.
 */
public interface IMultiItemViewType<T>
{
    /**
     * ItemView type
     *
     * @param position current position of ViewHolder
     * @param t        model item
     * @return viewType
     */
    int getItemViewType(int position, T t);

    /**
     * Layout Res
     *
     * @param viewType {@link #getItemViewType(int, Object)}
     * @return {@link android.support.annotation.LayoutRes}
     */
    int getLayoutId(int viewType);
}