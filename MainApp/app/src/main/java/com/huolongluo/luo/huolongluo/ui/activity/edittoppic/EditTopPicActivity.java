package com.huolongluo.luo.huolongluo.ui.activity.edittoppic;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;

import com.gyf.immersionbar.ImmersionBar;
import com.huolongluo.luo.R;
import com.huolongluo.luo.huolongluo.base.BaseActivity;
import com.huolongluo.luo.huolongluo.bean.UserBasicInfoBean;
import com.huolongluo.luo.huolongluo.constants.Constants;
import com.huolongluo.luo.huolongluo.manager.DialogClickListener;
import com.huolongluo.luo.huolongluo.share.Event;
import com.huolongluo.luo.huolongluo.ui.adapter.EditDataCardGridImageAdapter;
import com.huolongluo.luo.util.ToastSimple;
import com.huolongluo.luo.util.engine.MyGlideEngine;
import com.luck.picture.lib.animators.AnimationType;
import com.luck.picture.lib.basic.PictureSelectionModel;
import com.luck.picture.lib.basic.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.config.PictureSelectionConfig;
import com.luck.picture.lib.config.SelectLimitType;
import com.luck.picture.lib.config.SelectMimeType;
import com.luck.picture.lib.config.SelectModeConfig;
import com.luck.picture.lib.decoration.GridSpacingItemDecoration;
import com.luck.picture.lib.engine.ImageEngine;
import com.luck.picture.lib.engine.UriToFileTransformEngine;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.entity.MediaExtraInfo;
import com.luck.picture.lib.interfaces.OnExternalPreviewEventListener;
import com.luck.picture.lib.interfaces.OnKeyValueResultCallbackListener;
import com.luck.picture.lib.interfaces.OnResultCallbackListener;
import com.luck.picture.lib.interfaces.OnSelectLimitTipsListener;
import com.luck.picture.lib.language.LanguageConfig;
import com.luck.picture.lib.style.PictureSelectorStyle;
import com.luck.picture.lib.utils.DensityUtil;
import com.luck.picture.lib.utils.MediaUtils;
import com.luck.picture.lib.utils.SandboxTransformUtils;
import com.luck.picture.lib.utils.ToastUtils;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import cn.bmob.v3.datatype.BmobFile;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.UpdateListener;
import cn.bmob.v3.listener.UploadBatchListener;

/**
 * <p>
 * Created by 火龙裸 on 2017/9/6.
 */

public class EditTopPicActivity extends BaseActivity {
    private static final String TAG = "EditTopPicActivity";
    @BindView(R.id.toolbar_center_title)
    TextView toolbar_center_title;
    @BindView(R.id.iv_left)
    ImageView iv_left;
    /********************* 以上为ToolBar ************************/
    @BindView(R.id.rv_top_pic)
    RecyclerView rv_top_pic;
    @BindView(R.id.tv_to_save)
    TextView tv_to_save;

    private EditDataCardGridImageAdapter gridImageAdapter;

    private int maxSelectVideoNum = 1; // 1视频
    private int maxSelectNum = 6;
    private int animationMode = AnimationType.DEFAULT_ANIMATION;
    private ImageEngine imageEngine;
    private PictureSelectorStyle selectorStyle;
    private int chooseMode = SelectMimeType.ofImage();
    private int language = LanguageConfig.UNKNOWN_LANGUAGE;
    private List<LocalMedia> selectList = new ArrayList<>();
    private int resultMode = 0;//LAUNCHER_RESULT = 0、ACTIVITY_RESULT = 1、CALLBACK_RESULT = 2
    private ActivityResultLauncher<Intent> launcherResult;

    private UserBasicInfoBean userBasicInfoBean;//用户资料
    //    private AboutMeBean aboutMeBean;//用户“关于我”资料
    private String objectId;//Bmob中的用户详情数据对应的id
    //    private int editDataCardType;//1表示自我描述，2表示家庭背景，3表示兴趣爱好，4爱情观，5理想的另一半，6我为什么单身，7如果遇到对的人，我期待什么样的生活
//    private String editDataCardText;//资料卡中的内容
    private String editDataCardTextHint;
//    private List<String> editDataCardPic = new ArrayList<>();//编辑卡中的精美图片

    @Override
    protected int getContentViewId() {
        return R.layout.activity_top_pic;
    }

    @Override
    protected void injectDagger() {
        activityComponent().inject(this);
    }

    private void initToolBar() {
        ImmersionBar.with(this).statusBarDarkFont(true).init();
        toolbar_center_title.setText("编辑精美图片");
    }

    @Override
    protected void initViewsAndEvents(@Nullable Bundle savedInstanceState) {
        initToolBar();
        if (getBundle() != null) {
//            objectId = getBundle().getString(Constants.OBJECT_ID);
            userBasicInfoBean = (UserBasicInfoBean) getBundle().getSerializable(Constants.USER_BASE_INFO);
//            aboutMeBean = (AboutMeBean) getBundle().getSerializable(Constants.USER_ABOUT_ME);
//            editDataCardType = getBundle().getInt(Constants.EDIT_DATA_CARD_TYPE);
//            editDataCardText = getBundle().getString(Constants.EDIT_DATA_CARD_TEXT);
//            editDataCardPic = getBundle().getStringArrayList(Constants.EDIT_DATA_CARD_PIC);
            Log.e(TAG, "initViewsAndEvents: 编辑用户卡片信息：" + userBasicInfoBean.getUsername());
        }

        eventClick(iv_left).subscribe(o -> // 点击返回键
        {
            close();
        });
        eventClick(tv_to_save).subscribe(o -> // 点击保存
        {
            if (selectList.isEmpty()) {
                showConfirmNoCancelDialog("提示", "需要保存的图片为空，请选择你的精美图片后再保存哦", new DialogClickListener() {
                    @Override
                    public void onConfirm() {
                    }

                    @Override
                    public void onCancel() {
                    }
                });
            } else {
                saveDataCardPic();
            }
        });

        // 注册需要写在onCreate或Fragment onAttach里，否则会报java.lang.IllegalStateException异常
        launcherResult = createActivityResultLauncher();

        rv_top_pic.setNestedScrollingEnabled(false);
        rv_top_pic.setFocusable(false);
        GridLayoutManager manager = new GridLayoutManager(EditTopPicActivity.this, 4, GridLayoutManager.VERTICAL, false);
        rv_top_pic.setLayoutManager(manager);
        RecyclerView.ItemAnimator itemAnimator = rv_top_pic.getItemAnimator();
        if (itemAnimator != null) {
            ((SimpleItemAnimator) itemAnimator).setSupportsChangeAnimations(false);
        }
        rv_top_pic.addItemDecoration(new GridSpacingItemDecoration(4,
                DensityUtil.dip2px(this, 8), false));
        gridImageAdapter = new EditDataCardGridImageAdapter(EditTopPicActivity.this, selectList);
        gridImageAdapter.setSelectMax(maxSelectNum);
        rv_top_pic.setAdapter(gridImageAdapter);
        if (savedInstanceState != null && savedInstanceState.getParcelableArrayList("selectorList") != null) {
            selectList.clear();
            selectList.addAll(savedInstanceState.getParcelableArrayList("selectorList"));
        }

        imageEngine = MyGlideEngine.createGlideEngine();

        gridImageAdapter.setOnItemClickListener(new EditDataCardGridImageAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                // 预览图片、视频、音频
                PictureSelector.create(EditTopPicActivity.this)
                        .openPreview()
                        .setImageEngine(imageEngine)
                        .setSelectorUIStyle(selectorStyle)
                        .setLanguage(language)
                        .isPreviewFullScreenMode(true)
                        .setExternalPreviewEventListener(new EditTopPicActivity.MyExternalPreviewEventListener(gridImageAdapter))
                        .startActivityPreview(position, true, gridImageAdapter.getData());
            }

            @Override
            public void openPicture() {
                // 进入相册0
                PictureSelectionModel selectionModel = PictureSelector.create(EditTopPicActivity.this)
                        .openGallery(chooseMode)
                        .setSelectorUIStyle(selectorStyle)
                        .setImageEngine(imageEngine)
//                                .setCropEngine(getCropFileEngine())
//                                .setCompressEngine(getCompressFileEngine())
                        .setSandboxFileEngine(new EditTopPicActivity.MeSandboxFileEngine())
                        .setCameraInterceptListener(null)
                        .setRecordAudioInterceptListener(null)
                        .setSelectLimitTipsListener(new EditTopPicActivity.MeOnSelectLimitTipsListener())
                        .setEditMediaInterceptListener(null)
                        .setPermissionDescriptionListener(null)
                        .setPreviewInterceptListener(null)
                        .setPermissionDeniedListener(null)
                        .setAddBitmapWatermarkListener(null)
                        .setVideoThumbnailListener(null)
//                              .setQueryFilterListener(new OnQueryFilterListener() {
//                                    @Override
//                                    public boolean onFilter(String absolutePath) {
//                                        return PictureMimeType.isUrlHasVideo(absolutePath);
//                                    }
//                                })
                        //.setExtendLoaderEngine(getExtendLoaderEngine())
                        .setInjectLayoutResourceListener(null)
                        .setSelectionMode(SelectModeConfig.MULTIPLE)
                        .setLanguage(language)
                        .setQuerySortOrder("")
                        .setOutputCameraDir("")
                        .setOutputAudioDir("")
                        .setQuerySandboxDir("")
                        .isDisplayTimeAxis(true)
                        .isOnlyObtainSandboxDir(false)
                        .isPageStrategy(true)
                        .isOriginalControl(false)
                        .isDisplayCamera(true)
                        .isOpenClickSound(false)
                        .setSkipCropMimeType("")
                        .isFastSlidingSelect(true)
                        //.setOutputCameraImageFileName("luck.jpeg")
                        //.setOutputCameraVideoFileName("luck.mp4")
                        .isWithSelectVideoImage(true)
                        .isPreviewFullScreenMode(true)
                        .isPreviewZoomEffect(true)
                        .isPreviewImage(true)
                        .isPreviewVideo(true)
                        .isPreviewAudio(true)
                        //.setQueryOnlyMimeType(PictureMimeType.ofGIF())
                        .isMaxSelectEnabledMask(true)
                        .isDirectReturnSingle(false)
                        .setMaxSelectNum(maxSelectNum)
                        .setMaxVideoSelectNum(maxSelectVideoNum)
                        .setRecyclerAnimationMode(animationMode)
                        .isGif(false)
                        .setSelectedData(gridImageAdapter.getData());
                forSelectResult(selectionModel);
            }
        });
    }

    public void saveDataCardPic() {
        showProgressDialog("");
        final String[] filePaths = new String[selectList.size()];
        for (int i = 0; i < selectList.size(); i++) {
//            filePaths[i] = selectList.get(i).getPath();
            filePaths[i] = selectList.get(i).getRealPath();
        }

        //批量上传文件
        BmobFile.uploadBatch(filePaths, new UploadBatchListener() {
            @Override
            public void onSuccess(List<BmobFile> files, List<String> urls) {
                //1、files-上传完成后的BmobFile集合，是为了方便大家对其上传后的数据进行操作，例如你可以将该文件保存到表中
                //2、urls-上传文件的完整url地址
                if (urls.size() == filePaths.length) {//如果数量相等，则代表文件全部上传完成
                    //do something
                    Log.e(TAG, "onSuccess: 图片上传完成：" + urls.size() + " || " + urls.toString());
                    updateUserTopPic(urls);
                }
            }

            @Override
            public void onError(int statuscode, String errormsg) {
                ToastSimple.show("错误码" + statuscode + ",错误描述：" + errormsg);
                Log.e(TAG, "错误码" + statuscode + ",错误描述：" + errormsg);
                hideProgressDialog();
            }

            @Override
            public void onProgress(int curIndex, int curPercent, int total, int totalPercent) {
                //1、curIndex--表示当前第几个文件正在上传
                //2、curPercent--表示当前上传文件的进度值（百分比）
                //3、total--表示总的上传文件数
                //4、totalPercent--表示总的上传进度（百分比）
            }
        });
    }

    private void updateUserTopPic(List<String> picUrl) {
        if (!picUrl.isEmpty() && userBasicInfoBean != null) {
            userBasicInfoBean.setUserTopPic(picUrl);
            userBasicInfoBean.update(userBasicInfoBean.getObjectId(), new UpdateListener() {
                @Override
                public void done(BmobException e) {
                    if (e == null) {
                        EventBus.getDefault().post(new Event.updataUserTopPic(picUrl));
                        ToastSimple.show("更新成功");
                        close();
                    } else {
                        ToastSimple.show("更新失败：" + e.getMessage());
                    }
                    hideProgressDialog();
                }
            });
        }


        //更新Person表里面id为6b6c11c537的数据，address内容更新为“北京朝阳”
//        UserBasicInfoBean userBasicInfoBean = new UserBasicInfoBean();
//        AboutMeBean aboutMeBean = new AboutMeBean();
//        UserBasicInfoBean.UserDetailInfoBean userDetailInfoBean = new UserBasicInfoBean.UserDetailInfoBean();
//        UserBasicInfoBean.UserDetailInfoBean.UserPicBean userPicBean = new UserBasicInfoBean.UserDetailInfoBean.UserPicBean();
//        UserDetailInfoBean userDetailInfoBean = new UserDetailInfoBean();
        /*Log.e(TAG, "updateEditCardText: 类型：" + editDataCardType + "  内容：" + editDataCardText);
        aboutMeBean.setUsername(userBasicInfoBean.getUsername());
        switch (editDataCardType) {
            case 1:
                aboutMeBean.setSelfDescription(editDataCardText);
                if (!picUrl.isEmpty()) {
                    aboutMeBean.setSelfDescriptionPic(picUrl);
                }
                break;
            case 2:
                aboutMeBean.setFamilyBackground(editDataCardText);
                if (!picUrl.isEmpty()) {
                    aboutMeBean.setFamilyBackgroundPic(picUrl);
                }
                break;
            case 3:
                aboutMeBean.setHobby(editDataCardText);
                if (!picUrl.isEmpty()) {
                    aboutMeBean.setHobbyPic(picUrl);
                }
                break;
            case 4:
                aboutMeBean.setLoveIdea(editDataCardText);
                if (!picUrl.isEmpty()) {
                    aboutMeBean.setLoveIdeaPic(picUrl);
                }
                break;
            case 5:
                aboutMeBean.setIdealPartner(editDataCardText);
                if (!picUrl.isEmpty()) {
                    aboutMeBean.setIdealPartnerPic(picUrl);
                }
                break;
            case 6:
                aboutMeBean.setWhySingle(editDataCardText);
                if (!picUrl.isEmpty()) {
                    aboutMeBean.setWhySinglePic(picUrl);
                }
                break;
            case 7:
                aboutMeBean.setQiDaiShengHuo(editDataCardText);
                if (!picUrl.isEmpty()) {
                    aboutMeBean.setQiDaiShengHuoPic(picUrl);
                }
                break;
        }

//        Log.e(TAG, "updateEditCardText: 更新ID为:" + objectId);
//        aboutMeBean.setObjectId(userBasicInfoBean.getObjectId());

//        aboutMeBean.save(new SaveListener<String>() {
//            @Override
//            public void done(String objectId,BmobException e) {
//                if(e==null){
//                    EventBus.getDefault().post(new Event.updataUserInfoDetail(editDataCardType, editDataCardText, picUrl));
//                    ToastSimple.show("更新成功");
//                    close();
//                }else{
//                    ToastSimple.show("更新失败：" + e.getMessage());
//                }
//            }
//        });

        aboutMeBean.update(aboutMeBean.getObjectId(), new UpdateListener() {
            @Override
            public void done(BmobException e) {
                if(e==null){
                    EventBus.getDefault().post(new Event.updataUserInfoDetail(editDataCardType, editDataCardText, picUrl));
                    ToastSimple.show("更新成功");
                    close();
                }else{
                    ToastSimple.show("更新失败：" + e.getMessage());
                }
            }
        });*/
        /*************/
//        User user = new User();
//        user.setObjectId("此处填写你需要关联的用户");
//        Post post = new Post();
//        post.setObjectId("此处填写需要修改的帖子");
//        //修改一对一关联，修改帖子和用户的关系
//        post.setAuthor(user);
    }

    private void forSelectResult(PictureSelectionModel model) {
        switch (resultMode) {
            case 1:
                model.forResult(PictureConfig.CHOOSE_REQUEST);
                break;
            case 2:
                model.forResult(new EditTopPicActivity.MeOnResultCallbackListener());
                break;
            default:
                model.forResult(launcherResult);
                break;
        }
    }

    /**
     * 创建一个ActivityResultLauncher
     *
     * @return
     */
    private ActivityResultLauncher<Intent> createActivityResultLauncher() {
        return registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult result) {
                        int resultCode = result.getResultCode();
                        if (resultCode == RESULT_OK) {
                            ArrayList<LocalMedia> selectList = PictureSelector.obtainSelectorList(result.getData());
                            analyticalSelectResults(selectList);
                        } else if (resultCode == RESULT_CANCELED) {
                            Log.i(TAG, "onActivityResult PictureSelector Cancel");
                        }
                    }
                });
    }

    /**
     * 选择结果
     */
    private class MeOnResultCallbackListener implements OnResultCallbackListener<LocalMedia> {
        @Override
        public void onResult(ArrayList<LocalMedia> result) {
            analyticalSelectResults(result);
        }

        @Override
        public void onCancel() {
            Log.i(TAG, "PictureSelector Cancel");
        }
    }

    /**
     * 处理选择结果
     *
     * @param result
     */
    private void analyticalSelectResults(ArrayList<LocalMedia> result) {
        selectList.clear();
        selectList.addAll(result);
        for (LocalMedia media : result) {
            if (media.getWidth() == 0 || media.getHeight() == 0) {
                if (PictureMimeType.isHasImage(media.getMimeType())) {
                    MediaExtraInfo imageExtraInfo = MediaUtils.getImageSize(this, media.getPath());
                    media.setWidth(imageExtraInfo.getWidth());
                    media.setHeight(imageExtraInfo.getHeight());
                } else if (PictureMimeType.isHasVideo(media.getMimeType())) {
                    MediaExtraInfo videoExtraInfo = MediaUtils.getVideoSize(this, media.getPath());
                    media.setWidth(videoExtraInfo.getWidth());
                    media.setHeight(videoExtraInfo.getHeight());
                }
            }
            Log.i(TAG, "文件名: " + media.getFileName());
            Log.i(TAG, "是否压缩:" + media.isCompressed());
            Log.i(TAG, "压缩:" + media.getCompressPath());
            Log.i(TAG, "初始路径:" + media.getPath());
            Log.i(TAG, "绝对路径:" + media.getRealPath());
            Log.i(TAG, "是否裁剪:" + media.isCut());
            Log.i(TAG, "裁剪:" + media.getCutPath());
            Log.i(TAG, "是否开启原图:" + media.isOriginal());
            Log.i(TAG, "原图路径:" + media.getOriginalPath());
            Log.i(TAG, "沙盒路径:" + media.getSandboxPath());
            Log.i(TAG, "水印路径:" + media.getWatermarkPath());
            Log.i(TAG, "视频缩略图:" + media.getVideoThumbnailPath());
            Log.i(TAG, "原始宽高: " + media.getWidth() + "x" + media.getHeight());
            Log.i(TAG, "裁剪宽高: " + media.getCropImageWidth() + "x" + media.getCropImageHeight());
            Log.i(TAG, "文件大小: " + media.getSize());
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                boolean isMaxSize = result.size() == gridImageAdapter.getSelectMax();
                int oldSize = gridImageAdapter.getData().size();
                gridImageAdapter.notifyItemRangeRemoved(0, isMaxSize ? oldSize + 1 : oldSize);
                gridImageAdapter.getData().clear();

                gridImageAdapter.getData().addAll(result);
                gridImageAdapter.notifyItemRangeInserted(0, result.size());
            }
        });
    }

    /**
     * 创建自定义输出目录
     *
     * @return
     */
    private String getSandboxPath() {
        File externalFilesDir = this.getExternalFilesDir("");
        File customFile = new File(externalFilesDir.getAbsolutePath(), "Sandbox");
        if (!customFile.exists()) {
            customFile.mkdirs();
        }
        return customFile.getAbsolutePath() + File.separator;
    }

    /**
     * 创建自定义输出目录
     *
     * @return
     */
    private String getSandboxMarkDir() {
        File externalFilesDir = this.getExternalFilesDir("");
        File customFile = new File(externalFilesDir.getAbsolutePath(), "Mark");
        if (!customFile.exists()) {
            customFile.mkdirs();
        }
        return customFile.getAbsolutePath() + File.separator;
    }

    /**
     * 创建自定义输出目录
     *
     * @return
     */
    private String getVideoThumbnailDir() {
        File externalFilesDir = this.getExternalFilesDir("");
        File customFile = new File(externalFilesDir.getAbsolutePath(), "Thumbnail");
        if (!customFile.exists()) {
            customFile.mkdirs();
        }
        return customFile.getAbsolutePath() + File.separator;
    }

    /**
     * 外部预览监听事件
     */
    private static class MyExternalPreviewEventListener implements OnExternalPreviewEventListener {
        private final EditDataCardGridImageAdapter adapter;

        public MyExternalPreviewEventListener(EditDataCardGridImageAdapter adapter) {
            this.adapter = adapter;
        }

        @Override
        public void onPreviewDelete(int position) {
            adapter.remove(position);
            adapter.notifyItemRemoved(position);
        }

        @Override
        public boolean onLongPressDownload(LocalMedia media) {
            return false;
        }
    }

    /**
     * 自定义沙盒文件处理
     */
    private static class MeSandboxFileEngine implements UriToFileTransformEngine {

        @Override
        public void onUriToFileAsyncTransform(Context context, String srcPath, String mineType, OnKeyValueResultCallbackListener call) {
            if (call != null) {
                call.onCallback(srcPath, SandboxTransformUtils.copyPathToSandbox(context, srcPath, mineType));
            }
        }
    }

    /**
     * 拦截自定义提示
     */
    private static class MeOnSelectLimitTipsListener implements OnSelectLimitTipsListener {

        @Override
        public boolean onSelectLimitTips(Context context, PictureSelectionConfig config, int limitType) {
            if (limitType == SelectLimitType.SELECT_NOT_SUPPORT_SELECT_LIMIT) {
                ToastUtils.showToast(context, "暂不支持的选择类型");
                return true;
            }
            return false;
        }
    }

    /**
     * @describe：拖拽监听事件
     */
    public interface DragListener {
        /**
         * 是否将 item拖动到删除处，根据状态改变颜色
         *
         * @param isDelete
         */
        void deleteState(boolean isDelete);

        /**
         * 是否于拖拽状态
         *
         * @param isStart
         */
        void dragState(boolean isStart);
    }
}
