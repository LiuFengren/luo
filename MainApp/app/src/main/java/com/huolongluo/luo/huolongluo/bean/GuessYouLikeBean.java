package com.huolongluo.luo.huolongluo.bean;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GuessYouLikeBean implements Serializable {
    /**
     * users : [{"uid":13374913,"nick":"丰满❤️好女人","portrait":"https://img.hnyuntong.cn/MTY0NzUwMzU4MzM2MiMzNTgjanBn.jpg","gender":0,"birth":"1979-05-10","age":0,"description":"单身女人。","location":"","is_online":1,"authentication":0,"member_status":0,"photo_info":null,"hall_text":"","more_num":0,"token":"rec_18_1^14980156_1649256824340_0^0_1^11"},{"uid":7173409,"nick":"蕉个朋友","portrait":"https://img.hnyuntong.cn/MTY0ODI5ODE3OTQ4NyM3NzUjanBn.jpg","gender":0,"birth":"1983-08-24","age":0,"description":"真心的来，非诚勿扰 ","location":"","is_online":1,"authentication":0,"member_status":0,"photo_info":null,"hall_text":"","more_num":0,"token":"rec_18_1^14980156_1649256824340_1^0_1^11"},{"uid":8431139,"nick":"微胖❤️❤️","portrait":"https://img.hnyuntong.cn/MTY0ODY4ODkyMDQzNSM4NjAjanBn.jpg","gender":0,"birth":"1979-05-11","age":0,"description":"微胖才是极品","location":"","is_online":1,"authentication":0,"member_status":0,"photo_info":null,"hall_text":"","more_num":0,"token":"rec_18_1^14980156_1649256824340_2^0_1^11"},{"uid":13579431,"nick":"丰满单身女人","portrait":"https://img.hnyuntong.cn/MTY0Nzc3ODk5ODExMSMgOTUjanBn.jpg","gender":0,"birth":"1979-05-01","age":0,"description":"孤独也是苦\n单身一人","location":"","is_online":1,"authentication":0,"member_status":0,"photo_info":null,"hall_text":"","more_num":0,"token":"rec_18_1^14980156_1649256824340_3^0_1^11"},{"uid":8655182,"nick":"等哥来爱我","portrait":"https://img.hnyuntong.cn/MTYzNjU5NTU5NzAwMyM1NDMjanBn.jpg","gender":0,"birth":"1969-03-06","age":0,"description":"","location":"","is_online":1,"authentication":1,"member_status":0,"photo_info":null,"hall_text":"","more_num":0,"token":"rec_18_1^14980156_1649256824340_4^0_1^11"},{"uid":7690412,"nick":"单身女人","portrait":"https://img.hnyuntong.cn/MTYzMjAyNjkxOTg0NSM3NDAjanBn.jpg","gender":0,"birth":"1969-11-22","age":0,"description":"好人一生平安","location":"","is_online":1,"authentication":1,"member_status":0,"photo_info":null,"hall_text":"","more_num":0,"token":"rec_18_1^14980156_1649256824340_5^0_1^11"},{"uid":14644786,"nick":"独宠","portrait":"https://img.hnyuntong.cn/MTY0OTIzMDgxOTg2NCMxOTAjanBn.jpg","gender":0,"birth":"1981-01-01","age":0,"description":"男人贵在独宠一女 女人贵在安分守己","location":"","is_online":1,"authentication":0,"member_status":0,"photo_info":null,"hall_text":"","more_num":0,"token":"rec_18_1^14980156_1649256824340_6^0_1^11"},{"uid":2831142,"nick":"倾城似雪","portrait":"https://img.yuntonghn.cn/MTYxNzc2NzU3MTA1OCMgNzMjanBn.jpg","gender":0,"birth":"1967-09-21","age":0,"description":"","location":"","is_online":1,"authentication":0,"member_status":0,"photo_info":null,"hall_text":"","more_num":0,"token":"rec_18_1^14980156_1649256824340_7^0_1^11"},{"uid":9858484,"nick":"晓雪","portrait":"https://img.hnyuntong.cn/MTYzNjU0MzIwNzI1OCMxMzAjanBn.jpg","gender":0,"birth":"1965-10-08","age":0,"description":"执子之手，与子偕老","location":"","is_online":1,"authentication":1,"member_status":0,"photo_info":null,"hall_text":"","more_num":0,"token":"rec_18_1^14980156_1649256824340_8^0_1^11"},{"uid":8501194,"nick":"霸道温柔","portrait":"https://img.yuntonghn.cn/MTYzNzczMDkzNzQ5MyMzMjAjanBn.jpg","gender":0,"birth":"1976-10-10","age":0,"description":"","location":"","is_online":1,"authentication":0,"member_status":0,"photo_info":null,"hall_text":"","more_num":0,"token":"rec_18_1^14980156_1649256824340_9^0_1^11"},{"uid":14733732,"nick":"老太太","portrait":"https://img.hnyuntong.cn/MTY0ODEwNDgxNzE1NyM4NDIjanBn.jpg","gender":0,"birth":"1979-12-20","age":0,"description":"找个值得依靠的人","location":"","is_online":1,"authentication":0,"member_status":0,"photo_info":null,"hall_text":"","more_num":0,"token":"rec_18_1^14980156_1649256824340_10^0_1^11"},{"uid":9192606,"nick":"肉肉爱吃肉 ～小娘子","portrait":"https://img.hnyuntong.cn/MTYzNTY2MTYzNjM3NiM1MzIjanBn.jpg","gender":0,"birth":"1973-01-01","age":0,"description":"我就是我不一样的烟火。","location":"","is_online":1,"authentication":1,"member_status":0,"photo_info":null,"hall_text":"","more_num":0,"token":"rec_18_1^14980156_1649256824340_11^0_1^11"},{"uid":14593901,"nick":"静","portrait":"https://img.dsqun.tech/MTY0ODIyMjMwNTM2NSM4OTkjanBn.jpg","gender":0,"birth":"1973-10-26","age":0,"description":"宠我的那个人，是你吗？","location":"","is_online":1,"authentication":1,"member_status":0,"photo_info":null,"hall_text":"","more_num":0,"token":"rec_18_1^14980156_1649256824340_12^0_1^11"},{"uid":4007253,"nick":"想要个温暖的家","portrait":"https://img.hnyuntong.cn/MTYyMjA0ODY5Mzc2MCM4ODIjanBn.jpg","gender":0,"birth":"1979-08-11","age":0,"description":"被伤过的心，想要个人疼","location":"","is_online":1,"authentication":0,"member_status":0,"photo_info":null,"hall_text":"","more_num":0,"token":"rec_18_1^14980156_1649256824340_13^0_1^11"},{"uid":13729824,"nick":"热忱的尼采","portrait":"https://img.hnyuntong.cn/MTY0NDIwNDQyNTI2NiMzNjMjanBn.jpg","gender":0,"birth":"1991-01-01","age":0,"description":"","location":"","is_online":1,"authentication":1,"member_status":0,"photo_info":null,"hall_text":"","more_num":0,"token":"rec_18_1^14980156_1649256824340_14^0_1^11"},{"uid":9734108,"nick":"༺༼࿅࿆࿄࿆静静࿄࿆࿅࿆༽༻","portrait":"https://img.hnyuntong.cn/MTY0NTk3NTM5MzcwNCM1MTkjanBn.jpg","gender":0,"birth":"1980-11-08","age":0,"description":"找一个志趣相同的过余生，非诚勿扰","location":"","is_online":1,"authentication":0,"member_status":0,"photo_info":null,"hall_text":"","more_num":0,"token":"rec_18_1^14980156_1649256824340_15^0_1^11"},{"uid":8715736,"nick":"开心宝贝","portrait":"https://img.hnyuntong.cn/MTYzNjYyMDYzNDY3NyM2MTEjanBn.jpg","gender":0,"birth":"1978-09-18","age":0,"description":"单身久了好想找个人来陪","location":"","is_online":1,"authentication":0,"member_status":0,"photo_info":null,"hall_text":"","more_num":0,"token":"rec_18_1^14980156_1649256824340_16^0_1^11"},{"uid":8340596,"nick":"单亲妈妈，有个女儿","portrait":"https://img.hnyuntong.cn/MTYzMzQwOTQ4ODYxOCM1OTkjanBn.jpg","gender":0,"birth":"1981-09-05","age":0,"description":"","location":"","is_online":1,"authentication":1,"member_status":0,"photo_info":null,"hall_text":"","more_num":0,"token":"rec_18_1^14980156_1649256824340_17^0_1^11"},{"uid":3784859,"nick":"淡然","portrait":"https://img.hnyuntong.cn/MTYyMDMwOTQ4ODE5NyM4OTUjanBn.jpg","gender":0,"birth":"1976-03-06","age":0,"description":"真心找另一半，另有目的的请走开","location":"","is_online":1,"authentication":1,"member_status":0,"photo_info":null,"hall_text":"","more_num":0,"token":"rec_18_1^14980156_1649256824340_18^0_1^11"},{"uid":4824809,"nick":"孤单的女人","portrait":"https://img.hnyuntong.cn/MTYzMTE5NjUxNzY0MiM1MTYjanBn.jpg","gender":0,"birth":"1977-06-02","age":0,"description":"善良的我，用真诚的心交友","location":"","is_online":1,"authentication":1,"member_status":0,"photo_info":null,"hall_text":"","more_num":0,"token":"rec_18_1^14980156_1649256824340_19^0_1^11"}]
     * ts : 1649256824
     */

    private Integer ts;
    private List<UsersBean> users;

    public Integer getTs() {
        return ts;
    }

    public void setTs(Integer ts) {
        this.ts = ts;
    }

    public List<UsersBean> getUsers() {
        return users;
    }

    public void setUsers(List<UsersBean> users) {
        this.users = users;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class UsersBean {
        /**
         * uid : 13374913
         * nick : 丰满❤️好女人
         * portrait : https://img.hnyuntong.cn/MTY0NzUwMzU4MzM2MiMzNTgjanBn.jpg
         * gender : 0
         * birth : 1979-05-10
         * age : 0
         * description : 单身女人。
         * location :
         * is_online : 1
         * authentication : 0
         * member_status : 0
         * photo_info : null
         * hall_text :
         * more_num : 0
         * token : rec_18_1^14980156_1649256824340_0^0_1^11
         */

        private Integer uid;
        private String nick;
        private String portrait;
        private Integer gender;
        private String birth;
        private Integer age;
        private String description;
        private String location;
        private Integer is_online;
        private Integer authentication;
        private Integer member_status;
        private Object photo_info;
        private String hall_text;
        private Integer more_num;
        private String token;

        public Integer getUid() {
            return uid;
        }

        public void setUid(Integer uid) {
            this.uid = uid;
        }

        public String getNick() {
            return nick;
        }

        public void setNick(String nick) {
            this.nick = nick;
        }

        public String getPortrait() {
            return portrait;
        }

        public void setPortrait(String portrait) {
            this.portrait = portrait;
        }

        public Integer getGender() {
            return gender;
        }

        public void setGender(Integer gender) {
            this.gender = gender;
        }

        public String getBirth() {
            return birth;
        }

        public void setBirth(String birth) {
            this.birth = birth;
        }

        public Integer getAge() {
            return age;
        }

        public void setAge(Integer age) {
            this.age = age;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public Integer getIs_online() {
            return is_online;
        }

        public void setIs_online(Integer is_online) {
            this.is_online = is_online;
        }

        public Integer getAuthentication() {
            return authentication;
        }

        public void setAuthentication(Integer authentication) {
            this.authentication = authentication;
        }

        public Integer getMember_status() {
            return member_status;
        }

        public void setMember_status(Integer member_status) {
            this.member_status = member_status;
        }

        public Object getPhoto_info() {
            return photo_info;
        }

        public void setPhoto_info(Object photo_info) {
            this.photo_info = photo_info;
        }

        public String getHall_text() {
            return hall_text;
        }

        public void setHall_text(String hall_text) {
            this.hall_text = hall_text;
        }

        public Integer getMore_num() {
            return more_num;
        }

        public void setMore_num(Integer more_num) {
            this.more_num = more_num;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }
    }
}
