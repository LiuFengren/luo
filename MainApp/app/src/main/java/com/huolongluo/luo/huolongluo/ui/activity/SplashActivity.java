package com.huolongluo.luo.huolongluo.ui.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.WindowManager;

import androidx.annotation.Nullable;

import com.huolongluo.luo.MainActivity;
import com.huolongluo.luo.R;
import com.huolongluo.luo.huolongluo.base.BaseActivity;
import com.huolongluo.luo.huolongluo.base.BaseApp;
import com.huolongluo.luo.huolongluo.bean.UserBean;
import com.huolongluo.luo.huolongluo.share.Share;
import com.huolongluo.luo.huolongluo.share.ShareData;
import com.huolongluo.luo.huolongluo.ui.activity.login.LoginActivity;
import com.huolongluo.luo.manager.pay.wechat.WxConfig;
import com.huolongluo.luo.util.DeviceUtils;
import com.huolongluo.luo.util.L;
import com.tencent.imsdk.v2.V2TIMCallback;
import com.tencent.mm.opensdk.constants.ConstantsAPI;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.tencent.qcloud.tuicore.TUILogin;
import com.tencent.qcloud.tuicore.util.ToastUtil;

import cn.bmob.v3.BmobUser;

public class SplashActivity extends BaseActivity {
    private static final String TAG = "SplashActivity";

    // IWXAPI 是第三方 app 和微信通信的 openApi 接口
    private IWXAPI api;

    @Override
    protected int getContentViewId() {
        return R.layout.activity_splash;
    }

    @Override
    protected void injectDagger() {
    }

    @Override
    protected void initViewsAndEvents(@Nullable Bundle savedInstanceState) {
        L.d("分辨率：高 " + DeviceUtils.getScreenPix(this).heightPixels);
        L.d("分辨率：宽 " + DeviceUtils.getScreenPix(this).widthPixels);
        regToWx();

        //沉浸式全屏
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

//        new Handler().postDelayed(() ->
//        {
//            init();
//        }, 2000);

        handleData();
    }

    private void handleData() {
        UserBean userBean = BmobUser.getCurrentUser(UserBean.class);

        if (userBean == null || !Share.get().getIsLogin() || TextUtils.isEmpty(Share.get().getUserName()) || TextUtils.isEmpty(Share.get().getImUserSig())) {
            startActivity(LoginActivity.class);
            finish();
        } else {
            BaseApp.get(this).init(0);
            TUILogin.login(Share.get().getUserName(), Share.get().getImUserSig(), new V2TIMCallback() {
                @Override
                public void onError(final int code, final String desc) {
                    Log.e(TAG, "onError: 登录IM失败：" + desc);
//                    runOnUiThread(new Runnable() {
//                        public void run() {
//                            ToastUtil.toastLongMessage(getString(R.string.failed_login_tip) + ", errCode = " + code + ", errInfo = " + desc);
//                            startLogin();
//                        }
//                    });
//                    DemoLog.i(TAG, "imLogin errorCode = " + code + ", errorInfo = " + desc);
                    startActivity(LoginActivity.class);
                    finish();
                }

                @Override
                public void onSuccess() {
                    Log.e(TAG, "onSuccess: 登录IM成功");
                    startActivity(MainActivity.class);
                    finish();
                }
            });
        }



    }

    private void init() {
        if (!Share.get().getIsLogin()) {
            startActivity(LoginActivity.class);
            finish();
        } else {
            startActivity(MainActivity.class);
            finish();
        }
    }

    //注册到微信
    private void regToWx() {
        // 通过 WXAPIFactory 工厂，获取 IWXAPI 的实例
        api = WXAPIFactory.createWXAPI(this, WxConfig.APP_ID, true);

        // 将应用的 appId 注册到微信
        api.registerApp(WxConfig.APP_ID);

        //建议动态监听微信启动广播进行注册到微信
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // 将该 app 注册到微信
                api.registerApp(WxConfig.APP_ID);
            }
        }, new IntentFilter(ConstantsAPI.ACTION_REFRESH_WXAPP));
    }
}
