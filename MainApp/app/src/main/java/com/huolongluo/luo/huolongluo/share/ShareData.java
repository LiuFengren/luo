package com.huolongluo.luo.huolongluo.share;

import com.cocosw.favor.AllFavor;
import com.cocosw.favor.Commit;
import com.cocosw.favor.Default;
import com.cocosw.favor.Favor;

/**
 * Created by 火龙裸先生 on 2020/3/12 0011.
 * <p>
 * 数据保存在sharepreference中
 */

@AllFavor
public interface ShareData {

    String BMOB_CURRENT_USER_OBJECTID = "objectId";//当前登录用户的UserBean表中的objectId
    String BMOB_CURRENT_USER_WECHAT = "wechat";//当前登录用户的微信号
    String USER_NAME = "username";
    String TI_XIAN_REAL_FULL_NAME = "tixian_real_full_name";//提现 真实全名
    String TI_XIAN_ACCOUNT = "tixian_account";//提现 账号
    String MY_INVITE_USER_NAME = "myInviteUsername";//邀请我注册的那个人的用户名
    String PASS_WORD = "passWord";
    String IM_USER_SIG = "imUserSig";//IM登录用的userSig
    String NICK_NAME = "nickName";//用户昵称
    String HEAD_PIC = "headPic";//用户头像
    String BIRTH_DAY = "birthday";//用户生日
    String DEVICE_ID = "deviceId";
    String IS_LOGIN = "isLogin";//用户是否已登录
    String SEX = "sex";//性别
    String HEIGHT = "height";//身高
    String WEIGHT = "weight";//体重
    String ADDRESS = "address";//住址
    String ZUJI_ADDRESS = "zujiAddress";//祖籍地址
    String XUELI = "xueli";//学历
    String BIYE_SCHOOL = "biYeSchool";//毕业院校
    String WORK = "work";//职业
    String YEAR_IN_COME = "yearInCome";//年收入
    String IS_VIP = "isVIP";//是否VIP
    String IS_AGENT = "isAgent";//是否代理
    String USER_WALLET_BEAN_OBJECT_ID = "user_wallet_bean_object_id";//当前用户钱包的ObjectId
    String GOLD_BALANCE = "goldBalance";//金币余额
    String INTEGRAL_BALANCE = "integralBalance";//积分余额
//    String TIXIAN_ACCOUNT = "tiXianAccount";//提现账户名
    String IS_ACCEPT_HONGNIANG = "isAcceptHongNiang";//是否接受红娘服务
    String KE_FU_ACCOUNT = "kefuAccount";//客服账号
    String RECHARGE_COUNT = "rechargeCount";//用户充值次数统计

    String UN_CONSUME_GOLD = "unConsumeGold";//未被成功消耗的金币数量

    String UP_WALL_PRICE = "upWallPrice";//上墙消耗多少金币
    String UN_LOCK_WECHAT_PRICE = "unlockWechatPrice";//解锁微信消耗多少金币
    String MESSAGE_PRICE = "messagePrice";//一条消息消耗多少金币
    String AUDIO_PRICE = "audioPrice";//一分钟语音消耗多少金币
    String VIDEO_PRICE = "videoPrice";//一分钟视频消耗多少金币
    String PROFIT_INTEGRAL_UNIT = "profitIntegralUnit";//消耗对方一个金币可分佣多少积分
    String NEW_USER_WILL_GET_GOLD = "newUserWillGetGold";//新注册用户将获得多少金币
    String TIXIAN_GET_JIFEN_UNIT = "tiXianGetJiFenUnit";//他人提现后，邀请的人将获得多少积分

    String WX_REFRESH_TOKEN = "wxRefreshToken";//微信授权登录的RefreshToken

    @Commit
    @Favor(BMOB_CURRENT_USER_OBJECTID)
    void setBmobCurrentUserObject(String objectId);

    @Default("")
    @Favor(BMOB_CURRENT_USER_OBJECTID)
    String getBmobCurrentUserObject();

    @Commit
    @Favor(BMOB_CURRENT_USER_WECHAT)
    void setBmobCurrentUserWechat(String wechat);

    @Default("")
    @Favor(BMOB_CURRENT_USER_WECHAT)
    String getBmobCurrentUserWechat();

    @Commit
    @Favor(USER_NAME)
    void setUserName(String username);

    @Default("")
    @Favor(USER_NAME)
    String getUserName();

    @Commit
    @Favor(TI_XIAN_REAL_FULL_NAME)
    void setTiXianRealFullName(String tixian_real_full_name);

    @Default("")
    @Favor(TI_XIAN_REAL_FULL_NAME)
    String getTiXianRealFullName();

    @Commit
    @Favor(TI_XIAN_ACCOUNT)
    void setTiXianAccount(String tixian_account);

    @Default("")
    @Favor(TI_XIAN_ACCOUNT)
    String getTiXianAccount();

    @Commit
    @Favor(MY_INVITE_USER_NAME)
    void setMyInviteUserName(String myInviteUserName);

    @Default("")
    @Favor(MY_INVITE_USER_NAME)
    String getMyInviteUserName();

    @Commit
    @Favor(PASS_WORD)
    void setPassWord(String passWord);

    @Default("")
    @Favor(PASS_WORD)
    String getPassWord();

    @Commit
    @Favor(IM_USER_SIG)
    void setImUserSig(String imUserSig);

    @Default("")
    @Favor(IM_USER_SIG)
    String getImUserSig();

    @Commit
    @Favor(NICK_NAME)
    void setNickName(String nickName);

    @Default("")
    @Favor(NICK_NAME)
    String getNickName();

    @Commit
    @Favor(DEVICE_ID)
    void setDeviceId(String deviceId);

    @Default("")
    @Favor(DEVICE_ID)
    String getDeviceId();

    @Commit
    @Favor(IS_LOGIN)
    void setIsLogin(boolean isLogin);

    @Default("false")
    @Favor(IS_LOGIN)
    boolean getIsLogin();

    @Commit
    @Favor(SEX)
    void setSex(String sex);

    @Default("")
    @Favor(SEX)
    String getSex();

    @Commit
    @Favor(HEIGHT)
    void setHeight(int height);

    @Default("0")
    @Favor(HEIGHT)
    int getHeight();

    @Commit
    @Favor(WEIGHT)
    void setWeight(int weight);

    @Default("0")
    @Favor(WEIGHT)
    int getWeight();

    @Commit
    @Favor(ADDRESS)
    void setAddress(String address);

    @Default("")
    @Favor(ADDRESS)
    String getAddress();

    @Commit
    @Favor(ZUJI_ADDRESS)
    void setZujiAddress(String zujiAddress);

    @Default("")
    @Favor(ZUJI_ADDRESS)
    String getZujiAddress();

    @Commit
    @Favor(XUELI)
    void setXueli(String xueli);

    @Default("")
    @Favor(XUELI)
    String getXueli();

    @Commit
    @Favor(BIYE_SCHOOL)
    void setBiyeSchool(String biYeSchool);

    @Default("")
    @Favor(BIYE_SCHOOL)
    String getBiyeSchool();

    @Commit
    @Favor(WORK)
    void setWork(String work);

    @Default("")
    @Favor(WORK)
    String getWork();

    @Commit
    @Favor(YEAR_IN_COME)
    void setYearInCome(String yearInCome);

    @Default("")
    @Favor(YEAR_IN_COME)
    String getYearInCome();

    @Commit
    @Favor(IS_VIP)
    void setIsVip(boolean isVip);

    @Default("false")
    @Favor(IS_VIP)
    boolean getIsVip();

    @Commit
    @Favor(IS_AGENT)
    void setIsAgent(boolean isAgent);

    @Default("false")
    @Favor(IS_AGENT)
    boolean getIsAgent();

    @Commit
    @Favor(USER_WALLET_BEAN_OBJECT_ID)
    void setUser_wallet_bean_object_id(String user_wallet_bean_object_id);

    @Default("")
    @Favor(USER_WALLET_BEAN_OBJECT_ID)
    String getUser_wallet_bean_object_id();

    @Commit
    @Favor(GOLD_BALANCE)
    void setGoldBalance(String goldBalance);

    @Default("0")
    @Favor(GOLD_BALANCE)
    String getGoldBalance();

    @Commit
    @Favor(INTEGRAL_BALANCE)
    void setIntegralBalance(String integralBalance);

    @Default("0")
    @Favor(INTEGRAL_BALANCE)
    String getIntegralBalance();

//    @Commit
//    @Favor(TIXIAN_ACCOUNT)
//    void setTixianAccount(String tiXianAccount);
//
//    @Default("")
//    @Favor(TIXIAN_ACCOUNT)
//    String getTixianAccount();

    @Commit
    @Favor(HEAD_PIC)
    void setHeadPic(String headPic);

    @Default("")
    @Favor(HEAD_PIC)
    String getHeadPic();

    @Commit
    @Favor(BIRTH_DAY)
    void setBirthDay(String birthday);

    @Default("")
    @Favor(BIRTH_DAY)
    String getBirthDay();

    @Commit
    @Favor(IS_ACCEPT_HONGNIANG)
    void setIsAcceptHongniang(boolean isAcceptHongniang);

    @Default("false")
    @Favor(IS_ACCEPT_HONGNIANG)
    boolean getIsAcceptHongNiang();

    @Commit
    @Favor(KE_FU_ACCOUNT)
    void setKeFuAccount(String kefuAccount);

    @Default("13058083706")
    @Favor(KE_FU_ACCOUNT)
    String getKeFuAccount();

    @Commit
    @Favor(RECHARGE_COUNT)
    void setRechargeCount(int rechargeCount);

    @Default("0")
    @Favor(RECHARGE_COUNT)
    int getRechargeCount();

    @Commit
    @Favor(UN_CONSUME_GOLD)
    void setUnConsumeGold(int unConsumeGold);

    @Default("0")
    @Favor(UN_CONSUME_GOLD)
    int getUnConsumeGold();

    @Commit
    @Favor(UP_WALL_PRICE)
    void setUpWallPrice(String upWallPrice);

    @Default("0")
    @Favor(UP_WALL_PRICE)
    String getUpWallPrice();

    @Commit
    @Favor(UN_LOCK_WECHAT_PRICE)
    void setUnLockWechatPrice(String unlockWechatPrice);

    @Default("0")
    @Favor(UN_LOCK_WECHAT_PRICE)
    String getUnLockWechatPrice();

    @Commit
    @Favor(MESSAGE_PRICE)
    void setMessagePrice(String messagePrice);

    @Default("0")
    @Favor(MESSAGE_PRICE)
    String getMessagePrice();

    @Commit
    @Favor(AUDIO_PRICE)
    void setAudioPrice(String audioPrice);

    @Default("0")
    @Favor(AUDIO_PRICE)
    String getAudioPrice();

    @Commit
    @Favor(VIDEO_PRICE)
    void setVideoPrice(String videoPrice);

    @Default("0")
    @Favor(VIDEO_PRICE)
    String getVideoPrice();

    @Commit
    @Favor(PROFIT_INTEGRAL_UNIT)
    void setProfitIntegralUnit(String profitIntegralUnit);

    @Default("0")
    @Favor(PROFIT_INTEGRAL_UNIT)
    String getProfitIntegralUnit();

    @Commit
    @Favor(NEW_USER_WILL_GET_GOLD)
    void setNewUserWillGetGold(String newUserWillGetGold);

    @Default("0")
    @Favor(NEW_USER_WILL_GET_GOLD)
    String getNewUserWillGetGold();

    @Commit
    @Favor(TIXIAN_GET_JIFEN_UNIT)
    void setTixianGetJifenUnit(String tiXianGetJiFenUnit);

    @Default("0")
    @Favor(TIXIAN_GET_JIFEN_UNIT)
    String getTixianGetJifenUnit();

    @Commit
    @Favor(WX_REFRESH_TOKEN)
    void setWxRefreshToken(String wxRefreshToken);

    @Default("0")
    @Favor(WX_REFRESH_TOKEN)
    String getWxRefreshToken();
}
