package com.huolongluo.luo.huolongluo.bean;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.RequiresApi;

import com.huolongluo.luo.manager.pay.wechat.WxConfig;

import okhttp3.HttpUrl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;

public class WXCreateOrderEntity {

    private String mchid;//直连商户号
    private String out_trade_no;//商户订单号
    private String appid;//应用ID
    private String description;//商品描述
    private String notify_url;//通知地址
    private AmountDTO amount;//订单金额信息

    public String getMchid() {
        return mchid;
    }

    public void setMchid(String mchid) {
        this.mchid = mchid;
    }

    public String getOut_trade_no() {
        return out_trade_no;
    }

    public void setOut_trade_no(String out_trade_no) {
        this.out_trade_no = out_trade_no;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNotify_url() {
        if (TextUtils.isEmpty(notify_url)) {
            return "https://weixin.qq.com/";
        }
        return notify_url;
    }

    public void setNotify_url(String notify_url) {
        this.notify_url = notify_url;
    }

    public AmountDTO getAmount() {
        return amount;
    }

    public void setAmount(AmountDTO amount) {
        this.amount = amount;
    }

    public static class AmountDTO {
        private Integer total;//订单总金额，单位为分。 示例值：100
        private String currency;//CNY：人民币，境内商户号仅支持人民币。 示例值：CNY

        public Integer getTotal() {
            return total;
        }

        public void setTotal(Integer total) {
            this.total = total;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }
    }

    /*public static String getAuthorizationHeader(String nonce_str) {
        return "WECHATPAY2-SHA256-RSA2048" + "mchid=" + WxConfig.PARTNER_ID + "nonce_str=" + nonce_str + "signature=\"uOVRnA4qG/MNnYzdQxJanN+zU+lTgIcnU9BxGw5dKjK+VdEUz2FeIoC+D5sB/LN+nGzX3hfZg6r5wT1pl2ZobmIc6p0ldN7J6yDgUzbX8Uk3sD4a4eZVPTBvqNDoUqcYMlZ9uuDdCvNv4TM3c1WzsXUrExwVkI1XO5jCNbgDJ25nkT/c1gIFvqoogl7MdSFGc4W4xZsqCItnqbypR3RuGIlR9h9vlRsy7zJR9PBI83X8alLDIfR1ukt1P7tMnmogZ0cuDY8cZsd8ZlCgLadmvej58SLsIkVxFJ8XyUgx9FmutKSYTmYtWBZ0+tNvfGmbXU7cob8H/4nLBiCwIUFluw==\",timestamp=\"1554208460\",serial_no=\"1DDE55AD98ED71D6EDD4A4A16996DE7B47773A8C\"";
    }*/

    // Authorization: <schema> <token>
    // GET - getToken("GET", httpurl, "")
    // POST - getToken("POST", httpurl, json)
    /**
     * @param nonce_str 随机字符串
     * */
    @RequiresApi(api = Build.VERSION_CODES.O)
    public static String getAuthorizationHeader(Context context, String method, HttpUrl url, String body, String nonceStr, long currentSystemTime) throws IOException, NoSuchAlgorithmException, SignatureException, InvalidKeyException {
//        String nonceStr = "your nonce string";
//        long timestamp = System.currentTimeMillis() / 1000;
//        String message = buildMessage(method, url, timestamp, nonceStr, body);
        String message = buildMessage(method, url, currentSystemTime, nonceStr, body);
        String signature = sign(context, message.getBytes("utf-8"));

        return "WECHATPAY2-SHA256-RSA2048 mchid=\"" + WxConfig.PARTNER_ID + "\","
                + "nonce_str=\"" + nonceStr + "\","
                + "timestamp=\"" + currentSystemTime + "\","
                + "serial_no=\"" + WxConfig.API_SERIAL_NO + "\","
                + "signature=\"" + signature + "\"";
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public static String sign(Context context, byte[] message) throws NoSuchAlgorithmException, SignatureException, IOException, InvalidKeyException {
        Signature sign = Signature.getInstance("SHA256withRSA");
        sign.initSign(getPrivateKey(getFromAssets(context, "apiclient_key.pem")));
        sign.update(message);

        return Base64.getEncoder().encodeToString(sign.sign());
    }

    public static String buildMessage(String method, HttpUrl url, long timestamp, String nonceStr, String body) {
        String canonicalUrl = url.encodedPath();
        if (url.encodedQuery() != null) {
            canonicalUrl += "?" + url.encodedQuery();
        }

        return method + "\n"
                + canonicalUrl + "\n"
                + timestamp + "\n"
                + nonceStr + "\n"
                + body + "\n";
    }

    /**
     * 获取私钥。
     *
     * @param filename 私钥文件路径  (required)
     * @return 私钥对象
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    public static PrivateKey getPrivateKey(String filename) throws IOException {
        Log.e("TAG", "getPrivateKey: 文件名称：" + filename);
//        String content = new String(Files.readAllBytes(Paths.get(filename)), "utf-8");
        String content = filename;
        try {
            String privateKey = content.replace("-----BEGIN PRIVATE KEY-----", "")
                    .replace("-----END PRIVATE KEY-----", "")
                    .replaceAll("\\s+", "");

            KeyFactory kf = KeyFactory.getInstance("RSA");
            return kf.generatePrivate(
                    new PKCS8EncodedKeySpec(Base64.getDecoder().decode(privateKey)));
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("当前Java环境不支持RSA", e);
        } catch (InvalidKeySpecException e) {
            throw new RuntimeException("无效的密钥格式");
        }
    }

    private static String getFromAssets(Context context, String fileName){
        try {
            InputStreamReader inputReader = new InputStreamReader(context.getResources().getAssets().open(fileName) );
            BufferedReader bufReader = new BufferedReader(inputReader);
            String line="";
            String Result="";
            while((line = bufReader.readLine()) != null)
                Result += line;
            Log.e("TAG", "getFromAssets: 签名文件信息：" + Result);
            return Result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
}
