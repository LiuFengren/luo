package com.huolongluo.luo.huolongluo.ui.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.huolongluo.luo.R;
import com.huolongluo.luo.huolongluo.bean.GoodsBean;
import com.huolongluo.luo.superAdapter.recycler.BaseViewHolder;
import com.huolongluo.luo.superAdapter.recycler.SuperAdapter;
import com.jakewharton.rxbinding.view.RxView;

import java.util.List;
import java.util.concurrent.TimeUnit;

import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by 火龙裸 on 2018/6/10 0010.
 */

public class RechargeGoodsAdapter extends SuperAdapter<GoodsBean>
{
    public RechargeGoodsAdapter(Context context, List<GoodsBean> list, int layoutResId)
    {
        super(context, list, layoutResId);
    }

    @Override
    public void onBind(int viewType, BaseViewHolder holder, int position, GoodsBean item)
    {
        holder.setText(R.id.tv_zhuanshi_num, item.getGoldNum() <= 0 ? "0" : item.getGoldNum() + "金币");
        holder.setText(R.id.tv_will_pay,item.getWillPay() <= 0 ? "0" : item.getWillPay() + "元");
        if (item.getGoldGiftNum() <= 0) {
            holder.setVisibility(R.id.tv_zhuanshi_gift_num, View.INVISIBLE);
        } else {
            holder.setText(R.id.tv_zhuanshi_gift_num,"多赠" + item.getGoldGiftNum() + "金币");
            holder.setVisibility(R.id.tv_zhuanshi_gift_num, View.VISIBLE);
        }

        if (item.isChecked()) {
            holder.getView(R.id.ll_chongzhi_type).setBackgroundResource(R.mipmap.icon_charge_item_selected);
        } else {
            holder.getView(R.id.ll_chongzhi_type).setBackgroundResource(0);
        }

        // 点击item
        RxView.clicks(holder.getItemView()).throttleFirst(500, TimeUnit.MILLISECONDS).observeOn(AndroidSchedulers.mainThread()).subscribe(v ->
        {
            Log.e("TAG", "onBind: 点击了：" + position + "  共：" + getItemCount());

            for (int i = 0; i < getItemCount(); i++) {
                getAllData().get(i).setChecked(false);
            }
            item.setChecked(true);
            for (int i = 0; i < getItemCount(); i++) {
                Log.e("TAG", "onBind: 最终的选中情况：" + item.isChecked());
            }
            notifyDataSetChanged();
        });

        // 点击任务弹出窗体
//        RxView.clicks(holder.getView(R.id.tv_task_name)).throttleFirst(500, TimeUnit.MILLISECONDS).observeOn(AndroidSchedulers.mainThread()).subscribe(v ->
//        {
//            EventBus.getDefault().post(new Event.Task(item.getContent()+""));
//        });
//        // 点击 提交 任务
//        RxView.clicks(holder.getView(R.id.tv_submit01)).throttleFirst(500, TimeUnit.MILLISECONDS).observeOn(AndroidSchedulers.mainThread()).subscribe(v ->
//        {
//            Intent intent = new Intent(mContext, SubMitTaskActivity.class);
//            intent.putExtra("tId", item.getId()+"");
//            intent.putExtra("position", position);
//            intent.putExtra("title", item.getContent());
//            mContext.startActivity(intent);
//        });

    }
}
