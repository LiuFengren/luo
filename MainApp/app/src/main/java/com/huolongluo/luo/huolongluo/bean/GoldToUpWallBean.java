package com.huolongluo.luo.huolongluo.bean;

import cn.bmob.v3.BmobObject;

/**
 * 通过金币上墙的用户表
 * */
public class GoldToUpWallBean extends BmobObject {
    private String username;//用户名

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
