package com.huolongluo.luo.huolongluo.bean;

import cn.bmob.v3.BmobObject;

public class JuBaoBean extends BmobObject {
    private String username;//用户名
    private String jubaoUserName;//被举报的用户名
    private String jubaoType;//被举报类型
    private String jubaoDongTaiObjectId;//被举报的动态objectId

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getJubaoUserName() {
        return jubaoUserName;
    }

    public void setJubaoUserName(String jubaoUserName) {
        this.jubaoUserName = jubaoUserName;
    }

    public String getJubaoType() {
        return jubaoType;
    }

    public void setJubaoType(String jubaoType) {
        this.jubaoType = jubaoType;
    }

    public String getJubaoDongTaiObjectId() {
        return jubaoDongTaiObjectId;
    }

    public void setJubaoDongTaiObjectId(String jubaoDongTaiObjectId) {
        this.jubaoDongTaiObjectId = jubaoDongTaiObjectId;
    }

    @Override
    public String toString() {
        return "JuBaoBean{" +
                "username='" + username + '\'' +
                ", jubaoUserName='" + jubaoUserName + '\'' +
                ", jubaoType='" + jubaoType + '\'' +
                ", jubaoDongTaiObjectId='" + jubaoDongTaiObjectId + '\'' +
                '}';
    }
}
