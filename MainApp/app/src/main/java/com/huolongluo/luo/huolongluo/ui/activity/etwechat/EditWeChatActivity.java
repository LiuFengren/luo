package com.huolongluo.luo.huolongluo.ui.activity.etwechat;

import android.os.Bundle;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.gyf.immersionbar.ImmersionBar;
import com.huolongluo.luo.R;
import com.huolongluo.luo.huolongluo.base.BaseActivity;
import com.huolongluo.luo.huolongluo.share.Event;
import com.huolongluo.luo.util.ToastSimple;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;

/**
 * <p>
 * Created by 火龙裸 on 2017/9/6.
 */

public class EditWeChatActivity extends BaseActivity {
    private static final String TAG = "MyAccountActivity";
    @BindView(R.id.toolbar_center_title)
    TextView toolbar_center_title;
    @BindView(R.id.iv_left)
    ImageView iv_left;
    @BindView(R.id.tv_right)
    TextView tv_right;
    /********************* 以上为ToolBar ************************/
    @BindView(R.id.et_we_chat)
    EditText et_we_chat;
    @BindView(R.id.tv_sure)
    TextView tv_sure;

    @Override
    protected int getContentViewId() {
        return R.layout.activity_edit_we_chat;
    }

    @Override
    protected void injectDagger() {
        activityComponent().inject(this);
    }

    private void initToolBar() {
        ImmersionBar.with(this).statusBarDarkFont(true).init();
        toolbar_center_title.setText("微信");
    }

    @Override
    protected void initViewsAndEvents(@Nullable Bundle savedInstanceState) {
        initToolBar();
        eventClick(iv_left).subscribe(o -> // 点击返回键
        {
            close();
        });
        eventClick(tv_sure).subscribe(o -> // 点击确定
        {
            String etWeChatContent = et_we_chat.getText().toString().trim();
            if (TextUtils.isEmpty(etWeChatContent)) {
                ToastSimple.show("微信号不能为空");
                return;
            }
            EventBus.getDefault().post(new Event.editWeChat(etWeChatContent));
            close();
        });

    }
}
