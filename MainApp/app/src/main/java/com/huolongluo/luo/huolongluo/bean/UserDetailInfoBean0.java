//package com.huolongluo.luo.huolongluo.bean;
//
//import java.io.Serializable;
//import java.util.List;
//
//import cn.bmob.v3.BmobObject;
//
///**
// * 用户详细信息表
// * @param username 主键，可修改更新，但是不可以重复插入
// * */
//public class UserDetailInfoBean extends BmobObject implements Serializable {
//    private String username;//用户名
////    private double goldBalance;//金币余额
//
//    private String selfDescription;//自我描述
//    private String familyBackground;//家庭背景
//    private String hobby;//兴趣爱好
//    private String loveIdea;//爱情观
//    private String idealPartner;//理想另一半
//    private String whySingle;//我为什么单身
//    private String qiDaiShengHuo;//遇到对的人，期待什么样的
//    private UserPicBean userPic;//照片 多张照片用逗号隔开
//
////    private int followNum;//关注了多少人
////    private int beFollowNum;//被多少人关注了
////    private boolean isOpenHongNiang;//是否同意红娘服务
//
//
//    public String getUsername() {
//        return username;
//    }
//
//    public void setUsername(String username) {
//        this.username = username;
//    }
//
//    public String getSelfDescription() {
//        return selfDescription;
//    }
//
//    public void setSelfDescription(String selfDescription) {
//        this.selfDescription = selfDescription;
//    }
//
//    public String getFamilyBackground() {
//        return familyBackground;
//    }
//
//    public void setFamilyBackground(String familyBackground) {
//        this.familyBackground = familyBackground;
//    }
//
//    public String getHobby() {
//        return hobby;
//    }
//
//    public void setHobby(String hobby) {
//        this.hobby = hobby;
//    }
//
//    public String getLoveIdea() {
//        return loveIdea;
//    }
//
//    public void setLoveIdea(String loveIdea) {
//        this.loveIdea = loveIdea;
//    }
//
//    public String getIdealPartner() {
//        return idealPartner;
//    }
//
//    public void setIdealPartner(String idealPartner) {
//        this.idealPartner = idealPartner;
//    }
//
//    public String getWhySingle() {
//        return whySingle;
//    }
//
//    public void setWhySingle(String whySingle) {
//        this.whySingle = whySingle;
//    }
//
//    public String getQiDaiShengHuo() {
//        return qiDaiShengHuo;
//    }
//
//    public void setQiDaiShengHuo(String qiDaiShengHuo) {
//        this.qiDaiShengHuo = qiDaiShengHuo;
//    }
//
//    public UserPicBean getUserPic() {
//        return userPic;
//    }
//
//    public void setUserPic(UserPicBean userPic) {
//        this.userPic = userPic;
//    }
//
//    public static class UserPicBean extends BmobObject implements Serializable {
//        private List<String> userTopPic;//用户置顶图片
//
//        private List<String> selfDescriptionPic;//自我描述 精美图片
//        private List<String> familyBackgroundPic;//家庭背景 精美图片
//        private List<String> hobbyPic;//兴趣爱好 精美图片
//        private List<String> loveIdeaPic;//爱情观 精美图片
//        private List<String> idealPartnerPic;//理想另一半 精美照片
//        private List<String> whySinglePic;//我为什么单身 精美照片
//        private List<String> qiDaiShengHuoPic;//遇到对的人，期待什么样的 精美照片
//
//        public List<String> getUserTopPic() {
//            return userTopPic;
//        }
//
//        public void setUserTopPic(List<String> userTopPic) {
//            this.userTopPic = userTopPic;
//        }
//
//        public List<String> getSelfDescriptionPic() {
//            return selfDescriptionPic;
//        }
//
//        public void setSelfDescriptionPic(List<String> selfDescriptionPic) {
//            this.selfDescriptionPic = selfDescriptionPic;
//        }
//
//        public List<String> getFamilyBackgroundPic() {
//            return familyBackgroundPic;
//        }
//
//        public void setFamilyBackgroundPic(List<String> familyBackgroundPic) {
//            this.familyBackgroundPic = familyBackgroundPic;
//        }
//
//        public List<String> getHobbyPic() {
//            return hobbyPic;
//        }
//
//        public void setHobbyPic(List<String> hobbyPic) {
//            this.hobbyPic = hobbyPic;
//        }
//
//        public List<String> getLoveIdeaPic() {
//            return loveIdeaPic;
//        }
//
//        public void setLoveIdeaPic(List<String> loveIdeaPic) {
//            this.loveIdeaPic = loveIdeaPic;
//        }
//
//        public List<String> getIdealPartnerPic() {
//            return idealPartnerPic;
//        }
//
//        public void setIdealPartnerPic(List<String> idealPartnerPic) {
//            this.idealPartnerPic = idealPartnerPic;
//        }
//
//        public List<String> getWhySinglePic() {
//            return whySinglePic;
//        }
//
//        public void setWhySinglePic(List<String> whySinglePic) {
//            this.whySinglePic = whySinglePic;
//        }
//
//        public List<String> getQiDaiShengHuoPic() {
//            return qiDaiShengHuoPic;
//        }
//
//        public void setQiDaiShengHuoPic(List<String> qiDaiShengHuoPic) {
//            this.qiDaiShengHuoPic = qiDaiShengHuoPic;
//        }
//    }
//}
