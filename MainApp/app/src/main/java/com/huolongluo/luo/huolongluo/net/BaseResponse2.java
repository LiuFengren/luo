package com.huolongluo.luo.huolongluo.net;

import java.io.Serializable;

public class BaseResponse2<T> implements Serializable
{
    private int dm_error;

    private String error_msg;

    private T data;

    public T getData()
    {
        return data;
    }

    public void setData(T data)
    {
        this.data = data;
    }

    public int getDm_error() {
        return dm_error;
    }

    public void setDm_error(int dm_error) {
        this.dm_error = dm_error;
    }

    public String getError_msg() {
        return error_msg;
    }

    public void setError_msg(String error_msg) {
        this.error_msg = error_msg;
    }
    @Override
    public String toString() {
        return "BaseResponse2{" +
                "dm_error=" + dm_error +
                ", error_msg='" + error_msg + '\'' +
                ", data=" + data +
                '}';
    }
}