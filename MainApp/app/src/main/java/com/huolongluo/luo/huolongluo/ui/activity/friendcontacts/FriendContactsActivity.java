package com.huolongluo.luo.huolongluo.ui.activity.friendcontacts;

import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import com.gyf.immersionbar.ImmersionBar;
import com.huolongluo.luo.R;
import com.huolongluo.luo.huolongluo.base.BaseActivity;
import com.huolongluo.luo.huolongluo.bean.LabelsBean;
import com.huolongluo.luo.huolongluo.share.Share;
import com.huolongluo.luo.huolongluo.ui.adapter.FriendContactsViewPager2Adapter;
import com.huolongluo.luo.huolongluo.ui.adapter.MyLabelViewPager2Adapter;
import com.huolongluo.luo.huolongluo.ui.fragment.friendcontact.FriendContactsFragment;
import com.huolongluo.luo.huolongluo.ui.fragment.selectlabel.SelectLabelFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;

/**
 * <p>
 * Created by 火龙裸 on 2017/9/6.
 */

public class FriendContactsActivity extends BaseActivity {
    private static final String TAG = "FriendContactsActivity";
    @BindView(R.id.toolbar_center_title)
    TextView toolbar_center_title;
    @BindView(R.id.iv_left)
    ImageView iv_left;
    @BindView(R.id.tv_right)
    TextView tv_right;
    /********************* 以上为ToolBar ************************/
    @BindView(R.id.tb_friend_contacts_title)
    TabLayout tb_friend_contacts_title;
    @BindView(R.id.vp_friend_contacts)
    ViewPager2 vp_friend_contacts;

//    private SelectLabelFragment myLabelViewPager2Fragment;

//    private List<LabelsBean> labelsBeans = new ArrayList<>();//标签原始数据

    //    private MyLabelViewPager2Adapter mAdapter;
    private List<String> list_title = new ArrayList<>(); // tab名称列表
    private List<Fragment> list_fragment = new ArrayList<>(); // 定义要装fragment的列表

    @Override
    protected int getContentViewId() {
        return R.layout.activity_friend_contacts;
    }

    @Override
    protected void injectDagger() {
        activityComponent().inject(this);
    }

    private void initToolBar() {
        ImmersionBar.with(this).statusBarDarkFont(true).init();
        toolbar_center_title.setText(Share.get().getNickName());
//        tv_right.setVisibility(View.VISIBLE);
//        tv_right.setText("充值记录");
    }

    @Override
    protected void initViewsAndEvents(@Nullable Bundle savedInstanceState) {
        initToolBar();
        eventClick(iv_left).subscribe(o -> // 点击返回键
        {
            close();
        });

        list_title.clear();
        list_fragment.clear();

        list_title.add("关注");
        list_title.add("粉丝");
        list_fragment.add(FriendContactsFragment.getInstance(0));
        list_fragment.add(FriendContactsFragment.getInstance(1));

        vp_friend_contacts.setAdapter(new FriendContactsViewPager2Adapter(getSupportFragmentManager(), getLifecycle(), list_fragment));

        //关联TabLayout 添加attach()
        new TabLayoutMediator(tb_friend_contacts_title, vp_friend_contacts, new TabLayoutMediator.TabConfigurationStrategy() {
            @Override
            public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {
                tab.setText(list_title.get(position));
            }
        }).attach();

        vp_friend_contacts.registerOnPageChangeCallback(callback);
    }

    private ViewPager2.OnPageChangeCallback callback = new ViewPager2.OnPageChangeCallback() {
        @Override
        public void onPageSelected(int position) {
            super.onPageSelected(position);
            Log.e(TAG, "onPageSelected: 改变选中了：" + position);
        }
    };
}
