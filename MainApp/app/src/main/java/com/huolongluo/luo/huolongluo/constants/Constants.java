package com.huolongluo.luo.huolongluo.constants;

/**
 * Created by 火龙裸 on 2018/7/23.
 */
public class Constants
{
    public static final String APP_ID = "wx10c40a735a9de08d";//微信开放平台审核通过的应用APPID


    public static final String FROM_SUPER = "mySuper";
    public static final String FROM_JUNIOR = "myJunior";

    public static final String UID = "uId";
    public static final String PID = "pId";
    public static final String FROM = "from";
    public static final String POSITION = "position";
    public static final String BUNDLE = "bundle";

    /**
     * 下拉刷新模式
     */
    public static final int MODE_REFRESH = 1;
    /**
     * 上拉加载模式
     */
    public static final int MODE_LOAD_MORE = 2;

    /**
     * 用户名
     */
    public static final String USER_NAME = "userName";
    public static final String USER_INFO = "userInfo";
//    public static final String IS_EDIT_USER_INFO = "isEditUserInfo";

    /**
     * 编辑用户详情信息
     * */
    public static final String OBJECT_ID = "objectId";//用户详情数据ID
    public static final String USER_BASE_INFO = "userBaseInfo";//用户详情数据
    public static final String USER_ABOUT_ME = "aboutMe";//用户“关于我”资料
    public static final String EDIT_DATA_CARD_TYPE = "editDataCardType";//编辑卡类型
//    public static final String EDIT_DATA_CARD_TEXT = "editDataCardText";//编辑卡编辑框的内容
    public static final String EDIT_DATA_CARD_PIC = "editDataCardPic";//编辑卡中的精美图片

    /**
     * 点击查看”我的关注“，或者”谁关注我“
     * */
    public static final String FOLLOW_TYPE = "followType";//1代表从”我的关注“点击进来的，2表示从”谁关注我“点击进来的
}
