package com.huolongluo.luo.huolongluo.ui.fragment.tuijian;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.huolongluo.luo.R;
import com.huolongluo.luo.huolongluo.base.BaseActivity;
import com.huolongluo.luo.huolongluo.base.BaseFragment;
import com.huolongluo.luo.huolongluo.bean.TuijianBean;
import com.huolongluo.luo.huolongluo.ui.adapter.HomeTuijianAdapter;
import com.tencent.imsdk.v2.V2TIMConversation;
import com.tencent.qcloud.tuicore.TUIConstants;
import com.tencent.qcloud.tuicore.TUICore;
import com.tencent.qcloud.tuikit.tuiconversation.bean.ConversationInfo;
import com.tencent.qcloud.tuikit.tuiconversation.ui.page.TUIConversationFragment;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

public class TuiJianFragment extends BaseFragment implements TuiJianContract.View {
    private static final String TAG = "TuiJianFragment";
    @Inject
    TuiJianPresent present;
    //    @BindView(R.id.banner)
//    Banner banner;
//    @BindView(R.id.rv_guess_you_like)
//    RecyclerView rv_guess_you_like;
    @BindView(R.id.rv_tuijian)
    RecyclerView rv_tuijian;
    @BindView(R.id.et_user_id)
    EditText et_user_id;
    @BindView(R.id.btn_start_chat)
    Button btn_start_chat;

    private HomeTuijianAdapter homeTuijianAdapter;
    private List<TuijianBean> tuijianBeanList = new ArrayList<>();//首页推荐的RecyclerView有几条Item

    public static TuiJianFragment getInstance() {
        Bundle args = new Bundle();
        TuiJianFragment fragment = new TuiJianFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void initDagger() {
        ((BaseActivity) getActivity()).activityComponent().inject(this);
        present.attachView(this);
    }

//    private void initToolBar() {
//        int statusbarHeight = ImmersedStatusbarUtils.getStatusBarHeight(getActivity());
//        lin1.setPadding(0, statusbarHeight, 0, 0);
//    }

    @Override
    protected int getContentViewId() {
        return R.layout.fragment_home_tuijian;
    }

    @Override
    protected void initViewsAndEvents(View rootView) {
//        initToolBar();
//        subscription = present.getGuessLiskList();
        rv_tuijian.setNestedScrollingEnabled(false);
        rv_tuijian.setFocusable(false);
        rv_tuijian.setLayoutManager(new GridLayoutManager(getActivity(), 2) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        });
        //rv_sucai.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false));
        homeTuijianAdapter = new HomeTuijianAdapter(getActivity(), tuijianBeanList, R.layout.item_home_tuijian);
        rv_tuijian.setAdapter(homeTuijianAdapter);

        btn_start_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String c2cUserId = et_user_id.getText().toString().trim();
                if (!TextUtils.isEmpty(c2cUserId)) {
                    startChatActivity(c2cUserId, c2cUserId + "AA");
                }
            }
        });
    }

    /**
     * 启动单聊 还是 群聊
     * */
    private void startChatActivity(String conversationId, String title) {
        Log.e("TAG", "startChatActivity: 会话ID：" + conversationId);
        Bundle param = new Bundle();
        param.putInt(TUIConstants.TUIChat.CHAT_TYPE, V2TIMConversation.V2TIM_C2C);
        param.putString(TUIConstants.TUIChat.CHAT_ID, conversationId);
        param.putString(TUIConstants.TUIChat.CHAT_NAME, title);
//        if (conversationInfo.getDraft() != null) {
//            param.putString(TUIConstants.TUIChat.DRAFT_TEXT, conversationInfo.getDraft().getDraftText());
//            param.putLong(TUIConstants.TUIChat.DRAFT_TIME, conversationInfo.getDraft().getDraftTime());
//        }
//        param.putBoolean(TUIConstants.TUIChat.IS_TOP_CHAT, conversationInfo.isTop());
//
//        if (conversationInfo.isGroup()) {
//            param.putString(TUIConstants.TUIChat.FACE_URL, conversationInfo.getIconPath());
//            param.putString(TUIConstants.TUIChat.GROUP_TYPE, conversationInfo.getGroupType());
//            param.putSerializable(TUIConstants.TUIChat.AT_INFO_LIST, (Serializable) conversationInfo.getGroupAtInfoList());
//        }
//        if (conversationInfo.isGroup()) {
//            //启动群聊
//            TUICore.startActivity(TUIConstants.TUIChat.GROUP_CHAT_ACTIVITY_NAME, param);
//        } else {
//            //启动单聊
//            TUICore.startActivity(TUIConstants.TUIChat.C2C_CHAT_ACTIVITY_NAME, param);
//        }

        //启动单聊
        TUICore.startActivity(TUIConstants.TUIChat.C2C_CHAT_ACTIVITY_NAME, param);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        present.detachView();
    }
}
