package com.huolongluo.luo.huolongluo.ui.activity.editbaseinfo;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.github.gzuliyujiang.wheelpicker.AddressPicker;
import com.github.gzuliyujiang.wheelpicker.BirthdayPicker;
import com.github.gzuliyujiang.wheelpicker.NumberPicker;
import com.github.gzuliyujiang.wheelpicker.OptionPicker;
import com.github.gzuliyujiang.wheelpicker.annotation.AddressMode;
import com.github.gzuliyujiang.wheelpicker.contract.OnAddressPickedListener;
import com.github.gzuliyujiang.wheelpicker.contract.OnDatePickedListener;
import com.github.gzuliyujiang.wheelpicker.contract.OnLinkageSelectedListener;
import com.github.gzuliyujiang.wheelpicker.contract.OnNumberPickedListener;
import com.github.gzuliyujiang.wheelpicker.contract.OnNumberSelectedListener;
import com.github.gzuliyujiang.wheelpicker.contract.OnOptionPickedListener;
import com.github.gzuliyujiang.wheelpicker.contract.OnOptionSelectedListener;
import com.github.gzuliyujiang.wheelpicker.entity.CityEntity;
import com.github.gzuliyujiang.wheelpicker.entity.CountyEntity;
import com.github.gzuliyujiang.wheelpicker.entity.ProvinceEntity;
import com.github.gzuliyujiang.wheelpicker.utility.AddressJsonParser;
import com.github.gzuliyujiang.wheelview.contract.WheelFormatter;
import com.gyf.immersionbar.ImmersionBar;
import com.huolongluo.luo.R;
import com.huolongluo.luo.huolongluo.base.BaseActivity;
import com.huolongluo.luo.huolongluo.bean.UserBasicInfoBean;
import com.huolongluo.luo.huolongluo.constants.Constants;
import com.huolongluo.luo.huolongluo.manager.DialogClickListener;
import com.huolongluo.luo.huolongluo.share.Event;
import com.huolongluo.luo.huolongluo.share.Share;
import com.huolongluo.luo.huolongluo.ui.activity.editjob.EditJobActivity;
import com.huolongluo.luo.huolongluo.ui.activity.editnickname.EditNickNameActivity;
import com.huolongluo.luo.huolongluo.ui.activity.editschool.EditSchoolActivity;
import com.huolongluo.luo.huolongluo.ui.activity.etwechat.EditWeChatActivity;
import com.huolongluo.luo.util.TUIKitUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.UpdateListener;

/**
 * <p>
 * Created by 火龙裸 on 2017/9/6.
 */

public class EditBaseInfoActivity extends BaseActivity {
    private static final String TAG = "EditBaseInfoActivity";
    @BindView(R.id.toolbar_center_title)
    TextView toolbar_center_title;
    @BindView(R.id.iv_left)
    ImageView iv_left;
    @BindView(R.id.tv_right)
    TextView tv_right;
    /********************* 以上为ToolBar ************************/
    @BindView(R.id.tv_et_nick_name)
    TextView tv_et_nick_name;
    @BindView(R.id.tv_et_wx)
    TextView tv_et_wx;
    @BindView(R.id.tv_birth_day)
    TextView tv_birth_day;
    @BindView(R.id.tv_sex)
    TextView tv_sex;
    @BindView(R.id.tv_height)
    TextView tv_height;
    @BindView(R.id.tv_weight)
    TextView tv_weight;
    @BindView(R.id.tv_address)
    TextView tv_address;
    @BindView(R.id.tv_zuji_address)
    TextView tv_zuji_address;
    @BindView(R.id.tv_xueli)
    TextView tv_xueli;
    @BindView(R.id.tv_school)
    TextView tv_school;
    @BindView(R.id.tv_work)
    TextView tv_work;
    @BindView(R.id.tv_income)
    TextView tv_income;

    @BindView(R.id.tv_to_save)
    TextView tv_to_save;

    private String etNickNameContent;
    private String etWeChatContent;
    private String birthDay;
    private String sex;
    private int height;//身高
    private int weight;//体重
    private String address;//住址
    private String zujiAddress;//祖籍地址
    private String xueli;//学历
    private String etSchoolContent;//毕业院校
    private String etJobContent;//工作
    private String yearinCome;//年收入

    private UserBasicInfoBean userBasicInfoBean;//用户资料

    @Override
    protected int getContentViewId() {
        return R.layout.activity_edit_base_info;
    }

    @Override
    protected void injectDagger() {
        activityComponent().inject(this);
    }

    private void initToolBar() {
        ImmersionBar.with(this).statusBarDarkFont(true).init();
        toolbar_center_title.setText("编辑基础资料");
    }

    @Override
    protected void initViewsAndEvents(@Nullable Bundle savedInstanceState) {
        if (getBundle() != null) {
            userBasicInfoBean = (UserBasicInfoBean) getBundle().getSerializable(Constants.USER_BASE_INFO);
        }
        if (userBasicInfoBean != null) {
            setUi();
        }
        EventBus.getDefault().register(this);
        initToolBar();
        eventClick(iv_left).subscribe(o -> // 点击返回键
        {
            close();
        });
        eventClick(tv_et_nick_name).subscribe(o -> // 编辑昵称
        {
            startActivity(EditNickNameActivity.class);
        });
        eventClick(tv_et_wx).subscribe(o -> // 编辑微信
        {
            if (userBasicInfoBean != null) {
                if (userBasicInfoBean.isHeYanWeChat()) {
                    showConfirmDialog("提示", "微信号核验已通过，如需修改请联系客服，或添加客服微信：huolongluoS", new DialogClickListener() {
                        @Override
                        public void onConfirm() {
                        }

                        @Override
                        public void onCancel() {
                        }
                    });
                } else {
                    showConfirmNoCancelDialog("提示", "请填写真实微信，他人解锁微信时，只有核验通过后的微信，你才会获得对应的收益哦。", new DialogClickListener() {
                        @Override
                        public void onConfirm() {
                            startActivity(EditWeChatActivity.class);
                        }

                        @Override
                        public void onCancel() {
                        }
                    });
                }
            }
        });
        eventClick(tv_birth_day).subscribe(o -> // 出生日期
        {
            BirthdayPicker picker = new BirthdayPicker(this);
            picker.setDefaultValue(1991, 11, 11);
            picker.setOnDatePickedListener(new OnDatePickedListener() {
                @Override
                public void onDatePicked(int year, int month, int day) {
                    Log.e(TAG, "onDatePicked: 选择的结果为：" + year + " " + month + " " + day);
                    birthDay = year + "-" + month + "-" + day;
                    tv_birth_day.setText(birthDay);
                }
            });
            picker.getWheelLayout().setResetWhenLinkage(false);
            picker.show();
        });
//        eventClick(tv_sex).subscribe(o -> // 性别
//        {
//            SexPicker picker = new SexPicker(this);
//            picker.setBodyWidth(140);
//            picker.setIncludeSecrecy(false);
//            picker.setDefaultValue("女");
//            picker.setOnOptionPickedListener(new OnOptionPickedListener() {
//                @Override
//                public void onOptionPicked(int position, Object item) {
//                    sex = ((SexEntity)item).getName();
//                    tv_sex.setText(sex);
//                    Log.e(TAG, "onOptionPicked: 选择的性别为：" + sex);
//                }
//            });
//            picker.getWheelLayout().setOnOptionSelectedListener(new OnOptionSelectedListener() {
//                @Override
//                public void onOptionSelected(int position, Object item) {
//                    picker.getTitleView().setText(picker.getWheelView().formatItem(position));
//                }
//            });
//            picker.show();
//        });
        eventClick(tv_height).subscribe(o -> // 点击身高
        {
            NumberPicker picker = new NumberPicker(this);
            picker.setOnNumberPickedListener(new OnNumberPickedListener() {
                @Override
                public void onNumberPicked(int position, Number item) {
                    height = (int) item;
                    tv_height.setText(height + "cm");
                    Log.e(TAG, "onNumberPicked: 选择身高为：" + height);
                }
            });
            picker.getWheelLayout().setOnNumberSelectedListener(new OnNumberSelectedListener() {
                @Override
                public void onNumberSelected(int position, Number item) {
                    picker.getTitleView().setText(picker.getWheelView().formatItem(position));
                }
            });
            picker.setFormatter(new WheelFormatter() {
                @Override
                public String formatItem(@NonNull Object item) {
                    return item.toString() + " cm";
                }
            });
            picker.setRange(140, 200, 1);
            picker.setDefaultValue(172);
            picker.setTitle("身高选择");
            picker.show();
        });
        eventClick(tv_weight).subscribe(o -> // 点击体重
        {
            NumberPicker picker = new NumberPicker(this);
            picker.setOnNumberPickedListener(new OnNumberPickedListener() {
                @Override
                public void onNumberPicked(int position, Number item) {
                    weight = (int) item;
                    tv_weight.setText(weight + "kg");
                    Log.e(TAG, "onNumberPicked: 选择体重为：" + weight);
                }
            });
            picker.getWheelLayout().setOnNumberSelectedListener(new OnNumberSelectedListener() {
                @Override
                public void onNumberSelected(int position, Number item) {
                    picker.getTitleView().setText(picker.getWheelView().formatItem(position));
                }
            });
            picker.setFormatter(new WheelFormatter() {
                @Override
                public String formatItem(@NonNull Object item) {
                    return item.toString() + " kg";
                }
            });
            picker.setRange(40, 120, 1);
            picker.setDefaultValue(172);
            picker.setTitle("体重");
            picker.show();
        });
        eventClick(tv_address).subscribe(o -> // 点击现居地址
        {
            AddressPicker picker = new AddressPicker(this);
            picker.setAddressMode("city.json", AddressMode.PROVINCE_CITY_COUNTY,
                    new AddressJsonParser.Builder()
                            .provinceCodeField("code")
                            .provinceNameField("name")
                            .provinceChildField("city")
                            .cityCodeField("code")
                            .cityNameField("name")
                            .cityChildField("area")
                            .countyCodeField("code")
                            .countyNameField("name")
                            .build());
            picker.setDefaultValue("广东省", "深圳市", "南山区");
            picker.setOnAddressPickedListener(new OnAddressPickedListener() {
                @Override
                public void onAddressPicked(ProvinceEntity province, CityEntity city, CountyEntity county) {
                    address = province.getName() + "-" + city.getName() + "-" + county.getName();
                    Log.e(TAG, "onAddressPicked: 选择的地址1为：" + province.getName() + "   " + city.getName() + "    " + county.getName());
                    tv_address.setText(address);
                }
            });
            picker.getWheelLayout().setOnLinkageSelectedListener(new OnLinkageSelectedListener() {
                @Override
                public void onLinkageSelected(Object first, Object second, Object third) {
                    picker.getTitleView().setText(String.format("%s%s%s",
                            picker.getFirstWheelView().formatItem(first),
                            picker.getSecondWheelView().formatItem(second),
                            picker.getThirdWheelView().formatItem(third)));
                }
            });
            picker.show();
        });
        eventClick(tv_zuji_address).subscribe(o -> // 点击家乡
        {
            AddressPicker picker = new AddressPicker(this);
            picker.setAddressMode("city.json", AddressMode.PROVINCE_CITY_COUNTY,
                    new AddressJsonParser.Builder()
                            .provinceCodeField("code")
                            .provinceNameField("name")
                            .provinceChildField("city")
                            .cityCodeField("code")
                            .cityNameField("name")
                            .cityChildField("area")
                            .countyCodeField("code")
                            .countyNameField("name")
                            .build());
            picker.setDefaultValue("广东省", "深圳市", "南山区");
            picker.setOnAddressPickedListener(new OnAddressPickedListener() {
                @Override
                public void onAddressPicked(ProvinceEntity province, CityEntity city, CountyEntity county) {
                    zujiAddress = province.getName() + "-" + city.getName() + "-" + county.getName();
                    Log.e(TAG, "onAddressPicked: 选择的地址2为：" + province.getName() + "   " + city.getName() + "    " + county.getName());
                    tv_zuji_address.setText(zujiAddress);
                }
            });
            picker.getWheelLayout().setOnLinkageSelectedListener(new OnLinkageSelectedListener() {
                @Override
                public void onLinkageSelected(Object first, Object second, Object third) {
                    picker.getTitleView().setText(String.format("%s%s%s",
                            picker.getFirstWheelView().formatItem(first),
                            picker.getSecondWheelView().formatItem(second),
                            picker.getThirdWheelView().formatItem(third)));
                }
            });
            picker.show();
        });
        eventClick(tv_xueli).subscribe(o -> // 点击学历
        {
            OptionPicker picker = new OptionPicker(this);
            picker.setData("中专", "高中及以下", "大专", "大学本科", "硕士", "博士");
            picker.setDefaultPosition(3);
            picker.setOnOptionPickedListener(new OnOptionPickedListener() {
                @Override
                public void onOptionPicked(int position, Object item) {
                    Log.e(TAG, "onOptionPicked: 学历选择了：" + item);
                    xueli = (String) item;
                    tv_xueli.setText(xueli + "");
                }
            });
            picker.getWheelLayout().setOnOptionSelectedListener(new OnOptionSelectedListener() {
                @Override
                public void onOptionSelected(int position, Object item) {
                    picker.getTitleView().setText(picker.getWheelView().formatItem(position));
                }
            });
            picker.getWheelView().setStyle(R.style.WheelStyleDemo);
            picker.show();
        });
        eventClick(tv_school).subscribe(o -> // 点击毕业学院
        {
            startActivity(EditSchoolActivity.class);
        });
        eventClick(tv_work).subscribe(o -> // 点击职业
        {
            startActivity(EditJobActivity.class);
        });
        eventClick(tv_income).subscribe(o -> // 点击年收入
        {
            OptionPicker picker = new OptionPicker(this);
            picker.setData("10万", "20万", "30万", "40万", "50万", "60万", "70万", "80万", "90万", "100万", "150万", "200万", "250万", "300万及300万以上", "保密");
            picker.setDefaultPosition(2);
            picker.setOnOptionPickedListener(new OnOptionPickedListener() {
                @Override
                public void onOptionPicked(int position, Object item) {
                    Log.e(TAG, "onOptionPicked: 年收入选择了：" + item);
                    yearinCome = (String) item;
                    tv_income.setText(yearinCome + "");
                }
            });
            picker.getWheelLayout().setOnOptionSelectedListener(new OnOptionSelectedListener() {
                @Override
                public void onOptionSelected(int position, Object item) {
                    picker.getTitleView().setText(picker.getWheelView().formatItem(position));
                }
            });
            picker.getWheelView().setStyle(R.style.WheelStyleDemo);
            picker.show();
        });

        eventClick(tv_to_save).subscribe(o -> // 点击保存
        {
            updateUserBaseInfo();
        });
    }

    private void setUi() {
        etNickNameContent = userBasicInfoBean.getNickName();
        etWeChatContent = userBasicInfoBean.getWechat();
        birthDay = userBasicInfoBean.getBirthday();
//        sex = userBasicInfoBean.getSex();
        height = userBasicInfoBean.getHeight();
        weight = userBasicInfoBean.getWeight();
        address = userBasicInfoBean.getAddress();
        zujiAddress = userBasicInfoBean.getZujiAddress();
        xueli = userBasicInfoBean.getXueli();
        etSchoolContent = userBasicInfoBean.getBiYeSchool();
        etJobContent = userBasicInfoBean.getWork();
        yearinCome = userBasicInfoBean.getYearInCome();

        tv_et_nick_name.setText(etNickNameContent);
        tv_et_wx.setText(TextUtils.isEmpty(etWeChatContent) ? "仅用于为你匹配推荐最合适的异性" : etWeChatContent);
        tv_birth_day.setText(birthDay);
        tv_sex.setText(Share.get().getSex());
        tv_height.setText(height + "cm");
        tv_weight.setText(weight + "kg");
        tv_address.setText(address);
        tv_zuji_address.setText(zujiAddress);
        tv_xueli.setText(xueli);
        tv_school.setText(etSchoolContent);
        tv_work.setText(etJobContent);
        tv_income.setText(yearinCome);
    }

    private void updateUserBaseInfo() {
        userBasicInfoBean.setNickName(etNickNameContent);
        userBasicInfoBean.setWechat(etWeChatContent);
        userBasicInfoBean.setBirthday(birthDay);
//        userBasicInfoBean.setSex(sex);
        userBasicInfoBean.setHeight(height);
        userBasicInfoBean.setWeight(weight);
        userBasicInfoBean.setAddress(address);
        userBasicInfoBean.setZujiAddress(zujiAddress);
        userBasicInfoBean.setXueli(xueli);
        userBasicInfoBean.setBiYeSchool(etSchoolContent);
        userBasicInfoBean.setWork(etJobContent);
        userBasicInfoBean.setYearInCome(yearinCome);

        userBasicInfoBean.update(userBasicInfoBean.getObjectId(), new UpdateListener() {
            @Override
            public void done(BmobException e) {
                if(e==null){
                    EventBus.getDefault().post(new Event.updateUserBaseInfo(userBasicInfoBean));
                    Share.get().setBirthDay(birthDay);
                    Share.get().setNickName(etNickNameContent);
                    Share.get().setBmobCurrentUserWechat(etWeChatContent);
                    Share.get().setHeight(height);
                    Share.get().setWeight(weight);
                    Share.get().setAddress(address);
                    Share.get().setZujiAddress(zujiAddress);
                    Share.get().setXueli(xueli);
                    Share.get().setBiyeSchool(etSchoolContent);
                    Share.get().setWork(etJobContent);
                    Share.get().setYearInCome(yearinCome);
                    TUIKitUtil.updateProfile(Share.get().getHeadPic(), Share.get().getNickName());
                    showConfirmNoCancelDialog("保存信息", "用户信息更新成功", new DialogClickListener() {
                        @Override
                        public void onConfirm() {
                            close();
                        }

                        @Override
                        public void onCancel() {
                            close();
                        }
                    });
                }else{
                    Log.e(TAG, "用户信息更新失败：" + e.getMessage());
                    showConfirmNoCancelDialog("保存信息", "用户信息更新失败，请稍后再试", new DialogClickListener() {
                        @Override
                        public void onConfirm() {
                        }

                        @Override
                        public void onCancel() {
                        }
                    });
                }
            }
        });
    }

    /**
     * 刷新用户昵称
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void editNickName(Event.editNickName event) {
        etNickNameContent = event.editNickName;
        tv_et_nick_name.setText(etNickNameContent);
    }

    /**
     * 刷新用户昵称
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void editNickName(Event.editWeChat event) {
        etWeChatContent = event.editWeChat;
        tv_et_wx.setText(etWeChatContent);
    }

    /**
     * 刷新用户毕业院校
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void editNickName(Event.editSchool event) {
        etSchoolContent = event.editSchoolName;
        tv_school.setText(etSchoolContent);
    }

    /**
     * 刷新用户工作信息
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void editJob(Event.editJob event) {
        etJobContent = event.editJobName;
        tv_work.setText(etJobContent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
