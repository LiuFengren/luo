package com.huolongluo.luo.huolongluo.net;

/**
 * UrlConstants
 * <p>
 * Created by 火龙裸先生 on 2020/03/11.
 */

public class UrlConstants
{
    //        public final static String DOMAIN = "https://120.76.237.235:8080/sjapi/";          // 线上开发服务器1
//    public final static String DOMAIN = "http://testapi.qiyu88.com/";          // 测试预发服务器
    public final static String DOMAIN = "https://service.hnyuntong.cn/";                     // 线上正式服务器1

    public final static String SEND_SMS = "login/sendSms"; // 获取验证码
    public final static String REGISTER_SEND_SMS = "login/registerSendSms"; // 注册验证码
    public final static String MODIFY_PASSWORD = "login/modifyPassword"; // 修改密码
    public final static String UP_PASSWORD = "login/upPassword"; // 忘记密码
    public final static String REGISTER = "login/register"; // 注册
    /********** 微信支付 **** 客户ID：3905941876679039934 *****/
    //获取验证码 post
    public static final String GET_PHONE_CODE = "https://service.hnyuntong.cn/user/account/phone_code?lc=30f8b9a536da7082&cv=ZYJT3.5.82_Android&cc=TG000067785&ua=meizumeizu16Xs&uid=0&sid=&evid=6434653730323935366137366533303436343338383233363331313662386335&dev_name=meizu&msid=&meid=&icc=&conn=wifi&vv=202102021805&aid=aafb39b56ff0dfa4&osversion=android_28&mtid=d9c471b76710fca876003681b785db87&mtxid=020000000000&proto=8&smid=DuK3sWPjLTS8EDl93qDpEUNJuZV0p15uCz7TmGnj%2BWonQfrdiGv7fCBrtrSi65tvRap8RkHi0xWqkGF3cVkE57uQ&logid=&cpu=[Adreno_(TM)_612][ARMv8_638_Qualcomm_Technologies,_Inc_SM6150]&ram=5888978944&ndid=&oaid=d47c71dbe23c07ad7bfaee22f88d5d54&source_info=eyJhcHBpZCI6IjI2MDAzNyIsInVpZCI6IjY0MzQ2NTM3MzAzMjM5MzUzNjYxMzczNjY1MzMzMDM0MzYzNDMzMzgzODMyMzMzNjMzMzEzMTM2NjIzODYzMzUiLCJwYWdlIjoidW5rbm93biIsInRpbWUiOiIwIn0%3D&ver=dev";
    //获取用户信息 get
    public static final String USER_INFO = "https://service.hnyuntong.cn/api/user/info?meid=313230333636303430393439393638&vv=202102021805&smid=DuK3sWPjLTS8EDl93qDpEUNJuZV0p15uCz7TmGnj%2BWonQfrdiGv7fCBrtrSi65tvRap8RkHi0xWqkGF3cVkE57uQ&conn=wifi&ast=1&icc=&ua=meizumeizu16Xs&code94927255=1649343673014&sid=20Qon4jnvzAl5oGOsqcMi2i0L6ltCvYKeji0x3Wmi1WSti08FLjgmB0agi3i3&uid=14980156&xid=323&oaid=d47c71dbe23c07ad7bfaee22f88d5d54&ram=5888978944&cc=TG000067785&dev_name=meizu&ndid=&ver=dev&evid=313230333636303430393439393638&cpu=%5BAdreno_%28TM%29_612%5D%5BARMv8_638_Qualcomm_Technologies%2C_Inc_SM6150%5D&mtid=d9c471b76710fca876003681b785db87&msid=&cv=ZYJT3.5.82_Android&lc=30f8b9a536da7082&proto=8&mtxid=020000000000&logid=247000601%2C247000702%2C247000803&osversion=android_28&aid=aafb39b56ff0dfa4&source_info=eyJhcHBpZCI6IjI2MDAzNyIsInVpZCI6IjMxMzIzMDMzMzYzNjMwMzQzMDM5MzQzOTM5MzYzOCIsInBhZ2UiOiJ1bmtub3duIiwidGltZSI6IjAifQ%253D%253D&id=14980156&with_album=true";
    //猜你喜欢 get
    public static final String GUESS_YOU_LIKE = "api/v1/hall/guess_you_like?meid=&vv=202102021805&smid=Due0%2F7S6lZZKdg7o9jKVOMAaACPIxUfW25WyOfZK3RYNjGtAdgrWlkAK2Kowa4x5wlIwyrQwMm3iOQ4HhQOga9Kw&conn=wifi&icc=&ua=XiaomiM2104K10AC&sid=20gxWMb0PYNKuit8t28Xh74QCmTEBSIDKhM2uUQ4OalKBQ1MLneAi3i3&uid=14980156&oaid=394c42334184a47b&ram=7909974016&cc=TG000014054&dev_name=Xiaomi&ndid=&ver=dev&evid=6137343431366636346338343564636566656661373665656532623762303934&cpu=%5BMali-G77_MC9%5D%5BARMv8_638_MT6891%5D&mtid=d9c471b76710fca876003681b785db87&msid=&cv=ZYJT3.5.82_Android&lc=35206c117b8d5e20&proto=8&mtxid=020000000000&logid=247000702%2C247000803%2C247000601&osversion=android_30&aid=72a73b89ed4cafb5&source_info=";
    //推荐 get
    public static final String RECOMMEND = "https://service.hnyuntong.cn/api/v2/hall/recommend?last=&num=30&meid=&vv=202102021805&smid=Due0%2F7S6lZZKdg7o9jKVOMAaACPIxUfW25WyOfZK3RYNjGtAdgrWlkAK2Kowa4x5wlIwyrQwMm3iOQ4HhQOga9Kw&conn=wifi&icc=&ua=XiaomiM2104K10AC&sid=20gxWMb0PYNKuit8t28Xh74QCmTEBSIDKhM2uUQ4OalKBQ1MLneAi3i3&uid=14980156&oaid=394c42334184a47b&ram=7909974016&cc=TG000014054&dev_name=Xiaomi&ndid=&ver=dev&evid=6137343431366636346338343564636566656661373665656532623762303934&cpu=%5BMali-G77_MC9%5D%5BARMv8_638_MT6891%5D&mtid=d9c471b76710fca876003681b785db87&msid=&cv=ZYJT3.5.82_Android&lc=35206c117b8d5e20&proto=8&mtxid=020000000000&logid=247000803%2C247000702%2C247000601&osversion=android_30&aid=72a73b89ed4cafb5&source_info=";
    //新人 get
    public static final String NEW_USER = "api/v1/hall/new_user?last=&num=30&meid=&vv=202102021805&smid=DugxN9E%2BUpHF4YwSYJRcfbgX4qrVnsfPyP8ipyoRZHQLzqrWqJSNVcx0KyVN0dlFf%2BslL4qxhllD2aE5W%2BB9%2Fgvw&conn=wifi&icc=&ua=OPPOPBBM00&sid=206hsJDPkGSTxYHi0uhKNuY3v9Blkkr2pRY0g8kkTVDztocTKY1Rgi3i3&uid=14980156&oaid=831BC1B804E54BE5954B86363238954F30e6d0e43a0087381445536f37bd9b8d&ram=3874324480&cc=TG000067785&dev_name=OPPO&ndid=&ver=dev&evid=6130623732313061346431616238386439363066616361613565316163663836&cpu=%5BMali-G72_MP3%5D%5BARMv8_638_MT6771V%2FC%5D&mtid=d9c471b76710fca876003681b785db87&msid=&cv=ZYJT3.5.82_Android&lc=30f8b9a536da7082&proto=8&mtxid=020000000000&logid=247000803%2C247000601%2C247000702&osversion=android_28&aid=ad2d5b3373bbc816&source_info=eyJhcHBpZCI6IjI2MDAzNyIsInVpZCI6IjYxMzA2MjM3MzIzMTMwNjEzNDY0MzE2MTYyMzgzODY0MzkzNjMwNjY2MTYzNjE2MTM1NjUzMTYxNjM2NjM4MzYiLCJwYWdlIjoidW5rbm93biIsInRpbWUiOiIwIn0%253D";
    //退出登录 post
    public static final String LOGOUT = "https://maidian.hnyuntong.cn/log/upload?meid=313230333636303430393439393638&vv=202102021805&smid=DuK3sWPjLTS8EDl93qDpEUNJuZV0p15uCz7TmGnj%2BWonQfrdiGv7fCBrtrSi65tvRap8RkHi0xWqkGF3cVkE57uQ&conn=wifi&ast=1&icc=&ua=meizumeizu16Xs&sid=&uid=0&xid=373&oaid=d47c71dbe23c07ad7bfaee22f88d5d54&ram=5888978944&cc=TG000067785&dev_name=meizu&ndid=&ver=dev&evid=313230333636303430393439393638&cpu=%5BAdreno_%28TM%29_612%5D%5BARMv8_638_Qualcomm_Technologies%2C_Inc_SM6150%5D&code626107046=1649343979805&mtid=d9c471b76710fca876003681b785db87&msid=&cv=ZYJT3.5.82_Android&lc=30f8b9a536da7082&proto=8&mtxid=020000000000&logid=247000601%2C247000803%2C247000702&osversion=android_28&aid=aafb39b56ff0dfa4&source_info=eyJhcHBpZCI6IjI2MDAzNyIsInVpZCI6IjMxMzIzMDMzMzYzNjMwMzQzMDM5MzQzOTM5MzYzOCIsInBhZ2UiOiJ1bmtub3duIiwidGltZSI6IjAifQ%253D%253D";

    //App下单 商户直连
    public static final String WX_CREATE_ORDER = "https://api.mch.weixin.qq.com/v3/pay/transactions/app";
    //统一下单
    public static final String WX_CREATE_ORDER2 = "https://api.mch.weixin.qq.com/pay/unifiedorder";
}
