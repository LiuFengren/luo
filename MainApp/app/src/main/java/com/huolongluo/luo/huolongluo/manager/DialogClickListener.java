package com.huolongluo.luo.huolongluo.manager;

public interface DialogClickListener {
    void onConfirm();

    void onCancel();
}