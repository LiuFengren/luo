package com.huolongluo.luo.wxapi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.huolongluo.luo.R;
import com.huolongluo.luo.huolongluo.bean.UserBean;
import com.huolongluo.luo.huolongluo.manager.DialogClickListener;
import com.huolongluo.luo.huolongluo.share.Share;
import com.huolongluo.luo.huolongluo.ui.activity.register.RegisterActivity;
import com.huolongluo.luo.manager.pay.wechat.WxConfig;
import com.huolongluo.luo.manager.pay.wechat.uikit.NetworkUtil;
import com.huolongluo.luo.util.L;
import com.huolongluo.luo.util.ToastSimple;
import com.tencent.mm.opensdk.constants.ConstantsAPI;
import com.tencent.mm.opensdk.modelbase.BaseReq;
import com.tencent.mm.opensdk.modelbase.BaseResp;
import com.tencent.mm.opensdk.modelbiz.SubscribeMessage;
import com.tencent.mm.opensdk.modelbiz.WXLaunchMiniProgram;
import com.tencent.mm.opensdk.modelbiz.WXOpenBusinessView;
import com.tencent.mm.opensdk.modelbiz.WXOpenBusinessWebview;
import com.tencent.mm.opensdk.modelmsg.SendAuth;
import com.tencent.mm.opensdk.modelmsg.ShowMessageFromWX;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.Random;

import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.SaveListener;

public class WXEntryActivity extends Activity implements IWXAPIEventHandler {
    private static String TAG = "WXEntryActivity";

    private IWXAPI api;
    private MyHandler handler;

    private static class MyHandler extends Handler {
        private final WeakReference<WXEntryActivity> wxEntryActivityWeakReference;

        public MyHandler(WXEntryActivity wxEntryActivity) {
            wxEntryActivityWeakReference = new WeakReference<WXEntryActivity>(wxEntryActivity);
        }

        @Override
        public void handleMessage(Message msg) {
            int tag = msg.what;
            switch (tag) {
                case NetworkUtil.GET_TOKEN:
                    Bundle data = msg.getData();
                    JSONObject json = null;
                    try {
                        json = new JSONObject(data.getString("result"));
                        String openId, accessToken, refreshToken, scope;
                        openId = json.getString("openid");
                        accessToken = json.getString("access_token");
                        refreshToken = json.getString("refresh_token");
                        scope = json.getString("scope");
                        WxConfig.WX_REFRESH_TOKEN = refreshToken;
                        Share.get().setWxRefreshToken(refreshToken);
                        Log.e(TAG, "handleMessage: 数据为：" + json.toString() + "  openId:" + openId + "  accessToken:" + accessToken + "refreshToken:" + refreshToken + " scope:" + scope);
						/*Intent intent = new Intent(wxEntryActivityWeakReference.get(), SendToWXActivity.class);
						intent.putExtra("openId", openId);
						intent.putExtra("accessToken", accessToken);
						intent.putExtra("refreshToken", refreshToken);
						intent.putExtra("scope", scope);
						wxEntryActivityWeakReference.get().startActivity(intent);*/

                        NetworkUtil.sendWxAPI(this, String.format("https://api.weixin.qq.com/sns/oauth2/refresh_token?" +
                                        "appid=%s&grant_type=refresh_token&refresh_token=%s", WxConfig.APP_ID,
                                WxConfig.WX_REFRESH_TOKEN), NetworkUtil.REFRESH_TOKEN);

                    } catch (JSONException e) {
                        Log.e(TAG, e.getMessage());
                    }
                    break;
                case NetworkUtil.REFRESH_TOKEN://RefreshToken续期
                    Bundle data_refresh = msg.getData();
                    JSONObject json_refresh = null;
                    try {
                        json_refresh = new JSONObject(data_refresh.getString("result"));
                        String openId, accessToken, refreshToken, scope;
                        openId = json_refresh.getString("openid");
                        accessToken = json_refresh.getString("access_token");
                        refreshToken = json_refresh.getString("refresh_token");
                        scope = json_refresh.getString("scope");
                        WxConfig.WX_REFRESH_TOKEN = refreshToken;
                        Share.get().setWxRefreshToken(refreshToken);
                        Log.e(TAG, "handleMessage: 刷新Token数据为：" + json_refresh.toString());

                        //开始获取用户个人信息（UnionID 机制）
                        NetworkUtil.sendWxAPI(this, "https://api.weixin.qq.com/sns/userinfo?access_token=" + accessToken + "&openid=" + openId, NetworkUtil.GET_INFO);
                    } catch (JSONException e) {
                        Log.e(TAG, e.getMessage());
                    }
                    break;
                case NetworkUtil.GET_INFO:
                    Bundle data_userInfo = msg.getData();
                    JSONObject json_userInfo = null;
                    try {
                        json_userInfo = new JSONObject(data_userInfo.getString("result"));
                        String openid, unionid, nickname, headimgurl;
                        int sex;
                        openid = json_userInfo.getString("openid");
                        unionid = json_userInfo.getString("unionid");
                        nickname = json_userInfo.getString("nickname");
                        sex = json_userInfo.getInt("sex");
                        headimgurl = json_userInfo.getString("headimgurl");
                        Share.get().setNickName(nickname);
                        Share.get().setSex((sex == 1) ? "男" : "女");
                        Share.get().setHeadPic(headimgurl);
                        Log.e(TAG, "handleMessage: 获取用户信息为：" + json_userInfo.toString());
                        UserBean.registerAndLoginBmob(unionid, unionid, (sex == 1) ? "男" : "女");
                    } catch (JSONException e) {
                        Log.e(TAG, e.getMessage());
                    }
                    break;
            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        api = WXAPIFactory.createWXAPI(this, WxConfig.APP_ID, false);
        handler = new MyHandler(this);

        try {
            Intent intent = getIntent();
            api.handleIntent(intent, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        setIntent(intent);
        api.handleIntent(intent, this);
    }

    @Override
    public void onReq(BaseReq req) {
        Log.e(TAG, "onReq: >>>>>>>> " + req.toString() + " | " + req.getType() + " | " + req.openId + " | " + req.transaction);
        switch (req.getType()) {
            case ConstantsAPI.COMMAND_GETMESSAGE_FROM_WX:
                goToGetMsg();
                break;
            case ConstantsAPI.COMMAND_SHOWMESSAGE_FROM_WX:
                goToShowMsg((ShowMessageFromWX.Req) req);
                break;
            default:
                break;
        }
        finish();
    }

    @Override
    public void onResp(BaseResp resp) {
        String result = "0";
        Log.e(TAG, "onResp: >>>> " + resp.errCode + " | " + resp.errStr + " | " + resp.getType() + " | " + resp.openId + " | " + resp.transaction);

        switch (resp.errCode) {
            case BaseResp.ErrCode.ERR_OK:
                result = "发送成功";
                break;
            case BaseResp.ErrCode.ERR_USER_CANCEL:
                result = "发送取消";
                break;
            case BaseResp.ErrCode.ERR_AUTH_DENIED:
                result = "发送被拒绝";
                break;
            case BaseResp.ErrCode.ERR_UNSUPPORT:
                result = "不支持错误";
                break;
            default:
                result = "发送返回";
                break;
        }

        Log.e(TAG, "onResp: 微信登录：" + result + ", type=" + resp.getType());
//        Toast.makeText(this, result + ", type=" + resp.getType(), Toast.LENGTH_LONG).show();


        if (resp.getType() == ConstantsAPI.COMMAND_SUBSCRIBE_MESSAGE) {
            SubscribeMessage.Resp subscribeMsgResp = (SubscribeMessage.Resp) resp;
            String text = String.format("openid=%s\ntemplate_id=%s\nscene=%d\naction=%s\nreserved=%s",
                    subscribeMsgResp.openId, subscribeMsgResp.templateID, subscribeMsgResp.scene, subscribeMsgResp.action, subscribeMsgResp.reserved);

            Toast.makeText(this, text, Toast.LENGTH_LONG).show();
        }

        if (resp.getType() == ConstantsAPI.COMMAND_LAUNCH_WX_MINIPROGRAM) {
            WXLaunchMiniProgram.Resp launchMiniProgramResp = (WXLaunchMiniProgram.Resp) resp;
            String text = String.format("openid=%s\nextMsg=%s\nerrStr=%s",
                    launchMiniProgramResp.openId, launchMiniProgramResp.extMsg, launchMiniProgramResp.errStr);

            Toast.makeText(this, text, Toast.LENGTH_LONG).show();
        }

        if (resp.getType() == ConstantsAPI.COMMAND_OPEN_BUSINESS_VIEW) {
            WXOpenBusinessView.Resp launchMiniProgramResp = (WXOpenBusinessView.Resp) resp;
            String text = String.format("openid=%s\nextMsg=%s\nerrStr=%s\nbusinessType=%s",
                    launchMiniProgramResp.openId, launchMiniProgramResp.extMsg, launchMiniProgramResp.errStr, launchMiniProgramResp.businessType);

            Toast.makeText(this, text, Toast.LENGTH_LONG).show();
        }

        if (resp.getType() == ConstantsAPI.COMMAND_OPEN_BUSINESS_WEBVIEW) {
            WXOpenBusinessWebview.Resp response = (WXOpenBusinessWebview.Resp) resp;
            String text = String.format("businessType=%d\nresultInfo=%s\nret=%d", response.businessType, response.resultInfo, response.errCode);

            Toast.makeText(this, text, Toast.LENGTH_LONG).show();
        }

        if (resp.getType() == ConstantsAPI.COMMAND_SENDAUTH) {
            SendAuth.Resp authResp = (SendAuth.Resp) resp;
            final String code = authResp.code;
            Log.e(TAG, "onResp: 发送授权》》》》》》》》》》》》》" + code + "  ****  " + authResp.errCode + " | ");
            NetworkUtil.sendWxAPI(handler, String.format("https://api.weixin.qq.com/sns/oauth2/access_token?" +
                            "appid=%s&secret=%s&code=%s&grant_type=authorization_code", WxConfig.APP_ID,
                    WxConfig.AppSecret, code), NetworkUtil.GET_TOKEN);
        }
        finish();
    }

    private void goToGetMsg() {
		/*Intent intent = new Intent(this, GetFromWXActivity.class);
		intent.putExtras(getIntent());
		startActivity(intent);
		finish();*/
    }

    private void goToShowMsg(ShowMessageFromWX.Req showReq) {
		/*WXMediaMessage wxMsg = showReq.message;
		WXAppExtendObject obj = (WXAppExtendObject) wxMsg.mediaObject;
		
		StringBuffer msg = new StringBuffer();
		msg.append("description: ");
		msg.append(wxMsg.description);
		msg.append("\n");
		msg.append("extInfo: ");
		msg.append(obj.extInfo);
		msg.append("\n");
		msg.append("filePath: ");
		msg.append(obj.filePath);
		
		Intent intent = new Intent(this, ShowFromWXActivity.class);
		intent.putExtra(Constants.ShowMsgActivity.STitle, wxMsg.title);
		intent.putExtra(Constants.ShowMsgActivity.SMessage, msg.toString());
		intent.putExtra(Constants.ShowMsgActivity.BAThumbData, wxMsg.thumbData);
		startActivity(intent);
		finish();*/
    }
}