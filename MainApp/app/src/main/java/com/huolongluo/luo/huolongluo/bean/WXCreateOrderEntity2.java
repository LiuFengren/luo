package com.huolongluo.luo.huolongluo.bean;

import android.text.TextUtils;
import android.util.Log;

import com.huolongluo.luo.manager.pay.wechat.WxConfig;
import com.tencent.qcloud.tuicore.util.MD5Utils;

public class WXCreateOrderEntity2 {
    private static final String TAG = "WXCreateOrderEntity2";

    private String appid;//应用ID
    private String mch_id;//直连商户号
    private String nonce_str;//随机字符串，不长于32位。推荐随机数生成算法
    private String sign;//签名，详见签名生成算法
    private String body;//商品描述
    private String out_trade_no;//商户系统内部订单号，要求32个字符内（最少6个字符），只能是数字、大小写字母_-|* 且在同一个商户号下唯一
    private int total_fee;//订单总金额，单位为分
    private String spbill_create_ip;//终端IP 调用微信支付API的机器IP
    private String trade_type;//支付类型，例如："APP"
    private String notify_url;//通知地址

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getMch_id() {
        return mch_id;
    }

    public void setMch_id(String mch_id) {
        this.mch_id = mch_id;
    }

    public String getNonce_str() {
        return nonce_str;
    }

    public void setNonce_str(String nonce_str) {
        this.nonce_str = nonce_str;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getOut_trade_no() {
        return out_trade_no;
    }

    public void setOut_trade_no(String out_trade_no) {
        this.out_trade_no = out_trade_no;
    }

    public int getTotal_fee() {
        return total_fee;
    }

    public void setTotal_fee(int total_fee) {
        this.total_fee = total_fee;
    }

    public String getSpbill_create_ip() {
        if (TextUtils.isEmpty(spbill_create_ip)) {
            return "\t192.168.123";
        }
        return spbill_create_ip;
    }

    public void setSpbill_create_ip(String spbill_create_ip) {
        this.spbill_create_ip = spbill_create_ip;
    }

    public String getTrade_type() {
        if (TextUtils.isEmpty(trade_type)) {
            return "APP";
        }
        return trade_type;
    }

    public void setTrade_type(String trade_type) {
        this.trade_type = trade_type;
    }

    public String getNotify_url() {
        if (TextUtils.isEmpty(notify_url)) {
            return "https://www.weixin.qq.com/wxpay/pay.php";
        }
        return notify_url;
    }

    public void setNotify_url(String notify_url) {
        this.notify_url = notify_url;
    }

    /**
     * @param body 商品描述
     * @param nonce_str 随机字符串，不长于32位。 例如：ibuaiVcKdpRxkhJA
     * */
    public static String getMD5_Sign(String body, String nonce_str) {
        String A = "appid=" + WxConfig.APP_ID + "&body=" + body + "&device_info=" + WxConfig.PARTNER_ID + "&mch_id=" + WxConfig.PARTNER_ID + "&nonce_str=" + nonce_str;
        String SignTemp= A + "&key=" + WxConfig.ApiV2_V3; //注：key为商户平台设置的密钥key
        String resultSign = MD5Utils.getMD5String(SignTemp).toUpperCase();
        Log.e(TAG, "getMD5_Sign: " + resultSign);
        return resultSign;
    }
}
