package com.huolongluo.luo.huolongluo.ui.fragment.tuijian;

import com.huolongluo.luo.base.BasePresenter;
import com.huolongluo.luo.base.BaseView;

/**
 * Created by 火龙裸 on 2018/5/26 0026.
 */

public interface TuiJianContract
{
    interface View extends BaseView
    {
//        void getGuessLiskListSucce(GuessYouLikeBean result);
//
//        void getTaskListSucce(List<TaskListBean> result);
//
//        void getDynamicSucce(String type, List<DynamicListBean> result);
//
//        void getPictureSucce(List<SlideShowBean> result);
//
//        void getRankSucce(RankBean result);
//
//        void getQuantitySucce(QuantityBean result);
//
//        void getQuantityErr();
//
//        void getUpTimeSucce(UpTimeBean result);
//
//        void loginEerpSucce(LoginErpBean result);
    }

    interface Presenter extends BasePresenter<View>
    {
//        Subscription getGuessLiskList();
//
//        Subscription getTaskList(String aId, String uId, String pageNo, String pageSize,String period,String star);
//
//        Subscription getDynamic(String type, String content, String uName, String uId, String isErp, int pageNo, int pageSize);
//
//        Subscription getPicture(String type);
//
//        Subscription getRank(String id);
//
//        Subscription getQuantity(String id);
//
//        Subscription getUpTime(String id);
//
//        Subscription loginEerp(String member_id, String ticket, String password, String type);
    }
}
