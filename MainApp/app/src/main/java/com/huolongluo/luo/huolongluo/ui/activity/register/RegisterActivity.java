package com.huolongluo.luo.huolongluo.ui.activity.register;

import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;

import com.gyf.immersionbar.ImmersionBar;
import com.huolongluo.luo.R;
import com.huolongluo.luo.huolongluo.base.BaseActivity;
import com.huolongluo.luo.huolongluo.bean.InviteBean;
import com.huolongluo.luo.huolongluo.bean.UserBean;
import com.huolongluo.luo.huolongluo.manager.DialogClickListener;
import com.huolongluo.luo.huolongluo.share.Share;
import com.huolongluo.luo.util.L;
import com.huolongluo.luo.util.StringUtil;
import com.huolongluo.luo.util.TelNumMatch;
import com.huolongluo.luo.util.TextDrawAbleUtils;
import com.huolongluo.luo.util.ToastSimple;
import com.jakewharton.rxbinding.widget.RxTextView;

import java.util.Random;

import butterknife.BindView;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.SaveListener;

/**
 * <p>
 * Created by 火龙裸 on 2017/9/6.
 */

public class RegisterActivity extends BaseActivity {
    @BindView(R.id.toolbar_center_title)
    TextView toolbar_center_title;
    @BindView(R.id.iv_left)
    ImageView iv_left;
    @BindView(R.id.my_toolbar)
    Toolbar my_toolbar;
    @BindView(R.id.lin1)
    LinearLayout lin1;
    /********************* 以上为ToolBar ************************/
//    private View iv_register_bg;
    @BindView(R.id.et_phone)
    EditText et_phone;
    @BindView(R.id.et_psw)
    EditText et_psw;
    @BindView(R.id.tv_sex_women)
    TextView tv_sex_women;
    @BindView(R.id.tv_sex_man)
    TextView tv_sex_man;
    @BindView(R.id.et_invite_code)
    TextView et_invite_code;
    @BindView(R.id.iv_close_psw)
    ImageView iv_close_psw;
    @BindView(R.id.ll_register)
    LinearLayout ll_register;

    private boolean isHidden = false; // 是否隐藏密码

    private String phone;//输入的手机号码
    private String psw;//输入的密码
    private String inviteCode;//输入的邀请码
    private String sex;//性别

    @Override
    protected int getContentViewId() {
        return R.layout.activity_register;
    }

    @Override
    protected void injectDagger() {
        activityComponent().inject(this);
    }

    private void initToolBar() {
        ImmersionBar.with(this).statusBarDarkFont(true).init();
        toolbar_center_title.setText("注册");
    }

    @Override
    protected void initViewsAndEvents(@Nullable Bundle savedInstanceState) {
        /*iv_register_bg = findViewById(R.id.iv_register_bg);
        startBgAnimation();//开始背景动画*/

        initToolBar();
        eventClick(iv_left).subscribe(o -> // 点击返回键
        {
            close();
        });
        eventClick(iv_close_psw).subscribe(o ->
        {
            if (isHidden) {
                et_psw.setTransformationMethod(PasswordTransformationMethod.getInstance()); // 隐藏密码
                iv_close_psw.setImageResource(R.mipmap.password_icon_unhide);
            } else {
                et_psw.setTransformationMethod(HideReturnsTransformationMethod.getInstance()); // 明文密码
                iv_close_psw.setImageResource(R.mipmap.password_icon_hide);
            }
            isHidden = !isHidden;
            et_psw.setSelection(et_psw.getText().toString().trim().length()); // 将光标移至文字末尾
        });
        eventClick(tv_sex_women).subscribe(o ->
        {
            sex = "女";
            TextDrawAbleUtils.setTextDrawable(this, R.mipmap.ic_register_female_checked, tv_sex_women);
            TextDrawAbleUtils.setTextDrawable(this, R.mipmap.ic_register_male_unchecked, tv_sex_man);
        });
        eventClick(tv_sex_man).subscribe(o ->
        {
            sex = "男";
            TextDrawAbleUtils.setTextDrawable(this, R.mipmap.ic_register_male_checked, tv_sex_man);
            TextDrawAbleUtils.setTextDrawable(this, R.mipmap.ic_register_female_unchecked, tv_sex_women);
        });
        eventClick(ll_register).subscribe(o ->
        {
            if (TextUtils.isEmpty(phone)) {
                View view = LayoutInflater.from(this).inflate(R.layout.toast_text, null);
                TextView tv_toast_message = view.findViewById(R.id.tv_toast_message);
                tv_toast_message.setText("请先填写手机号码");
                ToastSimple.makeText(Gravity.CENTER, 1, view).show();
            } else if (!StringUtil.judgePhoneNums(phone)) {
                View view = LayoutInflater.from(this).inflate(R.layout.toast_text, null);
                TextView tv_toast_message = view.findViewById(R.id.tv_toast_message);
                tv_toast_message.setText("请输入正确的手机号");
                ToastSimple.makeText(Gravity.CENTER, 1, view).show();
            } else if (TextUtils.isEmpty(sex)) {
                View view = LayoutInflater.from(this).inflate(R.layout.toast_text, null);
                TextView tv_toast_message = view.findViewById(R.id.tv_toast_message);
                tv_toast_message.setText("请输入性别");
                ToastSimple.makeText(Gravity.CENTER, 1, view).show();
            }
            /*else if (TextUtils.isEmpty(inviteCode)) {
                View view = LayoutInflater.from(this).inflate(R.layout.toast_text, null);
                TextView tv_toast_message = view.findViewById(R.id.tv_toast_message);
                tv_toast_message.setText("请输入邀请码");
                ToastSimple.makeText(Gravity.CENTER, 1, view).show();
            }*/
            else if (TextUtils.isEmpty(psw) || !StringUtil.pswIsLegal(psw) || StringUtil.isContainChinese(psw)) {
                View view = LayoutInflater.from(this).inflate(R.layout.toast_text, null);
                TextView tv_toast_message = view.findViewById(R.id.tv_toast_message);
                tv_toast_message.setText("密码需为8-20位字母或数字");
                ToastSimple.makeText(Gravity.CENTER, 1, view).show();
            } else {
                if (TelNumMatch.isValidPhoneNumber(phone)) {
                    showProgressDialog("");
                    UserBean user = new UserBean();
                    user.setUsername(phone);
                    user.setPassword(psw);
                    user.setSex(sex);
                    user.setInviteCode(inviteCode);
                    user.setDeviceId(Share.get().getDeviceId());

                    user.signUp(new SaveListener<UserBean>() {
                        @Override
                        public void done(UserBean user, BmobException e) {
                            if (e == null) {
                                /*View view = LayoutInflater.from(RegisterActivity.this).inflate(R.layout.toast_text, null);
                                TextView tv_toast_message = view.findViewById(R.id.tv_toast_message);
                                tv_toast_message.setText("注册成功");
                                ToastSimple.makeText(Gravity.CENTER, 2, view).show();*/

                                showConfirmNoCancelDialog("提示", "注册成功，请返回登录", new DialogClickListener() {
                                    @Override
                                    public void onConfirm() {
                                        close();
                                    }

                                    @Override
                                    public void onCancel() {
                                    }
                                });

                                //生成获取随机头像
                                String faceUrl = String.format("https://picsum.photos/id/%d/200/200", new Random().nextInt(1000));
                                Share.get().setHeadPic(faceUrl);
                                insertInviteBean();
                            } else {
                                L.e("注册失败了：" + e.toString());
                                View view = LayoutInflater.from(RegisterActivity.this).inflate(R.layout.toast_text, null);
                                TextView tv_toast_message = view.findViewById(R.id.tv_toast_message);
                                tv_toast_message.setText("注册失败");
                                ToastSimple.makeText(Gravity.CENTER, 1, view).show();
                            }
                            hideProgressDialog();
                        }
                    });
                } else {
                    View view = LayoutInflater.from(this).inflate(R.layout.toast_text, null);
                    TextView tv_toast_message = view.findViewById(R.id.tv_toast_message);
                    tv_toast_message.setText("手机号码不正确");
                    ToastSimple.makeText(Gravity.CENTER, 1, view).show();
                }
            }
        });
        RxTextView.afterTextChangeEvents(et_phone).subscribe(event ->
        {
            phone = event.view().getText().toString().trim();
            psw = et_psw.getText().toString().trim();
            inviteCode = et_invite_code.getText().toString().trim();
        });
        RxTextView.afterTextChangeEvents(et_psw).subscribe(event ->
        {
            psw = event.view().getText().toString().trim();
            phone = et_phone.getText().toString().trim();
            inviteCode = et_invite_code.getText().toString().trim();
        });
        RxTextView.afterTextChangeEvents(et_invite_code).subscribe(event ->
        {
            inviteCode = event.view().getText().toString().trim();
            phone = et_phone.getText().toString().trim();
            psw = et_psw.getText().toString().trim();
        });
    }

    /**
     * 插入一条邀请数据
     */
    private void insertInviteBean() {
        if (!TextUtils.isEmpty(inviteCode)) {
            InviteBean inviteBean = new InviteBean();
            inviteBean.setUsername(phone);
            inviteBean.setBeInvitedUsername(inviteCode);
//            inviteBean.setBeInvitedSex("");
            inviteBean.save(new SaveListener<String>() {
                @Override
                public void done(String objectId, BmobException e) {
                    if (e == null) {
                        L.e("添加一条邀请数据成功，返回objectId为：" + objectId);
                        Share.get().setMyInviteUserName(inviteCode);
                    } else {
                        L.e("创建数据失败：" + e.getMessage());
                    }
                }
            });
        }
    }

    /**
     * 背景微动画
     */
    /*private void startBgAnimation() {
        Animation animation = AnimationUtils.loadAnimation(RegisterActivity.this, R.anim.seal_login_bg_translate_anim);
        iv_register_bg.startAnimation(animation);
    }*/
}
