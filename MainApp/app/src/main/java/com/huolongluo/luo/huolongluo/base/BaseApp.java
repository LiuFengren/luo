package com.huolongluo.luo.huolongluo.base;

import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.util.Log;

import androidx.multidex.MultiDex;

import com.huolongluo.luo.base.s;
import com.huolongluo.luo.huolongluo.injection.component.ApplicationComponent;
import com.huolongluo.luo.huolongluo.injection.component.DaggerApplicationComponent;
import com.huolongluo.luo.huolongluo.injection.model.ApiModule;
import com.huolongluo.luo.huolongluo.injection.model.ApplicationModule;
import com.huolongluo.luo.huolongluo.share.Share;
import com.huolongluo.luo.huolongluo.ui.signature.GenerateTestUserSig;
import com.tencent.qcloud.tuicore.TUILogin;

import cn.bmob.v3.Bmob;

/**
 * Created by 火龙裸 on 2017/8/10 0010.
 */

public class BaseApp extends Application
{
    private static final String TAG = "BaseApp";
    private ApplicationComponent mAppComponent;

    @Override
    protected void attachBaseContext(Context base)
    {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        Bmob.initialize(this, "b48294fd9404f2a9c286b54c9ca94c8d");//Bmob
        /**
         * 做一些初始化操作
         * */
        Log.e(TAG, "onCreate: " + "执行了BaseApp基类============== 做一些初始化操作 =============");

        s.Ext.init(this);
        s.Ext.setDebug(true);              //日志开关

//        getDeviceInfo();
        Log.e(TAG, "onCreate: 设备信息为：" + getDeviceInfo2());


        // 在程序启动的时候初始化 TUI 组件，通常是在 Application 的 onCreate 中进行初始化：
        TUILogin.init(this, GenerateTestUserSig.SDKAPPID, null, null);
    }

    public void init(int sdkAppId) {
        if (sdkAppId != 0) {
            TUILogin.init(this, sdkAppId, null, null);
        } else {
            TUILogin.init(this, GenerateTestUserSig.SDKAPPID, null, null);
        }
    }

    public ApplicationComponent getAppComponent()
    {
        if (null == mAppComponent)
        {
            mAppComponent = (DaggerApplicationComponent) DaggerApplicationComponent.builder().applicationModule(new ApplicationModule(this)).apiModule(new ApiModule(this)).build();
        }
        return mAppComponent;
    }

    public static BaseApp get(Context context)
    {
        return (BaseApp) context.getApplicationContext();
    }

    public void setAppComponent(ApplicationComponent mAppComponent)
    {
        this.mAppComponent = (DaggerApplicationComponent) mAppComponent;
    }

    private void getDeviceInfo() {
        String manufacturer = Build.MANUFACTURER;// 制造商
        String deviceId = Build.SERIAL;//设备序列号

        Log.d(TAG, "型号： " + Build.MODEL);
        Log.d(TAG, "制造： " + manufacturer);
        Log.d(TAG, "序列号1： " + deviceId);
        Log.d(TAG, "ID： " + Build.ID);
        Log.d(TAG, "PRODUCT： " + Build.PRODUCT);
        Log.d(TAG, "DISPLAY： " + Build.DISPLAY);
        Log.d(TAG, "BRAND： " + Build.BRAND);

        Share.get().setDeviceId(deviceId);//设备id
    }

    public static String getDeviceInfo2() {
        StringBuffer sb = new StringBuffer();
        sb.append("主板： "+ Build.BOARD+"\n");
        sb.append("系统启动程序版本号： " + Build.BOOTLOADER+"\n");//unknown
        sb.append("系统定制商：" + Build.BRAND+"\n");
        sb.append("cpu指令集： " + Build.CPU_ABI+"\n");
        sb.append("cpu指令集2 "+ Build.CPU_ABI2+"\n");
        sb.append("设置参数： "+ Build.DEVICE+"\n");
        sb.append("显示屏参数：" + Build.DISPLAY+"\n");
        sb.append("无线电固件版本：" + Build.getRadioVersion()+"\n");
        sb.append("硬件识别码：" + Build.FINGERPRINT+"\n");
        sb.append("硬件名称：" + Build.HARDWARE+"\n");
        sb.append("HOST: " + Build.HOST+"\n");
        sb.append("修订版本列表：" + Build.ID+"\n");
        sb.append("硬件制造商：" + Build.MANUFACTURER+"\n");
        sb.append("版本：" + Build.MODEL+"\n");
        sb.append("硬件序列号：" + Build.SERIAL+"\n");//unknown
        sb.append("手机制造商：" + Build.PRODUCT+"\n");
        sb.append("描述Build的标签：" + Build.TAGS+"\n");
        sb.append("TIME: " + Build.TIME+"\n");
        sb.append("builder类型：" + Build.TYPE+"\n");
        sb.append("USER: " + Build.USER+"\n");
        return sb.toString();
    }
}
