package com.huolongluo.luo.huolongluo.bean;

import cn.bmob.v3.BmobObject;

public class FeedBackBean extends BmobObject {
    private String username;//用户名
    private String feedBackContent;//反馈内容

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFeedBackContent() {
        return feedBackContent;
    }

    public void setFeedBackContent(String feedBackContent) {
        this.feedBackContent = feedBackContent;
    }

    @Override
    public String toString() {
        return "FeedBackBean{" +
                "username='" + username + '\'' +
                ", feedBackContent='" + feedBackContent + '\'' +
                '}';
    }
}
