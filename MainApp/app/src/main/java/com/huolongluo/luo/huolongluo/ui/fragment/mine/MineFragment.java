package com.huolongluo.luo.huolongluo.ui.fragment.mine;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;

import com.huolongluo.luo.R;
import com.huolongluo.luo.huolongluo.base.BaseActivity;
import com.huolongluo.luo.huolongluo.base.BaseFragment;
import com.huolongluo.luo.huolongluo.bean.ConsumeBean;
import com.huolongluo.luo.huolongluo.bean.FollowBean;
import com.huolongluo.luo.huolongluo.bean.RechargeRecordBean;
import com.huolongluo.luo.huolongluo.bean.UserBasicInfoBean;
import com.huolongluo.luo.huolongluo.bean.UserBean;
import com.huolongluo.luo.huolongluo.bean.UserWalletBean;
import com.huolongluo.luo.huolongluo.bean.VisitBean;
import com.huolongluo.luo.huolongluo.constants.Constants;
import com.huolongluo.luo.huolongluo.manager.DialogClickListener;
import com.huolongluo.luo.huolongluo.share.Share;
import com.huolongluo.luo.huolongluo.ui.activity.account.MyAccountActivity;
import com.huolongluo.luo.huolongluo.ui.activity.feedback.FeedBackActivity;
import com.huolongluo.luo.huolongluo.ui.activity.follow.FollowActivity;
import com.huolongluo.luo.huolongluo.ui.activity.hongniang.HongNiangActivity;
import com.huolongluo.luo.huolongluo.ui.activity.invite.InviteActivity;
import com.huolongluo.luo.huolongluo.ui.activity.jiangli.JiangLiActivity;
import com.huolongluo.luo.huolongluo.ui.activity.login.LoginActivity;
import com.huolongluo.luo.huolongluo.ui.activity.setting.SettingActivity;
import com.huolongluo.luo.huolongluo.ui.activity.userdetail.UserDetailInfoActivity;
import com.huolongluo.luo.huolongluo.ui.activity.vip.VipActivity;
import com.huolongluo.luo.huolongluo.ui.activity.visit.VisitActivity;
import com.huolongluo.luo.manager.pay.wechat.WxConfig;
import com.huolongluo.luo.util.TextDrawAbleUtils;
import com.huolongluo.luo.util.ToastSimple;
import com.huolongluo.luo.util.engine.MyGlideEngine;
import com.luck.picture.lib.animators.AnimationType;
import com.luck.picture.lib.app.PictureAppMaster;
import com.luck.picture.lib.basic.PictureSelectionModel;
import com.luck.picture.lib.basic.PictureSelector;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.config.PictureSelectionConfig;
import com.luck.picture.lib.config.SelectLimitType;
import com.luck.picture.lib.config.SelectMimeType;
import com.luck.picture.lib.config.SelectModeConfig;
import com.luck.picture.lib.engine.UriToFileTransformEngine;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.entity.MediaExtraInfo;
import com.luck.picture.lib.interfaces.OnKeyValueResultCallbackListener;
import com.luck.picture.lib.interfaces.OnSelectLimitTipsListener;
import com.luck.picture.lib.language.LanguageConfig;
import com.luck.picture.lib.style.PictureSelectorStyle;
import com.luck.picture.lib.utils.MediaUtils;
import com.luck.picture.lib.utils.SandboxTransformUtils;
import com.tencent.imsdk.v2.V2TIMConversation;
import com.tencent.liteav.trtccalling.model.util.ImageLoader;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.tencent.qcloud.tuicore.TUIConstants;
import com.tencent.qcloud.tuicore.TUICore;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.BmobUser;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.CountListener;
import cn.bmob.v3.listener.FindListener;

/**
 * Created by 火龙裸 on 2018/5/26 0026.
 */

public class MineFragment extends BaseFragment implements MineContract.View {
    private static final String TAG = "MineFragment";
    @Inject
    MinePresent present;
    @BindView(R.id.iv_user_header)
    ImageView iv_user_header;
    @BindView(R.id.iv_vip)
    ImageView iv_vip;
    @BindView(R.id.tv_user_nick_name)
    TextView tv_user_nick_name;
    @BindView(R.id.tv_copy_invite_code)
    TextView tv_copy_invite_code;
    @BindView(R.id.iv_edit_user_info)
    ImageView iv_edit_user_info;
    @BindView(R.id.ll_my_guanzhu_who)
    LinearLayout ll_my_guanzhu_who;
    @BindView(R.id.tv_my_guanzhu_who_num)
    TextView tv_my_guanzhu_who_num;
    @BindView(R.id.ll_who_guanzhu_me)
    LinearLayout ll_who_guanzhu_me;
    @BindView(R.id.tv_who_guanzhu_me_num)
    TextView tv_who_guanzhu_me_num;
    @BindView(R.id.ll_viewed_me)
    LinearLayout ll_viewed_me;
    @BindView(R.id.tv_who_viewed_me_num)
    TextView tv_who_viewed_me_num;
    @BindView(R.id.tv_vip)
    TextView tv_vip;
    @BindView(R.id.tv_jiangli)
    TextView tv_jiangli;
    @BindView(R.id.tv_chongzhi)
    TextView tv_chongzhi;
    @BindView(R.id.tv_yaoqing)
    TextView tv_yaoqing;
    @BindView(R.id.tv_hongniang)
    TextView tv_hongniang;
    @BindView(R.id.tv_wentifankui)
    TextView tv_wentifankui;
    @BindView(R.id.tv_online_help)
    TextView tv_online_help;
    @BindView(R.id.tv_setting)
    TextView tv_setting;

    private UserBasicInfoBean currentUserBasicInfo;
    private ActivityResultLauncher<Intent> launcherResult;

    private IWXAPI api;

    public static MineFragment getInstance() {
        Bundle args = new Bundle();
        MineFragment fragment = new MineFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getContentViewId() {
        return R.layout.fragment_mine;
    }

    @Override
    protected void initDagger() {
        ((BaseActivity) getActivity()).activityComponent().inject(this);
        present.attachView(this);
    }

    /**
     * 处理选择结果
     *
     * @param result
     */
    private void analyticalSelectResults(ArrayList<LocalMedia> result) {
        StringBuilder builder = new StringBuilder();
        builder.append("Result").append("\n");
        for (LocalMedia media : result) {
            if (media.getWidth() == 0 || media.getHeight() == 0) {
                if (PictureMimeType.isHasImage(media.getMimeType())) {
                    MediaExtraInfo imageExtraInfo = MediaUtils.getImageSize(getActivity(), media.getPath());
                    media.setWidth(imageExtraInfo.getWidth());
                    media.setHeight(imageExtraInfo.getHeight());
                } else if (PictureMimeType.isHasVideo(media.getMimeType())) {
                    MediaExtraInfo videoExtraInfo = MediaUtils.getVideoSize(PictureAppMaster.getInstance().getAppContext(), media.getPath());
                    media.setWidth(videoExtraInfo.getWidth());
                    media.setHeight(videoExtraInfo.getHeight());
                }
            }
            builder.append(media.getAvailablePath()).append("\n");
            Log.i(TAG, "文件名: " + media.getFileName());
            Log.i(TAG, "是否压缩:" + media.isCompressed());
            Log.i(TAG, "压缩:" + media.getCompressPath());
            Log.i(TAG, "原图:" + media.getPath());
            Log.i(TAG, "绝对路径:" + media.getRealPath());
            Log.i(TAG, "是否裁剪:" + media.isCut());
            Log.i(TAG, "裁剪:" + media.getCutPath());
            Log.i(TAG, "是否开启原图:" + media.isOriginal());
            Log.i(TAG, "原图路径:" + media.getOriginalPath());
            Log.i(TAG, "沙盒路径:" + media.getSandboxPath());
            Log.i(TAG, "原始宽高: " + media.getWidth() + "x" + media.getHeight());
            Log.i(TAG, "裁剪宽高: " + media.getCropImageWidth() + "x" + media.getCropImageHeight());
            Log.i(TAG, "文件大小: " + media.getSize());
            ImageLoader.loadImage(getActivity(), iv_user_header, media.getRealPath());

            UserBasicInfoBean.saveUserHeadPic(result, currentUserBasicInfo);

            break;
        }
        Log.e(TAG, "analyticalSelectResults: 图片选择结果：" + builder);
    }

    /**
     * 自定义沙盒文件处理
     */
    private static class MeSandboxFileEngine implements UriToFileTransformEngine {
        @Override
        public void onUriToFileAsyncTransform(Context context, String srcPath, String mineType, OnKeyValueResultCallbackListener call) {
            if (call != null) {
                call.onCallback(srcPath, SandboxTransformUtils.copyPathToSandbox(context, srcPath, mineType));
            }
        }
    }

    /**
     * 拦截自定义提示
     */
    private static class MeOnSelectLimitTipsListener implements OnSelectLimitTipsListener {

        @Override
        public boolean onSelectLimitTips(Context context, PictureSelectionConfig config, int limitType) {
            if (limitType == SelectLimitType.SELECT_NOT_SUPPORT_SELECT_LIMIT) {
                ToastSimple.show("暂不支持的选择类型");
                return true;
            }
            return false;
        }
    }

    /**
     * 创建一个ActivityResultLauncher
     *
     * @return
     */
    private ActivityResultLauncher<Intent> createActivityResultLauncher() {
        return registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult result) {
                        int resultCode = result.getResultCode();
                        if (resultCode == -1) {
                            ArrayList<LocalMedia> selectList = PictureSelector.obtainSelectorList(result.getData());
                            analyticalSelectResults(selectList);
                        } else if (resultCode == 0) {
                            Log.e(TAG, "图片选择错误 onActivityResult PictureSelector Cancel");
                        }
                    }
                });
    }

    @Override
    protected void initViewsAndEvents(View rootView) {
        api = WXAPIFactory.createWXAPI(getActivity(), WxConfig.APP_ID, false);
        // 注册需要写在onCreate或Fragment onAttach里，否则会报java.lang.IllegalStateException异常
        launcherResult = createActivityResultLauncher();

        eventClick(iv_user_header).subscribe(o -> // 编辑用户头像
        {
            Log.e(TAG, "initViewsAndEvents: 编辑头像");
            /*// 进入系统相册
            PictureSelectionSystemModel systemGalleryMode = PictureSelector.create(getActivity())
                    .openSystemGallery(SelectMimeType.ofImage())
                    .setSelectionMode(SelectModeConfig.SINGLE)
//                    .setCompressEngine(null)//是否压缩图片
//                    .setCropEngine(null)//是否裁剪图片
                    .setSkipCropMimeType(null)//跳过裁剪GIF
                    .setAddBitmapWatermarkListener(null)//添加水印
                    .setVideoThumbnailListener(null)//处理视频缩略图
                    .isOriginalControl(false)//是否开启原图功能
                    .setPermissionDescriptionListener(null)//添加权限说明
                    .setSandboxFileEngine(new MeSandboxFileEngine());
            forSystemResult(systemGalleryMode);*/
            // 进入相册
            PictureSelectionModel selectionModel = PictureSelector.create(getContext())
                    .openGallery(SelectMimeType.ofImage())
                    .setSelectorUIStyle(new PictureSelectorStyle())
                    .setImageEngine(MyGlideEngine.createGlideEngine())
//                    .setCropEngine(getCropFileEngine())
//                    .setCompressEngine(getCompressFileEngine())
                    .setSandboxFileEngine(new MeSandboxFileEngine())
                    .setCameraInterceptListener(null)
//                    .setRecordAudioInterceptListener(new MeOnRecordAudioInterceptListener())//录音回调事件
                    .setSelectLimitTipsListener(new MeOnSelectLimitTipsListener())
                    .setEditMediaInterceptListener(null)
                    .setPermissionDescriptionListener(null)//添加权限说明
                    .setPreviewInterceptListener(null)//自定义预览
                    .setPermissionDeniedListener(null)//权限拒绝后回调
                    .setAddBitmapWatermarkListener(null)//给图片添加水印
                    .setVideoThumbnailListener(null)//处理视频缩略图
//                              .setQueryFilterListener(new OnQueryFilterListener() {
//                                    @Override
//                                    public boolean onFilter(String absolutePath) {
//                                        return PictureMimeType.isUrlHasVideo(absolutePath);
//                                    }
//                                })
                    //.setExtendLoaderEngine(getExtendLoaderEngine())
                    .setInjectLayoutResourceListener(null)//注入自定义布局
                    .setSelectionMode(SelectModeConfig.SINGLE)
                    .setLanguage(LanguageConfig.UNKNOWN_LANGUAGE)
                    .setQuerySortOrder("")
                    .setOutputCameraDir("")
                    .setOutputAudioDir("")
                    .setQuerySandboxDir("")
                    .isDisplayTimeAxis(true)//显示资源时间轴
                    .isOnlyObtainSandboxDir(false)
                    .isPageStrategy(true)
                    .isOriginalControl(false)
                    .isDisplayCamera(true)
                    .isOpenClickSound(false)
//                    .setSkipCropMimeType(null)
                    .isFastSlidingSelect(true)
                    //.setOutputCameraImageFileName("luck.jpeg")
                    //.setOutputCameraVideoFileName("luck.mp4")
                    .isWithSelectVideoImage(true)
                    .isPreviewFullScreenMode(true)
                    .isPreviewZoomEffect(true)
                    .isPreviewImage(true)
                    .isPreviewVideo(true)
                    .isPreviewAudio(true)
                    //.setQueryOnlyMimeType(PictureMimeType.ofGIF())
                    .isMaxSelectEnabledMask(true)
                    .isDirectReturnSingle(true)
                    .setMaxSelectNum(1)
                    .setMaxVideoSelectNum(1)
                    .setRecyclerAnimationMode(AnimationType.DEFAULT_ANIMATION)
                    .isGif(false);
//                    .setSelectedData(mAdapter.getData());
//            forSelectResult(selectionModel);
            selectionModel.forResult(launcherResult);
        });
        eventClick(tv_copy_invite_code).subscribe(o -> // 复制邀请码
        {
            ClipboardManager cm =(ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);//创建普通字符型ClipData
            ClipData mClipData = ClipData.newPlainText("simple text", Share.get().getUserName());//将ClipData内容放到系统剪贴板里。
            cm.setPrimaryClip(mClipData);

            View view = LayoutInflater.from(getActivity()).inflate(R.layout.toast_text, null);
            TextView tv_toast_message = view.findViewById(R.id.tv_toast_message);
            tv_toast_message.setText("邀请码已复制");
            ToastSimple.makeText(Gravity.CENTER, 1, view).show();
        });
        eventClick(iv_edit_user_info).subscribe(o -> // 编辑用户信息
        {
            Bundle bundle = new Bundle();
            bundle.putString(Constants.USER_NAME, Share.get().getUserName());
            startActivity(UserDetailInfoActivity.class, bundle);
        });
        eventClick(ll_my_guanzhu_who).subscribe(o -> // 我的关注
        {
            Log.e(TAG, "initViewsAndEvents: 我的关注");
            Bundle bundle = new Bundle();
            bundle.putInt(Constants.FOLLOW_TYPE, 1);
            startActivity(FollowActivity.class, bundle);
        });
        eventClick(ll_who_guanzhu_me).subscribe(o -> // 谁关注我
        {
            Bundle bundle = new Bundle();
            bundle.putInt(Constants.FOLLOW_TYPE, 2);
            startActivity(FollowActivity.class, bundle);
        });
        eventClick(ll_viewed_me).subscribe(o -> // 谁看过我
        {
            startActivity(VisitActivity.class);
        });
        eventClick(tv_vip).subscribe(o -> // 会员中心
        {
            startActivity(VipActivity.class);
        });
        eventClick(tv_jiangli).subscribe(o -> // 奖励
        {
            startActivity(JiangLiActivity.class);
        });
        eventClick(tv_chongzhi).subscribe(o -> // 充值
        {
            Log.e(TAG, "initViewsAndEvents: 充值");
            startActivity(MyAccountActivity.class);
        });
        eventClick(tv_yaoqing).subscribe(o -> // 邀请有礼
        {
            startActivity(InviteActivity.class);
        });
        eventClick(tv_hongniang).subscribe(o -> // 红娘牵线
        {
//            startActivity(HongNiangActivity.class);
            showConfirmDialog("提示", "添加红娘微信，获取永久免费聊天等权益，助力快速脱单。", new DialogClickListener() {
                @Override
                public void onConfirm() {
                    ClipboardManager cm = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);//创建普通字符型ClipData
                    ClipData mClipData = ClipData.newPlainText("simple text", "huolongluoS");//将ClipData内容放到系统剪贴板里。
                    cm.setPrimaryClip(mClipData);

                    ToastSimple.show("微信号已复制");

                    if (api != null) {
                        api.openWXApp();
                    }
                }

                @Override
                public void onCancel() {
                }
            });
        });
        eventClick(tv_wentifankui).subscribe(o -> // 问题反馈
        {
            startActivity(FeedBackActivity.class);
        });
        eventClick(tv_online_help).subscribe(o -> // 在线客服
        {
            UserBean userBean = BmobUser.getCurrentUser(UserBean.class);
            if (userBean != null) {
                String kefuAccount = userBean.getKefu();
                if (TextUtils.isEmpty(kefuAccount)) {
                    kefuAccount = Share.get().getKeFuAccount();
                }
                startChatActivity(kefuAccount, V2TIMConversation.V2TIM_C2C, "在线客服", "");
            } else {
                ToastSimple.show("登录状态失效，请重新登录", 2);
            }
        });
        eventClick(tv_setting).subscribe(o -> // 设置
        {
            startActivity(SettingActivity.class);
        });
    }

    /**
     * 启动聊天界面去发送消息
     *
     * @param chatId    用户ID
     * @param chatType  V2TIMConversation.V2TIM_C2C单聊；
     * @param chatName  聊天窗口顶部显示的名称文字
     * @param groupType 单聊的话，直接传空字符串
     */
    public static void startChatActivity(String chatId, int chatType, String chatName, String groupType) {
        if (TextUtils.isEmpty(chatId)) {
            return;
        }
        Bundle bundle = new Bundle();
        bundle.putString(TUIConstants.TUIChat.CHAT_ID, chatId);
        bundle.putString(TUIConstants.TUIChat.CHAT_NAME, chatName);
        bundle.putInt(TUIConstants.TUIChat.CHAT_TYPE, chatType);
        if (chatType == V2TIMConversation.V2TIM_C2C) {
            //启动单聊
            TUICore.startActivity(TUIConstants.TUIChat.C2C_CHAT_ACTIVITY_NAME, bundle);
        } else if (chatType == V2TIMConversation.V2TIM_GROUP) {
            //启动群聊
            bundle.putString(TUIConstants.TUIChat.GROUP_TYPE, groupType);
            TUICore.startActivity(TUIConstants.TUIChat.GROUP_CHAT_ACTIVITY_NAME, bundle);
        }
    }

    /**
     * 当通过fragment的hide，show方法去切换fragment时才会执行
     */
//    @Override
//    public void onHiddenChanged(boolean hidden) {
//        super.onHiddenChanged(hidden);
//        if (hidden) {   // 不在最前端显示 相当于调用了onPause();
//            L.e("=============== 不在最前端显示 相当于调用了onPause() =============");
//            Log.e(TAG, "onHiddenChanged: ");
//            return;
//        } else {  // 在最前端显示 相当于调用了onResume()  但是当该Fragment没有实例化的情况下，不会走这里;
//            //网络数据刷新
//            Log.e(TAG, "onHiddenChanged: ");
//            L.e("================= 在最前端显示 相当于调用了onResume() =====================");
////            checkUserInfo();
//        }
//    }
    @Override
    public void onResume() {
        super.onResume();
        Log.e(TAG, "onResume: ===========>");
        ImageLoader.loadImage(getActivity(), iv_user_header, Share.get().getHeadPic(), R.mipmap.default_header);
        if (TextUtils.isEmpty(Share.get().getNickName())) {
            tv_user_nick_name.setText(Share.get().getUserName());
        } else {
            tv_user_nick_name.setText(Share.get().getNickName());
        }
        if (TextUtils.equals("男", Share.get().getSex())) {
            TextDrawAbleUtils.setTextDrawable(getActivity(), R.mipmap.ic_register_male_checked, tv_user_nick_name, 3);
        } else {
            TextDrawAbleUtils.setTextDrawable(getActivity(), R.mipmap.ic_register_female_checked, tv_user_nick_name, 3);
        }
        //查询关注与被关注
        queryMyFollowCount();
        queryBeFollowCount();
        //查询访问过我的人
        queryVisitMeCount();
        //查询用户个人信息
        queryUserBasicInfo();
        //查询个人钱包余额，是否会员
        queryUserWallet();

        //查询用户充值次数
        RechargeRecordBean.queryRechargeCount();
        ConsumeBean.queryConsumeConfig();
    }

    /**
     * 查询用户钱包余额，是否会员
     */
    private void queryUserWallet() {
        BmobQuery<UserWalletBean> categoryBmobQuery = new BmobQuery<>();
        categoryBmobQuery.addWhereEqualTo("username", Share.get().getUserName());
        categoryBmobQuery.findObjects(new FindListener<UserWalletBean>() {
            @Override
            public void done(List<UserWalletBean> result, BmobException e) {
                if (e == null) {
                    if (!result.isEmpty()) {
                        UserWalletBean userWalletBean = result.get(0);
                        Share.get().setUser_wallet_bean_object_id(userWalletBean.getObjectId());
                        Share.get().setIsVip(userWalletBean.isVip());
                        Share.get().setGoldBalance(userWalletBean.getGoldBalance());
                        Share.get().setIntegralBalance(userWalletBean.getIntegralBalance());

                        if (Share.get().getIsVip()) {
                            iv_vip.setVisibility(View.VISIBLE);
                        } else {
                            iv_vip.setVisibility(View.INVISIBLE);
                        }
                    } else {
//                        createUserWallet();//给用户创建一个钱包
                        UserWalletBean.createUserWallet();
                    }
                } else {
                    Log.e(TAG, e.toString());
                }
            }
        });
    }

//    /**
//     * 给用户创建一个钱包
//     * */
//    private void createUserWallet() {
//        UserWalletBean p2 = new UserWalletBean();
//        p2.setUsername(Share.get().getUserName());
//        p2.setGoldBalance(100);
//        p2.save(new SaveListener<String>() {
//            @Override
//            public void done(String objectId,BmobException e) {
//                if(e==null){
//                    Log.e(TAG, "给用户创建一个钱包 数据成功，返回objectId为："+objectId);
//                    Share.get().setGoldBalance(100);
//                }else{
//                    Log.e(TAG, "给用户创建一个钱包 创建数据失败：" + e.getMessage());
//                }
//            }
//        });
//    }

    /**
     * 查询访问过我的人的数量
     */
    private void queryVisitMeCount() {
        BmobQuery<VisitBean> query = new BmobQuery<VisitBean>();
        query.addWhereEqualTo("visitToUserName", Share.get().getUserName());
        query.count(VisitBean.class, new CountListener() {
            @Override
            public void done(Integer count, BmobException e) {
                if (e == null) {
                    Log.e(TAG, "查询被访问 count对象个数为：" + count);
                    tv_who_viewed_me_num.setText(count + "");
                } else {
                    Log.e(TAG, "查询被访问 失败：" + e.getMessage() + "," + e.getErrorCode());
                }
            }
        });
    }

    /**
     * 查询我关注的数量
     */
    private void queryMyFollowCount() {
        BmobQuery<FollowBean> query = new BmobQuery<FollowBean>();
        query.addWhereEqualTo("username", Share.get().getUserName());
        query.count(FollowBean.class, new CountListener() {
            @Override
            public void done(Integer count, BmobException e) {
                if (e == null) {
                    Log.d(TAG, "查询我的关注 count对象个数为：" + count);
                    tv_my_guanzhu_who_num.setText(count + "");
                } else {
                    Log.i(TAG, "查询我的关注 失败：" + e.getMessage() + "," + e.getErrorCode());
                }
            }
        });
    }

    /**
     * 查询我被关注的数量
     */
    private void queryBeFollowCount() {
        BmobQuery<FollowBean> query = new BmobQuery<FollowBean>();
        query.addWhereEqualTo("followToUserName", Share.get().getUserName());
        query.count(FollowBean.class, new CountListener() {
            @Override
            public void done(Integer count, BmobException e) {
                if (e == null) {
                    Log.d(TAG, "查询被关注 count对象个数为：" + count);
                    tv_who_guanzhu_me_num.setText(count + "");
                } else {
                    Log.i(TAG, "查询被关注 失败：" + e.getMessage() + "," + e.getErrorCode());
                }
            }
        });
    }

    private void queryUserBasicInfo() {
        //查找UserBasicInfoBean表里面username为自己的数据
        BmobQuery<UserBasicInfoBean> categoryBmobQuery = new BmobQuery<>();
        categoryBmobQuery.addWhereEqualTo("username", Share.get().getUserName());
        categoryBmobQuery.findObjects(new FindListener<UserBasicInfoBean>() {
            @Override
            public void done(List<UserBasicInfoBean> result, BmobException e) {
                if (e == null) {
                    if (!result.isEmpty()) {
                        Log.e(TAG, "查询成功 当前用户的objectId>>>>>：" + result.get(0).getObjectId());
                        currentUserBasicInfo = result.get(0);
                        UserBasicInfoBean.updateLocalSpUserBasicInfo(currentUserBasicInfo);
                        if (tv_user_nick_name != null) {
                            tv_user_nick_name.setText(TextUtils.isEmpty(Share.get().getNickName()) ? Share.get().getUserName() : Share.get().getNickName());
                            if (TextUtils.equals("男", Share.get().getSex())) {
                                TextDrawAbleUtils.setTextDrawable(getActivity(), R.mipmap.ic_register_male_checked, tv_user_nick_name, 3);
                            } else {
                                TextDrawAbleUtils.setTextDrawable(getActivity(), R.mipmap.ic_register_female_checked, tv_user_nick_name, 3);
                            }
                        }
                    } else {
                        Log.e(TAG, "done: 查找UserBasicInfoBean表里面username为自己的数据 为空");
                    }
                } else {
                    Log.e(TAG, e.getMessage());
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        present.detachView();
//        EventBus.getDefault().unregister(this);
    }
}
