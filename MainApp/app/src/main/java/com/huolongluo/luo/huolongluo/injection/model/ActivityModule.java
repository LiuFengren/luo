package com.huolongluo.luo.huolongluo.injection.model;

import android.app.Activity;
import android.content.Context;

import com.huolongluo.luo.base.ActivityContext;
import com.huolongluo.luo.base.PerActivity;
import com.huolongluo.luo.huolongluo.net.okhttp.Api;
import com.huolongluo.luo.huolongluo.net.okhttp.ApiCache;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;

/**
 * <p>
 * Created by 火龙裸 on 2020/3/12.
 */

@Module
public class ActivityModule
{
    private Activity mActivity;
    
    public ActivityModule(Activity mActivity)
    {
        this.mActivity = mActivity;
    }

    @Provides
    Activity provideActivity()
    {
        return mActivity;
    }

    @Provides
    @ActivityContext
    Context provideContext()
    {
        return mActivity;
    }

    @Provides
    @PerActivity
    public Api provideApi(@Named("api") OkHttpClient okHttpClient, @ActivityContext Context mContext)
    {
        return new Api(okHttpClient, mContext);
    }

    @Provides
    @PerActivity
    public ApiCache provideApiCache(@Named("apiCache") OkHttpClient okHttpClient, @ActivityContext Context mContext)
    {
        return new ApiCache(okHttpClient, mContext);
    }
}
