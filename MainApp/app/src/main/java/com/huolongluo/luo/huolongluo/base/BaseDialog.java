package com.huolongluo.luo.huolongluo.base;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.huolongluo.luo.R;
import com.huolongluo.luo.base.BaseView;
import com.huolongluo.luo.huolongluo.manager.DialogClickListener;
import com.huolongluo.luo.huolongluo.manager.DialogManager2;
import com.huolongluo.luo.huolongluo.manager.StateLayoutManager;
import com.huolongluo.luo.util.ToastSimple;
import com.jakewharton.rxbinding.view.RxView;

import java.util.concurrent.TimeUnit;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import rx.Observable;
import rx.Subscription;

/**
 * <p>
 * Created by 火龙裸 on 2017/8/21.
 */

public abstract class BaseDialog extends DialogFragment implements BaseView
{
    /**
     * activity context of fragment
     */
    protected Activity mContext;
    public Subscription subscription;
    Unbinder unbinder;
    View view;
    protected int currentStyle = 0;

    @Nullable
    private StateLayoutManager mStatusLayout;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.MyAlertDialog);
        mContext = getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        setDialog(currentStyle);

        if (getContentViewId() != 0)
        {
            view = inflater.inflate(getContentViewId(), null);
            //设置多状态布局
            View rootView = view.findViewById(R.id.dialog_content_view);
            if (rootView != null) {
                mStatusLayout = new StateLayoutManager.Builder(mContext)
                        .initPage(view)
                        .create();
            }
            return view;
        }
        else
        {
            return super.onCreateView(inflater, container, savedInstanceState);
        }
    }

    protected View findViewById(int resId)
    {
        return view.findViewById(resId);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        unbinder = ButterKnife.bind(this, view);
        initViewsAndEvents(view);
    }

    public void startActivity(Class<?> clazz)
    {
        Intent intent = new Intent(mContext, clazz);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }

    public void startActivity(Class<?> clazz, Bundle bundle)
    {
        Intent intent = new Intent(mContext, clazz);
        if (null != bundle)
        {
            intent.putExtra("bundle", bundle);
        }
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }

    public Observable<Void> eventClick(View view)
    {
        return eventClick(view, 1000);
    }

    public Observable<Void> eventClick(View view, int milliseconds)
    {
        return RxView.clicks(view).throttleFirst(milliseconds, TimeUnit.MILLISECONDS);
    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();
        unbinder.unbind();
        if (null != subscription && !subscription.isUnsubscribed())
        {
            subscription.unsubscribe();
        }
    }

    /**
     * override this method to do operation in the fragment
     */
    protected abstract void initViewsAndEvents(View rootView);


    /**
     * override this method to return content view id of the fragment
     */
    protected abstract int getContentViewId();

    /**
     * set for choice witch dialog to show
     *
     * @return
     */
    public int setDialog(int style)
    {
        return currentStyle = style;
    }

    /**
     * implements methods in BaseView
     */
    @Override
    public void showToastMessage(String msg, double seconds)
    {
        ToastSimple.show(msg, seconds);
    }

    @Override
    public void close()
    {
        mContext.finish();
    }

    @Override
    public void showProgressDialog(String message)
    {
        DialogManager2.INSTANCE.showProgressDialog(mContext, message);
    }

//    @Override
//    public void showProgressDialog(String message, int progress)
//    {
//        DialogManager2.INSTANCE.showProgressDialog(mContext, message, progress);
//    }

    @Override
    public void hideProgressDialog()
    {
        DialogManager2.INSTANCE.dismiss();
    }

//    @Override
//    public void showErrorMessage(String msg, String content)
//    {
//        DialogManager2.INSTANCE.showErrorDialog(mContext, msg, content, SweetAlertDialog::dismissWithAnimation);
//    }


    @Override
    public void showConfirmDialog(String title, String content, DialogClickListener dialogClickListener) {
        DialogManager2.INSTANCE.showConfirmDialog(mContext, title, content, dialogClickListener);
    }

    @Override
    public void showConfirmNoCancelDialog(String title, String content, DialogClickListener dialogClickListener) {
        DialogManager2.INSTANCE.showConfirmNoCancelDialog(mContext, title, content, dialogClickListener);
    }

    /**
     * 加载中布局
     */
    @Override
    public void showDefaultListLoading() {
        if (mStatusLayout != null) {
            mStatusLayout.showLoading();
        }
    }

    /**
     * 展示正常布局
     */
    @Override
    public void showDefaultContent() {
        if (mStatusLayout != null) {
            mStatusLayout.showContent();
        }
    }

    /**
     * 展示空布局 默认文案
     */
    @Override
    public void showDefaultEmpty() {
        if (mStatusLayout != null) {
            mStatusLayout.showEmpty();
        }
    }

    /**
     * 展示空布局
     *
     * @param text 文案
     */
    @Override
    public void showDefaultEmpty(@Nullable String text) {
        if (mStatusLayout != null) {
            mStatusLayout.showEmpty();
            mStatusLayout.emptyText(text);
        }
    }

    @Override
    public void showDefaultEmpty(@Nullable String text, int resId) {
        if (mStatusLayout != null) {
            mStatusLayout.showEmpty();
            mStatusLayout.emptyText(text);
            mStatusLayout.emptyIv(resId);
        }
    }

    /**
     * 展示空布局 带有点击事件
     */
    @Override
    public void showDefaultEmptyClick(View.OnClickListener listener) {
        if (mStatusLayout != null) {
            mStatusLayout.showEmptyClick(listener);
        }
    }

    /**
     * 展示错误布局
     *
     * @param listener
     */
    @Override
    public void showDefaultError(View.OnClickListener listener) {
        if (mStatusLayout != null) {
            mStatusLayout.showError(listener);
        }
    }
}
