package com.huolongluo.luo.manager.pay.alipay.util;

public class AliPayConfig {
    /**
     * 用于支付宝支付业务的入参 app_id。
     */
    public static final String APPID = "2021003128647626";
    /**
     * 用于支付宝账户登录授权业务的入参 pid。
     */
    public static final String PID = "";

    /**
     * 用于支付宝账户登录授权业务的入参 target_id。
     */
    public static final String TARGET_ID = "";

    /**
     *  pkcs8 格式的商户私钥。
     *
     * 	如下私钥，RSA2_PRIVATE 或者 RSA_PRIVATE 只需要填入一个，如果两个都设置了，本 Demo 将优先
     * 	使用 RSA2_PRIVATE。RSA2_PRIVATE 可以保证商户交易在更加安全的环境下进行，建议商户使用
     * 	RSA2_PRIVATE。
     *
     * 	建议使用支付宝提供的公私钥生成工具生成和获取 RSA2_PRIVATE。
     * 	工具地址：https://doc.open.alipay.com/docs/doc.htm?treeId=291&articleId=106097&docType=1
     */
    public static final String RSA2_PRIVATE = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCr6/oHM6omKcVrNEwd663l+UQEyYo3LZLPLpJRrpcZKGigyqvuCS+FtiwoWAcUjpTzSYn6PnB0lr8yH2ZcAfUR+rjUWHZLX0bL0pUzdCtDDgdhGbJ8Yw+yKJeHfNTsIYuwkSKwJWWbCM6UztteP0UeepTgyw9OGSVMV2yuyi4FyhvQzevJtjCCAEuU+JiuuJu7eukNat/l84EQCFc5q5uEojyf7/JHhgR7sht1gwN3R4EzXObCEFrdBPOoo03bgzwn1VFAsuCmGnTeu++NXxhGGL8XLT5jbE8eW8fBO4ffmfzoMkDBeXXCc70ssuppXPLazlFRnuTFSVzjHXPijGZhAgMBAAECggEATaTPUL1WlNG8pUQDNGgiK4C95WOYFn9wlD02O8mb1/LV/fhKWcCwQ7UYkEfhOrmCL7YAqJHWe/Ew/FQOCHOh2LLhEpW5JyUqdhbu50QJ3cjSMFpIr8CVcCLA/KG2D8a+rcl+XwskrjX2BmK90/zDHJbD8hoDGYRlweqh3PbWfioe19eR6TU/8KQmCiiz7YFRoFq48JT8AsP6cdG6qZr3mtDNueYsbzHutvpxLltepehLKaQWTV9qz1LMHVmXwHycHQsyIHFdiOVONVpNUX4uN1XgTq6hAY+cxv+KQckx8oUZE1mdCyaN9HbtC1vbx6cB8cnWnmvCnFsarMIM4DJoWQKBgQD4kDoNuWpUSNx/C3sCuY1d7kYlqLjyzWt3qLClyNNQj0MgiE0JrewQQNxv47kltpfPATd+v9vXf57U9mzQDPi2VhmWf4bbjYCXVwzvkjQMyK69TSXi+OK026qM//5Y7yqQRpNZrJ9HGCyVflmjgP/jxltJorrMudl+QiCLOAIOOwKBgQCxEL5oEGhJ+6AvtN5jIgwyu7+6Wi2fUvc4hsW5eRl2hJT9YahJGKfLJ8duoysyLOon1+OjVGjtCOY4S+33e8g4noS+rzh1K6yJw+C6KpH9lgvawk423ORvFAkctpZ/ZZa2cKSOTJL45IgUlMGp0xRfzhBSSgmj1SO67Q9wTSKIEwKBgQDqC3vuYlEhbb3ZDmlUZn0bHfoFCbqkPRJdCITUhaWb7FmLr2vCmnZ102z9GhLC1QxaiU3E1kjdwDhBVIue1yf1IFtVn1zPLI0Rc5Eln6fNBcWrT5JGBo4JN/F+dKj8+q48TYf/XxBW9Ac++ErVLulDgwl/628yyChnv0I/dIxS8wKBgFDIXtaDhdEbJ/4b+sonooES/2o58k6aP4helJ8GDQPxgTgI8gpHYWZig66Kaw69Rydea9D8ldJvGPneSnd61eJfEBkfQBbVEibYYxMMkQatCkOIGr8VEHNNJMsg0pDU/GnIAouZjARZ32vkYmbkkosTLIcemD9S4rZJqZ2G05ZZAoGBAKpci/6TkcWJfAwnCKU+vQDGl/6mr+aL3wEL+piQaX/q6ElaZ9KdPb9t/ppW1NYGyWT4nrQ4iPNpyCmGYW5gHSl4ae/Ez2/qy86JtqreGf3zzlRnnA+KFudg/l/62qzXqf7MTtb2Lls6bfGf+Wq3vAyvqXXstyefqRaT36tp2NFX";
    public static final String RSA_PRIVATE = "";
    public static final int SDK_PAY_FLAG = 1;
    public static final int SDK_AUTH_FLAG = 2;
}
