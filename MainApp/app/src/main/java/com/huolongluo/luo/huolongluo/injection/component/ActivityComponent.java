package com.huolongluo.luo.huolongluo.injection.component;

import com.huolongluo.luo.MainActivity;
import com.huolongluo.luo.base.PerActivity;
import com.huolongluo.luo.huolongluo.injection.model.ActivityModule;
import com.huolongluo.luo.huolongluo.net.okhttp.Api;
import com.huolongluo.luo.huolongluo.net.okhttp.ApiCache;
import com.huolongluo.luo.huolongluo.ui.activity.aboutus.AboutUsActivity;
import com.huolongluo.luo.huolongluo.ui.activity.account.MyAccountActivity;
import com.huolongluo.luo.huolongluo.ui.activity.accountset.AccountSetActivity;
import com.huolongluo.luo.huolongluo.ui.activity.bindtixian.BindTiXianActivity;
import com.huolongluo.luo.huolongluo.ui.activity.editbaseinfo.EditBaseInfoActivity;
import com.huolongluo.luo.huolongluo.ui.activity.editdatacard.EditDataCardActivity;
import com.huolongluo.luo.huolongluo.ui.activity.editjob.EditJobActivity;
import com.huolongluo.luo.huolongluo.ui.activity.editnickname.EditNickNameActivity;
import com.huolongluo.luo.huolongluo.ui.activity.editschool.EditSchoolActivity;
import com.huolongluo.luo.huolongluo.ui.activity.edittoppic.EditTopPicActivity;
import com.huolongluo.luo.huolongluo.ui.activity.etwechat.EditWeChatActivity;
import com.huolongluo.luo.huolongluo.ui.activity.feedback.FeedBackActivity;
import com.huolongluo.luo.huolongluo.ui.activity.follow.FollowActivity;
import com.huolongluo.luo.huolongluo.ui.activity.friendcontacts.FriendContactsActivity;
import com.huolongluo.luo.huolongluo.ui.activity.hongniang.HongNiangActivity;
import com.huolongluo.luo.huolongluo.ui.activity.howtoupwall.HowToUpWallActivity;
import com.huolongluo.luo.huolongluo.ui.activity.invite.InviteActivity;
import com.huolongluo.luo.huolongluo.ui.activity.jiangli.JiangLiActivity;
import com.huolongluo.luo.huolongluo.ui.activity.jifentip.JiFenTipActivity;
import com.huolongluo.luo.huolongluo.ui.activity.label.MyLabelActivity;
import com.huolongluo.luo.huolongluo.ui.activity.login.LoginActivity;
import com.huolongluo.luo.huolongluo.ui.activity.publish.PublishActivity;
import com.huolongluo.luo.huolongluo.ui.activity.rechargerecord.RechargeRecordActivity;
import com.huolongluo.luo.huolongluo.ui.activity.register.RegisterActivity;
import com.huolongluo.luo.huolongluo.ui.activity.registernext.RegisterNextActivity;
import com.huolongluo.luo.huolongluo.ui.activity.setting.SettingActivity;
import com.huolongluo.luo.huolongluo.ui.activity.square.SquareActivity;
import com.huolongluo.luo.huolongluo.ui.activity.tixianrecord.TiXianRecordActivity;
import com.huolongluo.luo.huolongluo.ui.activity.upwall.UpWallActivity;
import com.huolongluo.luo.huolongluo.ui.activity.userdetail.UserDetailInfoActivity;
import com.huolongluo.luo.huolongluo.ui.activity.vip.VipActivity;
import com.huolongluo.luo.huolongluo.ui.activity.visit.VisitActivity;
import com.huolongluo.luo.huolongluo.ui.fragment.discover.DiscoverFragment;
import com.huolongluo.luo.huolongluo.ui.fragment.friendcontact.FriendContactsFragment;
import com.huolongluo.luo.huolongluo.ui.fragment.home.HomeFragment;
import com.huolongluo.luo.huolongluo.ui.fragment.home.HomeFragment2;
import com.huolongluo.luo.huolongluo.ui.fragment.mine.MineFragment;
import com.huolongluo.luo.huolongluo.ui.fragment.resource.ResourceFragment;
import com.huolongluo.luo.huolongluo.ui.fragment.selectlabel.SelectLabelFragment;
import com.huolongluo.luo.huolongluo.ui.fragment.tuijian.TuiJianFragment;
import com.huolongluo.luo.huolongluo.ui.fragment.upwall.UpWallFragment;
import com.huolongluo.luo.huolongluo.ui.fragment.xinren.XinRenFragment;

import dagger.Component;

/**
 * <p>
 * Created by 火龙裸 on 2020/3/12.
 */

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent
{
    Api getApi();

    ApiCache getApiCache();

    void inject(MainActivity activity);
    void inject(LoginActivity activity);
    void inject(RegisterActivity activity);
    void inject(PublishActivity activity);
    void inject(MyAccountActivity activity);
    void inject(JiangLiActivity activity);
    void inject(InviteActivity activity);
    void inject(VipActivity activity);
//    void inject(RechargeGoldActivity activity);
    void inject(HongNiangActivity activity);
    void inject(UserDetailInfoActivity activity);
    void inject(MyLabelActivity activity);
    void inject(EditBaseInfoActivity activity);
    void inject(RegisterNextActivity activity);
    void inject(SettingActivity activity);
    void inject(SquareActivity activity);
    void inject(FriendContactsActivity activity);
    void inject(EditDataCardActivity activity);
    void inject(EditNickNameActivity activity);
    void inject(EditSchoolActivity activity);
    void inject(EditJobActivity activity);
    void inject(UpWallActivity activity);
    void inject(HowToUpWallActivity activity);
    void inject(VisitActivity activity);
    void inject(BindTiXianActivity activity);
    void inject(FollowActivity activity);
    void inject(RechargeRecordActivity activity);
    void inject(JiFenTipActivity activity);
    void inject(FeedBackActivity activity);
    void inject(AccountSetActivity activity);
    void inject(EditWeChatActivity activity);
    void inject(EditTopPicActivity activity);
    void inject(TiXianRecordActivity activity);
    void inject(AboutUsActivity activity);
    void inject(HomeFragment2 fragment);
    void inject(HomeFragment fragment);
    void inject(TuiJianFragment fragment);
    void inject(XinRenFragment fragment);
    void inject(DiscoverFragment fragment);
    void inject(ResourceFragment fragment);
    void inject(MineFragment fragment);
    void inject(SelectLabelFragment fragment);
    void inject(FriendContactsFragment fragment);
    void inject(UpWallFragment fragment);
}
