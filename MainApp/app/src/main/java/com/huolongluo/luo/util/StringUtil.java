package com.huolongluo.luo.util;

import android.text.TextUtils;

import java.io.UnsupportedEncodingException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by 火龙裸 on 2020/3/12.
 */

public class StringUtil
{
    /**
     * 判断手机号码是否合理
     *
     * @param phoneNums
     */
    public static boolean judgePhoneNums(String phoneNums)
    {
        return isMatchLength(phoneNums, 11) && isMobileNO(phoneNums);
    }

    /**
     * 判断一个字符串的位数
     *
     * @param str
     * @param length
     * @return
     */
    private static boolean isMatchLength(String str, int length)
    {
        return !str.isEmpty() && str.length() == length;
    }

    /**
     * 验证手机格式
     */
    private static boolean isMobileNO(String mobileNums)
    {
        /*
         * 移动：134、135、136、137、138、139、150、151、157(TD)、158、159、187、188
         * 联通：130、131、132、152、155、156、185、186 电信：133、153、180、189、（1349卫通）
         * 总结起来就是第一位必定为1，第二位必定为3或5或8，其他位置的可以为0-9
         */
        String telRegex = "[1][34578]\\d{9}";//
        // "[1]"代表第1位为数字1，"[358]"代表第二位可以为3、5、8中的一个，"\\d{9}"代表后面是可以是0～9的数字，有9位。
        return !TextUtils.isEmpty(mobileNums) && mobileNums.matches(telRegex);
    }

    public static int getLengthForInputStr(String inputStr)
    {
        int orignLen = inputStr.length();

        int resultLen = 0;

        String temp = null;

        for (int i = 0; i < orignLen; i++)
        {

            temp = inputStr.substring(i, i + 1);

            try
            {
                // 3 bytes to indicate chinese word,1 byte to indicate english word ,in utf-8 encode
                if (temp.getBytes("utf-8").length == 3)
                {
                    resultLen += 2;
                }
                else
                {
                    resultLen++;
                }
            }
            catch (UnsupportedEncodingException e)
            {
                e.printStackTrace();
            }
        }
        return resultLen;
    }

    /***
     * 1，不能全部是数字
     * 2，不能全部是字母
     * 3，必须是数字或字母
     */
    public static boolean pswIsLegal(String str)
    {
        String regex = "^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{8,20}$";
//        L.e("=====正则结果：" + str.matches(regex));
        System.out.println("=====正则结果：" + str.matches(regex));
        return true;
    }

    /***
     * 1，判断是否有中文字符，有则返回true
     */
    public static boolean isContainChinese(String str)
    {
        Pattern p = Pattern.compile("[\u4e00-\u9fa5]");
        Matcher m = p.matcher(str);
        if (m.find())
        {
            return true;
        }
        return false;
    }

    /**
     * @param phone 手机号码
     * @param start 第几位开始隐藏
     * @param end   后面算起第几位结束隐藏
     * @return
     */
    public static String hidePhone(String phone, int start, int end)
    {
        if (!TextUtils.isEmpty(phone))
        {
            phone = phone.substring(0, phone.length() - (phone.substring(start)).length()) + "****" + phone.substring(phone.length() - end);
        }
        return phone;
    }

    /**
     * 秒数转换成 xx时xx分xx秒
     * a integer to xx:xx:xx
     */
    public static String secToTime(int time)
    {
        String timeStr = null;
        int hour = 0;
        int minute = 0;
        int second = 0;
        if (time <= 0)
        {
            return "00分00秒";
        }
        else
        {
            minute = time / 60;
            if (minute < 60)
            {
                second = time % 60;
                timeStr = unitFormat(minute) + "分" + unitFormat(second) + "秒";
            }
            else
            {
                hour = minute / 60;
                if (hour > 99)
                {
                    return "99时59分59秒";
                }
                minute = minute % 60;
                second = time - hour * 3600 - minute * 60;
                timeStr = unitFormat(hour) + "时" + unitFormat(minute) + "分" + unitFormat(second) + "秒";
            }
        }
        return timeStr;
    }

    public static String unitFormat(int i)
    {
        String retStr = null;
        if (i >= 0 && i < 10)
        {
            retStr = "0" + Integer.toString(i);
        }
        else
        {
            retStr = "" + i;
        }
        return retStr;
    }

    /**
     * 根据月、日，换算出星座
     * */
    public static String calculateXingzuo(String month, String day) {
//        String month = getIntent().getStringExtra("month");
//        String day = getIntent().getStringExtra("day");
        int m=Integer.parseInt(month);
        int d=Integer.parseInt(day);
        String name1="";
        if(m>0 &&m<13 && d>0 && d<32) {
            if((m ==3 && d>20) || (m ==4 && d<21)) {
                name1 = "白羊座";
            }else if((m ==4 && d>20) || (m ==5 && d<21)){
                name1 = "金牛座";
            }else if((m ==5 && d>20) || (m ==6 && d<22)){
                name1="双子座";
            }else if((m ==6 && d>21) || (m ==7 && d<23)){
                name1="巨蟹座";
            }else if((m ==7 && d>22) || (m ==8 && d<23)){
                name1="狮子座";
            }else if((m ==8 && d>22) || (m ==9 && d<23)){
                name1="处女座";
            }else if((m ==9 && d>22) || (m ==10 && d<23)){
                name1="天枰座";
            }else if((m ==10 && d>22) || (m ==11 && d<22)){
                name1="天蝎座";
            }else if((m ==11 && d>21) || (m ==12 && d<22)){
                name1="射手座";
            }else if((m ==12 && d>21) || (m ==1 && d<20)){
                name1="摩羯座";
            }else if((m ==1 && d>19) || (m ==2 && d<19)){
                name1="水牛座";
            }else if((m ==2 && d>18) || (m ==3 && d<21)) {
                name1 = "双鱼座";
            }
        }else{
//            name1="您输入的生日格式不正确或者不是真实生日！";
            name1="星座换算错误";
        }
        return name1;
    }
}

