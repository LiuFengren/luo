package com.tencent.qcloud.tuikit.tuichat.ui.view.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tencent.qcloud.tuikit.tuichat.R;
import com.tencent.qcloud.tuikit.tuichat.bean.TUIGiftListBean;
import com.tencent.qcloud.tuikit.tuichat.ui.page.TUIC2CChatActivity;

import java.util.List;

public class GriftAdapter0 extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = "GriftAdapter0";

    private Context mContext;
    private List<TUIGiftListBean> tuiGiftListBeans;
    private LayoutInflater mLayoutInflater;
    private int pageSize;

    private GriftAdapter1.OnGiftClickListener onGiftClickListener;

    public GriftAdapter0(Context mContext, List<TUIGiftListBean> tuiGiftListBeans, GriftAdapter1.OnGiftClickListener onGiftClickListener) {
        this.mContext = mContext;
        this.tuiGiftListBeans = tuiGiftListBeans;
        mLayoutInflater = LayoutInflater.from(mContext);
        if (!tuiGiftListBeans.isEmpty()) {
            pageSize = (tuiGiftListBeans.size() % 8 == 0) ? (tuiGiftListBeans.size() / 8) : ((tuiGiftListBeans.size() / 8) + 1);
        }
        this.onGiftClickListener = onGiftClickListener;
        Log.e(TAG, "GriftAdapter0: 最终分割出来的page Size:" + pageSize);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.item_gift_viewpager2, parent, false);
        ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        view.setLayoutParams(lp);
        return new GriftViewPager2ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        RecyclerView rv_gift = holder.itemView.findViewById(R.id.rv_gift);
        rv_gift.setNestedScrollingEnabled(false);
        rv_gift.setFocusable(false);
        rv_gift.setLayoutManager(new GridLayoutManager(mContext, 4)
        {
            @Override
            public boolean canScrollVertically()
            {
                return false;
            }
        });
//        if (position == 0) {
//            GriftAdapter1 griftAdapter1 = new GriftAdapter1(mContext, tuiGiftListBeans.subList(0, 8));
//            rv_gift.setAdapter(griftAdapter1);
//        } else {
//            GriftAdapter1 griftAdapter1 = new GriftAdapter1(mContext, tuiGiftListBeans.subList(position * 8, position * 8 + 8));
//            rv_gift.setAdapter(griftAdapter1);
//        }

        Log.e(TAG, "onBindViewHolder: 开始位置：" + (position * 8) + "  ||  " + (position * 8 + 8) + "   长度为：" + tuiGiftListBeans.size());
        if (position == 0) {
            tuiGiftListBeans.get(0).setSelect(true);
        }

        List<TUIGiftListBean> currentPageGiftListBeans;
        if ((position + 1) == pageSize) {
            currentPageGiftListBeans = tuiGiftListBeans.subList(position * 8, (position * 8) + (tuiGiftListBeans.size() - position * 8));
        } else {
            currentPageGiftListBeans = tuiGiftListBeans.subList(position * 8, position * 8 + 8);
        }


//        if ((position + 1) == pageSize) {
//            GriftAdapter1 griftAdapter1 = new GriftAdapter1(mContext, tuiGiftListBeans.subList(position * 8, (position * 8) + (tuiGiftListBeans.size() - position * 8)));
//            rv_gift.setAdapter(griftAdapter1);
//        } else {
//            GriftAdapter1 griftAdapter1 = new GriftAdapter1(mContext, tuiGiftListBeans.subList(position * 8, position * 8 + 8));
//            rv_gift.setAdapter(griftAdapter1);
//        }
        GriftAdapter1 griftAdapter1 = new GriftAdapter1(mContext, currentPageGiftListBeans, this.onGiftClickListener);
        rv_gift.setAdapter(griftAdapter1);
    }

    @Override
    public int getItemCount() {
        return pageSize;
    }

    class GriftViewPager2ViewHolder extends RecyclerView.ViewHolder {

//        private ImageView iv_gift_pic;
//        private TextView tv_gift_name;
//        private TextView tv_gift_gold_num;

        public GriftViewPager2ViewHolder(@NonNull View itemView) {
            super(itemView);
//            iv_gift_pic = itemView.findViewById(R.id.iv_gift_pic);
//            tv_gift_name = itemView.findViewById(R.id.tv_gift_name);
//            tv_gift_gold_num = itemView.findViewById(R.id.tv_gift_gold_num);
        }
    }
}
