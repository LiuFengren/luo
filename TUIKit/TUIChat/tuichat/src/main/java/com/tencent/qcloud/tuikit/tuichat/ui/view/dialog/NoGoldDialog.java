package com.tencent.qcloud.tuikit.tuichat.ui.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.tencent.qcloud.tuicore.TUICoreKeyEvent;
import com.tencent.qcloud.tuicore.TUICore;
import com.tencent.qcloud.tuikit.tuichat.R;

import java.util.HashMap;

public class NoGoldDialog extends Dialog {
    private Context context;

    private TextView tv_to_recharge;//立即充值
    private ImageView iv_close_dialog_no_gold;//关闭

    public NoGoldDialog(@NonNull Context context) {
        this(context, R.style.MyAlertDialog);
        this.context = context;
    }

    public NoGoldDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_no_gold);

        init();
        initView();
    }

    private void initView() {
        tv_to_recharge = findViewById(R.id.tv_to_recharge);
        iv_close_dialog_no_gold = findViewById(R.id.iv_close_dialog_no_gold);

        tv_to_recharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO: 2022/4/12
                dismiss();
                HashMap<String, Object> param = new HashMap<>();
//                param.put("key1", "id key1");
//                param.put("key2", "id key2");
                TUICore.notifyEvent(TUICoreKeyEvent.NoGoldDialog_CKCK_CHONGZHI, TUICoreKeyEvent.NoGoldDialog_CKCK_CHONGZHI, param);
            }
        });

        iv_close_dialog_no_gold.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    private void init()
    {
        Window window = this.getWindow();
        window.getDecorView().setPadding(0, 0, 0, 0);
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        window.setAttributes(lp);
    }
}
