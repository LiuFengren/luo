package com.tencent.qcloud.tuikit.tuichat.ui.view.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.tencent.qcloud.tuikit.tuichat.R;
import com.tencent.qcloud.tuikit.tuichat.bean.TUIGiftListBean;

import java.util.List;

public class GriftAdapter1 extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<TUIGiftListBean> tuiGiftListBeans;
    private List<TUIGiftListBean> originalTuiGiftListBeans;
    private LayoutInflater mLayoutInflater;

    private OnGiftClickListener onGiftClickListener;

    public GriftAdapter1(Context mContext, List<TUIGiftListBean> tuiGiftListBeans, OnGiftClickListener onGiftClickListener) {
        this.mContext = mContext;
        this.tuiGiftListBeans = tuiGiftListBeans;
        this.originalTuiGiftListBeans = originalTuiGiftListBeans;
        mLayoutInflater = LayoutInflater.from(mContext);
        this.onGiftClickListener = onGiftClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new GriftViewPager2ViewHolder(mLayoutInflater.inflate(R.layout.item_gift, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        ((GriftViewPager2ViewHolder)holder).tv_gift_name.setText(tuiGiftListBeans.get(position).getGiftName());
        ((GriftViewPager2ViewHolder)holder).tv_gift_gold_num.setText(tuiGiftListBeans.get(position).getGiftGoldNum() + "金币");
        Glide.with(mContext).load(tuiGiftListBeans.get(position).getGiftPic()).into(((GriftViewPager2ViewHolder)holder).iv_gift_pic);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onGiftClickListener.onClickGift(tuiGiftListBeans.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return tuiGiftListBeans.isEmpty() ? 0 : tuiGiftListBeans.size();
    }

    class GriftViewPager2ViewHolder extends RecyclerView.ViewHolder {

        private ImageView iv_gift_pic;
        private TextView tv_gift_name;
        private TextView tv_gift_gold_num;

        public GriftViewPager2ViewHolder(@NonNull View itemView) {
            super(itemView);
            iv_gift_pic = itemView.findViewById(R.id.iv_gift_pic);
            tv_gift_name = itemView.findViewById(R.id.tv_gift_name);
            tv_gift_gold_num = itemView.findViewById(R.id.tv_gift_gold_num);
        }
    }

    public interface OnGiftClickListener {
        void onClickGift(TUIGiftListBean tuiGiftListBean);
    }
}
