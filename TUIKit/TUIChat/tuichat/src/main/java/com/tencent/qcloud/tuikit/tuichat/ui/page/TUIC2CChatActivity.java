package com.tencent.qcloud.tuikit.tuichat.ui.page;

import android.os.Bundle;
import android.util.Log;

import com.tencent.qcloud.tuicore.TUICore;
import com.tencent.qcloud.tuicore.TUICoreKeyEvent;
import com.tencent.qcloud.tuicore.interfaces.ITUINotification;
import com.tencent.qcloud.tuicore.util.ToastUtil;
import com.tencent.qcloud.tuikit.tuichat.R;
import com.tencent.qcloud.tuikit.tuichat.TUIChatConstants;
import com.tencent.qcloud.tuikit.tuichat.bean.ChatInfo;
import com.tencent.qcloud.tuikit.tuichat.bean.message.TUIMessageBean;
import com.tencent.qcloud.tuikit.tuichat.presenter.C2CChatPresenter;
import com.tencent.qcloud.tuikit.tuichat.util.TUIChatLog;
import com.tencent.qcloud.tuikit.tuichat.util.TUIChatUtils;

import java.util.Map;

public class TUIC2CChatActivity extends TUIBaseChatActivity {
    private static final String TAG = TUIC2CChatActivity.class.getSimpleName();

    private TUIC2CChatFragment chatFragment;
    private C2CChatPresenter presenter;
    @Override
    public void initChat(ChatInfo chatInfo) {
        TUIChatLog.i(TAG, "inti chat " + chatInfo);

        if (!TUIChatUtils.isC2CChat(chatInfo.getType())) {
            TUIChatLog.e(TAG, "init C2C chat failed , chatInfo = " + chatInfo);
            ToastUtil.toastShortMessage("init c2c chat failed.");
        }
        chatFragment = new TUIC2CChatFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(TUIChatConstants.CHAT_INFO, chatInfo);
        chatFragment.setArguments(bundle);
        presenter = new C2CChatPresenter();
        presenter.initListener();
        chatFragment.setPresenter(presenter);
        getSupportFragmentManager().beginTransaction().replace(R.id.empty_view, chatFragment).commitAllowingStateLoss();

//        TUICore.registerEvent(TUICoreKeyEvent.SEND_IM_MESSAGE_HEYAN, TUICoreKeyEvent.SEND_IM_MESSAGE_HEYAN, ituiNotification);
    }

//    ITUINotification ituiNotification = new ITUINotification() {
//        @Override
//        public void onNotifyEvent(String key, String subKey, Map<String, Object> param) {
//            Log.e(TAG, "onNotifyEvent: 聊天 收到key为：" + key + "  subKey:" + subKey);
//            Log.e(TAG, "onNotifyEvent: 聊天 收到发送消息结果>>>>>>>>>>>>>>：" + param.get(TUICoreKeyEvent.SEND_IM_MESSAGE_HEYAN_RESULT) + " 发送的内容为：" + param.get(TUICoreKeyEvent.SEND_IM_MESSAGE_CONTENT));
//            chatFragment.chatView.sendMessage((TUIMessageBean) param.get(TUICoreKeyEvent.SEND_IM_MESSAGE_CONTENT), false);
//        }
//    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        TUICore.unRegisterEvent(TUICoreKeyEvent.SEND_IM_MESSAGE_HEYAN, TUICoreKeyEvent.SEND_IM_MESSAGE_HEYAN, ituiNotification);
    }
}
