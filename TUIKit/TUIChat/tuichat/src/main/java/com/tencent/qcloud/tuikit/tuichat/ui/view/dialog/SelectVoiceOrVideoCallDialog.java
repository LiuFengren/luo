package com.tencent.qcloud.tuikit.tuichat.ui.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.tencent.qcloud.tuikit.tuichat.R;

public class SelectVoiceOrVideoCallDialog extends Dialog {

    private Context context;

    private LinearLayout ll_voice_call;//语音通话
    private LinearLayout ll_video_call;//视频通话
    private TextView tv_cancel_dialog;//取消按钮

    private OnSelectVoiceOrVideoClickListener onSelectVoiceOrVideoClickListener;

    public SelectVoiceOrVideoCallDialog(@NonNull Context context, OnSelectVoiceOrVideoClickListener onSelectVoiceOrVideoClickListener) {
        this(context, R.style.MyAlertDialog);
        this.context = context;
        this.onSelectVoiceOrVideoClickListener = onSelectVoiceOrVideoClickListener;
    }

    public SelectVoiceOrVideoCallDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_call);

        init();
        initView();
    }

    private void initView() {
        ll_voice_call = findViewById(R.id.ll_voice_call);
        ll_video_call = findViewById(R.id.ll_video_call);
        tv_cancel_dialog = findViewById(R.id.tv_cancel_dialog);

        //语音通话
        ll_voice_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSelectVoiceOrVideoClickListener.onClickAudio();
                dismiss();
            }
        });

        //视频通话
        ll_video_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSelectVoiceOrVideoClickListener.onClickVideo();
                dismiss();
            }
        });

        //取消Dialog
        tv_cancel_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    private void init() {
        Window window = this.getWindow();
        window.getDecorView().setPadding(0, 0, 0, 0);
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.BOTTOM;
        window.setAttributes(lp);
    }

    public interface OnSelectVoiceOrVideoClickListener {
        void onClickAudio();

        void onClickVideo();
    }
}
