package com.tencent.qcloud.tuikit.tuichat.bean;

import java.io.Serializable;

public class TUIGiftListBean implements Serializable {
    private String giftGoldNum;
    private String giftName;
    private String giftPic;
    private String createdAt;
    private String objectId;
    private String updatedAt;

    private boolean isSelect;//是否被选中

    public String getGiftGoldNum() {
        return giftGoldNum;
    }

    public void setGiftGoldNum(String giftGoldNum) {
        this.giftGoldNum = giftGoldNum;
    }

    public String getGiftName() {
        return giftName;
    }

    public void setGiftName(String giftName) {
        this.giftName = giftName;
    }

    public String getGiftPic() {
        return giftPic;
    }

    public void setGiftPic(String giftPic) {
        this.giftPic = giftPic;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }

    @Override
    public String toString() {
        return "TUIGiftListBean{" +
                "giftGoldNum='" + giftGoldNum + '\'' +
                ", giftName='" + giftName + '\'' +
                ", giftPic='" + giftPic + '\'' +
                ", createdAt='" + createdAt + '\'' +
                ", objectId='" + objectId + '\'' +
                ", updatedAt='" + updatedAt + '\'' +
                '}';
    }
}
