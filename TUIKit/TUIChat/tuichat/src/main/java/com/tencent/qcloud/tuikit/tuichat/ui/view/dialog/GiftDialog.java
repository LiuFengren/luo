package com.tencent.qcloud.tuikit.tuichat.ui.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.viewpager2.widget.ViewPager2;

import com.tencent.qcloud.tuikit.tuichat.R;
import com.tencent.qcloud.tuikit.tuichat.bean.TUIGiftListBean;
import com.tencent.qcloud.tuikit.tuichat.ui.view.adapter.GriftAdapter0;
import com.tencent.qcloud.tuikit.tuichat.ui.view.adapter.GriftAdapter1;

import java.util.List;

public class GiftDialog extends Dialog implements View.OnClickListener {

    private Context context;
    private List<TUIGiftListBean> tuiGiftListBean;
    private ViewPager2 rv_gift_viewpager2;
    private GriftAdapter0 griftViewPager2Adapter;
    private TextView tv_my_gold;
    private TextView tv_gift1;
    private TextView tv_gift5;
    private TextView tv_gift10;
    private TextView tv_gift30;
    private TextView tv_gift50;
    private TextView tv_zengsong;
    private int zengSongGiftNum = 1;//赠送礼物的数量

    public GiftDialog(@NonNull Context context, List<TUIGiftListBean> tuiGiftListBean) {
        this(context, R.style.MyAlertDialog);
        this.context = context;
        this.tuiGiftListBean = tuiGiftListBean;
    }

    public GiftDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_gift);
        init();
        initViews();
//        initEvent();
//        initListener();
    }

    private void init() {
        Window window = this.getWindow();
        window.getDecorView().setPadding(0, 0, 0, 0);
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.BOTTOM;
        window.setAttributes(lp);
    }

    private void initViews() {
        rv_gift_viewpager2 = (ViewPager2) findViewById(R.id.rv_gift_viewpager2);
        tv_my_gold = findViewById(R.id.tv_my_gold);
        tv_gift1 = findViewById(R.id.tv_gift1);
        tv_gift5 = findViewById(R.id.tv_gift5);
        tv_gift10 = findViewById(R.id.tv_gift10);
        tv_gift30 = findViewById(R.id.tv_gift30);
        tv_gift50 = findViewById(R.id.tv_gift50);
        tv_zengsong = findViewById(R.id.tv_zengsong);
        tv_gift1.setOnClickListener(this);
        tv_gift5.setOnClickListener(this);
        tv_gift10.setOnClickListener(this);
        tv_gift30.setOnClickListener(this);
        tv_gift50.setOnClickListener(this);
        tv_zengsong.setOnClickListener(this);

        griftViewPager2Adapter = new GriftAdapter0(this.context, this.tuiGiftListBean, new GriftAdapter1.OnGiftClickListener() {
            @Override
            public void onClickGift(TUIGiftListBean tuiGiftListBean) {
                Log.e("TAG", "onClickGift: 点击的礼物为：" + tuiGiftListBean.getGiftName());
            }
        });

        rv_gift_viewpager2.setOrientation(ViewPager2.ORIENTATION_HORIZONTAL);
        rv_gift_viewpager2.setAdapter(griftViewPager2Adapter);

        //页面切换监听
        rv_gift_viewpager2.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                Log.d("TAG Dialog>>>>>>>>>> ", "ViewPager onPageSelected " + position);
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tv_my_gold) {
            NoGoldDialog noGoldDialog = new NoGoldDialog(context);
            noGoldDialog.show();
        } else if (v.getId() == R.id.tv_gift1) {
            zengSongGiftNum = 1;
            tv_gift1.setTextColor(context.getResources().getColor(R.color.white));
            tv_gift5.setTextColor(context.getResources().getColor(R.color.F9B9B9B));
            tv_gift10.setTextColor(context.getResources().getColor(R.color.F9B9B9B));
            tv_gift30.setTextColor(context.getResources().getColor(R.color.F9B9B9B));
            tv_gift50.setTextColor(context.getResources().getColor(R.color.F9B9B9B));
            tv_gift1.setBackgroundResource(R.drawable.bg_ff4769_radius30);
            tv_gift5.setBackgroundResource(R.drawable.bg_f8f8f8_radius30);
            tv_gift10.setBackgroundResource(R.drawable.bg_f8f8f8_radius30);
            tv_gift30.setBackgroundResource(R.drawable.bg_f8f8f8_radius30);
            tv_gift50.setBackgroundResource(R.drawable.bg_f8f8f8_radius30);
        } else if (v.getId() == R.id.tv_gift5) {
            zengSongGiftNum = 5;
            tv_gift1.setTextColor(context.getResources().getColor(R.color.F9B9B9B));
            tv_gift5.setTextColor(context.getResources().getColor(R.color.white));
            tv_gift10.setTextColor(context.getResources().getColor(R.color.F9B9B9B));
            tv_gift30.setTextColor(context.getResources().getColor(R.color.F9B9B9B));
            tv_gift50.setTextColor(context.getResources().getColor(R.color.F9B9B9B));
            tv_gift1.setBackgroundResource(R.drawable.bg_f8f8f8_radius30);
            tv_gift5.setBackgroundResource(R.drawable.bg_ff4769_radius30);
            tv_gift10.setBackgroundResource(R.drawable.bg_f8f8f8_radius30);
            tv_gift30.setBackgroundResource(R.drawable.bg_f8f8f8_radius30);
            tv_gift50.setBackgroundResource(R.drawable.bg_f8f8f8_radius30);
        } else if (v.getId() == R.id.tv_gift10) {
            zengSongGiftNum = 10;
            tv_gift1.setTextColor(context.getResources().getColor(R.color.F9B9B9B));
            tv_gift5.setTextColor(context.getResources().getColor(R.color.F9B9B9B));
            tv_gift10.setTextColor(context.getResources().getColor(R.color.white));
            tv_gift30.setTextColor(context.getResources().getColor(R.color.F9B9B9B));
            tv_gift50.setTextColor(context.getResources().getColor(R.color.F9B9B9B));
            tv_gift1.setBackgroundResource(R.drawable.bg_f8f8f8_radius30);
            tv_gift5.setBackgroundResource(R.drawable.bg_f8f8f8_radius30);
            tv_gift10.setBackgroundResource(R.drawable.bg_ff4769_radius30);
            tv_gift30.setBackgroundResource(R.drawable.bg_f8f8f8_radius30);
            tv_gift50.setBackgroundResource(R.drawable.bg_f8f8f8_radius30);
        } else if (v.getId() == R.id.tv_gift30) {
            zengSongGiftNum = 30;
            tv_gift1.setTextColor(context.getResources().getColor(R.color.F9B9B9B));
            tv_gift5.setTextColor(context.getResources().getColor(R.color.F9B9B9B));
            tv_gift10.setTextColor(context.getResources().getColor(R.color.F9B9B9B));
            tv_gift30.setTextColor(context.getResources().getColor(R.color.white));
            tv_gift50.setTextColor(context.getResources().getColor(R.color.F9B9B9B));
            tv_gift1.setBackgroundResource(R.drawable.bg_f8f8f8_radius30);
            tv_gift5.setBackgroundResource(R.drawable.bg_f8f8f8_radius30);
            tv_gift10.setBackgroundResource(R.drawable.bg_f8f8f8_radius30);
            tv_gift30.setBackgroundResource(R.drawable.bg_ff4769_radius30);
            tv_gift50.setBackgroundResource(R.drawable.bg_f8f8f8_radius30);
        } else if (v.getId() == R.id.tv_gift50) {
            zengSongGiftNum = 50;
            tv_gift1.setTextColor(context.getResources().getColor(R.color.F9B9B9B));
            tv_gift5.setTextColor(context.getResources().getColor(R.color.F9B9B9B));
            tv_gift10.setTextColor(context.getResources().getColor(R.color.F9B9B9B));
            tv_gift30.setTextColor(context.getResources().getColor(R.color.F9B9B9B));
            tv_gift50.setTextColor(context.getResources().getColor(R.color.white));
            tv_gift1.setBackgroundResource(R.drawable.bg_f8f8f8_radius30);
            tv_gift5.setBackgroundResource(R.drawable.bg_f8f8f8_radius30);
            tv_gift10.setBackgroundResource(R.drawable.bg_f8f8f8_radius30);
            tv_gift30.setBackgroundResource(R.drawable.bg_f8f8f8_radius30);
            tv_gift50.setBackgroundResource(R.drawable.bg_ff4769_radius30);
        } else if (v.getId() == R.id.tv_zengsong) {
            Toast.makeText(context, "选择的：" + zengSongGiftNum, Toast.LENGTH_SHORT).show();
        }
    }

//    @Override
//    public void show() {
//        super.show();
//        Log.e("TAG", "show: Dialog对话框显示:");
//    }
}
