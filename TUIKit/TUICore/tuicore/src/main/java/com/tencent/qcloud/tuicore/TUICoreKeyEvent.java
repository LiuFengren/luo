package com.tencent.qcloud.tuicore;

public class TUICoreKeyEvent {
    public static final String NoGoldDialog_CKCK_CHONGZHI = "click_chongzhi";
    public static final String CLICK_GIFT = "click_gift";
    public static final String GIFT_LIST_BEAN = "giftListBean";
    public static final String CLICK_FRIEND_CONTACTS = "click_friend_contacts";
    public static final String SEND_IM_MESSAGE = "click_send_im_message";
    public static final String SEND_IM_MESSAGE_CONTENT = "click_send_im_message_content";
    public static final String SEND_IM_MESSAGE_TARGET_USER_ID = "click_send_im_message_target_user_id";
    public static final String SEND_IM_MESSAGE_HEYAN = "send_im_message_heyan";
    public static final String SEND_IM_MESSAGE_HEYAN_RESULT = "send_im_message_heyan_result";//1表示核验通过，非1表示未通过

    public static final String CLICK_AUDIO_OR_VIDEO = "click_audio_or_video";//点击了语音通话，或者视频通话
    public static final String CLICK_AUDIO_OR_VIDEO_TYPE = "click_audio_or_video_type";//点击了语音通话，或者视频通话 语音类型还是视频类型
    public static final String CLICK_AUDIO_OR_VIDEO_USERID = "click_audio_or_video_userid";//点击了语音通话，或者视频通话 被呼叫的用户ID

    public static final String START_IM_CALLING = "start_im_calling";//开始音视频通话
    public static final String START_IM_CALLING_TYPE = "start_im_calling_type";//开始音视频通话 通话类型
    public static final String START_IM_CALLING_USERIDS = "start_im_calling_userids";//被叫的用户ID（假如为空，说明被叫就是自己）
    public static final String STOP_IM_CALLING = "start_im_calling_stop";//结束音视频通话

    public static final String FORCE_HANG_UP_CALLING = "force_hang_up_calling";//强制挂断音视频通话
    public static final String FORCE_HANG_UP_CALLING_TYPE = "force_hang_up_calling_type";//强制挂断语音通话，还是视频通话
}
