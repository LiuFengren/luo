package com.tencent.qcloud.tuicore.interfaces;

public interface IMainAppResultCallBack {
    void onResultData();
}
